package com.affinislabs.zakatify.app.api;

import android.os.AsyncTask;

import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.responses.BooleanStateResponse;
import com.affinislabs.zakatify.app.api.responses.CategoryResponse;
import com.affinislabs.zakatify.app.api.responses.CharitiesResponse;
import com.affinislabs.zakatify.app.api.responses.ChatSessionsResponse;
import com.affinislabs.zakatify.app.api.responses.DefaultResponse;
import com.affinislabs.zakatify.app.api.responses.FCMTokenResponse;
import com.affinislabs.zakatify.app.api.responses.FollowersResponse;
import com.affinislabs.zakatify.app.api.responses.ForgotPasswordResponse;
import com.affinislabs.zakatify.app.api.responses.GoldValueResponse;
import com.affinislabs.zakatify.app.api.responses.InvitationsResponse;
import com.affinislabs.zakatify.app.api.responses.LoginResponse;
import com.affinislabs.zakatify.app.api.responses.PaypalAccountsResponse;
import com.affinislabs.zakatify.app.api.responses.PaymentResponse;
import com.affinislabs.zakatify.app.api.responses.PublicUserResponse;
import com.affinislabs.zakatify.app.api.responses.RegistrationResponse;
import com.affinislabs.zakatify.app.api.responses.ReviewsResponse;
import com.affinislabs.zakatify.app.api.responses.TopZakatifiersResponse;
import com.affinislabs.zakatify.app.api.responses.UserMenuResponse;
import com.affinislabs.zakatify.app.api.responses.UsernameCheckResponse;
import com.affinislabs.zakatify.app.api.responses.ZakatGoalResponse;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.utils.ApiUtils;
import com.affinislabs.zakatify.app.utils.Constants;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ApiClient {
    public static class NetworkCallsRunner extends AsyncTask<Void, Void, Void> {
        private String REQUEST_TYPE;
        private Map<String, String> requestMap;
        private String emailAddress, username, invitorUsername, inviteeEmail, categoryIds, pageNumber, pageSize, query, charityId, id, requestingParticipant, otherParticipant;
        private String follower, followed, messageId;
        private String paymentOptionId;
        private boolean state;
        private Map<String, RequestBody> partDataMap;


        private ApiClientListener.AccountRegistrationListener mAccountRegistrationListener;
        private ApiClientListener.UsernameCreationListener mUsernameCreationListener;
        private ApiClientListener.AccountAuthenticationListener mAccountAuthenticationListener;
        private ApiClientListener.ProfileUpdateListener mProfileUpdateListener;
        private ApiClientListener.SocialNetworkLinkingListener mSocialNetworkLinkingListener;
        private ApiClientListener.ForgotPasswordListener mForgotPasswordListener;
        private ApiClientListener.FetchCategoriesListener mFetchCategoriesListener;
        private ApiClientListener.UpdateUserCategoriesListener mUpdateUserCategoriesListener;
        private ApiClientListener.AddZakatGoalListener mAddZakatGoalListener;
        private ApiClientListener.UpdateZakatGoalListener mUpdateZakatGoalListener;
        private ApiClientListener.DeleteZakatGoalListener mDeleteZakatGoalListener;
        private ApiClientListener.FetchZakatGoalListener mFetchZakatGoalListener;
        private ApiClientListener.FetchGoldValueListener mFetchGoldValueListener;
        private ApiClientListener.FetchSuggestedCharitiesListener mFetchSuggestedCharitiesListener;
        private ApiClientListener.SearchCharitiesListener mSearchCharitiesListener;
        private ApiClientListener.FetchUserCharitiesListener mFetchUserCharitiesListener;
        private ApiClientListener.FetchUserMenuListener mFetchUserMenuListener;
        private ApiClientListener.FetchPaymentOptionsListener mFetchPaymentOptionsListener;
        private ApiClientListener.AddPaymentOptionListener mAddPaymentOptionListener;
        private ApiClientListener.UpdatePaymentOptionListener mUpdatePaymentOptionListener;
        private ApiClientListener.DeletePaymentOptionListener mDeletePaymentOptionListener;
        private ApiClientListener.SetPrimaryPaymentOptionListener mSetPrimaryPaymentOptionListener;
        private ApiClientListener.FetchTopZakatifiersListener mFetchTopZakatifiersListener;
        private ApiClientListener.FetchCharityDetailsListener mFetchCharityDetailsListener;
        private ApiClientListener.FetchPublicUserDetailsListener mFetchPublicUserDetailsListener;
        private ApiClientListener.AddUserCharityListener mAddUserCharityListener;
        private ApiClientListener.RemoveUserCharityListener mRemoveUserCharityListener;
        private ApiClientListener.FetchReviewsListener mFetchReviewsListener;
        private ApiClientListener.FetchFollowersListener mFetchFollowersListener;
        private ApiClientListener.WriteCharityReviewListener mWriteCharityReviewListener;
        private ApiClientListener.FollowUserListener mFollowUserListener;
        private ApiClientListener.UploadFCMTokenListener mUploadFCMTokenListener;
        private ApiClientListener.FetchUserChatSessionsListener mFetchUserChatSessionsListener;
        private ApiClientListener.InitializeChatSessionsListener mInitializeChatSessionsListener;
        private ApiClientListener.FetchUserInvitationsListener mFetchUserInvitationsListener;
        private ApiClientListener.SendUserInvitationListener mSendUserInvitationListener;
        private ApiClientListener.FetchPaypalAccountsListener mFetchPaypalAccountsListener;
        private ApiClientListener.InitiateManualDonationListener mInitiateManualDonationListener;
        private ApiClientListener.DeleteUserChatSessionListener mDeleteUserChatSessionListener;

        private DefaultResponse defaultResponse;
        private RegistrationResponse registrationResponse;
        private LoginResponse loginResponse;
        private CategoryResponse categoryResponse;
        private ForgotPasswordResponse forgotPasswordResponse;
        private ZakatGoalResponse zakatGoalResponse;
        private GoldValueResponse goldValueResponse;
        private CharitiesResponse charitiesResponse;
        private Charity charityResponse;
        private UserMenuResponse userMenuResponse;
        private PaymentResponse paymentResponse;
        private TopZakatifiersResponse topZakatifiersResponse;
        private ReviewsResponse reviewsResponse;
        private PublicUserResponse publicUserResponse;
        private BooleanStateResponse booleanStateResponse;
        private FollowersResponse followersResponse;
        private FCMTokenResponse fcmTokenResponse;
        private ChatSessionsResponse chatSessionsResponse;
        private InvitationsResponse invitationsResponse;
        private PaypalAccountsResponse paypalAccountsResponse;

        private static ApiService apiService = null;


        public static void resetApiService() {
            apiService = null;
        }


        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> accountMap, ApiClientListener.AccountRegistrationListener accountRegistrationListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.mAccountRegistrationListener = accountRegistrationListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> accountMap, ApiClientListener.UsernameCreationListener usernameCreationListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.mUsernameCreationListener = usernameCreationListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> accountMap, ApiClientListener.AccountAuthenticationListener accountAuthenticationListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.mAccountAuthenticationListener = accountAuthenticationListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> accountMap, ApiClientListener.ProfileUpdateListener profileUpdateListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.mProfileUpdateListener = profileUpdateListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, Map<String, String> accountMap, ApiClientListener.SocialNetworkLinkingListener socialNetworkLinkingListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = accountMap;
            this.username = username;
            this.mSocialNetworkLinkingListener = socialNetworkLinkingListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String emailAddress, ApiClientListener.ForgotPasswordListener forgotPasswordListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.emailAddress = emailAddress;
            this.mForgotPasswordListener = forgotPasswordListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, ApiClientListener.FetchCategoriesListener fetchCategoriesListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.mFetchCategoriesListener = fetchCategoriesListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, String categoryIds, ApiClientListener.UpdateUserCategoriesListener updateUserCategoriesListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.categoryIds = categoryIds;
            this.mUpdateUserCategoriesListener = updateUserCategoriesListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, Map<String, String> requestMap, ApiClientListener.AddZakatGoalListener addZakatGoalListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.requestMap = requestMap;
            this.mAddZakatGoalListener = addZakatGoalListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, Map<String, String> requestMap, ApiClientListener.UpdateZakatGoalListener updateZakatGoalListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.requestMap = requestMap;
            this.mUpdateZakatGoalListener = updateZakatGoalListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, ApiClientListener.FetchZakatGoalListener fetchZakatGoalListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.mFetchZakatGoalListener = fetchZakatGoalListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, ApiClientListener.DeleteZakatGoalListener deleteZakatGoalListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.mDeleteZakatGoalListener = deleteZakatGoalListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, ApiClientListener.FetchGoldValueListener fetchGoldValueListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.mFetchGoldValueListener = fetchGoldValueListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, String pageNumber, String pageSize, ApiClientListener.FetchSuggestedCharitiesListener mFetchSuggestedCharitiesListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.mFetchSuggestedCharitiesListener = mFetchSuggestedCharitiesListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, String pageNumber, String pageSize, ApiClientListener.FetchUserCharitiesListener fetchUserCharitiesListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.mFetchUserCharitiesListener = fetchUserCharitiesListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, String query, String pageNumber, String pageSize, ApiClientListener.SearchCharitiesListener searchCharitiesListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.query = query;
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.mSearchCharitiesListener = searchCharitiesListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, ApiClientListener.FetchUserMenuListener fetchUserMenuListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.mFetchUserMenuListener = fetchUserMenuListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, ApiClientListener.FetchPaymentOptionsListener fetchPaymentOptionsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.mFetchPaymentOptionsListener = fetchPaymentOptionsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, Map<String, String> requestMap, ApiClientListener.AddPaymentOptionListener addPaymentOptionListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.requestMap = requestMap;
            this.mAddPaymentOptionListener = addPaymentOptionListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, Map<String, String> requestMap, ApiClientListener.UpdatePaymentOptionListener updatePaymentOptionListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.requestMap = requestMap;
            this.mUpdatePaymentOptionListener = updatePaymentOptionListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String paymentOptionId, ApiClientListener.DeletePaymentOptionListener deletePaymentOptionListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.paymentOptionId = paymentOptionId;
            this.mDeletePaymentOptionListener = deletePaymentOptionListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String paymentOptionId, ApiClientListener.SetPrimaryPaymentOptionListener setPrimaryPaymentOptionListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.paymentOptionId = paymentOptionId;
            this.mSetPrimaryPaymentOptionListener = setPrimaryPaymentOptionListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, ApiClientListener.FetchTopZakatifiersListener fetchTopZakatifiersListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.mFetchTopZakatifiersListener = fetchTopZakatifiersListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, String charityId, ApiClientListener.FetchCharityDetailsListener fetchCharityDetailsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.charityId = charityId;
            this.mFetchCharityDetailsListener = fetchCharityDetailsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, String charityId, ApiClientListener.AddUserCharityListener addUserCharityListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.charityId = charityId;
            this.mAddUserCharityListener = addUserCharityListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, String charityId, ApiClientListener.RemoveUserCharityListener removeUserCharityListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.charityId = charityId;
            this.mRemoveUserCharityListener = removeUserCharityListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String id, String pageNumber, String pageSize, ApiClientListener.FetchReviewsListener fetchReviewsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.id = id;
            this.mFetchReviewsListener = fetchReviewsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String charityId, Map<String, String> requestMap, ApiClientListener.WriteCharityReviewListener writeCharityReviewListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.charityId = charityId;
            this.requestMap = requestMap;
            this.mWriteCharityReviewListener = writeCharityReviewListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, ApiClientListener.FetchPublicUserDetailsListener fetchPublicUserDetailsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.mFetchPublicUserDetailsListener = fetchPublicUserDetailsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String follower, String followed, boolean state, ApiClientListener.FollowUserListener followUserListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.follower = follower;
            this.followed = followed;
            this.state = state;
            this.mFollowUserListener = followUserListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, String pageNumber, String pageSize, ApiClientListener.FetchFollowersListener fetchFollowersListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            this.username = username;
            this.mFetchFollowersListener = fetchFollowersListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, Map<String, String> requestMap, ApiClientListener.UploadFCMTokenListener uploadFCMTokenListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.requestMap = requestMap;
            this.mUploadFCMTokenListener = uploadFCMTokenListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, ApiClientListener.FetchUserChatSessionsListener fetchUserChatSessionsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.mFetchUserChatSessionsListener = fetchUserChatSessionsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String requestingParticipant, String otherParticipant, ApiClientListener.InitializeChatSessionsListener initializeChatSessionsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestingParticipant = requestingParticipant;
            this.otherParticipant = otherParticipant;
            this.mInitializeChatSessionsListener = initializeChatSessionsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String username, ApiClientListener.FetchUserInvitationsListener fetchUserInvitationsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.username = username;
            this.mFetchUserInvitationsListener = fetchUserInvitationsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String invitorUsername, String inviteeEmail, ApiClientListener.SendUserInvitationListener sendUserInvitationListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.invitorUsername = invitorUsername;
            this.inviteeEmail = inviteeEmail;
            this.mSendUserInvitationListener = sendUserInvitationListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, ApiClientListener.FetchPaypalAccountsListener fetchPaypalAccountsListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.mFetchPaypalAccountsListener = fetchPaypalAccountsListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, Map<String, String> requestMap, ApiClientListener.InitiateManualDonationListener initiateManualDonationListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.requestMap = requestMap;
            this.mInitiateManualDonationListener = initiateManualDonationListener;
        }

        public NetworkCallsRunner(String REQUEST_TYPE, String messageId, ApiClientListener.DeleteUserChatSessionListener deleteUserChatSessionListener) {
            this.REQUEST_TYPE = REQUEST_TYPE;
            this.messageId = messageId;
            this.mDeleteUserChatSessionListener = deleteUserChatSessionListener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            apiService = getApiService();
        }

        private static ApiService getApiService() {
            if (apiService == null) {
                apiService = ApiModule.getApiModuleInstance(ZakatifyApplication.getCacheFile(), ApiUtils.buildHeaders(null)).getApiService();
            }

            return apiService;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            switch (REQUEST_TYPE) {
                case Constants.REGISTER_REQUEST:
                    registrationResponse = registerUserAccount(requestMap);
                    break;
                case Constants.LOGIN_REQUEST:
                    loginResponse = authenticateAccount(requestMap);
                    break;
                case Constants.RESET_PASSWORD_REQUEST:
                    forgotPasswordResponse = forgetPassAuthenticate(emailAddress);
                    break;
                case Constants.UPDATE_PROFILE_REQUEST:
                    loginResponse = updateUserProfile(requestMap);
                    break;
                case Constants.LINK_SOCIAL_NETWORK_REQUEST:
                    loginResponse = linkSocialAccount(username, requestMap);
                    break;
                case Constants.UNLINK_SOCIAL_NETWORK_REQUEST:
                    loginResponse = unLinkSocialAccount(username, requestMap);
                    break;
                case Constants.FETCH_CATEGORIES_REQUEST:
                    categoryResponse = fetchCategories(username, false);
                    break;
                case Constants.FETCH_USER_CATEGORIES_REQUEST:
                    categoryResponse = fetchCategories(username, true);
                    break;
                case Constants.UPDATE_USER_CATEGORIES_REQUEST:
                    categoryResponse = updateUserCategories(username, categoryIds);
                    break;
                case Constants.CREATE_USERNAME_REQUEST:
                    loginResponse = createUsername(requestMap);
                    break;
                case Constants.ADD_ZAKAT_GOAL_REQUEST:
                    zakatGoalResponse = addZakatGoal(username, requestMap, false);
                    break;
                case Constants.UPDATE_ZAKAT_GOAL_REQUEST:
                    zakatGoalResponse = addZakatGoal(username, requestMap, true);
                    break;
                case Constants.FETCH_ZAKAT_GOAL_REQUEST:
                    zakatGoalResponse = fetchZakatGoal(username);
                    break;
                case Constants.DELETE_ZAKAT_GOAL_REQUEST:
                    zakatGoalResponse = deleteZakatGoal(username);
                    break;
                case Constants.FETCH_GOLD_VALUE_REQUEST:
                    goldValueResponse = fetchGoldValue();
                    break;
                case Constants.FETCH_SUGGESTED_CHARITIES_REQUEST:
                    charitiesResponse = fetchSuggestedCharities(username, pageNumber, pageSize);
                    break;
                case Constants.FETCH_USER_PORTFOLIO_REQUEST:
                    charitiesResponse = fetchUserCharities(username, pageNumber, pageSize);
                    break;
                case Constants.FETCH_USER_MENU_REQUEST:
                    userMenuResponse = (UserMenuResponse) fetchUserMenu(username);
                    break;
                case Constants.FETCH_PAYMENT_OPTIONS_REQUEST:
                    paymentResponse = fetchPaymentOptions(username);
                    break;
                case Constants.ADD_PAYMENT_OPTION_REQUEST:
                    paymentResponse = addPaymentOption(username, requestMap, false);
                    break;
                case Constants.UPDATE_PAYMENT_OPTION_REQUEST:
                    paymentResponse = addPaymentOption(username, requestMap, true);
                    break;
                case Constants.DELETE_PAYMENT_OPTION_REQUEST:
                    paymentResponse = deletePaymentOption(paymentOptionId);
                    break;
                case Constants.SET_PRIMARY_PAYMENT_OPTION_REQUEST:
                    paymentResponse = setPrimaryPaymentOption(paymentOptionId);
                    break;
                case Constants.FETCH_CURRENT_MONTH_TOP_ZAKATIFIERS_REQUEST:
                    topZakatifiersResponse = fetchTopZakatifiers(0);
                    break;
                case Constants.FETCH_LIFETIME_TOP_ZAKATIFIERS_REQUEST:
                    topZakatifiersResponse = fetchTopZakatifiers(1);
                    break;
                case Constants.SEARCH_CHARITIES_REQUEST:
                    charitiesResponse = searchCharities(username, query, pageNumber, pageSize);
                    break;
                case Constants.FETCH_CHARITY_DETAILS_REQUEST:
                    charityResponse = fetchCharityDetails(charityId, username);
                    break;
                case Constants.ADD_USER_CHARITY_REQUEST:
                    charityResponse = addUserCharity(username, charityId);
                    break;
                case Constants.REMOVE_USER_CHARITY_REQUEST:
                    charityResponse = removeUserCharity(username, charityId);
                    break;
                case Constants.WRITE_CHARITY_REVIEW_REQUEST:
                    reviewsResponse = writeCharityReview(charityId, requestMap);
                    break;
                case Constants.FETCH_CHARITY_REVIEWS_REQUEST:
                    reviewsResponse = fetchReviews(id, pageNumber, pageSize, true);
                    break;
                case Constants.FETCH_USER_REVIEWS_REQUEST:
                    reviewsResponse = fetchReviews(id, pageNumber, pageSize, false);
                    break;
                case Constants.FETCH_PUBLIC_USER_DETAILS_REQUEST:
                    publicUserResponse = fetchPublicUserDetails(username);
                    break;
                case Constants.FOLLOW_USER_REQUEST:
                    booleanStateResponse = followUser(follower, followed, state);
                    break;
                case Constants.FETCH_FOLLOWERS_REQUEST:
                    followersResponse = fetchFollowers(username, pageNumber, pageSize);
                    break;
                case Constants.UPLOAD_FCM_TOKEN_REQUEST:
                    fcmTokenResponse = uploadFCMToken(username, requestMap);
                    break;
                case Constants.FETCH_USER_CHAT_SESSIONS_REQUEST:
                    chatSessionsResponse = fetchUserChatSessions(username);
                    break;
                case Constants.INITIALIZE_CHAT_SESSION_REQUEST:
                    chatSessionsResponse = initializeChatSession(requestingParticipant, otherParticipant);
                    break;
                case Constants.FETCH_USER_INVITES_REQUEST:
                    invitationsResponse = fetchUserInvitations(username);
                    break;
                case Constants.SEND_USER_INVITATION_REQUEST:
                    invitationsResponse = sendUserInvitation(invitorUsername, inviteeEmail);
                    break;
                case Constants.FETCH_PAYPAL_ACCOUNTS_REQUEST:
                    paypalAccountsResponse = fetchPaypalAccounts();
                    break;
                case Constants.INITIATE_MANUAL_DONATION_REQUEST:
                    booleanStateResponse = initiateManualDonation(requestMap);
                    break;
                case Constants.DELETE_USER_CHAT_SESSION_REQUEST:
                    booleanStateResponse = deleteUserChatSession(messageId);
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            switch (REQUEST_TYPE) {
                case Constants.REGISTER_REQUEST:
                    mAccountRegistrationListener.onAccountRegistered(registrationResponse);
                    break;
                case Constants.LOGIN_REQUEST:
                    mAccountAuthenticationListener.onAccountAuthenticated(loginResponse);
                    break;
                case Constants.RESET_PASSWORD_REQUEST:
                    mForgotPasswordListener.onPasswordReset(forgotPasswordResponse);
                    break;
                case Constants.UPDATE_PROFILE_REQUEST:
                    mProfileUpdateListener.onProfileUpdated(loginResponse);
                    break;
                case Constants.LINK_SOCIAL_NETWORK_REQUEST:
                    mSocialNetworkLinkingListener.onActionCommitted(loginResponse);
                    break;
                case Constants.UNLINK_SOCIAL_NETWORK_REQUEST:
                    mSocialNetworkLinkingListener.onActionCommitted(loginResponse);
                    break;
                case Constants.FETCH_USER_CATEGORIES_REQUEST:
                    mFetchCategoriesListener.onCategoriesFetched(categoryResponse);
                    break;
                case Constants.FETCH_CATEGORIES_REQUEST:
                    mFetchCategoriesListener.onCategoriesFetched(categoryResponse);
                    break;
                case Constants.UPDATE_USER_CATEGORIES_REQUEST:
                    mUpdateUserCategoriesListener.onCategoriesUpdated(categoryResponse);
                    break;
                case Constants.CREATE_USERNAME_REQUEST:
                    mUsernameCreationListener.onUsernameCreated(loginResponse);
                    break;
                case Constants.ADD_ZAKAT_GOAL_REQUEST:
                    mAddZakatGoalListener.onZakatGoalAdded(zakatGoalResponse);
                    break;
                case Constants.UPDATE_ZAKAT_GOAL_REQUEST:
                    mUpdateZakatGoalListener.onZakatGoalUpdated(zakatGoalResponse);
                    break;
                case Constants.FETCH_ZAKAT_GOAL_REQUEST:
                    mFetchZakatGoalListener.onZakatGoalFetched(zakatGoalResponse);
                    break;
                case Constants.DELETE_ZAKAT_GOAL_REQUEST:
                    mDeleteZakatGoalListener.onZakatGoalDeleted(zakatGoalResponse);
                    break;
                case Constants.FETCH_GOLD_VALUE_REQUEST:
                    mFetchGoldValueListener.onGoldValueFetched(goldValueResponse);
                    break;
                case Constants.FETCH_SUGGESTED_CHARITIES_REQUEST:
                    mFetchSuggestedCharitiesListener.onSuggestedCharitiesFetched(charitiesResponse);
                    break;
                case Constants.FETCH_USER_MENU_REQUEST:
                    mFetchUserMenuListener.onUserMenuFetched(userMenuResponse);
                    break;
                case Constants.FETCH_PAYMENT_OPTIONS_REQUEST:
                    mFetchPaymentOptionsListener.onPaymentOptionsFetched(paymentResponse);
                    break;
                case Constants.ADD_PAYMENT_OPTION_REQUEST:
                    mAddPaymentOptionListener.onPaymentOptionAdded(paymentResponse);
                    break;
                case Constants.UPDATE_PAYMENT_OPTION_REQUEST:
                    mUpdatePaymentOptionListener.onPaymentOptionUpdated(paymentResponse);
                    break;
                case Constants.DELETE_PAYMENT_OPTION_REQUEST:
                    mDeletePaymentOptionListener.onPaymentOptionDeleted(paymentResponse);
                    break;
                case Constants.SET_PRIMARY_PAYMENT_OPTION_REQUEST:
                    mSetPrimaryPaymentOptionListener.onPrimaryPaymentOptionSet(paymentResponse);
                    break;
                case Constants.FETCH_CURRENT_MONTH_TOP_ZAKATIFIERS_REQUEST:
                case Constants.FETCH_LIFETIME_TOP_ZAKATIFIERS_REQUEST:
                    mFetchTopZakatifiersListener.onTopZakatifiersFetched(topZakatifiersResponse);
                    break;
                case Constants.SEARCH_CHARITIES_REQUEST:
                    mSearchCharitiesListener.onSearchFinished(charitiesResponse);
                    break;
                case Constants.FETCH_USER_PORTFOLIO_REQUEST:
                    mFetchUserCharitiesListener.onUserCharitiesFetched(charitiesResponse);
                    break;
                case Constants.FETCH_CHARITY_DETAILS_REQUEST:
                    mFetchCharityDetailsListener.onCharityDetailsFetched(charityResponse);
                    break;
                case Constants.ADD_USER_CHARITY_REQUEST:
                    mAddUserCharityListener.onCharityAdded(charityResponse);
                    break;
                case Constants.REMOVE_USER_CHARITY_REQUEST:
                    mRemoveUserCharityListener.onCharityRemoved(charityResponse);
                    break;
                case Constants.FETCH_CHARITY_REVIEWS_REQUEST:
                    mFetchReviewsListener.onReviewsFetched(reviewsResponse);
                    break;
                case Constants.FETCH_USER_REVIEWS_REQUEST:
                    mFetchReviewsListener.onReviewsFetched(reviewsResponse);
                    break;
                case Constants.WRITE_CHARITY_REVIEW_REQUEST:
                    mWriteCharityReviewListener.onReviewSaved(reviewsResponse);
                    break;
                case Constants.FETCH_PUBLIC_USER_DETAILS_REQUEST:
                    mFetchPublicUserDetailsListener.onUserDetailsFetched(publicUserResponse);
                    break;
                case Constants.FOLLOW_USER_REQUEST:
                    mFollowUserListener.onFollowStateChanged(booleanStateResponse);
                    break;
                case Constants.FETCH_FOLLOWERS_REQUEST:
                    mFetchFollowersListener.onFollowersFetched(followersResponse);
                    break;
                case Constants.UPLOAD_FCM_TOKEN_REQUEST:
                    mUploadFCMTokenListener.onFCMTokenUploaded(fcmTokenResponse);
                    break;
                case Constants.FETCH_USER_CHAT_SESSIONS_REQUEST:
                    mFetchUserChatSessionsListener.onUserChatSessionsFetched(chatSessionsResponse);
                    break;
                case Constants.INITIALIZE_CHAT_SESSION_REQUEST:
                    mInitializeChatSessionsListener.onChatSessionInitialized(chatSessionsResponse);
                    break;
                case Constants.FETCH_USER_INVITES_REQUEST:
                    mFetchUserInvitationsListener.onInvitationsFetched(invitationsResponse);
                    break;
                case Constants.SEND_USER_INVITATION_REQUEST:
                    mSendUserInvitationListener.onInvitationSent(invitationsResponse);
                    break;
                case Constants.FETCH_PAYPAL_ACCOUNTS_REQUEST:
                    mFetchPaypalAccountsListener.onPaypalAccountsFetched(paypalAccountsResponse);
                    break;
                case Constants.INITIATE_MANUAL_DONATION_REQUEST:
                    mInitiateManualDonationListener.onManualDonationInitiated(booleanStateResponse);
                    break;
                case Constants.DELETE_USER_CHAT_SESSION_REQUEST:
                    mDeleteUserChatSessionListener.onUserChatSessionDeleted(booleanStateResponse);
                    break;
            }
        }

        private static RegistrationResponse registerUserAccount(Map<String, String> accountMap) {
            Call<RegistrationResponse> call;
            RegistrationResponse registrationResponse = null;

            try {
                call = getApiService().createAccount(accountMap);
                Response<RegistrationResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        registrationResponse = response.body();
                    } else {
                        Converter<ResponseBody, RegistrationResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(RegistrationResponse.class, new Annotation[0]);
                        try {
                            registrationResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return registrationResponse;
        }

        private static LoginResponse authenticateAccount(Map<String, String> accountMap) {
            Call<LoginResponse> call;
            LoginResponse loginResponse = null;

            try {
                call = getApiService().authenticateAccount(accountMap);
                Response<LoginResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        loginResponse = response.body();
                    } else {
                        Converter<ResponseBody, LoginResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(LoginResponse.class, new Annotation[0]);
                        try {
                            loginResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return loginResponse;
        }

        public static void checkUsernameAvailability(String username, final ApiClientListener.UsernameAvailabilityListener usernameAvailabilityListener) {
            Call<UsernameCheckResponse> call = getApiService().checkUsernameAvailability(username);
            call.enqueue(new Callback<UsernameCheckResponse>() {
                @Override
                public void onResponse(Call<UsernameCheckResponse> call, Response<UsernameCheckResponse> response) {
                    usernameAvailabilityListener.onUsernameChecked(response.body());
                }

                @Override
                public void onFailure(Call<UsernameCheckResponse> call, Throwable t) {
                    t.printStackTrace();
                    usernameAvailabilityListener.onUsernameChecked(null);
                }
            });
        }

        private static ForgotPasswordResponse forgetPassAuthenticate(String emailAddress) {
            Call<ForgotPasswordResponse> call;
            ForgotPasswordResponse forgetPasswordResponse = null;

            try {
                call = apiService.resetPassword(emailAddress);
                Response<ForgotPasswordResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        forgetPasswordResponse = response.body();
                    } else {
                        Converter<ResponseBody, ForgotPasswordResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(ForgotPasswordResponse.class, new Annotation[0]);
                        try {
                            forgetPasswordResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return forgetPasswordResponse;
        }

        private static LoginResponse updateUserProfile(Map<String, String> accountMap) {
            Call<LoginResponse> call;
            LoginResponse updateUserProfile = null;

            try {
                call = apiService.updateProfile(accountMap);
                Response<LoginResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        updateUserProfile = response.body();
                    } else {
                        Converter<ResponseBody, LoginResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(LoginResponse.class, new Annotation[0]);
                        try {
                            updateUserProfile = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return updateUserProfile;
        }

        private static LoginResponse linkSocialAccount(String username, Map<String, String> accountMap) {
            Call<LoginResponse> call;
            LoginResponse updateUserProfile = null;

            try {
                call = apiService.linkSocialNetwork(username, accountMap);
                Response<LoginResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        updateUserProfile = response.body();
                    } else {
                        Converter<ResponseBody, LoginResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(LoginResponse.class, new Annotation[0]);
                        try {
                            updateUserProfile = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return updateUserProfile;
        }

        private static LoginResponse unLinkSocialAccount(String username, Map<String, String> accountMap) {
            Call<LoginResponse> call;
            LoginResponse updateUserProfile = null;

            try {
                call = apiService.unLinkSocialNetwork(username, accountMap);
                Response<LoginResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        updateUserProfile = response.body();
                    } else {
                        Converter<ResponseBody, LoginResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(LoginResponse.class, new Annotation[0]);
                        try {
                            updateUserProfile = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return updateUserProfile;
        }

        private static CategoryResponse fetchCategories(String username, boolean isUserMode) {
            Call<CategoryResponse> call;
            CategoryResponse categoryResponse = null;
            try {
                if (isUserMode) {
                    call = apiService.fetchUserCategories(username);
                } else {
                    call = apiService.fetchCategories(username);
                }
                Response<CategoryResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        categoryResponse = response.body();
                    } else {
                        Converter<ResponseBody, CategoryResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(CategoryResponse.class, new Annotation[0]);
                        try {
                            categoryResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return categoryResponse;
        }

        private static CategoryResponse updateUserCategories(String username, String categoryIds) {
            Call<CategoryResponse> call;
            CategoryResponse categoryResponse = null;
            try {
                call = apiService.updateUserCategories(username, categoryIds);
                Response<CategoryResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        categoryResponse = response.body();
                    } else {
                        Converter<ResponseBody, CategoryResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(CategoryResponse.class, new Annotation[0]);
                        try {
                            categoryResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return categoryResponse;
        }

        private static LoginResponse createUsername(Map<String, String> accountMap) {
            Call<LoginResponse> call;
            LoginResponse createUsernameResponse = null;

            try {
                call = apiService.createUsername(accountMap);
                Response<LoginResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        createUsernameResponse = response.body();
                    } else {
                        Converter<ResponseBody, LoginResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(LoginResponse.class, new Annotation[0]);
                        try {
                            createUsernameResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return createUsernameResponse;
        }

        private static ZakatGoalResponse addZakatGoal(String username, Map<String, String> requestMap, boolean inUpdateMode) {
            Call<ZakatGoalResponse> call;
            ZakatGoalResponse zakatGoalResponse = null;

            try {
                if (!inUpdateMode) {
                    call = apiService.addZakatGoal(username, requestMap);
                } else {
                    call = apiService.updateZakatGoal(username, requestMap);
                }
                Response<ZakatGoalResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        zakatGoalResponse = response.body();
                    } else {
                        Converter<ResponseBody, ZakatGoalResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(ZakatGoalResponse.class, new Annotation[0]);
                        try {
                            zakatGoalResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return zakatGoalResponse;
        }

        private static ZakatGoalResponse fetchZakatGoal(String username) {
            Call<ZakatGoalResponse> call;
            ZakatGoalResponse zakatGoalResponse = null;

            try {

                call = apiService.fetchZakatGoal(username);
                Response<ZakatGoalResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        zakatGoalResponse = response.body();
                    } else {
                        Converter<ResponseBody, ZakatGoalResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(ZakatGoalResponse.class, new Annotation[0]);
                        try {
                            zakatGoalResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return zakatGoalResponse;
        }

        private static ZakatGoalResponse deleteZakatGoal(String username) {
            Call<ZakatGoalResponse> call;
            ZakatGoalResponse zakatGoalResponse = null;

            try {
                call = apiService.deleteZakatGoal(username);
                Response<ZakatGoalResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        zakatGoalResponse = response.body();
                    } else {
                        Converter<ResponseBody, ZakatGoalResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(ZakatGoalResponse.class, new Annotation[0]);
                        try {
                            zakatGoalResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return zakatGoalResponse;
        }

        private static GoldValueResponse fetchGoldValue() {
            Call<GoldValueResponse> call;
            GoldValueResponse goldValueResponse = null;

            try {
                call = apiService.fetchGoldValue();
                Response<GoldValueResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        goldValueResponse = response.body();
                    } else {
                        Converter<ResponseBody, GoldValueResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(GoldValueResponse.class, new Annotation[0]);
                        try {
                            goldValueResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return goldValueResponse;
        }

        private static CharitiesResponse fetchSuggestedCharities(String username, String pageNumber, String pageSize) {
            Call<CharitiesResponse> call;
            CharitiesResponse charitiesResponse = null;

            try {
                call = apiService.fetchSuggestedCharities(username, pageNumber, pageSize);
                Response<CharitiesResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        charitiesResponse = response.body();
                    } else {
                        Converter<ResponseBody, CharitiesResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(CharitiesResponse.class, new Annotation[0]);
                        try {
                            charitiesResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return charitiesResponse;
        }

        private static CharitiesResponse fetchUserCharities(String username, String pageNumber, String pageSize) {
            Call<CharitiesResponse> call;
            CharitiesResponse charitiesResponse = null;

            try {
                call = apiService.fetchUserCharities(username, pageNumber, pageSize);
                Response<CharitiesResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        charitiesResponse = response.body();
                    } else {
                        Converter<ResponseBody, CharitiesResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(CharitiesResponse.class, new Annotation[0]);
                        try {
                            charitiesResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return charitiesResponse;
        }

        public static void updateGoldValue(final ApiClientListener.FetchGoldValueListener fetchGoldValueListener) {
            Call<GoldValueResponse> call = getApiService().fetchGoldValue();
            call.enqueue(new Callback<GoldValueResponse>() {
                @Override
                public void onResponse(Call<GoldValueResponse> call, Response<GoldValueResponse> response) {
                    fetchGoldValueListener.onGoldValueFetched(response.body());
                }

                @Override
                public void onFailure(Call<GoldValueResponse> call, Throwable t) {
                    t.printStackTrace();
                    fetchGoldValueListener.onGoldValueFetched(null);
                }
            });
        }


        private static Object fetchUserMenu(String username) {
            Call<UserMenuResponse> call;
            UserMenuResponse userMenuResponse = null;

            try {
                call = apiService.fetchUserMenuData(username);
                Response<UserMenuResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        userMenuResponse = response.body();
                    } else {
                        Converter<ResponseBody, DefaultResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(DefaultResponse.class, new Annotation[0]);
                        try {
                            return (DefaultResponse) converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return userMenuResponse;
        }

        private static PaymentResponse fetchPaymentOptions(String username) {
            Call<PaymentResponse> call;
            PaymentResponse paymentResponse = null;

            try {

                call = apiService.fetchPaymentOptions(username);
                Response<PaymentResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        paymentResponse = response.body();
                    } else {
                        Converter<ResponseBody, PaymentResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(PaymentResponse.class, new Annotation[0]);
                        try {
                            paymentResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return paymentResponse;
        }

        private static PaymentResponse addPaymentOption(String username, Map<String, String> requestMap, boolean inUpdateMode) {
            Call<PaymentResponse> call;
            PaymentResponse paymentResponse = null;

            try {
                if (!inUpdateMode) {
                    call = apiService.addPaymentOption(username, requestMap);
                } else {
                    call = apiService.updatePaymentOption(username, requestMap);
                }
                Response<PaymentResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        paymentResponse = response.body();
                    } else {
                        Converter<ResponseBody, PaymentResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(PaymentResponse.class, new Annotation[0]);
                        try {
                            paymentResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return paymentResponse;
        }

        private static PaymentResponse deletePaymentOption(String paymentOptionId) {
            Call<PaymentResponse> call;
            PaymentResponse paymentResponse = null;

            try {
                call = apiService.deletePaymentOption(paymentOptionId);
                Response<PaymentResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        paymentResponse = response.body();
                    } else {
                        Converter<ResponseBody, PaymentResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(PaymentResponse.class, new Annotation[0]);
                        try {
                            paymentResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return paymentResponse;
        }

        private static PaymentResponse setPrimaryPaymentOption(String paymentOptionId) {
            Call<PaymentResponse> call;
            PaymentResponse paymentResponse = null;

            try {
                call = apiService.setPrimaryPaymentOption(paymentOptionId);
                Response<PaymentResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        paymentResponse = response.body();
                    } else {
                        Converter<ResponseBody, PaymentResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(PaymentResponse.class, new Annotation[0]);
                        try {
                            paymentResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return paymentResponse;
        }

        private static TopZakatifiersResponse fetchTopZakatifiers(int topZakatifierMode) {
            Call<TopZakatifiersResponse> call;
            TopZakatifiersResponse topZakatifiersResponse = null;

            try {

                call = topZakatifierMode == 0 ? apiService.fetchCurrentMonthZakatifiers() : apiService.fetchLifetimeTopZakatifiers();
                Response<TopZakatifiersResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        topZakatifiersResponse = response.body();
                    } else {
                        Converter<ResponseBody, TopZakatifiersResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(TopZakatifiersResponse.class, new Annotation[0]);
                        try {
                            topZakatifiersResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return topZakatifiersResponse;
        }

        private static CharitiesResponse searchCharities(String username, String query, String pageNumber, String pageSize) {
            Call<CharitiesResponse> call;
            CharitiesResponse charitiesResponse = null;

            try {
                call = apiService.searchCharities(username, query, pageNumber, pageSize);
                Response<CharitiesResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        charitiesResponse = response.body();
                    } else {
                        Converter<ResponseBody, CharitiesResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(CharitiesResponse.class, new Annotation[0]);
                        try {
                            charitiesResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return charitiesResponse;
        }

        private static Charity fetchCharityDetails(String charityId, String username) {
            Call<Charity> call;
            Charity charityResponse = null;

            try {
                call = apiService.fetchCharityDetails(charityId, username);
                Response<Charity> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        charityResponse = response.body();
                    } else {
                        Converter<ResponseBody, Charity> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(Charity.class, new Annotation[0]);
                        try {
                            charityResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return charityResponse;
        }

        private static Charity addUserCharity(String username, String charityId) {
            Call<Charity> call;
            Charity charityResponse = null;

            try {
                call = apiService.addUserCharity(username, charityId);
                Response<Charity> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        charityResponse = response.body();
                    } else {
                        Converter<ResponseBody, Charity> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(Charity.class, new Annotation[0]);
                        try {
                            charityResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return charityResponse;
        }

        private static Charity removeUserCharity(String username, String charityId) {
            Call<Charity> call;
            Charity charityResponse = null;

            try {
                call = apiService.removeUserCharity(username, charityId);
                Response<Charity> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        charityResponse = response.body();
                    } else {
                        Converter<ResponseBody, Charity> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(Charity.class, new Annotation[0]);
                        try {
                            charityResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return charityResponse;
        }

        private static ReviewsResponse fetchReviews(String id, String pageNumber, String pageSize, boolean inCharityMode) {
            Call<ReviewsResponse> call;
            ReviewsResponse reviewsResponse = null;

            try {
                call = inCharityMode ? apiService.fetchCharityReviews(id, pageNumber, pageSize) : apiService.fetchUserReviews(id, pageNumber, pageSize);
                Response<ReviewsResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        reviewsResponse = response.body();
                    } else {
                        Converter<ResponseBody, ReviewsResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(ReviewsResponse.class, new Annotation[0]);
                        try {
                            reviewsResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return reviewsResponse;
        }

        private static ReviewsResponse writeCharityReview(String charityId, Map<String, String> requestMap) {
            Call<ReviewsResponse> call;
            ReviewsResponse reviewsResponse = null;

            try {
                call = apiService.writeCharityReview(charityId, requestMap);
                Response<ReviewsResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        reviewsResponse = response.body();
                    } else {
                        Converter<ResponseBody, ReviewsResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(ReviewsResponse.class, new Annotation[0]);
                        try {
                            reviewsResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return reviewsResponse;
        }

        private static PublicUserResponse fetchPublicUserDetails(String username) {
            Call<PublicUserResponse> call;
            PublicUserResponse publicUserResponse = null;

            try {
                call = apiService.fetchPublicUserDetails(username);
                Response<PublicUserResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        publicUserResponse = response.body();
                    } else {
                        Converter<ResponseBody, PublicUserResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(Charity.class, new Annotation[0]);
                        try {
                            publicUserResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return publicUserResponse;
        }

        private static BooleanStateResponse followUser(String follower, String followed, boolean state) {
            Call<BooleanStateResponse> call;
            BooleanStateResponse booleanStateResponse = null;

            try {
                if (state) {
                    call = apiService.followUser(follower, followed);
                } else {
                    call = apiService.unFollowUser(follower, followed);
                }

                Response<BooleanStateResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        booleanStateResponse = response.body();
                    } else {
                        Converter<ResponseBody, BooleanStateResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(Charity.class, new Annotation[0]);
                        try {
                            booleanStateResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return booleanStateResponse;
        }

        private static FollowersResponse fetchFollowers(String username, String pageNumber, String pageSize) {
            Call<FollowersResponse> call;
            FollowersResponse followersResponse = null;

            try {
                call = apiService.fetchUserFollowers(username, pageNumber, pageSize);
                Response<FollowersResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        followersResponse = response.body();
                    } else {
                        Converter<ResponseBody, FollowersResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(FollowersResponse.class, new Annotation[0]);
                        try {
                            followersResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return followersResponse;
        }

        private FCMTokenResponse uploadFCMToken(String username, Map<String, String> FCMTokenMap) {
            Call<FCMTokenResponse> call;
            FCMTokenResponse fcmTokenResponse = null;

            try {
                call = apiService.saveUserDeviceToken(username, FCMTokenMap);
                Response<FCMTokenResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        fcmTokenResponse = response.body();
                    } else {
                        Converter<ResponseBody, FCMTokenResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(FCMTokenResponse.class, new Annotation[0]);
                        try {
                            fcmTokenResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return fcmTokenResponse;
        }

        private ChatSessionsResponse initializeChatSession(String requestingParticipant, String otherParticipant) {
            Call<ChatSessionsResponse> call;
            ChatSessionsResponse chatSessionsResponse = null;

            try {
                call = apiService.initializeChatSession(requestingParticipant, otherParticipant);
                Response<ChatSessionsResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        chatSessionsResponse = response.body();
                    } else {
                        Converter<ResponseBody, ChatSessionsResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(ChatSessionsResponse.class, new Annotation[0]);
                        try {
                            chatSessionsResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return chatSessionsResponse;
        }

        private ChatSessionsResponse fetchUserChatSessions(String username) {
            Call<ChatSessionsResponse> call;
            ChatSessionsResponse chatSessionsResponse = null;

            try {
                call = apiService.fetchUserChatSessions(username);
                Response<ChatSessionsResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        chatSessionsResponse = response.body();
                    } else {
                        Converter<ResponseBody, ChatSessionsResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(ChatSessionsResponse.class, new Annotation[0]);
                        try {
                            chatSessionsResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return chatSessionsResponse;
        }

        public static void notifyOfflineUser(Map<String, String> notifyMap, final ApiClientListener.NotifyOfflineUserListener notifyOfflineUserListener) {
            Call<DefaultResponse> call = getApiService().notifyOfflineUser(notifyMap);
            call.enqueue(new Callback<DefaultResponse>() {
                @Override
                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                    //notifyOfflineUserListener.onNotificationSent(response.body());
                }

                @Override
                public void onFailure(Call<DefaultResponse> call, Throwable t) {
                    //t.printStackTrace();
                    //notifyOfflineUserListener.onNotificationSent(null);
                }
            });
        }

        private InvitationsResponse fetchUserInvitations(String username) {
            Call<InvitationsResponse> call;
            InvitationsResponse invitationsResponse = null;

            try {
                call = apiService.fetchUserInvitations(username, "1", "100");
                Response<InvitationsResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        invitationsResponse = response.body();
                    } else {
                        Converter<ResponseBody, InvitationsResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(InvitationsResponse.class, new Annotation[0]);
                        try {
                            invitationsResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return invitationsResponse;
        }

        private InvitationsResponse sendUserInvitation(String invitorUsername, String inviteeEmail) {
            Call<InvitationsResponse> call;
            InvitationsResponse invitationsResponse = null;

            try {
                call = apiService.sendInvitation(invitorUsername, inviteeEmail);
                Response<InvitationsResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        invitationsResponse = response.body();
                    } else {
                        Converter<ResponseBody, InvitationsResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(InvitationsResponse.class, new Annotation[0]);
                        try {
                            invitationsResponse = converter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return invitationsResponse;
        }

        private static PaypalAccountsResponse fetchPaypalAccounts() {
            Call<PaypalAccountsResponse> call;
            PaypalAccountsResponse paypalAccountsResponse = null;

            try {
                call = apiService.fetchPaypalAccounts();
                Response<PaypalAccountsResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        paypalAccountsResponse = response.body();
                    } else {
                        Converter<ResponseBody, PaypalAccountsResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(PaypalAccountsResponse.class, new Annotation[0]);
                        try {
                            paypalAccountsResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return paypalAccountsResponse;
        }

        private static BooleanStateResponse initiateManualDonation(Map<String, String> requestMap) {
            Call<BooleanStateResponse> call;
            BooleanStateResponse booleanStateResponse = null;

            try {
                call = apiService.initiateManualDonation(requestMap);
                Response<BooleanStateResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        booleanStateResponse = response.body();
                    } else {
                        Converter<ResponseBody, BooleanStateResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(Charity.class, new Annotation[0]);
                        try {
                            booleanStateResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return booleanStateResponse;
        }

        private static BooleanStateResponse deleteUserChatSession(String messageId) {
            Call<BooleanStateResponse> call;
            BooleanStateResponse booleanStateResponse = null;

            try {
                call = apiService.deleteUserChatSession(messageId);
                Response<BooleanStateResponse> response = call.execute();
                if (response != null) {
                    if (response.isSuccessful()) {
                        booleanStateResponse = response.body();
                    } else {
                        Converter<ResponseBody, BooleanStateResponse> converter =
                                ApiModule.buildRetrofitAdapter(ZakatifyApplication.getCacheFile(), null)
                                        .responseBodyConverter(Charity.class, new Annotation[0]);
                        try {
                            booleanStateResponse = converter.convert(response.errorBody());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return booleanStateResponse;
        }

    }
}