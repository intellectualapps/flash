package com.affinislabs.zakatify.app.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.BooleanStateResponse;
import com.affinislabs.zakatify.app.api.responses.PaypalAccountsResponse;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.PaypalAccount;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.PaymentActivity;
import com.affinislabs.zakatify.app.ui.activities.PaypalActivity;
import com.affinislabs.zakatify.app.ui.activities.ProfileActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.PaymentOptionsAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomNumericEditText;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ManualDonationFragment extends BasePaymentsFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, ApiClientListener.FetchPaypalAccountsListener, TextView.OnEditorActionListener, View.OnFocusChangeListener {
    private User user;
    private Button makeDonationButton;
    private RatingBar charityRatingBar;
    private ImageView mCharityLogo;
    private Toolbar toolbar;
    private CustomRecyclerView paymentOptionsRecyclerView;
    private TextView mCharityName, mCharityDescription;
    private PaymentOptionsAdapter adapter;
    private List<PaypalAccount> paypalAccounts;
    private CustomNumericEditText donationAmountInput;
    private Charity charity;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new ManualDonationFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public ManualDonationFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = getArguments().getParcelable(Constants.USER);
            charity = getArguments().getParcelable(Constants.CHARITY);
        }
        paypalAccounts = new ArrayList<PaypalAccount>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manual_donation, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((PaymentActivity) getActivity()).initializeToolbar(getString(R.string.manual_donation_toolbar_label));
        makeDonationButton = view.findViewById(R.id.make_donation_button);
        paymentOptionsRecyclerView = view.findViewById(R.id.payment_options_list);
        donationAmountInput = view.findViewById(R.id.amount_input);
        mCharityLogo = view.findViewById(R.id.charity_icon);
        mCharityName = view.findViewById(R.id.charity_name);
        charityRatingBar = view.findViewById(R.id.charity_rating);

        adapter = new PaymentOptionsAdapter(getContext(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        paymentOptionsRecyclerView.setAdapter(adapter);
        paymentOptionsRecyclerView.setLayoutManager(linearLayoutManager);

        makeDonationButton.setOnClickListener(this);
        fetchPaymentOptions(this, null, true);

        donationAmountInput.setOnEditorActionListener(this);
        donationAmountInput.setOnFocusChangeListener(this);
        donationAmountInput.addNumericValueChangedListener(new CustomNumericEditText.NumericValueWatcher() {
            @Override
            public void onChanged(double newValue) {
                makeDonationButton.setEnabled(newValue > 0.0);
            }

            @Override
            public void onCleared() {
                makeDonationButton.setEnabled(false);
            }
        });
        loadCharityData(charity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.make_donation_button:
                if (NetworkUtils.isConnected(getContext())) {
                    hideKeyboard();
                    Map<String, String> requestMap = new HashMap<String, String>();
                    String donationAmount = String.format(Locale.getDefault(), "%.2f", donationAmountInput.getNumericValue());
                    requestMap.put(Constants.CHARITY_ID, String.valueOf(charity.getCharityId()));
                    requestMap.put(Constants.AMOUNT, donationAmount);
                    initiateManualDonation(requestMap);
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (adapter != null) {
            adapter.saveStates(outState);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (adapter != null) {
            adapter.restoreStates(savedInstanceState);
        }
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1 && ListUtils.isNotEmpty(paypalAccounts)) {
        }
    }

    @Override
    public void onPaypalAccountsFetched(PaypalAccountsResponse paypalAccountsResponse) {
        if (paypalAccountsResponse != null) {
            paypalAccounts.clear();
            if (ListUtils.isNotEmpty(paypalAccountsResponse.getExistingPayPalAccounts())) {
                paypalAccounts.add(getPrimaryPaymentOption(paypalAccountsResponse.getExistingPayPalAccounts()));
            }
            adapter.setItems(paypalAccounts);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.amount_input:
                donationAmountInput.setCursorVisible(hasFocus);
                break;
        }
    }

    private void loadCharityData(Charity charity) {
        if (charity != null) {
            mCharityName.setText(charity.getCharityName());
            ImageUtils.loadImageUrl(mCharityLogo, getContext(), charity.getCharityLogo());
            charityRatingBar.setRating(charity.getRating());
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            hideKeyboard();
        }
        return false;
    }

    private void initiateManualDonation(Map<String, String> requestMao) {
        showLoadingIndicator(getString(R.string.initiating_manual_donation_message));
        new ApiClient.NetworkCallsRunner(Constants.INITIATE_MANUAL_DONATION_REQUEST, requestMao, new ApiClientListener.InitiateManualDonationListener() {
            @Override
            public void onManualDonationInitiated(BooleanStateResponse booleanStateResponse) {
                hideLoadingIndicator();
                if (booleanStateResponse != null) {
                    if (booleanStateResponse.getStatus() == null && booleanStateResponse.getMessage() == null) {
                        if (booleanStateResponse.getState()) {
                            showSuccessPopup(getString(R.string.manual_donation_successful_request_message));
                            closeFragment();
                        } else {
                            showErrorPopup(getString(R.string.manual_donation_error_message));
                        }
                    } else {
                        showErrorPopup(booleanStateResponse.getMessage());
                    }

                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }
}
