package com.affinislabs.zakatify.app.ui.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.adapters.TabPagerAdapter;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FontUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;

public class AuthViewPagerFragment extends BaseFragment implements View.OnClickListener {
    private User user;
    private ViewPager viewPager;
    private TextView launchAboutView, launchPrivacyPolicyView;
    private TabLayout tabLayout;

    private int tabHeaderIds[] = new int[]{
            R.string.register_tab_label,
            R.string.login_tab_label
    };

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new AuthViewPagerFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public AuthViewPagerFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }

        if (user == null) {
            user = new User();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth_viewpager, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        launchAboutView = (TextView) view.findViewById(R.id.about_app_button);
        launchPrivacyPolicyView = (TextView) view.findViewById(R.id.privacy_policy_button);

        launchAboutView.setOnClickListener(this);
        launchPrivacyPolicyView.setOnClickListener(this);

        if (viewPager != null) {
            setupViewPager(viewPager);
            setupTabLayout(tabLayout);
        }

        if (PreferenceStorageManager.hasLoggedInOnce(ZakatifyApplication.getAppInstance().getApplicationContext())) {
            viewPager.setCurrentItem(1);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        if (viewPager != null) {
            TabPagerAdapter mAdapter = new TabPagerAdapter(getChildFragmentManager());
            Bundle bundle = new Bundle();
            mAdapter.addFragment(RegistrationFragment.newInstance(bundle), getString(tabHeaderIds[0]));
            mAdapter.addFragment(LoginFragment.newInstance(bundle), getString(tabHeaderIds[1]));
            viewPager.setAdapter(mAdapter);
        }
    }

    private void setupTabLayout(TabLayout tabLayout) {
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);

            Typeface typeface = FontUtils.selectTypeface(getContext(), FontUtils.STYLE_BOLD);
            tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.removeAllTabs();
            ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
            PagerAdapter adapter = viewPager.getAdapter();
            for (int i = 0, count = adapter.getCount(); i < count; i++) {
                TabLayout.Tab tab = tabLayout.newTab();
                tabLayout.addTab(tab.setText(adapter.getPageTitle(i)));
                AppCompatTextView view = (AppCompatTextView) ((ViewGroup) viewGroup.getChildAt(i)).getChildAt(1);
                view.setTypeface(typeface);
            }
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.about_app_button:
                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).launchWebViewDialog(getActivity(), Constants.ABOUT_LINK, getString(R.string.about_zakatify));
                }
                break;
            case R.id.privacy_policy_button:
                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).launchWebViewDialog(getActivity(), Constants.PRIVACY_LINK, getString(R.string.privacy_label));
                }
                break;
        }
    }
}
