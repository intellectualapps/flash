package com.affinislabs.zakatify.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.TopZakatifier;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.TopZakatifiersAdapter;

import java.util.ArrayList;
import java.util.List;


public class SearchZakatifiersFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener {
    private RecyclerView topZakatifiersRecyclerview;
    private List<TopZakatifier> topZakatifiersList = new ArrayList<TopZakatifier>();
    private TopZakatifiersAdapter adapter;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new SearchZakatifiersFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public SearchZakatifiersFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //topZakatifiersList = PreferenceStorageManager.getTopZakatifiers(ZakatifyApplication.getAppInstance().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_zakatifiers, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        //((BaseActivity) getActivity()).getToolbar().setVisibility(R.string.top_zakatifiers_label););
     /*   adapter = new TopZakatifiersAdapter(getContext(), this);
        topZakatifiersRecyclerview = (RecyclerView) view.findViewById(R.id.top_zakatifiers_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        topZakatifiersRecyclerview.setAdapter(adapter);
        topZakatifiersRecyclerview.setLayoutManager(linearLayoutManager);
        topZakatifiersRecyclerview.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        fetchTopZakatifiers();*/
    }

    private void loadAdapter(List<TopZakatifier> topZakatifiers) {
        adapter.setItems(topZakatifiers);
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {

    }
}
