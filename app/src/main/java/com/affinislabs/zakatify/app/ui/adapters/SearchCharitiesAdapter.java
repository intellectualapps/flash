package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Category;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.ui.customviews.DonorViewItemDecorator;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.affinislabs.zakatify.app.utils.Transformers;
import com.affinislabs.zakatify.app.utils.ZakatifyUtils;
import com.greenfrvr.hashtagview.HashtagView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchCharitiesAdapter extends BaseRecyclerAdapter {
    private List<Charity> charities;
    private ClickListener clickListener;
    private String username;
    private final int CHARITY_ITEM = 2;

    public SearchCharitiesAdapter(Context context, ClickListener clickListener, String username, ZakatifyInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        this.username = username;
        charities = new ArrayList<Charity>();
    }

    public void setItems(List<Charity> items) {
        this.charities = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (charities != null)
            this.charities.clear();
        notifyDataSetChanged();
    }

    public void addItem(Charity charity) {
        if (charity != null) {
            this.charities.add(charity);
            notifyDataSetChanged();
        }
    }

    public void add(Charity charity) {
        charities.add(charity);
        notifyItemInserted(charities.size() - 1);
    }

    public void addAll(List<Charity> charities) {
        if (charities != null && charities.size() > 0) {
            for (Charity charity : charities) {
                add(charity);
            }
        }
        notifyDataSetChanged();
    }

    public void removeItem(Charity charity) {
        int position = charities.indexOf(charity);
        if (position > -1) {
            charities.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(int position) {
        if (position > -1) {
            charities.remove(position);
            notifyItemRemoved(position);
        }
    }

    public List<Charity> getCharities() {
        return charities;
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView charityName, charityDescription, donationSummary, newsFeedView;
        private ImageView charityIcon, addCharityButton;
        private ClickListener clickListener;
        private CustomRecyclerView donorListRecyclerView;
        private DonorListAdapter donorListAdapter;
        private HashtagView categoryTags;

        public ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            charityName = (TextView) itemView.findViewById(R.id.charity_name);
            newsFeedView = (TextView) itemView.findViewById(R.id.news_feed_view);
            charityDescription = (TextView) itemView.findViewById(R.id.charity_description);
            charityIcon = (ImageView) itemView.findViewById(R.id.charity_icon);
            categoryTags = (HashtagView) itemView.findViewById(R.id.category_tags);
            addCharityButton = (ImageView) itemView.findViewById(R.id.add_charity_button);
            donationSummary = itemView.findViewById(R.id.donation_summary);
            donorListRecyclerView = itemView.findViewById(R.id.donor_list_recyclerview);
            donorListAdapter = new DonorListAdapter(context);
            donorListRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            donorListAdapter.setItems(null);
            donorListRecyclerView.addItemDecoration(new DonorViewItemDecorator(-10));
            donorListRecyclerView.setAdapter(donorListAdapter);
            itemView.setOnClickListener(this);
            addCharityButton.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        switch (viewType) {
            default:
            case CHARITY_ITEM:
                view = mInflater.inflate(R.layout.search_charity_item_layout, parent, false);
                viewHolder = new SearchCharitiesAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            case CHARITY_ITEM:
                final SearchCharitiesAdapter.ViewHolder viewHolder = (SearchCharitiesAdapter.ViewHolder) holder;
                final Charity charity = charities.get(position);
                if (charity != null) {
                    viewHolder.charityName.setText(charity.getCharityName());
                    viewHolder.charityDescription.setText(charity.getSlogan());
                    if (charity.getCharityLogo() != null) {
                        ImageUtils.loadImageUrl(viewHolder.charityIcon, context, charity.getCharityLogo());
                    }
                    if (charity.getSelected()) {
                        viewHolder.addCharityButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_following_blue));
                    } else {
                        viewHolder.addCharityButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_follow_blue));
                    }

                    Spannable newsFeed = ZakatifyUtils.getStyledNewsFeedActivity(charity.getNewsFeed(), context);
                    if (newsFeed != null) {
                        viewHolder.newsFeedView.setText(newsFeed);
                        viewHolder.newsFeedView.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.newsFeedView.setVisibility(View.GONE);
                    }

                    viewHolder.donorListAdapter.setItems(charity.getDonors());
                    viewHolder.donationSummary.setText(ZakatifyUtils.formatDonorAndAmountSummary(context, charity.getNumberOfDonators(), charity.getTotalDonation()));

                    viewHolder.addCharityButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (charity.getSelected()) {
                                removeFromPortfolio(username, String.valueOf(charity.getCharityId()), viewHolder, position);
                            } else {
                                addToPortfolio(username, String.valueOf(charity.getCharityId()), viewHolder, position);
                            }
                        }
                    });

                    List<String> DATA = ListUtils.getCategoryNames(new ArrayList<Category>(charity.getCategories())) != null
                            ? Arrays.asList(ListUtils.getCategoryNames(new ArrayList<Category>(charity.getCategories()))) : null;

                    if (DATA != null) {
                        viewHolder.categoryTags.setData(DATA, Transformers.CAPITALIZE_TAG, new HashtagView.DataSelector<String>() {
                            @Override
                            public boolean preselect(String item) {
                                return false;
                            }
                        });
                    }
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == charities.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return CHARITY_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(charities) ? 0 : charities.size();
    }

    private void addToPortfolio(String username, String charityId, final ViewHolder viewHolder, final int position) {
        new ApiClient.NetworkCallsRunner(Constants.ADD_USER_CHARITY_REQUEST, username, charityId, new ApiClientListener.AddUserCharityListener() {
            @Override
            public void onCharityAdded(Charity charityResponse) {
                if (charityResponse != null) {
                    if (charityResponse.getStatus() == null && charityResponse.getMessage() == null) {
                        if (charityResponse.getState()) {
                            viewHolder.addCharityButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_following_blue));
                            charities.get(position).setSelected(true);
                        }
                    }
                }
            }
        }).execute();
    }

    private void removeFromPortfolio(String username, String charityId, final ViewHolder viewHolder, final int position) {
        new ApiClient.NetworkCallsRunner(Constants.REMOVE_USER_CHARITY_REQUEST, username, charityId, new ApiClientListener.RemoveUserCharityListener() {
            @Override
            public void onCharityRemoved(Charity charityResponse) {
                if (charityResponse != null) {
                    if (charityResponse.getStatus() == null && charityResponse.getMessage() == null) {
                        if (charityResponse.getState()) {
                            viewHolder.addCharityButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_follow_blue));
                            charities.get(position).setSelected(false);
                        }
                    }
                }
            }
        }).execute();
    }
}