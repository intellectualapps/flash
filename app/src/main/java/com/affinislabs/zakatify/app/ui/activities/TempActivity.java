package com.affinislabs.zakatify.app.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;


public class TempActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(TempActivity.this, AuthActivity.class);
        startActivity(intent);
        finish();
    }
}
