package com.affinislabs.zakatify.app.api;

import com.affinislabs.zakatify.app.api.responses.BooleanStateResponse;
import com.affinislabs.zakatify.app.api.responses.CategoryResponse;
import com.affinislabs.zakatify.app.api.responses.CharitiesResponse;
import com.affinislabs.zakatify.app.api.responses.ChatSessionsResponse;
import com.affinislabs.zakatify.app.api.responses.DefaultResponse;
import com.affinislabs.zakatify.app.api.responses.FCMTokenResponse;
import com.affinislabs.zakatify.app.api.responses.FollowersResponse;
import com.affinislabs.zakatify.app.api.responses.ForgotPasswordResponse;
import com.affinislabs.zakatify.app.api.responses.GoldValueResponse;
import com.affinislabs.zakatify.app.api.responses.InvitationsResponse;
import com.affinislabs.zakatify.app.api.responses.LoginResponse;
import com.affinislabs.zakatify.app.api.responses.PaypalAccountsResponse;
import com.affinislabs.zakatify.app.api.responses.PaymentResponse;
import com.affinislabs.zakatify.app.api.responses.PublicUserResponse;
import com.affinislabs.zakatify.app.api.responses.RegistrationResponse;
import com.affinislabs.zakatify.app.api.responses.ReviewsResponse;
import com.affinislabs.zakatify.app.api.responses.TopZakatifiersResponse;
import com.affinislabs.zakatify.app.api.responses.UserMenuResponse;
import com.affinislabs.zakatify.app.api.responses.UsernameCheckResponse;
import com.affinislabs.zakatify.app.api.responses.ZakatGoalResponse;
import com.affinislabs.zakatify.app.models.Charity;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


interface ApiService {
    @POST("/api/v1/user")
    Call<RegistrationResponse> createAccount(@QueryMap Map<String, String> dataMap);

    @POST("/api/v1/user/authenticate")
    Call<LoginResponse> authenticateAccount(@QueryMap Map<String, String> dataMap);

    @GET("/api/v1/user/verify/{username}")
    Call<UsernameCheckResponse> checkUsernameAvailability(@Path("username") String username);

    @POST("/api/v1/user/password")
    Call<ForgotPasswordResponse> resetPassword(@Query("email") String emailAddress);

    @PUT("/api/v1/user/link-social/{username}")
    Call<LoginResponse> linkSocialNetwork(@Path("username") String username, @QueryMap Map<String, String> queryMap);

    @PUT("/api/v1/user/unlink-social/{username}")
    Call<LoginResponse> unLinkSocialNetwork(@Path("username") String username, @QueryMap Map<String, String> queryMap);

    @PUT("/api/v1/user/profile")
    Call<LoginResponse> updateProfile(@QueryMap Map<String, String> dataMap);

    @POST("/api/v1/user/username")
    Call<LoginResponse> createUsername(@QueryMap Map<String, String> dataMap);

    @GET("/api/v1/preference/category/{username}")
    Call<CategoryResponse> fetchCategories(@Path("username") String username);

    @GET("/api/v1/preference/user-category/{username}")
    Call<CategoryResponse> fetchUserCategories(@Path("username") String username);

    @POST("/api/v1/preference/user-category/{username}")
    Call<CategoryResponse> updateUserCategories(@Path("username") String username, @Query("category-id") String categoryIds);

    @GET("/api/v1/preference/user-zakat-goal/{username}")
    Call<ZakatGoalResponse> fetchZakatGoal(@Path("username") String username);

    @POST("/api/v1/preference/user-zakat-goal/{username}")
    Call<ZakatGoalResponse> addZakatGoal(@Path("username") String username, @QueryMap Map<String, String> data);

    @PUT("/api/v1/preference/user-zakat-goal/{username}")
    Call<ZakatGoalResponse> updateZakatGoal(@Path("username") String username, @QueryMap Map<String, String> data);

    @DELETE("/api/v1/preference/user-zakat-goal/{username}")
    Call<ZakatGoalResponse> deleteZakatGoal(@Path("username") String username);

    @GET("/api/v1/preference/user-payment-option/{username}")
    Call<PaymentResponse> fetchPaymentOptions(@Path("username") String username);

    @POST("/api/v1/preference/user-payment-option/{username}")
    Call<PaymentResponse> addPaymentOption(@Path("username") String username, @QueryMap Map<String, String> data);

    @PUT("/api/v1/preference/user-payment-option/{username}")
    Call<PaymentResponse> updatePaymentOption(@Path("username") String username, @QueryMap Map<String, String> data);

    @PUT("/api/v1/preference/user-payment-option/primary/{paymentOptionId}")
    Call<PaymentResponse> setPrimaryPaymentOption(@Path("paymentOptionId") String paymentOptionId);

    @DELETE("/api/v1/preference/user-payment-option/{username}")
    Call<PaymentResponse> deletePaymentOption(@Path("username") String username, @QueryMap Map<String, String> data);

    @GET("/api/v1/preference/gold-value")
    Call<GoldValueResponse> fetchGoldValue();

    @GET("/api/v1/charity/suggest/{username}")
    Call<CharitiesResponse> fetchSuggestedCharities(@Path("username") String username, @Query("page-number") String pageNumber, @Query("page-size") String pageSize);

    @GET("/api/v1/charity/search")
    Call<CharitiesResponse> searchCharities(@Query("username") String username, @Query("query") String query, @Query("page-number") String pageNumber, @Query("page-size") String pageSize);

    @GET("/api/v1/preference/user-charity/{username}")
    Call<CharitiesResponse> fetchUserCharities(@Path("username") String username, @Query("page-number") String pageNumber, @Query("page-size") String pageSize);

    @GET("/api/v1/charity/details/{charity-id}")
    Call<Charity> fetchCharityDetails(@Path("charity-id") String charityId, @Query("username") String username);

    @POST("/api/v1/preference/user-charity/{username}")
    Call<Charity> addUserCharity(@Path("username") String username, @Query("charity-id") String charityId);

    @DELETE("/api/v1/preference/user-charity/{username}")
    Call<Charity> removeUserCharity(@Path("username") String username, @Query("charity-id") String charityId);

    @GET("/api/v1/review/charity/{charity-id}")
    Call<ReviewsResponse> fetchCharityReviews(@Path("charity-id") String charityId, @Query("page-number") String pageNumber, @Query("page-size") String pageSize);

    @GET("/api/v1/review/user/{username}")
    Call<ReviewsResponse> fetchUserReviews(@Path("username") String username, @Query("page-number") String pageNumber, @Query("page-size") String pageSize);

    @GET("/api/v1/follower")
    Call<FollowersResponse> fetchUserFollowers(@Query("username") String username, @Query("page-number") String pageNumber, @Query("page-size") String pageSize);

    @POST("/api/v1/review/charity/{charity-id}")
    Call<ReviewsResponse> writeCharityReview(@Path("charity-id") String charityId, @QueryMap Map<String, String> data);

    @GET("/api/v1/user/menu/{username}")
    Call<UserMenuResponse> fetchUserMenuData(@Path("username") String username);

    @GET("/api/v1/user/public/{username}")
    Call<PublicUserResponse> fetchPublicUserDetails(@Path("username") String username);

    @GET("/api/v1/zakatifier/top")
    Call<TopZakatifiersResponse> fetchLifetimeTopZakatifiers();

    @GET("/api/v1/zakatifier/current-month/top")
    Call<TopZakatifiersResponse> fetchCurrentMonthZakatifiers();

    @POST("/api/v1/follower")
    Call<BooleanStateResponse> followUser(@Query("follower") String follower, @Query("followed") String followed);

    @DELETE("/api/v1/follower")
    Call<BooleanStateResponse> unFollowUser(@Query("follower") String follower, @Query("followed") String followed);

    @POST("/api/v1/user/device-token/{username}")
    Call<FCMTokenResponse> saveUserDeviceToken(@Path("username") String username, @QueryMap Map<String, String> token);

    @GET("/api/v1/message/{username}")
    Call<ChatSessionsResponse> initializeChatSession(@Path("username") String requestingParticipant, @Query("other-participant") String otherParticipant);

    @GET("/api/v1/message/list/{username}")
    Call<ChatSessionsResponse> fetchUserChatSessions(@Path("username") String username);

    @POST("/api/v1/message/notify")
    Call<DefaultResponse> notifyOfflineUser(@QueryMap Map<String, String> notifyMap);

    @GET("/api/v1/invitation/{username}")
    Call<InvitationsResponse> fetchUserInvitations(@Path("username") String username, @Query("page-number") String pageNumber, @Query("page-size") String pageSize);

    @POST("/api/v1/invitation")
    Call<InvitationsResponse> sendInvitation(@Query("invitor-username") String invitorUsername, @Query("invitee-email") String inviteeUsername);

    @GET("/api/v1/preference/paypal-accounts")
    Call<PaypalAccountsResponse> fetchPaypalAccounts();

    @POST("/api/v1/donation/manual/donation-request")
    Call<BooleanStateResponse> initiateManualDonation(@QueryMap Map<String, String> requestMap);

    @DELETE("/api/v1/payment/remove/{paymentOptionId}")
    Call<PaymentResponse> deletePaymentOption(@Path("paymentOptionId") String paymentOptionId);

    @DELETE("/api/v1/message/list/{messageId}")
    Call<BooleanStateResponse> deleteUserChatSession(@Path("messageId") String messageId);
}
