package com.affinislabs.zakatify.app.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ZakatGoalResponse extends DefaultResponse {
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("cash")
    @Expose
    private Double cash;
    @SerializedName("frequencyId")
    @Expose
    private Integer frequencyId;
    @SerializedName("gold")
    @Expose
    private Double gold;
    @SerializedName("investment")
    @Expose
    private Double investment;
    @SerializedName("liability")
    @Expose
    private Double liability;
    @SerializedName("period")
    @Expose
    private String period;
    @SerializedName("personalUse")
    @Expose
    private Double personalUse;
    @SerializedName("progress")
    @Expose
    private Integer progress;
    @SerializedName("realEstate")
    @Expose
    private Double realEstate;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("state")
    @Expose
    private Boolean state;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    public Integer getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(Integer frequencyId) {
        this.frequencyId = frequencyId;
    }

    public Double getGold() {
        return gold;
    }

    public void setGold(Double gold) {
        this.gold = gold;
    }

    public Double getInvestment() {
        return investment;
    }

    public void setInvestment(Double investment) {
        this.investment = investment;
    }

    public Double getLiability() {
        return liability;
    }

    public void setLiability(Double liability) {
        this.liability = liability;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Double getPersonalUse() {
        return personalUse;
    }

    public void setPersonalUse(Double personalUse) {
        this.personalUse = personalUse;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public Double getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(Double realEstate) {
        this.realEstate = realEstate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }


}
