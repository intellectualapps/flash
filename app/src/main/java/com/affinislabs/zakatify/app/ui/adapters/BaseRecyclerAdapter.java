package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.Message;
import com.affinislabs.zakatify.app.models.Review;
import com.affinislabs.zakatify.app.models.User;

import java.util.List;

/**
 *
 */
public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    static Context context;
    LayoutInflater mInflater;
    public boolean isLoadingAdded = false;
    public boolean retryPageLoad = false;
    public ZakatifyInterfaces.PaginationAdapterCallback callback;
    public String errorMsg;
    public final int LOADING = 1;
    public final int REVIEW_ITEM = 2;
    private int position = -1;

    public BaseRecyclerAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    public interface ClickListener {
        public void onClick(View v, int position, boolean isLongClick);
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ProgressBar mProgressBar;
        public ImageButton mRetryBtn;
        public TextView mErrorTxt;
        public LinearLayout mErrorLayout;

        public LoadingViewHolder(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    hideRetry();
                    if (callback != null) {
                        callback.loadMoreItems();
                    }
                    break;
            }
        }
    }

    public void initializeLoadingViewHolder(RecyclerView.ViewHolder holder) {
        LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
        if (retryPageLoad) {
            loadingViewHolder.mErrorLayout.setVisibility(View.VISIBLE);
            loadingViewHolder.mProgressBar.setVisibility(View.GONE);
            loadingViewHolder.mErrorTxt.setText(
                    errorMsg != null ?
                            errorMsg :
                            context.getString(R.string.error_msg_unknown));

        } else {
            loadingViewHolder.mErrorLayout.setVisibility(View.GONE);
            loadingViewHolder.mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideRetry() {
        retryPageLoad = false;
        notifyItemChanged(getItemCount());
        this.errorMsg = null;
    }

    private String getClassName(String className) {
        if (className.contains(".")) {
            String[] splitName = className.split("\\.");
            return splitName[splitName.length - 1];
        }

        return className;
    }

    public void addLoadingFooter(Class<?> clazz, Object adapter) {
        isLoadingAdded = true;
        switch (getClassName(clazz.getName())) {
            case "ChatSessionsAdapter":
                ChatSessionsAdapter chatSessionsAdapter = (ChatSessionsAdapter) adapter;
                chatSessionsAdapter.add(new ChatSession());
                break;
            case "CharitiesAdapter":
                CharitiesAdapter charitiesAdapter = (CharitiesAdapter) adapter;
                charitiesAdapter.add(new Charity());
                break;
            case "CharityReviewsAdapter":
                CharityReviewsAdapter charityReviewsAdapter = (CharityReviewsAdapter) adapter;
                charityReviewsAdapter.add(new Review());
                break;
            case "UserReviewsAdapter":
                UserReviewsAdapter userReviewsAdapter = (UserReviewsAdapter) adapter;
                userReviewsAdapter.add(new Review());
                break;
            case "SearchCharitiesAdapter":
                SearchCharitiesAdapter searchCharitiesAdapter = (SearchCharitiesAdapter) adapter;
                searchCharitiesAdapter.add(new Charity());
                break;
            case "UsersAdapter":
                UsersAdapter usersAdapter = (UsersAdapter) adapter;
                usersAdapter.add(new User());
                break;
            case "MessagesAdapter":
                MessagesAdapter messagesAdapter = (MessagesAdapter) adapter;
                messagesAdapter.add(new Message());
                break;
        }
    }

    public void removeLoadingFooter(Class<?> clazz, Object adapter) {
        isLoadingAdded = false;
        switch (getClassName(clazz.getName())) {
            case "ChatSessionsAdapter":
                ChatSessionsAdapter chatSessionsAdapter = (ChatSessionsAdapter) adapter;
                position = chatSessionsAdapter.getChatSessions().size() - 1;
                if (position > -1) {
                    ChatSession chatSession = chatSessionsAdapter.getChatSessions().get(position);
                    if (chatSession != null && chatSession.getMessageId() == null) {
                        chatSessionsAdapter.getChatSessions().remove(position);
                        chatSessionsAdapter.notifyItemRemoved(position);
                    }
                }
                break;
            case "CharitiesAdapter":
                CharitiesAdapter charitiesAdapter = (CharitiesAdapter) adapter;
                position = charitiesAdapter.getCharities().size() - 1;
                if (position > -1) {
                    Charity charity = charitiesAdapter.getCharities().get(position);
                    if (charity != null && charity.getCharityId() == null) {
                        charitiesAdapter.getCharities().remove(position);
                        charitiesAdapter.notifyItemRemoved(position);
                    }
                }
                break;
            case "CharityReviewsAdapter":
                CharityReviewsAdapter charityReviewsAdapter = (CharityReviewsAdapter) adapter;
                position = charityReviewsAdapter.getReviews().size() - 1;
                if (position > -1) {
                    Review review = charityReviewsAdapter.getReviews().get(position);
                    if (review != null && review.getReviewId() < 1) {
                        charityReviewsAdapter.getReviews().remove(position);
                        charityReviewsAdapter.notifyItemRemoved(position);
                    }
                }
                break;
            case "UserReviewsAdapter":
                UserReviewsAdapter userReviewsAdapter = (UserReviewsAdapter) adapter;
                position = userReviewsAdapter.getReviews().size() - 1;
                if (position > -1) {
                    Review review = userReviewsAdapter.getReviews().get(position);
                    if (review != null && review.getReviewId() < 1) {
                        userReviewsAdapter.getReviews().remove(position);
                        userReviewsAdapter.notifyItemRemoved(position);
                    }
                }
                break;
            case "SearchCharitiesAdapter":
                SearchCharitiesAdapter searchCharitiesAdapter = (SearchCharitiesAdapter) adapter;
                position = searchCharitiesAdapter.getCharities().size() - 1;
                if (position > -1) {
                    Charity charity = searchCharitiesAdapter.getCharities().get(position);
                    if ((charity != null) && (charity.getCharityId() == null || charity.getCharityId() < 1)) {
                        searchCharitiesAdapter.getCharities().remove(position);
                        searchCharitiesAdapter.notifyItemRemoved(position);
                    }
                }
                break;
            case "UsersAdapter":
                UsersAdapter usersAdapter = (UsersAdapter) adapter;
                position = usersAdapter.getUsers().size() - 1;
                if (position > -1) {
                    User user = usersAdapter.getUsers().get(position);
                    if (user != null && TextUtils.isEmpty(user.getUsername())) {
                        usersAdapter.getUsers().remove(position);
                        usersAdapter.notifyItemRemoved(position);
                    }
                }
                break;
            case "MessagesAdapter":
                MessagesAdapter messagesAdapter = (MessagesAdapter) adapter;
                position = 0;

                Message message = messagesAdapter.getMessages().get(position);
                if (message != null && TextUtils.isEmpty(message.getId())) {
                    messagesAdapter.getMessages().remove(position);
                    messagesAdapter.notifyItemRemoved(position);
                }
                break;
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg, List<?> list) {
        retryPageLoad = show;
        notifyItemChanged(list.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }
}
