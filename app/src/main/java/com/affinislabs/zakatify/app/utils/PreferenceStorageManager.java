package com.affinislabs.zakatify.app.utils;

import android.content.Context;

import com.affinislabs.zakatify.app.models.Category;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.Counts;
import com.affinislabs.zakatify.app.models.Invite;
import com.affinislabs.zakatify.app.models.TopZakatifier;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.models.UserMenu;
import com.affinislabs.zakatify.app.models.ZakatGoal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PreferenceStorageManager {
    private static final String PREFS_FCM_TOKEN = "prefs_fcm_token";
    private static final String PREFS_AUTH_TOKEN = "prefs_auth_token";

    private static final String PREFS_DEVICE_ID = "prefs_device_id";
    private static final String PREFS_FIRST_NAME = "prefs_first_name";
    private static final String PREFS_GENDER = "prefs_gender";
    private static final String PREFS_LAST_NAME = "prefs_last_name";
    private static final String PREFS_PROFILE_PHOTO = "prefs_profile_photo";
    private static final String PREFS_USERNAME = "prefs_username";
    private static final String PREFS_ID = "prefs_id";
    private static final String PREFS_EMAIL_ADDRESS = "prefs_email_address";
    private static final String PREFS_TWITTER_EMAIL_ADDRESS = "prefs_twitter_email_address";
    private static final String PREFS_PASSWORD = "prefs_password";
    private static final String PREFS_USER = "prefs_user";
    private static final String PREFS_COUNTS = "prefs_counts";
    private static final String PREFS_ZAKAT_GOAL = "prefs_zakat_goal";
    private static final String PREFS_CURRENT_MONTH_TOP_ZAKATIFIERS = "prefs_top_zakatifiers";
    private static final String PREFS_LIFETIME_TOP_ZAKATIFIERS = "prefs_top_zakatifiers";
    private static final String PREFS_USER_MENU = "prefs_user_menu";
    private static final String PREFS_GOLD_VALUE = "prefs_gold_value";
    private static final String PREFS_CHAT_SESSIONS = "prefs_chat_sessions";
    private static final String PREFS_CATEGORIES = "prefs_categories";
    private static final String PREFS_INVITES = "prefs_invites";
    private static final String PREFS_USER_CATEGORIES = "prefs_user_categories";
    private static final String PREFS_USER_CHARITIES = "prefs_user_charities";
    private static final String PREFS_SUGGESTED_CHARITIES = "prefs_suggested_charities";
    private static final String PREFS_PHOTO_BASE64 = "prefs_photo_base64";
    private static final String PREFS_USER_ID = "prefs_user_id";
    private static final String PREFS_FULLNAME = "prefs_full_name";
    private static final String PREFS_FB_ACCESS_TOKEN = "prefs_fb_access_token";
    private static final String PREFS_FB_ID = "prefs_fb_id";
    private static final String PREFS_USER_SIGNED_IN = "prefs_signed_in";
    private static final String PREFS_HAS_LOGGED_IN = "prefs_has_logged_in";
    private static final String PREFS_HAS_LOGGED_IN_ONCE = "prefs_has_logged_in_once";
    private static final String PREFS_HAS_UPDATED_PROFILE = "prefs_has_updated_profile";
    private static final String PREFS_IS_FIRST_USE_ON_DEVICE = "prefs_is_first_use_on_device";
    private static final String PREFS_IS_ONBOARDING_ACTIVITY_SHOWN = "prefs_is_onboarding_activity_shown";
    private static final String PREFS_IS_PROFILE_ONBOARDED = "prefs_is_profile_onboarded";
    private static final String PREFS_IS_ZAKATIFY_ONBOARDED = "prefs_is_zakatify_onboarded";
    private static final String PREFS_IS_PAYMENT_ONBOARDED = "prefs_is_payment_onboarded";
    private static final String PREFS_IS_INTERESTS_ONBOARDED = "prefs_is_interests_onboarded";

    public static void resetUser(Context context) {
        SharedPrefsUtils.clearPreference(context, PREFS_USER);
        SharedPrefsUtils.clearPreference(context, PREFS_USER_SIGNED_IN);
        SharedPrefsUtils.clearPreference(context, PREFS_USER_ID);
        SharedPrefsUtils.clearPreference(context, PREFS_AUTH_TOKEN);
        SharedPrefsUtils.clearPreference(context, PREFS_CHAT_SESSIONS);
        SharedPrefsUtils.clearPreference(context, PREFS_INVITES);
        SharedPrefsUtils.clearPreference(context, PREFS_CURRENT_MONTH_TOP_ZAKATIFIERS);
        SharedPrefsUtils.clearPreference(context, PREFS_LIFETIME_TOP_ZAKATIFIERS);

        //clearSocialData(context);
    }

    public static void clearOnBoardingPreferences(Context context) {
        SharedPrefsUtils.clearPreference(context, PREFS_IS_FIRST_USE_ON_DEVICE);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_ONBOARDING_ACTIVITY_SHOWN);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_PROFILE_ONBOARDED);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_ZAKATIFY_ONBOARDED);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_PAYMENT_ONBOARDED);
        SharedPrefsUtils.clearPreference(context, PREFS_IS_INTERESTS_ONBOARDED);
    }

    public static void clearSocialData(Context context) {
        SharedPrefsUtils.clearPreference(context, PREFS_FULLNAME);
        SharedPrefsUtils.clearPreference(context, PREFS_EMAIL_ADDRESS);
        SharedPrefsUtils.clearPreference(context, PREFS_PROFILE_PHOTO);
    }

    public static void saveDeviceId(Context context, String deviceId) {
        SharedPrefsUtils.setStringPreference(context, PREFS_DEVICE_ID, deviceId);
    }

    public static void saveAuthToken(Context context, String apiToken) {
        SharedPrefsUtils.setStringPreference(context, PREFS_AUTH_TOKEN, apiToken);
    }

    public static void saveTwitterEmailAddress(Context context, String emailAddress) {
        SharedPrefsUtils.setStringPreference(context, PREFS_TWITTER_EMAIL_ADDRESS, emailAddress);
    }

    public static void saveFCMToken(Context context, String FCMToken) {
        SharedPrefsUtils.setStringPreference(context, PREFS_FCM_TOKEN, FCMToken);
    }

    public static void saveUserId(Context context, String userId) {
        SharedPrefsUtils.setStringPreference(context, PREFS_USER_ID, userId);
    }

    public static void saveProfileData(Context context, Map<String, String> profileData) {
        SharedPrefsUtils.setStringPreference(context, PREFS_FULLNAME, profileData.get(Constants.FULL_NAME));
        SharedPrefsUtils.setStringPreference(context, PREFS_EMAIL_ADDRESS, profileData.get(Constants.EMAIL_ADDRESS));
        SharedPrefsUtils.setStringPreference(context, PREFS_PROFILE_PHOTO, profileData.get(Constants.PROFILE_PHOTO));
    }

    public static void saveFacebookAuthParams(Context context, Map<String, String> facebookAuthParams) {
        SharedPrefsUtils.setStringPreference(context, PREFS_FB_ACCESS_TOKEN, facebookAuthParams.get(Constants.FACEBOOK_ACCESS_TOKEN));
        SharedPrefsUtils.setStringPreference(context, PREFS_FB_ID, facebookAuthParams.get(Constants.FACEBOOK_ID));
    }

    public static void saveZakatGoal(Context context, ZakatGoal zakatGoal) {
        SharedPrefsUtils.setStringPreference(context, PREFS_ZAKAT_GOAL, StringUtils.zakatGoalToString(zakatGoal));
    }

    public static void saveUserMenu(Context context, UserMenu userMenu) {
        SharedPrefsUtils.setStringPreference(context, PREFS_USER_MENU, StringUtils.userMenuToString(userMenu));
    }

    public static void saveBase64Photo(Context context, String base64String) {
        SharedPrefsUtils.setStringPreference(context, PREFS_PHOTO_BASE64, base64String);
    }

    public static void saveCategories(Context context, ArrayList<Category> categories) {
        SharedPrefsUtils.setStringPreference(context, PREFS_CATEGORIES, StringUtils.categoriesToString(categories));
    }

    public static void saveInvites(Context context, List<Invite> invites) {
        SharedPrefsUtils.setStringPreference(context, PREFS_INVITES, StringUtils.invitesToString(invites));
    }

    public static void saveUserCategories(Context context, ArrayList<Category> userCategories) {
        SharedPrefsUtils.setStringPreference(context, PREFS_USER_CATEGORIES, StringUtils.categoriesToString(userCategories));
    }

    public static void saveUserCharities(Context context, List<Charity> userCharities) {
        SharedPrefsUtils.setStringPreference(context, PREFS_USER_CHARITIES, StringUtils.charitiesToString(userCharities));
    }

    public static void saveSuggestedCharities(Context context, List<Charity> userCharities) {
        SharedPrefsUtils.setStringPreference(context, PREFS_SUGGESTED_CHARITIES, StringUtils.charitiesToString(userCharities));
    }

    public static void saveCurrentMonthTopZakatifiers(Context context, List<TopZakatifier> topZakatifiers) {
        SharedPrefsUtils.setStringPreference(context, PREFS_CURRENT_MONTH_TOP_ZAKATIFIERS, StringUtils.topZakatifiersToString(topZakatifiers));
    }

    public static void saveLifetimeTopZakatifiers(Context context, List<TopZakatifier> topZakatifiers) {
        SharedPrefsUtils.setStringPreference(context, PREFS_LIFETIME_TOP_ZAKATIFIERS, StringUtils.topZakatifiersToString(topZakatifiers));
    }

    public static void saveChatSessions(Context context, List<ChatSession> chatSessions) {
        SharedPrefsUtils.setStringPreference(context, PREFS_CHAT_SESSIONS, StringUtils.chatSessionsToString(chatSessions));
    }

    public static void setSignInStatus(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_USER_SIGNED_IN, status);
    }

    public static void setLoggedInStatus(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_HAS_LOGGED_IN, status);
    }

    public static void setHasLoggedInOnce(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_HAS_LOGGED_IN_ONCE, status);
    }

    public static void setProfileUpdateStatus(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_HAS_UPDATED_PROFILE, status);
    }

    public static void setLoginData(Context context, String username, String password) {
        SharedPrefsUtils.setStringPreference(context, PREFS_ID, username);
        SharedPrefsUtils.setStringPreference(context, PREFS_PASSWORD, password);
    }

    public static void saveUser(Context context, User user) {
        SharedPrefsUtils.setStringPreference(context, PREFS_USER, StringUtils.userToString(user));
    }

    public static void saveCounts(Context context, Counts counts) {
        SharedPrefsUtils.setStringPreference(context, PREFS_COUNTS, StringUtils.countsToString(counts));
    }

    public static void setFirstUseOnDevice(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_IS_FIRST_USE_ON_DEVICE, status);
    }

    public static void setOnBoardingActivityShown(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_IS_ONBOARDING_ACTIVITY_SHOWN, status);
    }

    public static void setProfileOnBoarded(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_IS_PROFILE_ONBOARDED, status);
    }

    public static void setZakatifyOnBoarded(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_IS_ZAKATIFY_ONBOARDED, status);
    }

    public static void setPaymentsOnBoarded(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_IS_PAYMENT_ONBOARDED, status);
    }

    public static void setInterestsOnBoarded(Context context, boolean status) {
        SharedPrefsUtils.setBooleanPreference(context, PREFS_IS_INTERESTS_ONBOARDED, status);
    }

    public static void setGoldValue(Context context, double goldValue) {
        long transientValue = Double.doubleToRawLongBits(goldValue);
        SharedPrefsUtils.setLongPreference(context, PREFS_GOLD_VALUE, transientValue);
    }

    /**************************************************
     *
     *
     **************************************************/

    public static User getUser(Context context) {
        User user;
        user = StringUtils.userFromString(SharedPrefsUtils.getStringPreference(context, PREFS_USER));
        return user;
    }

    public static Counts getCounts(Context context) {
        return StringUtils.countsFromString(SharedPrefsUtils.getStringPreference(context, PREFS_COUNTS));
    }

    public static ZakatGoal getZakatGoal(Context context) {
        return StringUtils.zakatGoalFromString(SharedPrefsUtils.getStringPreference(context, PREFS_ZAKAT_GOAL));
    }

    public static List<TopZakatifier> getCurrentMonthTopZakatifiers(Context context) {
        return StringUtils.topZakatifiersFromString(SharedPrefsUtils.getStringPreference(context, PREFS_CURRENT_MONTH_TOP_ZAKATIFIERS));
    }

    public static List<TopZakatifier> getLifetimeTopZakatifiers(Context context) {
        return StringUtils.topZakatifiersFromString(SharedPrefsUtils.getStringPreference(context, PREFS_LIFETIME_TOP_ZAKATIFIERS));
    }

    public static List<ChatSession> getChatSessions(Context context) {
        return StringUtils.chatSessionsFromString(SharedPrefsUtils.getStringPreference(context, PREFS_CHAT_SESSIONS));
    }

    public static UserMenu getUserMenu(Context context) {
        return StringUtils.userMenuFromString(SharedPrefsUtils.getStringPreference(context, PREFS_USER_MENU));
    }

    public static String getUserId(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_USER_ID);
    }

    public static String getTwitterEmailAddress(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_TWITTER_EMAIL_ADDRESS);
    }

    public static String getBase64Photo(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_PHOTO_BASE64);
    }

    public static ArrayList<Category> getCategories(Context context) {
        return StringUtils.categoriesFromString(SharedPrefsUtils.getStringPreference(context, PREFS_CATEGORIES));
    }

    public static List<Invite> getInvites(Context context) {
        return StringUtils.invitesFromString(SharedPrefsUtils.getStringPreference(context, PREFS_INVITES));
    }

    public static ArrayList<Category> getUserCategories(Context context) {
        return StringUtils.categoriesFromString(SharedPrefsUtils.getStringPreference(context, PREFS_USER_CATEGORIES));
    }

    public static List<Charity> getSuggestedCharities(Context context) {
        return StringUtils.charitiesFromString(SharedPrefsUtils.getStringPreference(context, PREFS_SUGGESTED_CHARITIES));
    }

    public static List<Charity> getUserCharities(Context context) {
        return StringUtils.charitiesFromString(SharedPrefsUtils.getStringPreference(context, PREFS_USER_CHARITIES));
    }

    public static String getAuthToken(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_AUTH_TOKEN);
    }

    public static String getFCMToken(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_FCM_TOKEN);
    }

    public static String getDeviceId(Context context) {
        return SharedPrefsUtils.getStringPreference(context, PREFS_DEVICE_ID);
    }

    public static Map<String, String> getLoginData(Context context) {
        Map<String, String> loginDataMap = new HashMap<String, String>();
        loginDataMap.put(Constants.ID, SharedPrefsUtils.getStringPreference(context, PREFS_ID));
        loginDataMap.put(Constants.PASSWORD, SharedPrefsUtils.getStringPreference(context, PREFS_PASSWORD));
        return loginDataMap;
    }

    public static Map<String, String> getProfileData(Context context) {
        Map<String, String> profileData = new HashMap<String, String>();
        profileData.put(Constants.FULL_NAME, SharedPrefsUtils.getStringPreference(context, PREFS_FULLNAME));
        profileData.put(Constants.PROFILE_PHOTO, SharedPrefsUtils.getStringPreference(context, PREFS_PROFILE_PHOTO));
        return profileData;
    }

    public static Map<String, String> getFacebookAuthParams(Context context) {
        Map<String, String> loginDataMap = new HashMap<String, String>();
        loginDataMap.put(Constants.FACEBOOK_ACCESS_TOKEN, SharedPrefsUtils.getStringPreference(context, PREFS_FB_ACCESS_TOKEN));
        loginDataMap.put(Constants.FACEBOOK_ID, SharedPrefsUtils.getStringPreference(context, PREFS_FB_ID));
        return loginDataMap;
    }

    public static boolean getSignInStatus(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_USER_SIGNED_IN, false);
    }

    public static boolean getLoggedInStatus(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_HAS_LOGGED_IN, false);
    }

    public static boolean hasLoggedInOnce(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_HAS_LOGGED_IN_ONCE, false);
    }

    public static boolean getProfileUpdateStatus(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_HAS_UPDATED_PROFILE, false);
    }


    public static boolean isFirstUseOnDevice(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_IS_FIRST_USE_ON_DEVICE, true);
    }

    public static boolean isOnBoardingActivityShown(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_IS_ONBOARDING_ACTIVITY_SHOWN, false);
    }

    public static boolean isProfileOnBoarded(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_IS_PROFILE_ONBOARDED, false);
    }

    public static boolean isZakatifyOnBoarded(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_IS_ZAKATIFY_ONBOARDED, false);
    }

    public static boolean isPaymentsOnBoarded(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_IS_PAYMENT_ONBOARDED, false);
    }

    public static boolean isInterestsOnBoarded(Context context) {
        return SharedPrefsUtils.getBooleanPreference(context, PREFS_IS_INTERESTS_ONBOARDED, false);
    }

    public static double getGoldValue(Context context) {
        return Double.longBitsToDouble(SharedPrefsUtils.getLongPreference(context, PREFS_GOLD_VALUE, Double.doubleToLongBits(0.00)));
    }
}
