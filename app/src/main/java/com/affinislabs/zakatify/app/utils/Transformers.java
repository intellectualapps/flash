package com.affinislabs.zakatify.app.utils;

import com.greenfrvr.hashtagview.HashtagView;

public class Transformers {

    public static final HashtagView.DataTransform<String> CAPITALIZE_TAG = new HashtagView.DataTransform<String>() {
        @Override
        public CharSequence prepare(String item) {
            return capitalizeTag(item);
        }
    };

    public static String capitalizeTag(String tag) {
        return Character.toUpperCase(tag.charAt(0)) + tag.substring(1);
    }
}
