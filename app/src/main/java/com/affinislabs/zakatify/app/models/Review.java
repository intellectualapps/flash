package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Review implements Parcelable {
    private String username;
    private String firstName;
    private String lastName;
    private int charityId;
    private int rating;
    private int reviewId;
    private String comment;
    private String charity;
    private String photoUrl;
    private String createdOn;

    public Review(){

    }

    protected Review(Parcel in) {
        username = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        charityId = in.readInt();
        rating = in.readInt();
        reviewId = in.readInt();
        comment = in.readString();
        charity = in.readString();
        photoUrl = in.readString();
        createdOn = in.readString();
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFullName() {
        return (firstName + " " + lastName).trim();
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getCharityId() {
        return charityId;
    }

    public void setCharityId(int charityId) {
        this.charityId = charityId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCharity() {
        return charity;
    }

    public void setCharity(String charity) {
        this.charity = charity;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeInt(charityId);
        dest.writeInt(rating);
        dest.writeInt(reviewId);
        dest.writeString(comment);
        dest.writeString(charity);
        dest.writeString(photoUrl);
        dest.writeString(createdOn);
    }
}
