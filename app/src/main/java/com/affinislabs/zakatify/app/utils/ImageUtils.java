package com.affinislabs.zakatify.app.utils;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.affinislabs.zakatify.app.R;
import com.squareup.picasso.Picasso;

public class ImageUtils {

    public static String TAG = ImageUtils.class.getSimpleName();

    public static void loadImageUrl(final ImageView imageView, final Context context, String imagePath) {
        try {
            Picasso.with(context).load(imagePath).placeholder(R.color.colorGray).into(imageView);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void loadImageUri(final ImageView imageView, final Context context, Uri imagePath) {
        try {
            Picasso.with(context).load(imagePath).placeholder(R.color.colorGray).into(imageView);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void loadImageResource(final ImageView imageView, final Context context, int resourceId) {
        try {
            Picasso.with(context).load(resourceId).placeholder(R.color.colorGray).into(imageView);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
