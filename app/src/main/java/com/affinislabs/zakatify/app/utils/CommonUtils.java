package com.affinislabs.zakatify.app.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.affinislabs.zakatify.app.ZakatifyApplication;

public class CommonUtils {
    public static void displaySnackBarMessage(View mSnackBarAnchor, String message) {
        if (mSnackBarAnchor != null && mSnackBarAnchor.getContext() != null) {
            Snackbar.make(mSnackBarAnchor, message, Snackbar.LENGTH_LONG).show();
        }
    }

    public static void displayShortToastMessage(String message) {
        Context context = ZakatifyApplication.getAppInstance().getApplicationContext();
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    public static void displayLongToastMessage(String message) {
        Context context = ZakatifyApplication.getAppInstance().getApplicationContext();
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
}
