package com.affinislabs.zakatify.app.utils;

public class Constants {
    public static final String USER = "user";
    public static final String EMAIL_ADDRESS = "email";
    public static final String COMMENT = "comment";
    public static final String RATING = "rating";
    public static final String FACEBOOK_EMAIL = "facebook-email";
    public static final String TWITTER_EMAIL = "twitter-email";
    public static final String PLATFORM = "platform";
    public static final String FIRST_NAME = "first-name";
    public static final String LAST_NAME = "last-name";
    public static final String FULL_NAME = "full-name";
    public static final String FACEBOOK_ACCESS_TOKEN = "fb_token";
    public static final String FACEBOOK_ID = "fb_id";
    public static final String PHONE_NUMBER = "phone-number";
    public static final String USER_TYPE = "user-type";
    public static final String USER_EMAIL = "user-email";
    public static final String PASSWORD = "password";
    public static final String USERNAME = "username";
    public static final String MAIN_USERNAME = "main_username";
    public static final String ID = "id";
    public static final String AUTH_TYPE = "auth-type";
    public static final String EMAIL_AUTH_TYPE = "email";
    public static final String FACEBOOK_AUTH_TYPE = "facebook";
    public static final String TWITTER_AUTH_TYPE = "twitter";
    public static final String GENDER = "gender";
    public static final String PROFILE_PHOTO = "profile-photo";
    public static final String LOCATION = "location";
    public static final String CATEGORY_ID = "category-id";
    public static final String CATEGORY_IDS = "category-ids";
    public static final String PHOTO_URL = "photo-url";
    public static final String TEMP_KEY = "temp-key";
    public static final String AMOUNT = "amount";
    public static final String FREQUENCY_ID = "frequency-id";
    public static final String PERIOD = "period";
    public static final String CASH = "cash";
    public static final String GOLD = "gold";
    public static final String REAL_ESTATE = "real-estate";
    public static final String PAYMENT_OWED = "payment-owed";
    public static final String INVESTMENT = "investment";
    public static final String PERSONAL_USE = "personal-use";
    public static final String AUTH_TOKEN = "auth-token";
    public static final String ZAKAT_GOAL = "zakat-goal";
    public static final String CHARITY_REVIEW = "charity-review";
    public static final String CHARITY_ID = "charity-id";
    public static final String POSITION = "position";
    public static final String CHARITY_REVIEWS_LIST = "charity-reviews-list";
    public static final String USER_REVIEWS_LIST = "user-reviews-list";
    public static final String OFFLINE_USERNAME = "offline-username";
    public static final String ONLINE_USERNAME = "online-username";
    public static final String MESSAGE_NOTIFICATION_ID = "message-id";
    public static final String PAYPAL_ACCOUNT_URL = "paypal-account-url";
    public static final String PAYPAL_CALLBACK_CODE = "paypal-callback-code";
    public static final String URL = "url";
    public static final String PRIMARY_LABEL = "primary";

    /*
    Zakatify Web Links
     */

    public static final String ABOUT_LINK =  "https://www.zakatify.com/txt/about.html";
    public static final String PRIVACY_LINK = "https://www.zakatify.com/txt/privacy.html";
    public static final String TERMS_LINK = "https://www.zakatify.com/txt/terms.html";
    public static final String FAQ_LINK = "https://www.zakatify.com/txt/faq.html";


    /*
    Push Notification Keys
     */

    public static final String PUSH_OFFLINE_EMAIL = "offlineUserEmail";
    public static final String PUSH_ONLINE_EMAIL = "onlineUserEmail";
    public static final String PUSH_PHOTO_URL = "photoUrl";
    public static final String PUSH_SENDER_FULL_NAME = "senderFullName";
    public static final String PUSH_NOTIFICATION_PLATFORM = "notificationPlatform";
    public static final String PUSH_MESSAGE_ID = "messageId";
    public static final String CONNECTION_ID = "connection-id";


    /*
    Chat Related
     */
    public static final String CHAT_SESSIONS = "connections";
    public static final String CHAT_SESSION = "chat_session";
    public static final String CHAT_SESSION_MESSAGES_KEY = "messages";
    public static final String CHAT_SESSION_PARTICIPANTS_KEY = "participants";
    public static final String MESSAGE_ID = "id";
    public static final String REQUESTING_PARTICIPANT = "requesting-participant";
    public static final String OTHER_PARTICIPANT = "other-participant";
    public static final String MESSAGE_NOTIFICATION = "offline-user";
    public static final String HTML_LINE_BREAK = "<br/>";

    public static final String MESSAGE_ID_KEY = "id";
    public static final String MESSAGE_AUTHOR_KEY = "author";
    public static final String MESSAGE_TEXT_KEY = "text";
    public static final String MESSAGE_TIME_KEY = "time";


    /*
    Invite Related
     */
    public static final String INVITE_ACCEPTED = "ACCEPTED";
    public static final String INVITE_REJECTED = "REJECTED";
    public static final String INVITE_OPEN = "OPEN";
    public static final String PRE_INVITE = "NULL";
    public static final String INVITE_PENDING = "PENDING";




    public static final String REFRESH_CHARITY_LIST_FILTER = "com.affinislabs.zakatify.refreshcharitylist";
    public static final String REFRESH_TOP_ZAKATIFIERS_LIST_FILTER = "com.affinislabs.zakatify.topzakatifierslist";
    public static final String REFRESH_INVITE_FRIEND_LIST_FILTER = "com.affinislabs.zakatify.invitelist";
    public static final String REFRESH_FOLLOWERS_LIST_FILTER = "com.affinislabs.zakatify.followerslist";
    public static final String LOCAL_RECEIVER_LISTENER = "com.affinislabs.zakatify.local.receiver";
    public static final String CHAT_NOTIFICATION_POPUP_LISTENER = "com.affinislabs.zakatify.chat.notification";
    public static final String REFRESH_CHARITY_DETAILS_LISTENER = "com.affinislabs.zakatify.refresh.charity.details";
    public static final String LIABILITY = "liability";
    public static final String TOOLTIP_HELP = "tooltip";
    public static final String PROFILE_DATA = "profile-data";
    public static final String ONBOARDING_CONTENT = "onboarding-content";
    public static final String CHARITY = "charity";
    public static final String REFRESH_FLAG = "refresh";
    public static final String STATUS = "status";
    public static final String DOWNLOAD_COMPLETE = "complete";
    public static final String DOWNLOAD_FAILED = "failed";
    public static final String DOWNLOAD_URI = "download-uri";
    public static final String FILE_PATH = "file-path";
    public static final String TOP_ZAKATIFIER_MODE = "top-zakatifier-mode";


    public static final String VIEW_TYPE = "viewType";
    public static final String REGISTER_REQUEST = "REGISTER";
    public static final String CREATE_USERNAME_REQUEST = "CREATE_USERNAME";
    public static final String LOGIN_REQUEST = "LOGIN";
    public static final String VERIFY_USERNAME_REQUEST = "VERIFY_USERNAME";
    public static final String RESET_PASSWORD_REQUEST = "RESET_PASSWORD";
    public static final String UPDATE_PROFILE_REQUEST = "UPDATE_PROFILE";
    public static final String LINK_SOCIAL_NETWORK_REQUEST = "LINK_SOCIAL_NETWORK";
    public static final String UNLINK_SOCIAL_NETWORK_REQUEST = "UNLINK_SOCIAL_NETWORK";
    public static final String FETCH_CATEGORIES_REQUEST = "FETCH_CATEGORIES";
    public static final String FETCH_USER_CATEGORIES_REQUEST = "FETCH_USER_CATEGORIES";
    public static final String UPDATE_USER_CATEGORIES_REQUEST = "UPDATE_USER_CATEGORIES";
    public static final String ADD_ZAKAT_GOAL_REQUEST = "ADD_ZAKAT_GOAL";
    public static final String UPDATE_ZAKAT_GOAL_REQUEST = "UPDATE_ZAKAT_GOAL";
    public static final String DELETE_ZAKAT_GOAL_REQUEST = "DELETE_ZAKAT_GOAL";
    public static final String FETCH_ZAKAT_GOAL_REQUEST = "FETCH_ZAKAT_GOAL";
    public static final String FETCH_SUGGESTED_CHARITIES_REQUEST = "FETCH_SUGGESTED_CHARITIES";
    public static final String FETCH_GOLD_VALUE_REQUEST = "FETCH_GOLD_VALUE";
    public static final String FETCH_USER_PORTFOLIO_REQUEST = "FETCH_USER_PORTFOLIO";
    public static final String FETCH_USER_MENU_REQUEST = "FETCH_USER_MENU";
    public static final String FETCH_PAYMENT_OPTIONS_REQUEST = "FETCH_PAYMENT_OPTIONS";
    public static final String ADD_PAYMENT_OPTION_REQUEST = "ADD_PAYMENT_OPTION";
    public static final String UPDATE_PAYMENT_OPTION_REQUEST = "UPDATE_PAYMENT_OPTION";
    public static final String DELETE_PAYMENT_OPTION_REQUEST = "DELETE_PAYMENT_OPTION";
    public static final String FETCH_PAYPAL_ACCOUNTS_REQUEST = "FETCH_PAYPAL_ACCOUNTS";
    public static final String SET_PRIMARY_PAYMENT_OPTION_REQUEST = "SET_PRIMARY_PAYMENT_OPTION";
    public static final String FETCH_CURRENT_MONTH_TOP_ZAKATIFIERS_REQUEST = "FETCH_CURRENT_MONTH_TOP_ZAKATIFIERS";
    public static final String FETCH_LIFETIME_TOP_ZAKATIFIERS_REQUEST = "FETCH_LIFETIME_TOP_ZAKATIFIERS";
    public static final String SEARCH_CHARITIES_REQUEST = "SEARCH_CHARITIES";
    public static final String FETCH_CHARITY_DETAILS_REQUEST = "FETCH_CHARITY";
    public static final String ADD_USER_CHARITY_REQUEST = "ADD_CHARITY";
    public static final String REMOVE_USER_CHARITY_REQUEST = "REMOVE_CHARITY";
    public static final String FETCH_CHARITY_REVIEWS_REQUEST = "FETCH_CHARITY_REVIEWS";
    public static final String FETCH_USER_REVIEWS_REQUEST = "FETCH_USER_REVIEWS";
    public static final String FETCH_PUBLIC_USER_DETAILS_REQUEST = "FETCH_PUBLIC_USER_DETAILS";
    public static final String WRITE_CHARITY_REVIEW_REQUEST = "WRITE_CHARITY_REVIEW";
    public static final String FOLLOW_USER_REQUEST = "FOLLOW_USER";
    public static final String FETCH_FOLLOWERS_REQUEST = "FETCH_FOLLOWERS";
    public static final String UPLOAD_FCM_TOKEN_REQUEST = "UPLOAD_FCM_TOKEN";
    public static final String FETCH_USER_CHAT_SESSIONS_REQUEST = "FETCH_USER_CHAT_SESSIONS";
    public static final String FETCH_USER_INVITES_REQUEST = "FETCH_USER_INVITES";
    public static final String SEND_USER_INVITATION_REQUEST = "SEND_USER_INVITATION";
    public static final String INITIALIZE_CHAT_SESSION_REQUEST = "INITIALIZE_CHAT_SESSION";
    public static final String INITIATE_MANUAL_DONATION_REQUEST = "INITIATE_MANUAL_DONATION";
    public static final String DELETE_USER_CHAT_SESSION_REQUEST = "DELETE_USER_CHAT_SESSION";


    /*
    Cloud Messaging
     */
    public static final String NOTIFICATION_TYPE = "notification-type";
    public static final String NOTIFICATION_MESSAGE = "notification-message";
    public static final String NOTIFICATION_USER_PARAM = "user-param";
    public static final String MESSAGE = "message";
    public static final String TITLE = "title";
    public static final String NOTIFICATION_FLAG = "NOTIFICATION_FLAG";


    public static final String DEVICE_PLATFORM = "1";
    public static final String DEVICE_PLATFORM_KEY = "platform";
    public static final String FCM_TOKEN = "token";
    public static final String DEVICE_ID = "uuid";

    /*
    View Tags
     */
    public static final String WELCOME_VIEW_TAG = "WELCOME_VIEW";
    public static final String LOGIN_VIEW_TAG = "LOGIN_VIEW";
    public static final String SEARCH_VIEW_TAG = "SEARCH_VIEW";
    public static final String ABOUT_VIEW_TAG = "ABOUT_VIEW";
    public static final String TOP_ZAKATIFIERS_VIEW_TAG = "TOP_ZAKATIFIERS_VIEW";
    public static final String INVITE_FRIENDS_VIEW_TAG = "INVITE_FRIENDS_VIEW";
    public static final String MESSAGE_LIST_VIEW_TAG = "MESSAGE_LIST_VIEW";
    public static final String CHAT_SESSIONS_LIST_VIEW_TAG = "CHAT_SESSIONS_LIST_VIEW";
    public static final String AUTH_VIEWPAGER_TAG = "AUTH_VIEWPAGER_VIEW";
    public static final String PROFILE_VIEW_TAG = "PROFILE_VIEW";
    public static final String ZAKATIFY_PREFERENCES_VIEW_TAG = "ZAKATIFY_PREFERENCES_VIEW";
    public static final String PAYMENTS_OPTIONS_VIEW_TAG = "PAYMENT_OPTIONS_VIEW";
    public static final String ONBOARDING_PAYMENTS_VIEW_TAG = "ONBOARDING_PAYMENTS_VIEW";
    public static final String ONBOARDING_INTERESTS_VIEW_TAG = "ONBOARDING_INTERESTS_VIEW";
    public static final String ONBOARDING_PROFILE_VIEW_TAG = "ONBOARDING_PROFILE_VIEW";
    public static final String ONBOARDING_ZAKATIFY_PREFERENCES_VIEW_TAG = "ONBOARDING_ZAKATIFY_PREFERENCES_VIEW";
    public static final String CHARITY_DETAILS_VIEW_TAG = "CHARITY_DETAILS_VIEW";
    public static final String USER_PORTFOLIO_VIEW_TAG = "USER_PORTFOLIO_VIEW";
    public static final String FOLLOWERS_VIEW_TAG = "FOLLOWERS_VIEW";
    public static final String PUBLIC_USER_PROFILE_VIEW_TAG = "PUBLIC_USER_PROFILE_VIEW";
    public static final String NOTIFICATION_VIEW_TAG = "NOTIFICATION_VIEW";
    public static final String MANUAL_DONATION_VIEW_TAG = "MANUAL_DONATION_VIEW";

}