package com.affinislabs.zakatify.app.utils;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.Message;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class FirebaseUtils {
    private static final String TAG = FirebaseUtils.class.getSimpleName();
    private static FirebaseDatabase firebaseDatabase;
    private static DatabaseReference firebaseDatabaseReference;
    private static DatabaseReference chatSessionsDatabaseReference;
    private static DatabaseReference chatSessionMessagesDatabaseReference;
    private static DatabaseReference chatSessionsParticipantsDatabaseReference;

    public static void initializeFirebase() {
        if (firebaseDatabase == null) {
            firebaseDatabase = FirebaseDatabase.getInstance();
        }

        if (firebaseDatabaseReference == null) {
            firebaseDatabaseReference = firebaseDatabase.getReference();
        }

        if (chatSessionsDatabaseReference != null) {
            chatSessionsDatabaseReference = firebaseDatabaseReference.child(Constants.CHAT_SESSIONS);
            chatSessionsDatabaseReference.keepSynced(true);
        }
    }

    public static void resetReferences() {
        firebaseDatabase = null;
        firebaseDatabaseReference = null;
        chatSessionsParticipantsDatabaseReference = null;
        chatSessionMessagesDatabaseReference = null;
    }


    public static DatabaseReference getChatSessionsDatabaseReference() {
        if (chatSessionsDatabaseReference == null) {
            chatSessionsDatabaseReference = getFirebaseDatabaseReference().child(Constants.CHAT_SESSIONS);
            chatSessionsDatabaseReference.keepSynced(true);
        }

        return chatSessionsDatabaseReference;
    }

    public static DatabaseReference getChatSessionMessagesDatabaseReference(String messageId) {
        chatSessionMessagesDatabaseReference = getChatSessionsDatabaseReference().child(messageId).child(Constants.CHAT_SESSION_MESSAGES_KEY);
        chatSessionMessagesDatabaseReference.keepSynced(true);

        return chatSessionMessagesDatabaseReference;
    }

    public static DatabaseReference getChatSessionsParticipantsDatabaseReference(String messageId) {
        chatSessionMessagesDatabaseReference = getChatSessionsDatabaseReference().child(messageId).child(Constants.CHAT_SESSION_PARTICIPANTS_KEY);
        chatSessionMessagesDatabaseReference.keepSynced(true);

        return chatSessionMessagesDatabaseReference;
    }

    public static DatabaseReference getFirebaseDatabaseReference() {
        if (firebaseDatabaseReference == null) {
            firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
            firebaseDatabaseReference.keepSynced(true);
        }

        return firebaseDatabaseReference;
    }

    public static Message getMessageFromMap(Map<String, Object> messageMap) {
        Message message = new Message();
        if (messageMap != null && messageMap.size() > 0) {
            message.setId((String) messageMap.get(Constants.MESSAGE_ID_KEY));
            message.setAuthor((String) messageMap.get(Constants.MESSAGE_AUTHOR_KEY));
            message.setText((String) messageMap.get(Constants.MESSAGE_TEXT_KEY));
            message.setTime((Long) messageMap.get(Constants.MESSAGE_TIME_KEY));
        }
        return message;
    }

    private static void setupChatSession(final String messageId, String requestingParticipant, String otherParticipant, final ZakatifyInterfaces.SessionInitializationListener sessionInitializationListener) {
        DatabaseReference chatSessionDatabaseReference = getChatSessionsDatabaseReference().child(messageId);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(Constants.MESSAGE_ID, messageId);
        final Map<String, Object> participantDataMap = new HashMap<String, Object>();

        participantDataMap.put(requestingParticipant, true);
        participantDataMap.put(otherParticipant, false);

        map.put(Constants.CHAT_SESSION_PARTICIPANTS_KEY, participantDataMap);

        chatSessionDatabaseReference.setValue(map, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Log.w("Setup Chat Session", databaseReference.toString());
                    ChatSession chatSession = new ChatSession();
                    chatSession.setMessageId(messageId);
                    chatSession.setParticipantData(participantDataMap);
                    sessionInitializationListener.onSessionInitialized(chatSession);
                }
            }
        });
    }

    private static void updateChatParticipants(final String messageId, String requestingParticipant, String otherParticipant, final ZakatifyInterfaces.SessionInitializationListener sessionInitializationListener) {
        DatabaseReference chatSessionParticipantsDatabaseReference = getChatSessionsDatabaseReference().child(messageId).child(Constants.CHAT_SESSION_PARTICIPANTS_KEY);

        final Map<String, Object> participantDataMap = new HashMap<String, Object>();
        participantDataMap.put(requestingParticipant, true);
        participantDataMap.put(otherParticipant, false);

        chatSessionParticipantsDatabaseReference.setValue(participantDataMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Log.w("Update Participants", databaseReference.toString());
                    ChatSession chatSession = new ChatSession();
                    chatSession.setMessageId(messageId);
                    chatSession.setParticipantData(participantDataMap);
                    sessionInitializationListener.onSessionInitialized(chatSession);
                }
            }
        });
    }

    public static void deleteChatThread(final String messageId, final ZakatifyInterfaces.FirebaseChatDeletionCallback firebaseChatDeletionCallback) {
        DatabaseReference chatSessionDatabaseReference = getChatSessionsDatabaseReference().child(messageId);

        chatSessionDatabaseReference.removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                firebaseChatDeletionCallback.onThreadDeleted(databaseError);
            }
        });
    }

    public static void setupOrLoadChatSession(final String messageId, final String requestingParticipant, final String otherParticipant, final ZakatifyInterfaces.SessionInitializationListener sessionInitializationListener) {
        if (!TextUtils.isEmpty(messageId)) {
            final DatabaseReference connectionNodeReference = getChatSessionsDatabaseReference().child(messageId);
            connectionNodeReference.child(Constants.MESSAGE_ID).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {

                       /* if (!TextUtils.isEmpty(requestingParticipant) && !TextUtils.isEmpty(otherParticipant)) {
                            updateChatParticipants(messageId, requestingParticipant, otherParticipant, sessionInitializationListener);
                        } else {

                        }*/
                        connectionNodeReference.child(Constants.CHAT_SESSION_PARTICIPANTS_KEY).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                                    ChatSession chatSession = new ChatSession();
                                    chatSession.setMessageId(messageId);
                                    chatSession.setParticipantData((Map<String, Object>) dataSnapshot.getValue());
                                    sessionInitializationListener.onSessionInitialized(chatSession);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    } else {
                        if (!TextUtils.isEmpty(requestingParticipant) && !TextUtils.isEmpty(otherParticipant)) {
                            setupChatSession(messageId, requestingParticipant, otherParticipant, sessionInitializationListener);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
