package com.affinislabs.zakatify.app.models;

public abstract class ChatListObject {
    public static final int LOADING = 1;
    public static final int MESSAGE = 2;
    public static final int DATE = 3;

    abstract public int getType(int userId);
}
