package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Review;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.affinislabs.zakatify.app.utils.ZakatifyUtils;

import java.util.ArrayList;
import java.util.List;

public class CharityReviewsAdapter extends BaseRecyclerAdapter {
    public ClickListener getClickListener() {
        return clickListener;
    }

    private List<Review> reviews;
    private ClickListener clickListener;
    public CharityReviewsAdapter(Context context, ClickListener clickListener, ZakatifyInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        reviews = new ArrayList<Review>();
    }

    public void setItems(List<Review> items) {
        this.reviews = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (reviews != null) {
            this.reviews.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Review review) {
        if (review != null) {
            this.reviews.add(review);
            notifyDataSetChanged();
        }
    }

    public void add(Review review) {
        reviews.add(review);
        notifyItemInserted(reviews.size() - 1);
    }

    public void addAll(List<Review> reviews) {
        if (reviews != null && reviews.size() > 0) {
            for (Review review : reviews) {
                add(review);
            }
        }
        notifyDataSetChanged();
    }

    public void removeItem(Review review) {
        int position = reviews.indexOf(review);
        if (position > -1) {
            reviews.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(int position) {
        if (position > -1) {
            reviews.remove(position);
            notifyItemRemoved(position);
        }
    }

    public List<Review> getReviews() {
        return reviews;
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(reviews) ? 0 : reviews.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == reviews.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return REVIEW_ITEM;
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView reviewAuthor, reviewDate, reviewComment;
        ImageView reviewAuthorPhoto;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            reviewAuthor = (TextView) itemView.findViewById(R.id.review_author);
            reviewAuthorPhoto = (ImageView) itemView.findViewById(R.id.review_author_photo);
            reviewDate = (TextView) itemView.findViewById(R.id.review_date);
            reviewComment = (TextView) itemView.findViewById(R.id.review_comment);
            itemView.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        switch (viewType) {
            default:
            case REVIEW_ITEM:
                view = mInflater.inflate(R.layout.charity_review_item_layout, parent, false);
                viewHolder = new CharityReviewsAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            case REVIEW_ITEM:
                final CharityReviewsAdapter.ViewHolder viewHolder = (CharityReviewsAdapter.ViewHolder) holder;
                Review review = reviews.get(position);
                if (review != null) {
                    viewHolder.reviewAuthor.setText(review.getFullName());
                    viewHolder.reviewDate.setText(ZakatifyUtils.timeSinceReview(review.getCreatedOn()));
                    viewHolder.reviewComment.setText(review.getComment());
                    if (review.getPhotoUrl() != null) {
                        ImageUtils.loadImageUrl(viewHolder.reviewAuthorPhoto, context, review.getPhotoUrl());
                    }
                }
                break;
        }
    }
}