package com.affinislabs.zakatify.app.ui.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;

public class LaunchActivity extends BaseActivity {
    private static final int LAUNCH_DURATION = 100;
    private static final String TAG = LaunchActivity.class.getSimpleName();
    Animation animation;
    private ImageView splashIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_launch);

        splashIcon = (ImageView) findViewById(R.id.app_icon);

        animation = AnimationUtils.loadAnimation(this, R.anim.image_splash_translation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new LaunchTask().execute();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        splashIcon.startAnimation(animation);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class LaunchTask extends AsyncTask {
        Intent intent;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                Thread.sleep(LAUNCH_DURATION);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            User user = PreferenceStorageManager.getUser(ZakatifyApplication.getAppInstance().getApplicationContext());
            Boolean signedInFlag = PreferenceStorageManager.getSignInStatus(getApplicationContext());

            if (signedInFlag && user != null && user.getAuthToken() != null && user.getAuthToken().trim().length() > 0) {
                showMainActivity(user);
            } else {
                intent = new Intent(LaunchActivity.this, TempActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }
    }
}
