package com.affinislabs.zakatify.app.ui.dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.utils.AppUtils;
import com.affinislabs.zakatify.app.utils.ToastUtil;
import com.affinislabs.zakatify.app.utils.Validator;


public class BaseDialogFragment extends DialogFragment {
    private ProgressDialog progressDialog;
    public static String ARG_ARGS = "args";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);

    }

    public boolean validateFields(EditText[] views) {
        return Validator.validateInputViewsNotEmpty(views);
    }

    public void showLoadingIndicator(String message) {

        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
            }

            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        } catch (Exception ignored) {
        }
    }

    public void hideLoadingIndicator() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
        } catch (Exception ignored) {
        }
    }

    public void showKeyboard() {
        if (getActivity() != null) {
            View view = getActivity().getCurrentFocus();
            if (view != null && view instanceof EditText) {
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(view, 0);
            }
        }
    }

    public void hideKeyboard() {
        AppUtils.hideKeyboard(getActivity());
    }

    public void closeFragment() {
        if (getActivity() != null) {
            hideKeyboard();
            dismiss();
        }
    }

    public void showSuccessPopup(Object message) {
        ToastUtil.showToast((String) message, ToastUtil.SUCCESS);
    }

    public void showErrorPopup(Object message) {
        ToastUtil.showToast((String) message, ToastUtil.ERROR);
    }
}
