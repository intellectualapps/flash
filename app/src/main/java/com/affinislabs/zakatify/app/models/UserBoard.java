package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserBoard implements Parcelable {
    @SerializedName("rank")
    @Expose
    private int rank;
    @SerializedName("points")
    @Expose
    private int points;

    public UserBoard() {

    }

    private UserBoard(Parcel in) {
        rank = in.readInt();
        points = in.readInt();
    }

    public static final Creator<UserBoard> CREATOR = new Creator<UserBoard>() {
        @Override
        public UserBoard createFromParcel(Parcel in) {
            return new UserBoard(in);
        }

        @Override
        public UserBoard[] newArray(int size) {
            return new UserBoard[size];
        }
    };

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(rank);
        dest.writeInt(points);
    }
}
