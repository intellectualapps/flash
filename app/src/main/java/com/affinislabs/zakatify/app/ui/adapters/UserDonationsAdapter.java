package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.Donation;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;

import java.util.List;

public class UserDonationsAdapter extends BaseRecyclerAdapter {

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView charityName, donationSummary;
        ImageView charityLogo;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            charityName = (TextView) itemView.findViewById(R.id.charity_name);
            charityLogo = (ImageView) itemView.findViewById(R.id.charity_photo);
            donationSummary = (TextView) itemView.findViewById(R.id.donation_summary);
            itemView.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }

    private List<Donation> donationList;
    private ClickListener clickListener;

    public UserDonationsAdapter(Context context, ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    public void setItems(List<Donation> items) {
        this.donationList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (donationList != null) {
            this.donationList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Donation donation) {
        if (donation != null) {
            this.donationList.add(donation);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.charity_review_item_layout, parent, false);

        return new UserDonationsAdapter.ViewHolder(view, this.clickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final UserDonationsAdapter.ViewHolder viewHolder = (UserDonationsAdapter.ViewHolder) holder;
        Donation donation = donationList.get(position);
        if (donation != null) {
            viewHolder.charityName.setText(donation.getCharityName());
            viewHolder.donationSummary.setText(donation.getDonationSummary());
            if (donation.getPhotoUrl() != null) {
                ImageUtils.loadImageUrl(viewHolder.charityLogo, context, donation.getPhotoUrl());
            }
        }
    }


    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(donationList) ? 0 : donationList.size();
    }
}