package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class OnBoardingContent implements Parcelable {
    private String heading;
    private String description;
    private int iconResource;

    public OnBoardingContent(String heading, String description, int iconResource) {
        this.heading = heading;
        this.description = description;
        this.iconResource = iconResource;
    }

    private OnBoardingContent(Parcel in) {
        heading = in.readString();
        description = in.readString();
        iconResource = in.readInt();
    }

    public static final Creator<OnBoardingContent> CREATOR = new Creator<OnBoardingContent>() {
        @Override
        public OnBoardingContent createFromParcel(Parcel in) {
            return new OnBoardingContent(in);
        }

        @Override
        public OnBoardingContent[] newArray(int size) {
            return new OnBoardingContent[size];
        }
    };

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIconResource() {
        return iconResource;
    }

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(heading);
        dest.writeString(description);
        dest.writeInt(iconResource);
    }
}
