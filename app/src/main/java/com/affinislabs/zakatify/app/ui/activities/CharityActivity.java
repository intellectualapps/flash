package com.affinislabs.zakatify.app.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.dialogs.MediaPickerDialog;
import com.affinislabs.zakatify.app.ui.fragments.AuthFragment;
import com.affinislabs.zakatify.app.ui.fragments.CharityDetailsFragment;
import com.affinislabs.zakatify.app.ui.fragments.MediaPickerBaseFragment;
import com.affinislabs.zakatify.app.utils.Constants;

public class CharityActivity extends BaseActivity {
    private Bundle fragmentBundle;
    private String viewType;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setElevation(0);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.CHARITY_DETAILS_VIEW_TAG:
                        frag = CharityDetailsFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = CharityDetailsFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = CharityDetailsFragment.newInstance(fragmentBundle);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(AuthFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
