package com.affinislabs.zakatify.app.ui.fragments;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.InvitationsResponse;
import com.affinislabs.zakatify.app.enums.InviteState;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Invite;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.InviteFriendsAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ContactsLoaderTask;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.parceler.apache.commons.collections.MapUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class InviteFriendsFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, View.OnClickListener, ZakatifyInterfaces.SocialAuthenticationListener, ZakatifyInterfaces.ContactLoaderListener, ZakatifyInterfaces.ContactStateChangedListener {
    private CustomRecyclerView friendsListRecyclerview;
    private List<Invite> inviteList = new ArrayList<Invite>();
    private List<Invite> sentInviteList = new ArrayList<Invite>();
    private InviteFriendsAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private User user;
    private String mainUsername;
    private View emptyView;
    private TextView emptyViewLabel;
    private TextView retryButton;
    private boolean loadContactsFlag = false, invitesInCache = false;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new InviteFriendsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public InviteFriendsFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
                mainUsername = user != null ? user.getUsername() : null;
            }
        }
        inviteList = PreferenceStorageManager.getInvites(getZakatifyApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invite_friends, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (loadContactsFlag) {
            fetchUserInvitations(mainUsername, true);
            loadContactsFlag = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.invite_friends_label));

        adapter = new InviteFriendsAdapter(getContext(), this, user.getUsername(), InviteFriendsFragment.this);
        emptyView = view.findViewById(R.id.empty_view);
        emptyViewLabel = view.findViewById(R.id.empty_view_label);
        retryButton = view.findViewById(R.id.retry_button);
        friendsListRecyclerview = (CustomRecyclerView) view.findViewById(R.id.friends_list_recyclerview);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        emptyViewLabel.setText(R.string.load_contacts_prompt);
        retryButton.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        friendsListRecyclerview.setAdapter(adapter);
        friendsListRecyclerview.setLayoutManager(linearLayoutManager);
        friendsListRecyclerview.setEmptyView(emptyView);
        friendsListRecyclerview.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blue),
                ContextCompat.getColor(getContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchUserInvitations(mainUsername, ListUtils.isEmpty(inviteList));
                    } else {
                        endSwipeRefresh(swipeRefreshLayout);
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        invitesInCache = ListUtils.isEmpty(inviteList);

        if (ListUtils.isEmpty(inviteList)) {
            if (checkContactAccessPermission(getActivity())) {
                fetchUserInvitations(mainUsername, true);
            }
        } else {
            loadAdapter(inviteList);
            if (checkContactAccessPermission(getActivity())) {
                fetchUserInvitations(mainUsername, false);
                new ContactsLoaderTask(getZakatifyApplicationContext(), mainUsername, this).execute();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CONTACT_PERMISSION_REQUEST_CODE:
                loadContactsFlag = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
    }

    private void getUserFriends(String fbId) {
        showLoadingIndicator("Loading friend list");
        String graphPath = "/{user-id}/taggable_friends".replace("{user-id}", fbId);
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                graphPath,
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        hideLoadingIndicator();
                        Log.w("Graph Response", "Data: " + response.toString());
                    }
                }
        ).executeAsync();
    }

    private void getUserDetails(String fbId) {
        showLoadingIndicator("Loading friend list");
        String graphPath = "/me/photos".replace("{user-id}", fbId);
        Log.w("Path", graphPath);
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                graphPath,
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        hideLoadingIndicator();
                        Log.w("Graph Response", "Data: " + response.toString());
                        Log.w("Size", response.getRawResponse().toString());
                    }
                }
        ).executeAsync();
    }

    private void fetchUserInvitations(String username, final boolean shouldLoadContacts) {
        if (checkContactAccessPermission(getActivity())) {
            animateSwipeRefreshLayout(swipeRefreshLayout);
            showLoadingIndicator(invitesInCache ? getString(R.string.refreshing_invite_list_label) : getString(R.string.fetching_invite_list_label));
            new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_INVITES_REQUEST, username, new ApiClientListener.FetchUserInvitationsListener() {
                @Override
                public void onInvitationsFetched(InvitationsResponse invitationsResponse) {
                    if (invitationsResponse != null) {
                        if (invitationsResponse.getStatus() == null && invitationsResponse.getMessage() == null) {
                            sentInviteList = invitationsResponse.getInvites();
                            if (shouldLoadContacts) {
                                loadBackgroundContactsWorker(sentInviteList);
                            } else {
                                inviteList = reconcileInviteList(inviteList, sentInviteList);
                                PreferenceStorageManager.saveInvites(getZakatifyApplicationContext(), inviteList);
                                hideLoadingIndicator();
                                endSwipeRefresh(swipeRefreshLayout);
                                loadAdapter(inviteList);
                            }
                        } else {
                            hideLoadingIndicator();
                            endSwipeRefresh(swipeRefreshLayout);
                            showErrorPopup(invitationsResponse.getMessage());
                        }
                    } else {
                        hideLoadingIndicator();
                        endSwipeRefresh(swipeRefreshLayout);
                        showErrorPopup(getString(R.string.null_response_label));
                    }
                }
            }).execute();
        }
    }

    private void loadAdapter(List<Invite> inviteList) {
        adapter.setItems(inviteList);
    }

    private void loadBackgroundContactsWorker(List<Invite> invites) {
        this.sentInviteList = invites;
        new ContactsLoaderTask(getContext(), mainUsername, InviteFriendsFragment.this).execute();
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1 && inviteList != null && inviteList.size() > 0) {
            switch (v.getId()) {
                case R.id.invite_button:
                    Log.w("Invite Button", "Clicked");
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retry_button:
                fetchUserInvitations(mainUsername, true);
                break;
        }
    }

    @Override
    public void onAuthenticationCompleted(Map<String, String> accountData, Map<String, String> profileData) {
        if (!MapUtils.isEmpty(profileData)) {
            hideLoadingIndicator();
            getUserFriends(profileData.get(Constants.FACEBOOK_ID));
        }
    }

    @Override
    public void onContactsLoaded(List<Invite> inviteList) {
        inviteList = reconcileInviteList(inviteList, sentInviteList);
        PreferenceStorageManager.saveInvites(getZakatifyApplicationContext(), inviteList);
        if (isAdded() && isVisible()) {
            hideLoadingIndicator();
            endSwipeRefresh(swipeRefreshLayout);
            loadAdapter(inviteList);
        }
    }

    private List<Invite> reconcileInviteList(List<Invite> inviteList, List<Invite> sentInviteList) {
        for (Invite invite : sentInviteList) {
            if (inviteList.contains(invite)) {
                int index = inviteList.indexOf(invite);
                Invite inviteParam = inviteList.get(index);
                inviteParam.setState(invite.getState());
                inviteParam.setDateInvited(invite.getDateInvited());
                inviteParam.setId(invite.getId());
                inviteList.set(index, inviteParam);
            }
        }

        Collections.sort(inviteList, Invite.COMPARE_BY_NAME);

        return segmentInviteList(inviteList);
    }

    private List<Invite> segmentInviteList(List<Invite> inviteList) {
        List<Invite> sentInvites = new ArrayList<Invite>();
        List<Invite> restOfInvites = new ArrayList<Invite>(inviteList);
        if(ListUtils.isNotEmpty(inviteList)) {
            restOfInvites.clear();
            for (Invite invite : inviteList) {
                if (invite.getState().equalsIgnoreCase(Constants.PRE_INVITE)) {
                    restOfInvites.add(invite);
                } else {
                    sentInvites.add(invite);
                }
            }
        }
        restOfInvites.addAll(0, sentInvites);
        return restOfInvites;
    }

    @Override
    public void onInviteStateChanged(int position, InviteState inviteState) {

    }
}
