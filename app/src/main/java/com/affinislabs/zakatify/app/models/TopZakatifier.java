package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopZakatifier implements Parcelable {
    @SerializedName("donationProgress")
    @Expose
    private DonationProgress donationProgress;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("isFollowing")
    @Expose
    private FollowState followState;

    private TopZakatifier(Parcel in) {
        donationProgress = in.readParcelable(DonationProgress.class.getClassLoader());
        user = in.readParcelable(User.class.getClassLoader());
        followState = in.readParcelable(FollowState.class.getClassLoader());
    }

    public static final Creator<TopZakatifier> CREATOR = new Creator<TopZakatifier>() {
        @Override
        public TopZakatifier createFromParcel(Parcel in) {
            return new TopZakatifier(in);
        }

        @Override
        public TopZakatifier[] newArray(int size) {
            return new TopZakatifier[size];
        }
    };

    public DonationProgress getDonationProgress() {
        return donationProgress;
    }

    public void setDonationProgress(DonationProgress donationProgress) {
        this.donationProgress = donationProgress;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public FollowState getFollowState() {
        return followState;
    }

    public void setFollowState(FollowState followState) {
        this.followState = followState;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(donationProgress, flags);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(followState, flags);
    }
}
