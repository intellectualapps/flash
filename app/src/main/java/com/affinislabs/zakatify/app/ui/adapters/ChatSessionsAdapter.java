package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.customviews.SwipeRevealLayout;
import com.affinislabs.zakatify.app.ui.customviews.ViewBinderHelper;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class ChatSessionsAdapter extends BaseRecyclerAdapter implements ZakatifyInterfaces.ThreadDeletionCloseCallback {
    private List<ChatSession> chatSessions;
    private ClickListener clickListener;
    private final int CHAT_SESSION_ITEM = 2;
    private User user;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    private ZakatifyInterfaces.ThreadDeletionCloseCallback threadDeletionCloseCallback;

    public ChatSessionsAdapter(Context context, ClickListener clickListener, User user, ZakatifyInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.user = user;
        this.callback = paginationAdapterCallback;
        this.threadDeletionCloseCallback = ChatSessionsAdapter.this;
        this.clickListener = clickListener;
        chatSessions = new ArrayList<ChatSession>();
        binderHelper.setOpenOnlyOne(true);
    }

    public void setItems(List<ChatSession> items) {
        this.chatSessions = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (chatSessions != null) {
            this.chatSessions.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(ChatSession chatSession) {
        if (chatSession != null) {
            this.chatSessions.add(chatSession);
            notifyDataSetChanged();
        }
    }

    public void add(ChatSession chatSession) {
        chatSessions.add(chatSession);
        notifyItemInserted(chatSessions.size() - 1);
    }

    public void addAll(List<ChatSession> chatSessions) {
        if (chatSessions != null && chatSessions.size() > 0) {
            for (ChatSession chatSession : chatSessions) {
                add(chatSession);
            }
        }
        notifyDataSetChanged();
    }

    public void removeItem(ChatSession chatSession) {
        int position = chatSessions.indexOf(chatSession);
        if (position > -1) {
            chatSessions.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(int position) {
        if (position > -1) {
            chatSessions.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void saveStates(Bundle outState) {
        binderHelper.saveStates(outState);
    }

    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == chatSessions.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return CHAT_SESSION_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(chatSessions) ? 0 : chatSessions.size();
    }

    public List<ChatSession> getChatSessions() {
        return chatSessions;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        switch (viewType) {
            default:
            case CHAT_SESSION_ITEM:
                view = mInflater.inflate(R.layout.chat_session_item_layout, parent, false);
                viewHolder = new ChatSessionsAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            case CHAT_SESSION_ITEM:
                final ChatSessionsAdapter.ViewHolder viewHolder = (ChatSessionsAdapter.ViewHolder) holder;
                ChatSession chatSession = chatSessions.get(position);
                if (chatSession != null) {
                    binderHelper.bind(viewHolder.swipeLayout, chatSession.getMessageId());
                    viewHolder.author.setText(chatSession.getOtherParticipant().getFullName());
                    if (chatSession.getOtherParticipant() != null && !TextUtils.isEmpty(chatSession.getOtherParticipant().getPhotoUrl())) {
                        ImageUtils.loadImageUrl(viewHolder.authorPhoto, context, chatSession.getOtherParticipant().getPhotoUrl());
                    } else {
                        ImageUtils.loadImageResource(viewHolder.authorPhoto, context, R.drawable.ic_user);
                    }
                }
                break;
        }
    }

    @Override
    public void onThreadDeletionCompleted(String id, int position) {
        binderHelper.closeLayout(id);
        remove(position);
    }

    @Override
    public void onThreadDeletionDismissed(String id) {
        binderHelper.closeLayout(id);
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView author, dateReceived, messageContent;
        private ImageView authorPhoto;
        private SwipeRevealLayout swipeLayout;
        private View mainLayout, subLayout;
        private ImageView deleteThreadIcon;
        private ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            author = itemView.findViewById(R.id.author);
            authorPhoto = itemView.findViewById(R.id.author_photo);
            dateReceived = itemView.findViewById(R.id.date_received);
            messageContent = itemView.findViewById(R.id.message_content);
            swipeLayout = itemView.findViewById(R.id.swipe_view_layout);
            mainLayout = itemView.findViewById(R.id.main_layout);
            subLayout = itemView.findViewById(R.id.sub_layout);
            deleteThreadIcon = itemView.findViewById(R.id.delete_thread);
            itemView.setOnClickListener(this);
            deleteThreadIcon.setOnClickListener(this);
            mainLayout.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }
}