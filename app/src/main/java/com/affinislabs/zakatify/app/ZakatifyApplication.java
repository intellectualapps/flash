package com.affinislabs.zakatify.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.affinislabs.zakatify.app.ui.activities.LaunchActivity;
import com.affinislabs.zakatify.app.utils.AppLifecycleHandler;
import com.affinislabs.zakatify.app.utils.DeviceUuidFactory;
import com.affinislabs.zakatify.app.utils.FirebaseUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.cloudinary.Cloudinary;
import com.cloudinary.android.MediaManager;
import com.google.firebase.database.FirebaseDatabase;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class ZakatifyApplication extends MultiDexApplication {
    static File cacheFile;
    private static ZakatifyApplication zakatifyApplication;
    private static Cloudinary cloudinary;

    public static ZakatifyApplication get(Context context) {
        return (ZakatifyApplication) context.getApplicationContext();
    }

    public static Resources getApplicationResources() {
        return getAppInstance().getResources();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        cacheFile = new File(getCacheDir(), "responses");
        zakatifyApplication = this;

        DeviceUuidFactory deviceIdFactory = new DeviceUuidFactory(this);
        PreferenceStorageManager.saveDeviceId(getApplicationContext(), deviceIdFactory.getStringUUID());
        initializeTwitter();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseUtils.initializeFirebase();
        registerActivityLifecycleCallbacks(new AppLifecycleHandler());
        MediaManager.init(this, getCloudinaryConfig());
        initUncaughtExceptionHandler();
    }

    public static Map getCloudinaryConfig() {
        Map config = new HashMap();
        config.put("cloud_name", BuildConfig.CLOUDINARY_NAME);
        config.put("api_key", BuildConfig.CLOUDINARY_API_KEY);
        config.put("api_secret", BuildConfig.CLOUDINARY_API_SECRET);
        return config;
    }

    private void initializeTwitter() {
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(BuildConfig.TWITTER_API_KEY, BuildConfig.TWITTER_API_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);
    }

    public static Cloudinary getCloudinaryInstance() {
        if (cloudinary == null) {
            Map config = new HashMap();
            config.put("cloud_name", BuildConfig.CLOUDINARY_NAME);
            config.put("api_key", BuildConfig.CLOUDINARY_API_KEY);
            config.put("api_secret", BuildConfig.CLOUDINARY_API_SECRET);
            cloudinary = new Cloudinary(config);
        }

        return cloudinary;
    }

    public static File getCacheFile() {
        return cacheFile;
    }

    public static ZakatifyApplication getAppInstance() {
        return zakatifyApplication;
    }

    private void initUncaughtExceptionHandler() {
        final ScheduledThreadPoolExecutor c = new ScheduledThreadPoolExecutor(1);
        c.schedule(new Runnable() {
            @Override
            public void run() {
                final Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
                Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                        Intent intent = new Intent(getApplicationContext(), LaunchActivity.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        PendingIntent pendingIntent = PendingIntent.getActivity(getAppInstance().getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);

                        AlarmManager mgr = (AlarmManager) getAppInstance().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, pendingIntent);

                        defaultHandler.uncaughtException(paramThread, paramThrowable);
                    }
                });
            }
        }, 5, TimeUnit.SECONDS);
    }

}
