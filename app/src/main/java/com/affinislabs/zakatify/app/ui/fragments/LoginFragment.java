package com.affinislabs.zakatify.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.ApiModule;
import com.affinislabs.zakatify.app.api.responses.LoginResponse;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;

import java.util.HashMap;
import java.util.Map;

public class LoginFragment extends BaseFragment implements View.OnClickListener {
    private User user;
    private TextView resetPasswordButton;
    private EditText usernameInput, passwordInput;
    private Button loginButton;
    private Toolbar toolbar;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new LoginFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public LoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        resetPasswordButton = (TextView) view.findViewById(R.id.forgot_password_button);
        usernameInput = (EditText) view.findViewById(R.id.username);
        passwordInput = (EditText) view.findViewById(R.id.password);
        loginButton = (Button) view.findViewById(R.id.login_button);

        resetPasswordButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        loadSavedLoginData();
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.login_progress_label);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.login_button:
                if (validateFields(new EditText[]{usernameInput, passwordInput})) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        PreferenceStorageManager.setLoginData(getContext().getApplicationContext(), getUserInput().get(Constants.ID), getUserInput().get(Constants.PASSWORD));
                        makeAPICall(getUserInput());
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.forgot_password_button:
                startFragment(ForgotPasswordFragment.newInstance(bundle));
                break;
        }
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String password = passwordInput.getText().toString().trim();
        String username = usernameInput.getText().toString().trim();
        map.put(Constants.PASSWORD, password);
        map.put(Constants.ID, username);
        map.put(Constants.AUTH_TYPE, Constants.EMAIL_AUTH_TYPE);
        return map;
    }

    private void loadSavedLoginData() {
        Map<String, String> loginData = new HashMap<String, String>();
        loginData = PreferenceStorageManager.getLoginData(getContext().getApplicationContext());
        if (loginData != null && loginData.size() > 0) {
            usernameInput.setText(loginData.get(Constants.ID));
            passwordInput.setText(loginData.get(Constants.PASSWORD));
        }
    }

    private void makeAPICall(Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(Constants.LOGIN_REQUEST, accountMap, new ApiClientListener.AccountAuthenticationListener() {
            @Override
            public void onAccountAuthenticated(LoginResponse loginResponse) {
                hideLoadingIndicator();
                if (loginResponse != null) {
                    PreferenceStorageManager.setHasLoggedInOnce(ZakatifyApplication.getAppInstance().getApplicationContext(), true);
                    if (loginResponse.getStatus() == null && loginResponse.getMessage() == null) {
                        User user = extractUserDetails(loginResponse);

                        ApiClient.NetworkCallsRunner.resetApiService();
                        ApiModule.resetApiClient();
                        saveUserData(ZakatifyApplication.getAppInstance().getApplicationContext(), user);
                        showMainActivity(user, getActivity());
                    } else {
                        if (isAdded() && getActivity() != null) {
                            showErrorPopup(loginResponse.getMessage());
                        }
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }
}
