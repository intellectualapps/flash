package com.affinislabs.zakatify.app.api.responses;

public class BooleanStateResponse extends DefaultResponse {
    private boolean state;

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

}
