package com.affinislabs.zakatify.app.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.FollowersResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.ProfileActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.UsersAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;


public class FollowersFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, ZakatifyInterfaces.PaginationAdapterCallback {
    private CustomRecyclerView followersRecyclerview;
    private List<User> followers = new ArrayList<User>();
    private UsersAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private User user;
    private int pageNumber = 1;
    private int currentPage = pageNumber;
    private String username, mainUsername;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new FollowersFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public FollowersFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USERNAME)) {
                username = getArguments().getString(Constants.USERNAME);
            }

            if (getArguments().containsKey(Constants.MAIN_USERNAME)) {
                mainUsername = getArguments().getString(Constants.MAIN_USERNAME);
            }
        }
        registerReceiver(getContext(), refreshListReceiver, new IntentFilter(Constants.REFRESH_FOLLOWERS_LIST_FILTER));
        isLoading = false;
        hasListEnded = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_followers, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(getContext(), refreshListReceiver);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.followers_label));

        adapter = new UsersAdapter(getContext(), this, mainUsername, this);
        followersRecyclerview = view.findViewById(R.id.followers_recyclerview);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blue),
                ContextCompat.getColor(getContext(), R.color.colorGold)
        );

        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);
        mRetryButton = mEmptyView.findViewById(R.id.retry_button);
        mEmptyViewLabel.setText(getString(R.string.no_records_label));

        followersRecyclerview.setEmptyView(mEmptyView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchFollowers(username, 1, pageSize, false);
                    } else {
                        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        followersRecyclerview.setAdapter(adapter);
        followersRecyclerview.setLayoutManager(linearLayoutManager);
        followersRecyclerview.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        followersRecyclerview.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        if (followers != null && followers.size() >= pageSize) {
                            fetchFollowers(username, currentPage, pageSize, true);
                        }
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        initializeData();
    }

    private void initializeData() {
        animateSwipeRefreshLayout(swipeRefreshLayout);
        showLoadingIndicator(getString(R.string.followers_loading_message));
        fetchFollowers(username, 1, pageSize, false);
    }

    private void loadAdapter(List<User> followers) {
        if (isAdded() && isVisible()) {
            adapter.addAll(followers);
        }
    }

    private void updateFollowerList(List<User> followerListParam) {
        if (followers == null) {
            followers = new ArrayList<User>();
        }
        if (followerListParam != null && followerListParam.size() > 0) {
            followers.addAll(followerListParam);
        }
    }

    private void fetchFollowers(String username, int pageNumber, final int pageSize, final boolean inLoadMoreMode) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_FOLLOWERS_REQUEST, username, String.valueOf(pageNumber), String.valueOf(pageSize), new ApiClientListener.FetchFollowersListener() {
            @Override
            public void onFollowersFetched(FollowersResponse followersResponse) {
                hideLoadingIndicator();
                endSwipeRefresh(swipeRefreshLayout);
                isLoading = false;

                if (inLoadMoreMode) {
                    adapter.removeLoadingFooter(UsersAdapter.class, adapter);
                }

                if (followersResponse != null) {
                    if (followersResponse.getStatus() == null && followersResponse.getMessage() == null) {
                        List<User> followerListParam = followersResponse.getFollowers();

                        if (followerListParam == null || followerListParam.size() == 0 || followerListParam.size() < pageSize) {
                            hasListEnded = true;
                        }

                        if (!inLoadMoreMode) {
                            if (followers != null) {
                                followers.clear();
                            }
                            adapter.clearItems();
                        }

                        updateFollowerList(followerListParam);

                        loadAdapter(followerListParam);

                        if (!hasListEnded) {
                            adapter.addLoadingFooter(UsersAdapter.class, adapter);
                        }
                    } else {
                        showErrorPopup(followersResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1 && followers != null && followers.size() > 0) {
            Intent intent = new Intent(getContext(), ProfileActivity.class);
            intent.putExtra(Constants.USERNAME, followers.get(position).getUsername());
            intent.putExtra(Constants.MAIN_USERNAME, mainUsername);
            intent.putExtra(Constants.VIEW_TYPE, Constants.PUBLIC_USER_PROFILE_VIEW_TAG);
            intent.putExtra(Constants.USER, user);
            startActivity(intent);
        }
    }

    private BroadcastReceiver refreshListReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getIntExtra(Constants.POSITION, -1);
            if (position > -1) {
                //adapter.remove(position);
            }
        }
    };

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getContext())) {
            fetchFollowers(username, currentPage, pageSize, true);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }
}
