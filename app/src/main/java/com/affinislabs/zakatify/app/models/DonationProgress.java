package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DonationProgress implements Parcelable {
    @SerializedName("zakatGoal")
    @Expose
    private double zakatGoal;
    @SerializedName("totalDonation")
    @Expose
    private double totalDonation;
    @SerializedName("percentageCompleted")
    @Expose
    private double percentageCompleted;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("points")
    @Expose
    private Integer points;

    protected DonationProgress(Parcel in) {
        zakatGoal = in.readDouble();
        totalDonation = in.readDouble();
        percentageCompleted = in.readDouble();
        username = in.readString();
        points = in.readInt();
    }

    public static final Creator<DonationProgress> CREATOR = new Creator<DonationProgress>() {
        @Override
        public DonationProgress createFromParcel(Parcel in) {
            return new DonationProgress(in);
        }

        @Override
        public DonationProgress[] newArray(int size) {
            return new DonationProgress[size];
        }
    };

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public DonationProgress() {

    }

    public double getZakatGoal() {
        return zakatGoal;
    }

    public void setZakatGoal(double zakatGoal) {
        this.zakatGoal = zakatGoal;
    }

    public double getTotalDonation() {
        return totalDonation;
    }

    public void setTotalDonation(double totalDonation) {
        this.totalDonation = totalDonation;
    }

    public double getPercentageCompleted() {
        return percentageCompleted;
    }

    public void setPercentageCompleted(double percentageCompleted) {
        this.percentageCompleted = percentageCompleted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(zakatGoal);
        dest.writeDouble(totalDonation);
        dest.writeDouble(percentageCompleted);
        dest.writeString(username);
        dest.writeInt(points);
    }
}
