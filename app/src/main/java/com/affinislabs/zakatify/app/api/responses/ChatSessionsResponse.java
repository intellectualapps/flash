package com.affinislabs.zakatify.app.api.responses;

import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatSessionsResponse extends DefaultResponse {
    @SerializedName("messages")
    @Expose
    private List<ChatSession> chatSessions = null;
    @SerializedName("messageId")
    @Expose
    private String messageId;
    @SerializedName("otherParticipant")
    @Expose
    private User otherParticipant;
    @SerializedName("requestingParticipant")
    @Expose
    private User requestingParticipant;

    public List<ChatSession> getChatSessions() {
        return chatSessions;
    }

    public void setChatSessions(List<ChatSession> chatSessions) {
        this.chatSessions = chatSessions;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public User getOtherParticipant() {
        return otherParticipant;
    }

    public void setOtherParticipant(User otherParticipant) {
        this.otherParticipant = otherParticipant;
    }

    public User getRequestingParticipant() {
        return requestingParticipant;
    }

    public void setRequestingParticipant(User requestingParticipant) {
        this.requestingParticipant = requestingParticipant;
    }
}
