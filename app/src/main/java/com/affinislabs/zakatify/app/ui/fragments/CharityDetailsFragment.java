package com.affinislabs.zakatify.app.ui.fragments;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.affinislabs.zakatify.app.BuildConfig;
import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.ReviewsResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.Review;
import com.affinislabs.zakatify.app.models.TrendData;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.PaymentActivity;
import com.affinislabs.zakatify.app.ui.activities.ProfileActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.CharityReviewsAdapter;
import com.affinislabs.zakatify.app.ui.adapters.DonorListAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.ui.customviews.DonorViewItemDecorator;
import com.affinislabs.zakatify.app.ui.dialogs.WriteReviewDialog;
import com.affinislabs.zakatify.app.utils.CommonUtils;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FontUtils;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.ZakatifyUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.DOWNLOAD_SERVICE;

public class CharityDetailsFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, ZakatifyInterfaces.ReviewCreatedListener {
    private Button writeReviewButton, donateNowButton, addToPortfolioButton;
    private ImageView mCharityLogo;
    private RatingBar charityRatingBar;
    private TextView mCharityName, mCharityDescription, mCharityAddress, mCharityEINNumber, mCharityPhoneNumber;
    private TextView mTotalDonation, mDonorSummary, mReportUrl, mCategoryTagView, mDonationActivityLabel, mCharityReviewsLabel, mReadReviewsButton;
    private TextView charityEmailAddressView, charityWebsiteView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CustomRecyclerView donorListRecyclerView;
    private DonorListAdapter donorListAdapter;
    private CustomRecyclerView reviewsRecyclerView;
    private User user = null;
    private Charity charity;
    private LineChart donationActivityChart;
    private DownloadManager downloadManager;
    private long downloadReferenceId = 0;
    private IntentFilter downloadStatusIntentFilter = new IntentFilter(Constants.LOCAL_RECEIVER_LISTENER);
    private ProgressDialog progressDialog;
    private CharityReviewsAdapter charityReviewsAdapter;
    private int pageNumber = 1, pageSize = 10;
    private int currentPage = pageNumber;
    private static final String SHARED_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".provider";

    private static boolean isReportDownloaded = false;
    private static String REPORTS_DIRECTORY = "reports";
    private static String REPORTS_DIRECTORY_PATH = null;
    private static String REPORT_FILE_PATH = null;
    private static String REPORT_FILE_NAME = null;
    private List<Review> reviews;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new CharityDetailsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public CharityDetailsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            user = getArguments().getParcelable(Constants.USER);
            charity = getArguments().getParcelable(Constants.CHARITY);
        }

        initializeReportsDirectory(getActivity());
        progressDialog = new ProgressDialog(getActivity());
    }

    private void initializeReportsDirectory(Activity activity) {
        if (activity != null && activity.getExternalFilesDir(null) != null) {
            File externalFilesDirectory = activity.getExternalFilesDir(null);
            if (externalFilesDirectory != null) {
                REPORTS_DIRECTORY_PATH = externalFilesDirectory.getAbsolutePath() + File.separator + REPORTS_DIRECTORY;
                File reportsDirectory = new File(REPORTS_DIRECTORY_PATH);
                File reportsFilePath = new File(reportsDirectory, charity.getCharityId() + ".pdf");
                REPORT_FILE_PATH = reportsFilePath.getPath();
                REPORT_FILE_NAME = reportsFilePath.getName();
                isReportDownloaded = reportsFilePath.exists();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_charity_details, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.charity_details));
        donateNowButton = view.findViewById(R.id.donate_now_button);
        addToPortfolioButton = view.findViewById(R.id.add_to_portfolio_button);
        writeReviewButton = view.findViewById(R.id.write_review_button);
        mCharityLogo = view.findViewById(R.id.charity_icon);
        mCharityName = view.findViewById(R.id.charity_name);
        mCharityDescription = view.findViewById(R.id.charity_description);
        mCharityAddress = view.findViewById(R.id.charity_address);
        mCharityPhoneNumber = view.findViewById(R.id.charity_phone_number);
        mCharityEINNumber = view.findViewById(R.id.charity_ein_number);
        mReportUrl = view.findViewById(R.id.charity_report_url);
        mTotalDonation = view.findViewById(R.id.total_donation);
        mCategoryTagView = view.findViewById(R.id.category_tags);
        mDonorSummary = view.findViewById(R.id.donation_summary);
        mDonationActivityLabel = view.findViewById(R.id.donation_activity_label);
        mCharityReviewsLabel = view.findViewById(R.id.charity_reviews_label);
        mReadReviewsButton = view.findViewById(R.id.read_reviews_button);
        charityRatingBar = view.findViewById(R.id.charity_rating);
        donationActivityChart = view.findViewById(R.id.donation_activity_chart);
        reviewsRecyclerView = view.findViewById(R.id.reviews_list);
        charityEmailAddressView = view.findViewById(R.id.charity_email_address);
        charityWebsiteView = view.findViewById(R.id.charity_website);
        charityReviewsAdapter = new CharityReviewsAdapter(getContext(), this, null);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        reviewsRecyclerView.setAdapter(charityReviewsAdapter);
        reviewsRecyclerView.setLayoutManager(linearLayoutManager);
        reviewsRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        donorListRecyclerView = view.findViewById(R.id.donor_list_recyclerview);
        donorListAdapter = new DonorListAdapter(getContext());
        donorListRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        donorListAdapter.setItems(null);
        donorListRecyclerView.addItemDecoration(new DonorViewItemDecorator(-10));
        donorListRecyclerView.setAdapter(donorListAdapter);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blue),
                ContextCompat.getColor(getContext(), R.color.colorRed),
                ContextCompat.getColor(getContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchCharityDetails(user.getUsername(), String.valueOf(charity.getCharityId()));
                        fetchCharityReviews(String.valueOf(charity.getCharityId()), pageNumber, pageSize, false);
                    } else {
                        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mCharityLogo.setOnClickListener(this);
        mReportUrl.setOnClickListener(this);
        writeReviewButton.setOnClickListener(this);
        addToPortfolioButton.setOnClickListener(this);
        donateNowButton.setOnClickListener(this);
        mReadReviewsButton.setOnClickListener(this);

        loadCharityData(charity);
        loadReviewsResponse(null);
        if (NetworkUtils.isConnected(getContext())) {
            fetchCharityDetails(user.getUsername(), String.valueOf(charity.getCharityId()));
            fetchCharityReviews(String.valueOf(charity.getCharityId()), pageNumber, pageSize, false);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }

    private void loadCharityData(Charity charity) {
        if (charity != null && isAdded() && isVisible()) {
            mCharityName.setText(charity.getCharityName());
            mCharityDescription.setText(charity.getDescription());
            mCharityAddress.setText(charity.getAddress());
            mCharityPhoneNumber.setText(charity.getPhoneNumber());
            charityWebsiteView.setText(charity.getWebsite());
            charityEmailAddressView.setText(charity.getEmailAddress());
            charityEmailAddressView.setVisibility(!TextUtils.isEmpty(charity.getEmailAddress()) ? View.VISIBLE : View.GONE);
            charityWebsiteView.setVisibility(!TextUtils.isEmpty(charity.getWebsite()) ? View.VISIBLE : View.GONE);
            ImageUtils.loadImageUrl(mCharityLogo, getContext(), charity.getCharityLogo());
            mCharityEINNumber.setText(getString(R.string.ein_number_format, charity.getEin()));
            mTotalDonation.setText(charity.getTotalDonation() > 0 ? ZakatifyUtils.formatCurrencyAmountWithDecimalPlaces(charity.getTotalDonation()) : getString(R.string.no_donations_label));
            mCategoryTagView.setText(ZakatifyUtils.getCategoryTags(charity.getCategories(), "\t\t\t"));
            charityRatingBar.setRating(charity.getRating());

            if (charity.getTrendData() != null && charity.getTrendData().size() > 0) {
                mDonationActivityLabel.setText(getString(R.string.no_donation_activity_label)); //donation_activity_label
                donationActivityChart.setVisibility(View.GONE); //Pending when the server sends live data
                //loadDonationActivityData();
            } else {
                donationActivityChart.setVisibility(View.GONE);
                mDonationActivityLabel.setText(getString(R.string.no_donation_activity_label));
            }

            donorListAdapter.setItems(charity.getDonors());
            mDonorSummary.setText(ZakatifyUtils.formatDonorSummary(getContext(), charity.getNumberOfDonators()));

            addToPortfolioButton.setText(charity.getSelected() ? getString(R.string.remove_from_portfolio_label) : getString(R.string.add_to_portfolio_label));
            addToPortfolioButton.setBackground(ContextCompat.getDrawable(getContext(), charity.getSelected() ? R.drawable.curved_bordered_deselected_button : R.drawable.curved_bordered_selected_button));
        }
    }

    private void loadReviewsResponse(ReviewsResponse reviewsResponse) {
        if (isAdded() && isVisible()) {
            if (reviewsResponse != null && reviewsResponse.getReviews() != null && reviewsResponse.getReviews().size() > 0 && isAdded() && isVisible()) {
                reviews = reviewsResponse.getReviews();
                int reviewsCount = reviewsResponse.getTotalCount();
                List<Review> displayedReviews = reviews.subList(0, reviewsCount > 5 ? 5 : reviewsCount);
                mCharityReviewsLabel.setText(getString(R.string.latest_reviews_label));
                mReadReviewsButton.setVisibility(reviewsCount > 5 ? View.VISIBLE : View.GONE);
                mReadReviewsButton.setText(getString(R.string.read_reviews_label, reviewsCount));
                reviewsRecyclerView.setVisibility(View.VISIBLE);
                charityReviewsAdapter.setItems(displayedReviews);
            } else {
                mReadReviewsButton.setVisibility(View.GONE);
                reviewsRecyclerView.setVisibility(View.GONE);
                mCharityReviewsLabel.setText(getString(R.string.no_reviews_label));
            }
        }
    }

    private void loadDonationActivityData() {
        if (isAdded() && isVisible()) {
            List<Entry> entries = new ArrayList<Entry>();
            final ArrayList<String> labelList = new ArrayList<String>();

            ArrayList<Float> dataList = new ArrayList<Float>();

            for (int i = 0; i < charity.getTrendData().size(); i++) {
                TrendData trendData = charity.getTrendData().get(i);
                entries.add(new Entry(i, (float) trendData.getValue()));
                labelList.add(trendData.getLabel());
                dataList.add((float) trendData.getValue());
            }

            final String[] trendLabels = labelList.toArray(new String[labelList.size()]);

            LineDataSet dataSet = new LineDataSet(entries, getString(R.string.months_label)); // add entries to dataset
            dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            dataSet.setColor(ContextCompat.getColor(getContext(), R.color.colorBaseBlue));
            dataSet.setValueTextColor(ContextCompat.getColor(getContext(), R.color.colorLightGreen));
            dataSet.setValueTypeface(FontUtils.selectTypeface(getContext(), FontUtils.STYLE_BOLD));

            LineData lineData = new LineData(dataSet);
            donationActivityChart.setData(lineData);
            donationActivityChart.getLegend().setEnabled(false);
            XAxis xAxis = donationActivityChart.getXAxis();
            donationActivityChart.getAxisRight().setEnabled(false);
            YAxis yAxis = donationActivityChart.getAxisLeft();
            yAxis.setTypeface(FontUtils.selectTypeface(getContext(), FontUtils.STYLE_MEDIUM));
            yAxis.setTextColor(ContextCompat.getColor(getContext(), R.color.colorOrange));
            yAxis.setTextSize(8f);
            xAxis.setValueFormatter(new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    return trendLabels[(int) value];
                }
            });

            xAxis.setTextSize(8f);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setGranularityEnabled(true);
            xAxis.setGranularity(1);
            xAxis.setTypeface(FontUtils.selectTypeface(getContext(), FontUtils.STYLE_MEDIUM));
            xAxis.setDrawGridLines(false);
            xAxis.setAvoidFirstLastClipping(true);
            donationActivityChart.invalidate();
        }
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.profile_update_loading);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.write_review_button:
                bundle.putParcelable(Constants.CHARITY, charity);
                bundle.putParcelable(Constants.USER, user);
                WriteReviewDialog writeReviewDialog = new WriteReviewDialog();
                writeReviewDialog.setReviewCreatedListener(this);
                writeReviewDialog.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
                fragmentTransaction.addToBackStack(null);
                if (getActivity() != null) {
                    writeReviewDialog.show(fragmentManager, writeReviewDialog.getClass().getSimpleName());
                }
                break;
            case R.id.charity_icon:
                break;
            case R.id.charity_report_url:
                if (!isReportDownloaded) {
                    downloadCharityReportFile(requireContext());
                } else {
                    launchReportIntent(requireContext());
                }
                break;
            case R.id.add_to_portfolio_button:
                if (!charity.getSelected()) {
                    addToPortfolio(user.getUsername(), String.valueOf(charity.getCharityId()));
                } else {
                    removeFromPortfolio(user.getUsername(), String.valueOf(charity.getCharityId()));
                }
                break;
            case R.id.donate_now_button:
                Intent intent = new Intent(getActivity(), PaymentActivity.class);
                intent.putExtra(Constants.VIEW_TYPE, Constants.MANUAL_DONATION_VIEW_TAG);
                intent.putExtra(Constants.USER, user);
                intent.putExtra(Constants.CHARITY, charity);
                startActivity(intent);
                break;
            case R.id.read_reviews_button:
                bundle.putParcelable(Constants.USER, user);
                bundle.putParcelable(Constants.CHARITY, charity);
                bundle.putSerializable(Constants.USER_REVIEWS_LIST, (Serializable) reviews);
                startFragment(CharityReviewsFragment.newInstance(bundle));
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(getContext(), downloadBroadCastReceiver);
    }

    private void finishDownload(String status) {
        switch (status) {
            case Constants.DOWNLOAD_COMPLETE:
                CommonUtils.displayShortToastMessage(getString(R.string.report_download_successful_message));
                isReportDownloaded = true;
                launchReportIntent(requireContext());

                break;
            case Constants.DOWNLOAD_FAILED:
                CommonUtils.displayShortToastMessage(getString(R.string.report_download_error_message));
                isReportDownloaded = false;
                break;
        }
        progressDialog.dismiss();
    }

    private String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (!TextUtils.isEmpty(extension)) {
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            type = mimeTypeMap.getMimeTypeFromExtension(extension);
        }

        return type;
    }

    private void launchReportIntent(Context context) {
        File reportsDirectory = new File(context.getExternalFilesDir(null), REPORTS_DIRECTORY);
        File report = new File(reportsDirectory, REPORT_FILE_NAME);
        Uri reportUri = FileProvider.getUriForFile(context, SHARED_PROVIDER_AUTHORITY, report);
        String reportMimeType = getMimeType(REPORT_FILE_PATH);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(reportUri, !TextUtils.isEmpty(reportMimeType) ? reportMimeType : "application/pdf");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        try {
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            showErrorPopup(e.getMessage());
        }
    }

    private void downloadCharityReportFile(Context context) {
        Uri reportUri = Uri.parse(charity.getReportUrl());
        if (reportUri != null) {
            downloadCharityReport(reportUri, charity.getCharityId(), context);
        }
    }

    private long downloadCharityReport(Uri uri, Integer charityId, Context context) {
        progressDialog.setTitle(getString(R.string.charity_download_progress_title));
        progressDialog.setMessage(getString(R.string.charity_download_progress_message));
        progressDialog.setCancelable(false);
        progressDialog.show();
        context.registerReceiver(downloadBroadCastReceiver, downloadStatusIntentFilter);

        downloadManager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(uri);

        String reportMimeType = getMimeType(uri.toString());
        request.setMimeType(!TextUtils.isEmpty(reportMimeType) ? reportMimeType : "application/pdf");

        String reportFileName = REPORTS_DIRECTORY + File.separator + charityId + ".pdf";
        request.setTitle("Charity Report Download");
        request.setVisibleInDownloadsUi(false);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDescription("Downloading document");

        request.setDestinationInExternalFilesDir(getActivity(), null, reportFileName);

        downloadReferenceId = downloadManager.enqueue(request);
        cancelProgressDialogHandler();
        return downloadReferenceId;
    }

    private void reloadCharityList(Context context) {
        Intent intent = new Intent();
        intent.setAction(Constants.REFRESH_CHARITY_LIST_FILTER);
        intent.putExtra(Constants.REFRESH_FLAG, true);
        context.sendBroadcast(intent);
    }

    private void addToPortfolio(String username, String charityId) {
        showLoadingIndicator(getString(R.string.adding_charity_to_portfolio_message));
        new ApiClient.NetworkCallsRunner(Constants.ADD_USER_CHARITY_REQUEST, username, charityId, new ApiClientListener.AddUserCharityListener() {
            @Override
            public void onCharityAdded(Charity charityResponse) {
                hideLoadingIndicator();
                if (charityResponse != null) {
                    if (charityResponse.getStatus() == null && charityResponse.getMessage() == null) {
                        if (charityResponse.getState()) {
                            CommonUtils.displayShortToastMessage(getString(R.string.charity_add_success));
                            addToPortfolioButton.setText(getString(R.string.remove_from_portfolio_label));
                            charity.setSelected(true);
                            loadCharityData(charity);
                            reloadCharityList(getContext());
                        }
                    } else {
                        showErrorPopup(charityResponse.getMessage());
                    }

                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void removeFromPortfolio(String username, String charityId) {
        showLoadingIndicator(getString(R.string.removing_charity_progress_message));
        new ApiClient.NetworkCallsRunner(Constants.REMOVE_USER_CHARITY_REQUEST, username, charityId, new ApiClientListener.RemoveUserCharityListener() {
            @Override
            public void onCharityRemoved(Charity charityResponse) {
                hideLoadingIndicator();
                if (charityResponse != null) {
                    if (charityResponse.getStatus() == null && charityResponse.getMessage() == null) {
                        if (charityResponse.getState()) {
                            CommonUtils.displayShortToastMessage(getString(R.string.charity_removed_success));
                            addToPortfolioButton.setText(getString(R.string.add_to_portfolio_label));
                            charity.setSelected(false);
                            loadCharityData(charity);
                            reloadCharityList(getContext());
                        }
                    } else {
                        showErrorPopup(charityResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void cancelProgressDialogHandler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isReportDownloaded) {
                    finishDownload(Constants.DOWNLOAD_FAILED);
                }
            }
        }, 15000);
    }

    private void fetchCharityDetails(String username, final String charityId) {
        showLoadingIndicator(getString(R.string.fetching_charity_details_progress_message));
        new ApiClient.NetworkCallsRunner(Constants.FETCH_CHARITY_DETAILS_REQUEST, username, charityId, new ApiClientListener.FetchCharityDetailsListener() {
            @Override
            public void onCharityDetailsFetched(Charity charityResponse) {
                hideLoadingIndicator();
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
                if (charityResponse != null) {
                    if (charityResponse.getStatus() == null && charityResponse.getMessage() == null) {
                        charity = charityResponse;
                        loadCharityData(charity);
                    } else {
                        showErrorPopup(charityResponse.getMessage());
                    }

                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void fetchCharityReviews(final String id, int pageNumber, int pageSize, boolean inLoadMoreMode) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_CHARITY_REVIEWS_REQUEST, id, String.valueOf(pageNumber), String.valueOf(pageSize), new ApiClientListener.FetchReviewsListener() {
            @Override
            public void onReviewsFetched(ReviewsResponse reviewsResponse) {
                if (reviewsResponse != null) {
                    if (reviewsResponse.getStatus() == null && reviewsResponse.getMessage() == null) {
                        loadReviewsResponse(reviewsResponse);
                    }
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1 && reviews != null && reviews.size() > 0) {
            Intent intent = new Intent(getContext(), ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constants.USERNAME, reviews.get(position).getUsername());
            intent.putExtra(Constants.MAIN_USERNAME, user.getUsername());
            intent.putExtra(Constants.VIEW_TYPE, Constants.PUBLIC_USER_PROFILE_VIEW_TAG);
            startActivity(intent);
        }
    }

    private BroadcastReceiver downloadBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = intent.getStringExtra(Constants.STATUS);
            finishDownload(status);
            unregisterReceiver(getContext(), downloadBroadCastReceiver);
        }
    };

    @Override
    public void onReviewCreated() {
        if (NetworkUtils.isConnected(getContext())) {
            fetchCharityDetails(user.getUsername(), String.valueOf(charity.getCharityId()));
            fetchCharityReviews(String.valueOf(charity.getCharityId()), pageNumber, pageSize, false);
        }
    }
}
