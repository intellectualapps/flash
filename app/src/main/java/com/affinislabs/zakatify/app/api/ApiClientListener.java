package com.affinislabs.zakatify.app.api;

import com.affinislabs.zakatify.app.api.responses.BooleanStateResponse;
import com.affinislabs.zakatify.app.api.responses.CategoryResponse;
import com.affinislabs.zakatify.app.api.responses.CharitiesResponse;
import com.affinislabs.zakatify.app.api.responses.ChatSessionsResponse;
import com.affinislabs.zakatify.app.api.responses.DefaultResponse;
import com.affinislabs.zakatify.app.api.responses.FCMTokenResponse;
import com.affinislabs.zakatify.app.api.responses.FollowersResponse;
import com.affinislabs.zakatify.app.api.responses.ForgotPasswordResponse;
import com.affinislabs.zakatify.app.api.responses.GoldValueResponse;
import com.affinislabs.zakatify.app.api.responses.InvitationsResponse;
import com.affinislabs.zakatify.app.api.responses.LoginResponse;
import com.affinislabs.zakatify.app.api.responses.PaymentResponse;
import com.affinislabs.zakatify.app.api.responses.PaypalAccountsResponse;
import com.affinislabs.zakatify.app.api.responses.PublicUserResponse;
import com.affinislabs.zakatify.app.api.responses.RegistrationResponse;
import com.affinislabs.zakatify.app.api.responses.ReviewsResponse;
import com.affinislabs.zakatify.app.api.responses.TopZakatifiersResponse;
import com.affinislabs.zakatify.app.api.responses.UserMenuResponse;
import com.affinislabs.zakatify.app.api.responses.UsernameCheckResponse;
import com.affinislabs.zakatify.app.api.responses.ZakatGoalResponse;
import com.affinislabs.zakatify.app.models.Charity;

public class ApiClientListener {
    public interface AccountRegistrationListener {
        void onAccountRegistered(RegistrationResponse registrationResponse);
    }

    public interface UsernameCreationListener {
        void onUsernameCreated(LoginResponse loginResponse);
    }

    public interface AccountAuthenticationListener {
        void onAccountAuthenticated(LoginResponse loginResponse);
    }

    public interface ProfileUpdateListener {
        void onProfileUpdated(LoginResponse loginResponse);
    }

    public interface SocialNetworkLinkingListener {
        void onActionCommitted(LoginResponse loginResponse);
    }

    public interface UsernameAvailabilityListener {
        void onUsernameChecked(UsernameCheckResponse usernameCheckResponse);
    }

    public interface ForgotPasswordListener {
        void onPasswordReset(ForgotPasswordResponse forgotPasswordResponse);
    }

    public interface FetchCategoriesListener {
        void onCategoriesFetched(CategoryResponse categoryResponse);
    }

    public interface UpdateUserCategoriesListener {
        void onCategoriesUpdated(CategoryResponse categoryResponse);
    }

    public interface AddZakatGoalListener {
        void onZakatGoalAdded(ZakatGoalResponse zakatGoalResponse);
    }

    public interface UpdateZakatGoalListener {
        void onZakatGoalUpdated(ZakatGoalResponse zakatGoalResponse);
    }

    public interface DeleteZakatGoalListener {
        void onZakatGoalDeleted(ZakatGoalResponse zakatGoalResponse);
    }

    public interface FetchZakatGoalListener {
        void onZakatGoalFetched(ZakatGoalResponse zakatGoalResponse);
    }

    public interface FetchGoldValueListener {
        void onGoldValueFetched(GoldValueResponse goldValueResponse);
    }

    public interface FetchSuggestedCharitiesListener {
        void onSuggestedCharitiesFetched(CharitiesResponse charitiesResponse);
    }

    public interface FetchUserCharitiesListener {
        void onUserCharitiesFetched(CharitiesResponse charitiesResponse);
    }

    public interface SearchCharitiesListener {
        void onSearchFinished(CharitiesResponse charitiesResponse);
    }

    public interface FetchCharityDetailsListener {
        void onCharityDetailsFetched(Charity charityResponse);
    }

    public interface FetchPublicUserDetailsListener {
        void onUserDetailsFetched(PublicUserResponse publicUserResponse);
    }

    public interface AddUserCharityListener {
        void onCharityAdded(Charity charityResponse);
    }

    public interface RemoveUserCharityListener {
        void onCharityRemoved(Charity charityResponse);
    }

    public interface FetchUserMenuListener {
        void onUserMenuFetched(UserMenuResponse userMenuResponse);
    }

    public interface AddPaymentOptionListener {
        void onPaymentOptionAdded(PaymentResponse paymentResponse);
    }

    public interface UpdatePaymentOptionListener {
        void onPaymentOptionUpdated(PaymentResponse paymentResponse);
    }

    public interface FetchPaymentOptionsListener {
        void onPaymentOptionsFetched(PaymentResponse paymentResponse);
    }

    public interface DeletePaymentOptionListener {
        void onPaymentOptionDeleted(PaymentResponse paymentResponse);
    }

    public interface SetPrimaryPaymentOptionListener {
        void onPrimaryPaymentOptionSet(PaymentResponse paymentResponse);
    }

    public interface FetchTopZakatifiersListener {
        void onTopZakatifiersFetched(TopZakatifiersResponse topZakatifiersResponse);
    }

    public interface FetchReviewsListener {
        void onReviewsFetched(ReviewsResponse reviewsResponse);
    }

    public interface FetchFollowersListener {
        void onFollowersFetched(FollowersResponse followersResponse);
    }

    public interface WriteCharityReviewListener {
        void onReviewSaved(ReviewsResponse reviewsResponse);
    }

    public interface FollowUserListener {
        void onFollowStateChanged(BooleanStateResponse booleanStateResponse);
    }

    public interface UploadFCMTokenListener {
        void onFCMTokenUploaded(FCMTokenResponse fcmTokenResponse);
    }

    public interface InitializeChatSessionsListener {
        void onChatSessionInitialized(ChatSessionsResponse chatSessionsResponse);
    }

    public interface FetchUserChatSessionsListener {
        void onUserChatSessionsFetched(ChatSessionsResponse chatSessionsResponse);
    }

    public interface NotifyOfflineUserListener {
        void onNotificationSent(DefaultResponse defaultResponse);
    }

    public interface FetchUserInvitationsListener {
        void onInvitationsFetched(InvitationsResponse invitationsResponse);
    }

    public interface SendUserInvitationListener {
        void onInvitationSent(InvitationsResponse invitationsResponse);
    }

    public interface FetchPaypalAccountsListener {
        void onPaypalAccountsFetched(PaypalAccountsResponse paypalAccountsResponse);
    }

    public interface InitiateManualDonationListener {
        void onManualDonationInitiated(BooleanStateResponse booleanStateResponse);
    }

    public interface DeleteUserChatSessionListener {
        void onUserChatSessionDeleted(BooleanStateResponse booleanStateResponse);
    }
}
