package com.affinislabs.zakatify.app.ui.dialogs;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.TooltipHelp;
import com.affinislabs.zakatify.app.utils.Constants;

public class TooltipDialog extends BaseDialogFragment implements View.OnClickListener {

    private ImageView closeDialogButton, tooltipImageView;
    private View closeTooltipDialogButton;
    private TextView tooltipTitleView, tooltipContentView;
    private String tooltipTitle, tooltipContent;
    private TooltipHelp tooltipHelp;


    public static DialogFragment getInstance(Bundle args) {
        TooltipDialog frag = new TooltipDialog();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            tooltipHelp = getArguments().getParcelable(Constants.TOOLTIP_HELP);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.dialog_tooltip, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        closeTooltipDialogButton = view.findViewById(R.id.close_button);
        closeDialogButton = (ImageView) view.findViewById(R.id.close_button_icon);
        tooltipImageView = (ImageView) view.findViewById(R.id.tooltip_content_image);
        tooltipTitleView = (TextView) view.findViewById(R.id.tooltip_title);
        tooltipContentView = (TextView) view.findViewById(R.id.tooltip_content);

        closeDialogButton.setOnClickListener(this);
        closeTooltipDialogButton.setOnClickListener(this);

        tooltipTitleView.setText(tooltipHelp.getTooltipTitle());
        tooltipContentView.setText(tooltipHelp.getTooltipContent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_button_icon:
            case R.id.close_button:
                dismiss();
                break;
        }
    }
}
