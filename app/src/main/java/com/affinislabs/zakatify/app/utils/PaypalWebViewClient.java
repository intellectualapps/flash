package com.affinislabs.zakatify.app.utils;


import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PaypalWebViewClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return false;
    }
}
