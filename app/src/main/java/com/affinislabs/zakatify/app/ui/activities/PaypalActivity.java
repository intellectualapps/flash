package com.affinislabs.zakatify.app.ui.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ui.fragments.BasePaymentsFragment;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.PaypalWebViewClient;

public class PaypalActivity extends BaseActivity implements View.OnClickListener {

    private String URL;
    private WebView webView;
    private TextView dismissDialogButton;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        if (getIntent() != null) {
            URL = getIntent().getStringExtra(Constants.PAYPAL_ACCOUNT_URL);
        }
        webView = findViewById(R.id.web_view);
        dismissDialogButton = findViewById(R.id.dismiss_dialog);
        dismissDialogButton.setOnClickListener(this);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        CookieManager.getInstance().setAcceptCookie(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.setWebViewClient(new PaypalWebViewClient());
        ZakatifyWebviewCallback zakatifyWebviewCallback = new ZakatifyWebviewCallback(this);
        webView.addJavascriptInterface(zakatifyWebviewCallback, "Zakatify");
        webView.loadUrl(URL);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dismiss_dialog:
                Intent intent = new Intent();
                intent.putExtra(Constants.PAYPAL_CALLBACK_CODE, -2);
                setResult(BasePaymentsFragment.PAYPAL_ACCOUNT_SET_UP_REQUEST_CODE, intent);
                finish();
                break;
        }
    }

    public class ZakatifyWebviewCallback {
        private Activity activity;

        ZakatifyWebviewCallback(Activity activity) {
            this.activity = activity;
        }

        @JavascriptInterface
        public void addPaypalAccountSuccessful() {
            Intent intent = new Intent();
            intent.putExtra(Constants.PAYPAL_CALLBACK_CODE, 1);
            setResult(BasePaymentsFragment.PAYPAL_ACCOUNT_SET_UP_REQUEST_CODE, intent);
            finish();
        }

        @JavascriptInterface
        public void addPaypalAccountFailed() {
            Intent intent = new Intent();
            intent.putExtra(Constants.PAYPAL_CALLBACK_CODE, -1);
            setResult(BasePaymentsFragment.PAYPAL_ACCOUNT_SET_UP_REQUEST_CODE, intent);
            finish();
        }
    }
}

