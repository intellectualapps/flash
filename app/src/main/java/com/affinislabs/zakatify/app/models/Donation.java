package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Donation implements Parcelable{

    private int charityId;
    private String charityName;
    private String photoUrl;
    private String donationSummary;
    private String createdOn;

    public Donation() {

    }

    protected Donation(Parcel in) {
        charityId = in.readInt();
        charityName = in.readString();
        photoUrl = in.readString();
        donationSummary = in.readString();
        createdOn = in.readString();
    }

    public static final Creator<Donation> CREATOR = new Creator<Donation>() {
        @Override
        public Donation createFromParcel(Parcel in) {
            return new Donation(in);
        }

        @Override
        public Donation[] newArray(int size) {
            return new Donation[size];
        }
    };

    public int getCharityId() {
        return charityId;
    }

    public void setCharityId(int charityId) {
        this.charityId = charityId;
    }

    public String getCharityName() {
        return charityName;
    }

    public void setCharityName(String charityName) {
        this.charityName = charityName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDonationSummary() {
        return donationSummary;
    }

    public void setDonationSummary(String donationSummary) {
        this.donationSummary = donationSummary;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(charityId);
        dest.writeString(charityName);
        dest.writeString(photoUrl);
        dest.writeString(donationSummary);
        dest.writeString(createdOn);
    }
}
