package com.affinislabs.zakatify.app.api.responses;

import com.affinislabs.zakatify.app.models.Category;
import com.affinislabs.zakatify.app.models.PaypalAccount;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PaypalAccountsResponse extends DefaultResponse {
    @SerializedName("addNewPayPalAccountUrl")
    @Expose
    private String addNewPayPalAccountUrl;
    @SerializedName("existingPayPalAccounts")
    @Expose
    private ArrayList<PaypalAccount> existingPayPalAccounts;

    public String getAddNewPayPalAccountUrl() {
        return addNewPayPalAccountUrl;
    }

    public void setAddNewPayPalAccountUrl(String addNewPayPalAccountUrl) {
        this.addNewPayPalAccountUrl = addNewPayPalAccountUrl;
    }

    public ArrayList<PaypalAccount> getExistingPayPalAccounts() {
        return existingPayPalAccounts;
    }

    public void setExistingPayPalAccounts(ArrayList<PaypalAccount> existingPayPalAccounts) {
        this.existingPayPalAccounts = existingPayPalAccounts;
    }
}
