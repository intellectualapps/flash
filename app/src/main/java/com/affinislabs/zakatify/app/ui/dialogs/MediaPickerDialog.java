package com.affinislabs.zakatify.app.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;

import com.affinislabs.zakatify.app.BuildConfig;
import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.fragments.BaseFragment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MediaPickerDialog extends BaseDialogFragment {
    private static final String SCREEN_NAME = "Media Picker screen";
    private static String TAG = "MediaPicker";
    public static final String DIR_NAME = "Zakatify";
    public static final int TYPE_IMAGE = 0;

    public static final int TYPE_VIDEO = 1;
    public static final int TYPE_TAKE_IMAGE = 3;
    public static final int TYPE_PICK_IMAGE = 4;
    public static final int MAX_DURATION_VIDEO = 60;
    private int mMediaType = -1;
    private String mTitle;
    private String mFragmentTag;
    private MediaPickedListener mMediaPickedListener;
    private final static int REQ_CAPTURE_IMAGE = 100;
    private final static int REQ_CAPTURE_VIDEO = 200;

    private final static int REQ_PICK_IMAGE = 300;
    private final static int REQ_PICK_VIDEO = 400;
    private static final String KEY_MEDIATYPE = "key_media_type";
    private static final String KEY_TITLE = "key_title";

    private static final String KEY_FRAGMENT_TAG = "key_fragment_tag";
    private static String mediaFilePath;
    private static final String SHARED_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".provider";

    public static DialogFragment newInstance(String title, int mediaType, String fragmentType) {
        return newInstance(title, mediaType, fragmentType, null);
    }

    public static DialogFragment newInstance(String title, int mediaType, String fragmentTag, MediaPickedListener mediaPickedListener) {
        MediaPickerDialog frag = new MediaPickerDialog();
        frag.mMediaType = mediaType;
        frag.mTitle = title;
        frag.mFragmentTag = fragmentTag;
        frag.mMediaPickedListener = mediaPickedListener;
        return frag;
    }

    public MediaPickedListener getMediaPickedListener() {
        return mMediaPickedListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_MEDIATYPE)) {
                mMediaType = savedInstanceState.getInt(KEY_MEDIATYPE);
            }
            if (savedInstanceState.containsKey(KEY_TITLE)) {
                mTitle = savedInstanceState.getString(KEY_TITLE);
            }

            if (savedInstanceState.containsKey(KEY_FRAGMENT_TAG)) {
                mTitle = savedInstanceState.getString(KEY_FRAGMENT_TAG);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_MEDIATYPE, mMediaType);
        outState.putString(KEY_TITLE, mTitle);
        outState.putString(KEY_FRAGMENT_TAG, mFragmentTag);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mMediaPickedListener = (MediaPickerDialog.MediaPickedListener) getActivity();
        } catch (ClassCastException cce) {
            throw new ClassCastException("The activity must implement " + MediaPickerDialog.MediaPickedListener.class.getSimpleName());
        }
    }

    @NonNull
    @Override

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (TextUtils.isEmpty(mTitle)) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        } else {
            dialog.setTitle(mTitle);
        }

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());

        if (!TextUtils.isEmpty(mTitle)) {
            mBuilder.setTitle(mTitle);
        }

        int itemsRes = -1;

        switch (mMediaType) {
            case TYPE_VIDEO:
                itemsRes = R.array.items_dialog_media_video;
                break;
            case TYPE_IMAGE:
                itemsRes = R.array.items_dialog_media_image;
            default:
                break;
            case TYPE_TAKE_IMAGE:
                startPickerFragment(mMediaType, MediaPickerFragment.ACTION_NEW, mFragmentTag, mMediaPickedListener);
                dismiss();
                break;
            case TYPE_PICK_IMAGE:
                startPickerFragment(mMediaType, MediaPickerFragment.ACTION_PICK, mFragmentTag, mMediaPickedListener);
                dismiss();
                break;
        }

        if (itemsRes != -1) {
            mBuilder.setItems(itemsRes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i) {
                        case 0:
                            startPickerFragment(mMediaType, MediaPickerFragment.ACTION_NEW, mFragmentTag, mMediaPickedListener);
                            break;
                        case 1:
                            startPickerFragment(mMediaType, MediaPickerFragment.ACTION_PICK, mFragmentTag, mMediaPickedListener);
                            break;
                    }
                }
            });

            dialog = mBuilder.create();
        }

        return dialog;
    }


    private void startPickerFragment(int mediaType, int action, String fragmentTag, MediaPickedListener mediaPickedListener) {
        Fragment frag = MediaPickerFragment.newInstance(mediaType, action, fragmentTag);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, frag, frag.getClass().getSimpleName());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    public interface MediaPickedListener {
        public void onMediaItemPicked(Uri fileUri, String filePath, int mediaType, String fragmentTag);

        public void onCancel(String message, String fragmentTag);
    }

    public static class MediaPickerFragment extends BaseFragment {
        public static final int ACTION_NEW = 0;
        public static final int ACTION_PICK = 1;
        public int mAction = -1;
        private Uri mMediaUri;

        private String mFragmentTag;
        private MediaPickedListener mMediaPickedListener;
        private int mMediaType = -1;

        public static Fragment newInstance(int mediaType, int action, String fragmentTag) {
            MediaPickerFragment frag = new MediaPickerFragment();
            frag.mMediaType = mediaType;
            frag.mAction = action;
            frag.mFragmentTag = fragmentTag;
            return frag;
        }

        private static boolean isExternalStorageAvailable() {
            String state = Environment.getExternalStorageState();
            return state.equals(Environment.MEDIA_MOUNTED);
        }

        private static File getOutputMediaFile(int type) {
            if (isExternalStorageAvailable()) {
                try {
                    File mediaStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES), DIR_NAME);
                    if (!mediaStorageDirectory.exists()) {
                        if (!mediaStorageDirectory.mkdirs()) {
                            return null;
                        }
                    }

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                    File mediaFile;
                    if (type == TYPE_IMAGE) {
                        mediaFile = File.createTempFile("IMG_" + timeStamp, ".jpg", mediaStorageDirectory);

                    } else if (type == TYPE_VIDEO) {
                        mediaFile = File.createTempFile("VID_" + timeStamp, ".mp4", mediaStorageDirectory);
                    } else {
                        mediaFilePath = null;
                        return null;
                    }

                    mediaFilePath = "file:" + mediaFile.getAbsolutePath();
                    mediaFile.deleteOnExit();
                    return mediaFile;

                } catch (Exception e) {
                    mediaFilePath = null;
                    return null;
                }
            }
            mediaFilePath = null;
            return null;
        }

        private static Uri getOutputMediaFileUri(int type) {
            File output = getOutputMediaFile(type);
            if (output != null) {
                Context context = ZakatifyApplication.getAppInstance().getApplicationContext();
                return FileProvider.getUriForFile(context, SHARED_PROVIDER_AUTHORITY, output);
            }
            return null;
        }

        private void createNewMedia(int mediaType) {
            Intent intent = null;

            switch (mediaType) {
                case TYPE_TAKE_IMAGE:
                case TYPE_IMAGE:
                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    mMediaUri = getOutputMediaFileUri(TYPE_IMAGE);
                    if (mMediaUri != null) {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mMediaUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivityForResult(intent, REQ_CAPTURE_IMAGE);
                    } else {
                        if (mMediaPickedListener != null) {
                            mMediaPickedListener.onCancel(getString(R.string.picker_error_image), mFragmentTag);
                        }
                    }
                    break;

                case TYPE_VIDEO:
                    intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    mMediaUri = getOutputMediaFileUri(TYPE_VIDEO);
                    if (mMediaUri != null) {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mMediaUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, MAX_DURATION_VIDEO);
                        startActivityForResult(intent, REQ_CAPTURE_VIDEO);
                    } else {
                        if (mMediaPickedListener != null) {
                            mMediaPickedListener.onCancel(getString(R.string.picker_error_video), mFragmentTag);
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        private void chooseExistingMedia(int mediaType) {
            Intent intent = null;

            switch (mediaType) {
                case TYPE_PICK_IMAGE:
                case TYPE_IMAGE:
                    intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");

                    startActivityForResult(Intent.createChooser(intent, getString(R.string.picker_title_image)), REQ_PICK_IMAGE);
                    break;

                case TYPE_VIDEO:
                    intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("video/*");

                    startActivityForResult(Intent.createChooser(intent, getString(R.string.picker_title_image)), REQ_PICK_VIDEO);
                    break;
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            switch (mAction) {
                case ACTION_NEW:
                    createNewMedia(mMediaType);
                    break;
                case ACTION_PICK:
                    chooseExistingMedia(mMediaType);
                    break;
            }
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);

            try {
                mMediaPickedListener = (MediaPickerDialog.MediaPickedListener) getActivity();
            } catch (ClassCastException cce) {
                throw new ClassCastException("The activity must implement " + MediaPickerDialog.MediaPickedListener.class.getSimpleName());
            }
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == BaseActivity.RESULT_OK) {

                if (requestCode == REQ_CAPTURE_IMAGE || requestCode == REQ_CAPTURE_VIDEO) {
                    if (mMediaPickedListener != null && mMediaUri != null) {
                        mMediaPickedListener.onMediaItemPicked(mMediaUri, mediaFilePath, mMediaType, mFragmentTag);
                    }
                    closeFragment();
                    return;
                }

                if (requestCode == REQ_PICK_IMAGE || requestCode == REQ_PICK_VIDEO) {
                    mMediaUri = data.getData();
                    if (mMediaPickedListener != null && mMediaUri != null) {
                        mMediaPickedListener.onMediaItemPicked(mMediaUri, null, mMediaType, mFragmentTag);
                    }
                    closeFragment();
                }
            } else if (resultCode == BaseActivity.RESULT_CANCELED) {
                if (mMediaPickedListener != null) {
                    mMediaPickedListener.onCancel("User cancelled media picker", mFragmentTag);
                }
                closeFragment();
            }
        }
    }
}
