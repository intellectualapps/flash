package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.Message;
import com.affinislabs.zakatify.app.utils.DateUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;

import java.util.List;

public class MessagesAdapter extends BaseRecyclerAdapter {
    private List<Message> messages;
    private ClickListener clickListener;
    private final int SENT = 0, RECEIVED = 2, DATE = 3;
    private String username;

    public MessagesAdapter(Context context, BaseRecyclerAdapter.ClickListener clickListener, String username) {
        super(context);
        this.clickListener = clickListener;
        this.username = username;
    }

    public void setItems(List<Message> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (this.messages != null) {
            messages.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Message message) {
        if (message != null) {
            this.messages.add(message);
            notifyDataSetChanged();
        }
    }

    public void add(Message message) {
        messages.add(0, message);
        notifyItemInserted(0);
    }

    public void addAll(List<Message> messages) {
        int beginPosition = 0;
        int messageSize = 0;
        if (messages != null && messages.size() > 0) {
            for (Message message : messages) {
                add(message);
            }
            messageSize = messages.size();
        }
        notifyItemRangeInserted(0, messageSize);
    }

    public void removeItem(Message message) {
        int position = messages.indexOf(message);
        if (position > -1) {
            messages.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(int position) {
        if (position > -1) {
            messages.remove(position);
            notifyItemRemoved(position);
        }
    }

    public List<Message> getMessages() {
        return messages;
    }

    @Override
    public int getItemViewType(int position) {
        Message message = messages.get(position);
        if (TextUtils.isEmpty(message.getText()) && TextUtils.isEmpty(message.getAuthor()) && message.getTime() != null && message.getId() != null) {
            return DATE;
        }

        if (position == 0 && isLoadingAdded) {
            return LOADING;
        }

        Boolean flag = message.getAuthor().equalsIgnoreCase(username);
        if (flag) {
            return SENT;
        } else {
            return RECEIVED;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(messages) ? 0 : messages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements ClickListener, View.OnClickListener {
        TextView messageText;
        TextView messageTime;
        View container;
        private ClickListener clickListener;

        public ViewHolder(View view, ClickListener clickListener) {
            super(view);
            messageText = view.findViewById(R.id.message_text);
            messageTime = view.findViewById(R.id.message_time);
            container = view.findViewById(R.id.message_box);
            this.clickListener = clickListener;

            view.setOnClickListener(this);
            messageText.setOnClickListener(this);
            messageTime.setOnClickListener(this);
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v, int position, boolean isLongClick) {
            if (clickListener != null) {
                clickListener.onClick(v, position, false);
            }
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onClick(v, getAdapterPosition(), false);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = null;
        switch (viewType) {
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
            case RECEIVED:
                view = inflater.inflate(R.layout.layout_received_message, parent, false);
                viewHolder = new ViewHolder(view, clickListener);
                break;
            default:
            case SENT:
                view = inflater.inflate(R.layout.layout_sent_message, parent, false);
                viewHolder = new ViewHolder(view, clickListener);
                break;
            case DATE:
                view = inflater.inflate(R.layout.layout_date_separator, parent, false);
                viewHolder = new DateViewHolder(view);
                break;
        }
        return viewHolder;
    }

    public void initializeDateViewHolder(RecyclerView.ViewHolder holder, int position) {
        DateViewHolder dateViewHolder = (DateViewHolder) holder;
        Message message = messages.get(position);
        if (message != null && message.getTime() != null) {
            String date = com.affinislabs.zakatify.app.utils.DateUtils.getSimpleDateFormat(message.getTime(), "dd MMM yyyy");
            date = String.valueOf(android.text.format.DateUtils.getRelativeTimeSpanString(message.getTime(), System.currentTimeMillis(), android.text.format.DateUtils.DAY_IN_MILLIS));
            dateViewHolder.dateView.setText(date);
        }
    }

    public class DateViewHolder extends RecyclerView.ViewHolder {
        public TextView dateView;

        public DateViewHolder(View itemView) {
            super(itemView);
            dateView = itemView.findViewById(R.id.date);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(viewHolder);
                break;
            case DATE:
                initializeDateViewHolder(viewHolder, position);
                break;
            default:
                final Message message = messages.get(position);
                final ViewHolder holder = (ViewHolder) viewHolder;
                if (message != null) {
                    holder.messageText.setText(message.getText());
                    long timestamp = message.getTime();
                    String messageTimestamp = DateUtils.getSimpleDateFormat(timestamp, "hh:mm a");

                    holder.messageTime.setText(messageTimestamp);
                } else {
                    holder.itemView.setVisibility(View.GONE);
                }
                break;
        }
    }
}
