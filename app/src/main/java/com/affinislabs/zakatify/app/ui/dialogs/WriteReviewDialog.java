package com.affinislabs.zakatify.app.ui.dialogs;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.ReviewsResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.utils.CommonUtils;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.ZakatifyUtils;

import java.util.HashMap;
import java.util.Map;

public class WriteReviewDialog extends BaseDialogFragment implements View.OnClickListener, RatingBar.OnRatingBarChangeListener {

    private TextView submitReviewButton;
    private TextView charityReviewHeader;
    private EditText reviewInput;
    private AppCompatRatingBar ratingBar;
    private ZakatifyInterfaces.ReviewCreatedListener listener;

    private Charity charity;
    private User user;

    public static DialogFragment getInstance(Bundle args) {
        WriteReviewDialog frag = new WriteReviewDialog();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.CHARITY)) {
                charity = getArguments().getParcelable(Constants.CHARITY);
            }

            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.dialog_write_review, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        submitReviewButton = (TextView) view.findViewById(R.id.submit_review_button);
        charityReviewHeader = (TextView) view.findViewById(R.id.charity_header_view);
        reviewInput = (EditText) view.findViewById(R.id.review_input);
        ratingBar = (AppCompatRatingBar) view.findViewById(R.id.charity_rating);

        ratingBar.setOnRatingBarChangeListener(this);
        submitReviewButton.setOnClickListener(this);

        loadCharityData(charity);
    }


    private Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String comment = reviewInput.getText().toString().trim();
        String rating = ZakatifyUtils.formatNumberAmount(ratingBar.getRating());
        map.put(Constants.COMMENT, comment);
        map.put(Constants.RATING, rating);
        map.put(Constants.USERNAME, user.getUsername());
        return map;
    }

    private void loadCharityData(Charity charity) {
        if (charity != null) {
            charityReviewHeader.setText(getString(R.string.review_prompt, charity.getCharityName()));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_review_button:
                if (NetworkUtils.isConnected(getContext())) {
                    if (validateFields(new EditText[]{reviewInput})) {
                        String message = getString(R.string.write_review_progress_message);
                        showLoadingIndicator(message);
                        writeReviewRequest(String.valueOf(charity.getCharityId()), getUserInput());
                    }
                } else {
                    CommonUtils.displayShortToastMessage(getString(R.string.network_connection_label));
                }
                break;
        }
    }

    public void setReviewCreatedListener(ZakatifyInterfaces.ReviewCreatedListener reviewCreatedListener) {
        this.listener = reviewCreatedListener;
    }

    private void writeReviewRequest(String charityId, Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(Constants.WRITE_CHARITY_REVIEW_REQUEST, charityId, accountMap, new ApiClientListener.WriteCharityReviewListener() {
            @Override
            public void onReviewSaved(ReviewsResponse reviewsResponse) {
                hideLoadingIndicator();
                if (reviewsResponse != null) {
                    if (reviewsResponse.getStatus() == null && reviewsResponse.getMessage() == null) {
                        CommonUtils.displayShortToastMessage(getString(R.string.review_saved_success));
                        if (listener != null) {
                            listener.onReviewCreated();
                        }
                        dismiss();
                    } else {
                        CommonUtils.displayShortToastMessage(String.valueOf(reviewsResponse.getMessage()));
                    }
                } else {
                    CommonUtils.displayShortToastMessage(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

    }
}
