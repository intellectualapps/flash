package com.affinislabs.zakatify.app.api.responses;

public class GoldValueResponse extends DefaultResponse {
    private double value;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

}
