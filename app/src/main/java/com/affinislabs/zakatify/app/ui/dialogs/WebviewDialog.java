package com.affinislabs.zakatify.app.ui.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.TooltipHelp;
import com.affinislabs.zakatify.app.ui.activities.PaypalActivity;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.PaypalWebViewClient;

public class WebviewDialog extends BaseDialogFragment implements View.OnClickListener {
    private String url, title;
    private TextView dialogTitle;
    private WebView webView;
    private ImageView dismissDialogButton;

    public static DialogFragment getInstance(Bundle args) {
        WebviewDialog frag = new WebviewDialog();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            url = getArguments().getString(Constants.URL, "");
            title = getArguments().getString(Constants.TITLE, "Zakatify");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        View view = inflater.inflate(R.layout.dialog_web_view, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        webView = view.findViewById(R.id.web_view);
        dialogTitle = view.findViewById(R.id.dialog_title);
        dismissDialogButton = view.findViewById(R.id.dismiss_dialog);
        dismissDialogButton.setOnClickListener(this);

        dialogTitle.setText(title);

        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        CookieManager.getInstance().setAcceptCookie(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.loadUrl(url);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dismiss_dialog:
                dismiss();
                break;
        }
    }
}
