package com.affinislabs.zakatify.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.OnBoardingContent;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.utils.Constants;

public class OnBoardingChildFragment extends Fragment {
    private User user;
    private ImageView contentIconView;
    private TextView contentHeadingView, contentDescriptionView;
    private OnBoardingContent onBoardingContent;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new OnBoardingChildFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public OnBoardingChildFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            onBoardingContent = getArguments().getParcelable(Constants.ONBOARDING_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.onboarding_child_screen, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        contentHeadingView = (TextView) view.findViewById(R.id.content_heading_view);
        contentDescriptionView = (TextView) view.findViewById(R.id.content_description_view);
        contentIconView = (ImageView) view.findViewById(R.id.content_icon_view);
        setData(onBoardingContent);
    }

    private void setData(OnBoardingContent onBoardingContent) {
        contentHeadingView.setText(onBoardingContent.getHeading());
        contentDescriptionView.setText(onBoardingContent.getDescription());
        contentIconView.setImageResource(onBoardingContent.getIconResource());
    }
}
