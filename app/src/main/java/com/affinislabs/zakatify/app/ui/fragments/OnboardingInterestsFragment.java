package com.affinislabs.zakatify.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.CategoryResponse;
import com.affinislabs.zakatify.app.models.Category;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.ProfileActivity;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.affinislabs.zakatify.app.utils.Transformers;
import com.greenfrvr.hashtagview.HashtagView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OnboardingInterestsFragment extends BaseFragment implements View.OnClickListener, HashtagView.TagsSelectListener {
    private User user;
    private Button finishButton;
    private Toolbar toolbar;
    private ArrayList<Category> categories;
    private HashtagView mInterestsView;
    public static List<String> DATA = new ArrayList<String>();
    public static List<String> mSavedInterests = new ArrayList<String>();

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new OnboardingInterestsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public OnboardingInterestsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = getArguments().getParcelable(Constants.USER);
        }
        categories = new ArrayList<Category>();
        categories = PreferenceStorageManager.getCategories(ZakatifyApplication.getAppInstance().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboarding_interests, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((ProfileActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        finishButton = (Button) view.findViewById(R.id.finish_button);
        finishButton.setOnClickListener(this);
        mInterestsView = (HashtagView) view.findViewById(R.id.interests_view);
        mInterestsView.addOnTagSelectListener(this);

        fetchCategories(user.getUsername());
    }

    private void loadData() {
        mInterestsView.setData(DATA, Transformers.CAPITALIZE_TAG, new HashtagView.DataSelector<String>() {
            @Override
            public boolean preselect(String item) {
                return mSavedInterests.contains(item);
            }
        });
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.interests_update_loading);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.finish_button:
                if (NetworkUtils.isConnected(getContext())) {
                    if (mInterestsView.getSelectedItems() != null && mInterestsView.getSelectedItems().size() > 0) {
                        String selectedInterestsParam = getSelectedInterests();
                        showLoadingIndicator(message);
                        updateUserCategories(user.getUsername(), selectedInterestsParam);
                    } else {
                        showErrorPopup("Please select your interests");
                    }
                }
                break;
        }
    }

    private String getCategoryId(String description) {
        for (Category category : categories) {
            if (category.getDescription().equalsIgnoreCase(description)) {
                return category.getId();
            }
        }

        return null;
    }

    private String getSelectedInterests() {
        List<String> selectedInterests = new ArrayList<String>();
        for (Object categoryObject : mInterestsView.getSelectedItems()) {
            selectedInterests.add(getCategoryId(String.valueOf(categoryObject)));
        }

        return TextUtils.join(",", selectedInterests.toArray());
    }

    private void fetchCategories(String username) {
        String message = "We're loading categories, please wait";
        showLoadingIndicator(message);
        new ApiClient.NetworkCallsRunner(Constants.FETCH_CATEGORIES_REQUEST, username, new ApiClientListener.FetchCategoriesListener() {
            @Override
            public void onCategoriesFetched(CategoryResponse categoryResponse) {
                hideLoadingIndicator();
                if (categoryResponse != null) {
                    if (categoryResponse.getStatus() == null && categoryResponse.getMessage() == null) {
                        categories = categoryResponse.getCategories();
                        String[] categoryNames = ListUtils.getCategoryNames(categories);
                        DATA = Arrays.asList(categoryNames);

                        loadData();
                        PreferenceStorageManager.saveCategories(ZakatifyApplication.getAppInstance().getApplicationContext(),categories);
                    } else {
                        showErrorPopup(categoryResponse.getMessage());
                    }

                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void fetchUserCategories(String username) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_CATEGORIES_REQUEST, username, new ApiClientListener.FetchCategoriesListener() {
            @Override
            public void onCategoriesFetched(CategoryResponse categoryResponse) {
                if (categoryResponse != null) {
                    if (categoryResponse.getStatus() == null && categoryResponse.getMessage() == null) {
                        categories = categoryResponse.getCategories();
                        PreferenceStorageManager.saveUserCategories(ZakatifyApplication.getAppInstance().getApplicationContext(), categoryResponse.getCategories());
                    } else {
                        showErrorPopup(categoryResponse.getMessage());
                    }

                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void updateUserCategories(String username, String categoryIds) {
        new ApiClient.NetworkCallsRunner(Constants.UPDATE_USER_CATEGORIES_REQUEST, username, categoryIds, new ApiClientListener.UpdateUserCategoriesListener() {
            @Override
            public void onCategoriesUpdated(CategoryResponse categoryResponse) {
                hideLoadingIndicator();
                if (categoryResponse != null) {
                    if (categoryResponse.getStatus() == null && categoryResponse.getMessage() == null) {
                        categories = categoryResponse.getCategories();
                        PreferenceStorageManager.saveUserCategories(ZakatifyApplication.getAppInstance().getApplicationContext(), categoryResponse.getCategories());
                        PreferenceStorageManager.setInterestsOnBoarded(ZakatifyApplication.getAppInstance().getApplicationContext(), true);
                        user.setUserInterests(categoryResponse.getCategories());
                        showMainActivity(user, getActivity());
                    } else {
                        showErrorPopup(categoryResponse.getMessage());
                    }

                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onItemSelected(Object item, boolean selected) {

    }
}
