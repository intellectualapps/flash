package com.affinislabs.zakatify.app.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.CharitiesResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.CharityActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.SearchCharitiesAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PaginationScrollListener;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;

import java.util.ArrayList;
import java.util.List;


public class SearchCharitiesFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, ZakatifyInterfaces.SearchQueryListener, ZakatifyInterfaces.PaginationAdapterCallback {
    private CustomRecyclerView charitiesRecyclerview;
    private List<Charity> searchResults = new ArrayList<Charity>();
    private SearchCharitiesAdapter adapter;
    public static ZakatifyInterfaces.SearchQueryListener searchQueryListener;
    private boolean SEARCH_ACTIVE = false;
    private int pageNumber = 1;
    private int currentPage = pageNumber;
    private User user;
    private String username;
    private String query;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new SearchCharitiesFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public SearchCharitiesFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchQueryListener = this;

        if (getArguments() != null && getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
        }
        isLoading = false;
        hasListEnded = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_charities, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        adapter = new SearchCharitiesAdapter(getContext(), this, user.getUsername(), this);
        charitiesRecyclerview = view.findViewById(R.id.charity_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        charitiesRecyclerview.setAdapter(adapter);
        charitiesRecyclerview.setLayoutManager(linearLayoutManager);
        charitiesRecyclerview.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        charitiesRecyclerview.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        if (searchResults != null && searchResults.size() >= pageSize) {
                            performSearch(username, query, currentPage, pageSize, true);
                        }
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void loadAdapter(List<Charity> userCharities) {
        SearchViewPagerFragment.searchProgressListener.adjustSearchState(false);
        if (isAdded() && isVisible()) {
            adapter.addAll(userCharities);
        }
    }

    private void updateSavedCharities(List<Charity> charities) {
        PreferenceStorageManager.saveUserCharities(ZakatifyApplication.getAppInstance().getApplicationContext(), charities);
    }

    private void updateCharityList(List<Charity> charityListParam) {
        if (searchResults == null) {
            searchResults = new ArrayList<Charity>();
        }
        if (charityListParam != null && charityListParam.size() > 0) {
            searchResults.addAll(charityListParam);
        }
    }

    private void performSearch(String username, String query, int pageNumber, final int pageSize, final boolean inLoadMoreMode) {
        new ApiClient.NetworkCallsRunner(Constants.SEARCH_CHARITIES_REQUEST, username, query, String.valueOf(pageNumber), String.valueOf(pageSize), new ApiClientListener.SearchCharitiesListener() {
            @Override
            public void onSearchFinished(CharitiesResponse charitiesResponse) {
                SEARCH_ACTIVE = false;
                SearchViewPagerFragment.searchProgressListener.adjustSearchState(false);
                isLoading = false;

                if (inLoadMoreMode) {
                    adapter.removeLoadingFooter(SearchCharitiesAdapter.class, adapter);
                }
                if (charitiesResponse != null) {
                    if (charitiesResponse.getStatus() == null && charitiesResponse.getMessage() == null) {
                        List<Charity> charityListParam = charitiesResponse.getCharities();

                        if (charityListParam == null || charityListParam.size() == 0 || charityListParam.size() < pageSize) {
                            hasListEnded = true;
                        }

                        if (!inLoadMoreMode) {
                            if (searchResults != null) {
                                searchResults.clear();
                            }
                            adapter.clearItems();
                        }

                        updateCharityList(charityListParam);

                        loadAdapter(charityListParam);

                        if (!hasListEnded) {
                            adapter.addLoadingFooter(SearchCharitiesAdapter.class, adapter);
                        }

                    } else {
                        showErrorPopup(charitiesResponse.getDeveloperMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1) {
            switch (v.getId()) {
                default:
                    Intent intent = new Intent(getContext(), CharityActivity.class);
                    intent.putExtra(Constants.CHARITY, searchResults.get(position));
                    intent.putExtra(Constants.USER, user);
                    intent.putExtra(Constants.VIEW_TYPE, Constants.CHARITY_DETAILS_VIEW_TAG);
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onQueryEntered(String query, String username) {
        if (query != null && query.length() > 0 && !SEARCH_ACTIVE) {
            SEARCH_ACTIVE = true;
            this.query = query;
            this.username = username;
            performSearch(username, query, 1, pageSize, false);
        }
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getContext())) {
            performSearch(username, query, currentPage, pageSize, true);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }
}
