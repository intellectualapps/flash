package com.affinislabs.zakatify.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.ApiModule;
import com.affinislabs.zakatify.app.api.responses.LoginResponse;
import com.affinislabs.zakatify.app.api.responses.UsernameCheckResponse;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;

import java.util.HashMap;
import java.util.Map;


public class CreateUsernameFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener, ApiClientListener.UsernameAvailabilityListener {

    private Map<String, String> profileData;
    private User user;
    private EditText emailAddressInput, usernameInput;
    private TextView fullNameView, usernameView;
    private ImageView profilePhoto;
    private Button createUsernameButton;
    private boolean isUsernameUnique = false;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new CreateUsernameFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public CreateUsernameFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = getArguments().getParcelable(Constants.USER);
            profileData = (Map<String, String>) getArguments().getSerializable(Constants.PROFILE_DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_username, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        createUsernameButton = (Button) view.findViewById(R.id.create_username_button);
        emailAddressInput = (EditText) view.findViewById(R.id.email_address_input);
        usernameInput = (EditText) view.findViewById(R.id.username_input);
        fullNameView = (TextView) view.findViewById(R.id.fullname);
        usernameView = (TextView) view.findViewById(R.id.username);
        profilePhoto = (ImageView) view.findViewById(R.id.profile_photo);

        usernameInput.setOnFocusChangeListener(this);
        createUsernameButton.setOnClickListener(this);
        loadUserData();
    }

    private void loadUserData() {
        fullNameView.setText(profileData.get(Constants.FULL_NAME));
        emailAddressInput.setText(profileData.get(Constants.EMAIL_ADDRESS));

        if (profileData.get(Constants.PROFILE_PHOTO) != null) {
            ImageUtils.loadImageUrl(profilePhoto, getContext(), profileData.get(Constants.PROFILE_PHOTO));
        }

        if (profileData.get(Constants.USERNAME) != null) {
            usernameView.setText(profileData.get(Constants.USERNAME));
            usernameView.setVisibility(View.VISIBLE);
            usernameInput.setText(profileData.get(Constants.USERNAME));
        } else {
            usernameView.setVisibility(View.GONE);
        }
    }

    private Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String emailAddress = emailAddressInput.getText().toString().trim();
        String username = usernameInput.getText().toString().trim();
        map.put(Constants.EMAIL_ADDRESS, emailAddress);
        map.put(Constants.USERNAME, username);
        map.put(Constants.TEMP_KEY, user.getTempKey());
        return map;
    }

    private void makeAPICall(Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(Constants.CREATE_USERNAME_REQUEST, accountMap, new ApiClientListener.UsernameCreationListener() {
            @Override
            public void onUsernameCreated(LoginResponse loginResponse) {
                hideLoadingIndicator();
                if (loginResponse != null) {
                    if (loginResponse.getStatus() == null && loginResponse.getMessage() == null) {
                        User user = extractUserDetails(loginResponse);
                        if (user.getAuthToken() == null) {
                            user.setAuthToken(PreferenceStorageManager.getAuthToken(ZakatifyApplication.getAppInstance().getApplicationContext()));
                        }
                        PreferenceStorageManager.saveProfileData(ZakatifyApplication.getAppInstance().getApplicationContext(), profileData);
                        PreferenceStorageManager.clearOnBoardingPreferences(ZakatifyApplication.getAppInstance().getApplicationContext());
                        ApiClient.NetworkCallsRunner.resetApiService();
                        ApiModule.resetApiClient();

                        saveUserData(ZakatifyApplication.getAppInstance().getApplicationContext(), user);
                        showMainActivity(user, getActivity());
                    } else {
                        showErrorPopup(loginResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.creating_username_progress);
        switch (v.getId()) {
            case R.id.create_username_button:
                if (isUsernameUnique) {
                    if (validateFields(new EditText[]{emailAddressInput, usernameInput})) {
                        if (NetworkUtils.isConnected(getContext())) {
                            hideKeyboard();
                            showLoadingIndicator(message);
                            makeAPICall(getUserInput());
                        } else {
                            showErrorPopup(getString(R.string.network_connection_label));
                        }
                    }
                } else {
                    usernameInput.requestFocus();
                    usernameInput.setError("Please choose a unique username");
                    showErrorPopup(getString(R.string.username_verification_error));
                }
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.username_input:
                if (!hasFocus) {
                    final String usernameToCheck = usernameInput.getText().toString();
                    if (usernameToCheck.length() > 0) {
                        ApiClient.NetworkCallsRunner.checkUsernameAvailability(usernameToCheck, CreateUsernameFragment.this);
                    }
                }
                break;
        }
    }

    @Override
    public void onUsernameChecked(UsernameCheckResponse usernameCheckResponse) {
        if (usernameCheckResponse != null) {
            if (usernameCheckResponse.getStatus()) {
                usernameInput.setError(null);
                isUsernameUnique = true;
            } else {
                usernameInput.requestFocus();
                showErrorPopup(getString(R.string.username_verification_error));
                isUsernameUnique = false;
            }
        }
    }
}
