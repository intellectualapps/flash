package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class UserMenu implements Parcelable {
    public UserMenu() {

    }

    public UserMenu(Parcel in) {
        user = in.readParcelable(User.class.getClassLoader());
        donationProgress = in.readParcelable(DonationProgress.class.getClassLoader());
        userBoard = in.readParcelable(UserBoard.class.getClassLoader());
        notification = in.readParcelable(Notification.class.getClassLoader());
        messageData = in.readParcelable(Message.class.getClassLoader());
    }

    public static final Creator<UserMenu> CREATOR = new Creator<UserMenu>() {
        @Override
        public UserMenu createFromParcel(Parcel in) {
            return new UserMenu(in);
        }

        @Override
        public UserMenu[] newArray(int size) {
            return new UserMenu[size];
        }
    };

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DonationProgress getDonationProgress() {
        return donationProgress;
    }

    public void setDonationProgress(DonationProgress donationProgress) {
        this.donationProgress = donationProgress;
    }

    public UserBoard getUserBoard() {
        return userBoard;
    }

    public void setUserBoard(UserBoard userBoard) {
        this.userBoard = userBoard;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public Message getMessageData() {
        return messageData;
    }

    public void setMessageData(Message messageData) {
        this.messageData = messageData;
    }

    private User user;
    private DonationProgress donationProgress;
    private UserBoard userBoard;
    private Notification notification;
    private Message messageData;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(user, flags);
        dest.writeParcelable(donationProgress, flags);
        dest.writeParcelable(userBoard, flags);
        dest.writeParcelable(notification, flags);
        dest.writeParcelable(messageData, flags);
    }
}
