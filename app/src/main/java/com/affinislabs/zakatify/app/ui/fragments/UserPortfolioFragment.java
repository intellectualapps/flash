package com.affinislabs.zakatify.app.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.CharitiesResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.CharityActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.CharitiesAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PaginationScrollListener;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;

import java.util.ArrayList;
import java.util.List;


public class UserPortfolioFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, ZakatifyInterfaces.PaginationAdapterCallback {
    private CustomRecyclerView charitiesRecyclerview;
    private List<Charity> userCharities = new ArrayList<Charity>();
    private CharitiesAdapter charitiesAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private User user;
    private IntentFilter intentFilter;
    private int pageNumber = 1;
    private int currentPage = pageNumber;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new UserPortfolioFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public UserPortfolioFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            user = getArguments().getParcelable(Constants.USER);
        }

        isLoading = false;
        hasListEnded = false;
        userCharities = PreferenceStorageManager.getUserCharities(ZakatifyApplication.getAppInstance().getApplicationContext());
        intentFilter = new IntentFilter(Constants.REFRESH_CHARITY_LIST_FILTER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_portfolio, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.edit_portfolio_label));
        charitiesAdapter = new CharitiesAdapter(getContext(), this, this, user.getUsername());
        charitiesRecyclerview = view.findViewById(R.id.charity_list);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);
        mRetryButton = mEmptyView.findViewById(R.id.retry_button);
        mEmptyViewLabel.setText(getString(R.string.no_records_label));

        charitiesRecyclerview.setEmptyView(mEmptyView);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blue),
                ContextCompat.getColor(getContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        currentPage = 1;
                        fetchUserCharities(user.getUsername(), 1, pageSize, false);
                    } else {
                        endSwipeRefresh(swipeRefreshLayout);
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        charitiesRecyclerview.setAdapter(charitiesAdapter);
        charitiesRecyclerview.setLayoutManager(linearLayoutManager);
        charitiesRecyclerview.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        charitiesRecyclerview.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        if (userCharities != null && userCharities.size() >= pageSize) {
                            fetchUserCharities(user.getUsername(), currentPage, pageSize, true);
                        }
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        loadAdapter(userCharities);
        initializeData();
        registerReceiver(getContext(), charityStateBroadcast, intentFilter);
    }

    private void initializeData() {
        if (NetworkUtils.isConnected(getContext())) {
            animateSwipeRefreshLayout(swipeRefreshLayout);
            showLoadingIndicator(getString(R.string.fetching_user_profile_message));
            fetchUserCharities(user.getUsername(), 1, pageSize, false);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }

    private void loadAdapter(List<Charity> userCharities) {
        charitiesAdapter.addAll(userCharities);
    }

    private void updateSavedCharities(List<Charity> charities) {
        PreferenceStorageManager.saveUserCharities(ZakatifyApplication.getAppInstance().getApplicationContext(), charities);
    }

    private void updateCharityList(List<Charity> charityListParam) {
        if (userCharities == null) {
            userCharities = new ArrayList<Charity>();
        }
        if (charityListParam != null && charityListParam.size() > 0) {
            userCharities.addAll(charityListParam);
        }
    }

    private void fetchUserCharities(String username, int pageNumber, final int pageSize, final boolean inLoadMoreMode) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_PORTFOLIO_REQUEST, username, String.valueOf(pageNumber), String.valueOf(pageSize), new ApiClientListener.FetchUserCharitiesListener() {
            @Override
            public void onUserCharitiesFetched(CharitiesResponse charitiesResponse) {
                endSwipeRefresh(swipeRefreshLayout);
                hideLoadingIndicator();
                isLoading = false;

                if (inLoadMoreMode) {
                    charitiesAdapter.removeLoadingFooter(CharitiesAdapter.class, charitiesAdapter);
                }

                if (charitiesResponse != null) {
                    if (charitiesResponse.getStatus() == null && charitiesResponse.getMessage() == null) {
                        List<Charity> charityListParam = charitiesResponse.getCharities();

                        if (charityListParam == null || charityListParam.size() == 0 || charityListParam.size() < pageSize) {
                            hasListEnded = true;
                        }

                        if (!inLoadMoreMode) {
                            if (userCharities != null) {
                                userCharities.clear();
                            }
                            charitiesAdapter.clearItems();
                            updateSavedCharities(charityListParam);
                        }

                        updateCharityList(charityListParam);
                        if (isAdded() && isVisible()) {
                            loadAdapter(charityListParam);
                        }

                        if (!hasListEnded) {
                            charitiesAdapter.addLoadingFooter(CharitiesAdapter.class, charitiesAdapter);
                        }
                    } else {
                        showErrorPopup(charitiesResponse.getDeveloperMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1) {
            switch (v.getId()) {
                default:
                    Intent intent = new Intent(getContext(), CharityActivity.class);
                    intent.putExtra(Constants.CHARITY, userCharities.get(position));
                    intent.putExtra(Constants.USER, user);
                    intent.putExtra(Constants.VIEW_TYPE, Constants.CHARITY_DETAILS_VIEW_TAG);
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(getContext(), charityStateBroadcast);
    }

    private BroadcastReceiver charityStateBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getIntExtra(Constants.POSITION, -1);
            boolean shouldReloadList = intent.getBooleanExtra(Constants.REFRESH_FLAG, false);
            if (position > -1) {
                charitiesAdapter.remove(position);
            }
        }
    };

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getContext())) {
            fetchUserCharities(user.getUsername(), currentPage, pageSize, true);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }
}
