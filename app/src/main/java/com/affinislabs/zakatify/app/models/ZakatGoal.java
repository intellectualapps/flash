package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ZakatGoal implements Parcelable {

    private Double amount;
    private Double cash;
    private Integer frequencyId;
    private Double gold;
    private Double investment;
    private Double liability;
    private String period;
    private Double personalUse;
    private Integer progress;
    private Double realEstate;
    private String username;
    private Boolean state = false;

    public ZakatGoal() {

    }

    public ZakatGoal(Double amount, Double cash, Integer frequencyId, Double gold, Double investment, Double liability, String period, Double personalUse, Integer progress, Double realEstate, String username, Boolean state) {
        this.amount = amount;
        this.cash = cash;
        this.frequencyId = frequencyId;
        this.gold = gold;
        this.investment = investment;
        this.liability = liability;
        this.period = period;
        this.personalUse = personalUse;
        this.progress = progress;
        this.realEstate = realEstate;
        this.username = username;
        this.state = state;
    }

    private ZakatGoal(Parcel in) {
        amount = in.readDouble();
        cash = in.readDouble();
        frequencyId = in.readInt();
        gold = in.readDouble();
        investment = in.readDouble();
        liability = in.readDouble();
        period = in.readString();
        personalUse = in.readDouble();
        progress = in.readInt();
        realEstate = in.readDouble();
        username = in.readString();
        state = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(amount != null && amount > 0.0 ? amount : 0.0);
        dest.writeDouble(cash);
        dest.writeInt(frequencyId);
        dest.writeDouble(gold);
        dest.writeDouble(investment);
        dest.writeDouble(liability);
        dest.writeString(period);
        dest.writeDouble(personalUse);
        dest.writeInt(progress);
        dest.writeDouble(realEstate);
        dest.writeString(username);
        dest.writeByte((byte) (state ? 1 : 0));
    }

    public static final Creator<ZakatGoal> CREATOR = new Creator<ZakatGoal>() {
        @Override
        public ZakatGoal createFromParcel(Parcel in) {
            return new ZakatGoal(in);
        }

        @Override
        public ZakatGoal[] newArray(int size) {
            return new ZakatGoal[size];
        }
    };

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    public Integer getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(Integer frequencyId) {
        this.frequencyId = frequencyId;
    }

    public Double getGold() {
        return gold;
    }

    public void setGold(Double gold) {
        this.gold = gold;
    }

    public Double getInvestment() {
        return investment;
    }

    public void setInvestment(Double investment) {
        this.investment = investment;
    }

    public Double getLiability() {
        return liability;
    }

    public void setLiability(Double liability) {
        this.liability = liability;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Double getPersonalUse() {
        return personalUse;
    }

    public void setPersonalUse(Double personalUse) {
        this.personalUse = personalUse;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public Double getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(Double realEstate) {
        this.realEstate = realEstate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
