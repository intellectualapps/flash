package com.affinislabs.zakatify.app.utils;

import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.FCMTokenResponse;
import com.affinislabs.zakatify.app.models.User;

import java.util.HashMap;
import java.util.Map;

public class FCMTokenUtils {
    public static void refreshToken() {
        String FCMToken = AppUtils.getFCMToken(ZakatifyApplication.getAppInstance().getApplicationContext());
        User user = PreferenceStorageManager.getUser(ZakatifyApplication.getAppInstance().getApplicationContext());
        if (FCMToken != null && user != null && user.getUsername() != null) {
            PreferenceStorageManager.saveFCMToken(ZakatifyApplication.getAppInstance().getApplicationContext(), FCMToken);
            Map<String, String> metaDataMap = new HashMap<String, String>();
            metaDataMap.put(Constants.FCM_TOKEN, PreferenceStorageManager.getFCMToken(ZakatifyApplication.getAppInstance().getApplicationContext()));
            metaDataMap.put(Constants.DEVICE_ID, PreferenceStorageManager.getDeviceId(ZakatifyApplication.getAppInstance().getApplicationContext()));
            metaDataMap.put(Constants.DEVICE_PLATFORM_KEY, Constants.DEVICE_PLATFORM);
            uploadFCMToken(user.getUsername(), metaDataMap);
        }
    }

    private static void uploadFCMToken(String username, Map<String, String> metaDataMap) {
        new ApiClient.NetworkCallsRunner(Constants.UPLOAD_FCM_TOKEN_REQUEST, username, metaDataMap, new ApiClientListener.UploadFCMTokenListener() {
            @Override
            public void onFCMTokenUploaded(FCMTokenResponse fcmTokenResponse) {
            }
        }).execute();
    }
}
