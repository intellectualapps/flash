package com.affinislabs.zakatify.app.interfaces;

import android.net.Uri;

import com.affinislabs.zakatify.app.enums.InviteState;
import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.Invite;
import com.affinislabs.zakatify.app.models.User;
import com.google.firebase.database.DatabaseError;

import java.util.List;
import java.util.Map;

public class ZakatifyInterfaces {
    public interface ZakatCalculatorListener {
        void onAmountCalculated(double amount, Map<String, String> zakatCalculationMap);
    }

    public interface SocialAuthenticationListener {
        void onAuthenticationCompleted(Map<String, String> accountData, Map<String, String> profileData);
    }

    public interface UserProfileUpdateListener {
        void onProfileUpdated(User user);
    }

    public interface SearchQueryListener {
        void onQueryEntered(String query, String username);
    }

    public interface SearchViewpagerChangeListener {
        void onViewpagerChanged(int viewpagerIndex);
    }

    public interface SearchProgressListener {
        void adjustSearchState(boolean state);
    }

    public interface RefreshCharitiesListener {
        void onCharityStateChanged();
    }

    public interface ReviewCreatedListener {
        void onReviewCreated();
    }

    public interface SessionInitializationListener {
        void onSessionInitialized(ChatSession chatSession);
    }

    public interface MessagePaginationListener {
        void onLoadMore(int position, String messageKey);
    }

    public interface ContactLoaderListener {
        void onContactsLoaded(List<Invite> inviteList);
    }

    public interface ContactStateChangedListener {
        void onInviteStateChanged(int position, InviteState inviteState);
    }

    public interface PaginationAdapterCallback {
        void loadMoreItems();
    }

    public interface ImageOptimizationListener {
        void onImageOptimized(Uri mFileUri, byte[] bytes);
    }

    public interface ThreadDeletionCloseCallback {
        void onThreadDeletionCompleted(String id, int position);

        void onThreadDeletionDismissed(String id);
    }

    public interface FirebaseChatDeletionCallback{
        void onThreadDeleted(DatabaseError databaseError);
    }
}
