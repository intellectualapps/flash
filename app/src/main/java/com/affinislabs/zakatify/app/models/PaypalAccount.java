package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaypalAccount implements Parcelable{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("paypalEmail")
    @Expose
    private String paypalEmail;
    @SerializedName("paypalFirstName")
    @Expose
    private String paypalFirstName;
    @SerializedName("paypalLastName")
    @Expose
    private String paypalLastName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("dateCreated")
    @Expose
    private String dateCreated;
    private int drawableResource;

    public PaypalAccount() {

    }

    public PaypalAccount(String id, String paypalEmail, String paypalFirstName, String paypalLastName, String status, String dateCreated) {
        this.id = id;
        this.paypalEmail = paypalEmail;
        this.paypalFirstName = paypalFirstName;
        this.paypalLastName = paypalLastName;
        this.status = status;
        this.dateCreated = dateCreated;
    }

    protected PaypalAccount(Parcel in) {
        id = in.readString();
        paypalEmail = in.readString();
        paypalFirstName = in.readString();
        paypalLastName = in.readString();
        status = in.readString();
        dateCreated = in.readString();
        drawableResource = in.readInt();
    }

    public static final Creator<PaypalAccount> CREATOR = new Creator<PaypalAccount>() {
        @Override
        public PaypalAccount createFromParcel(Parcel in) {
            return new PaypalAccount(in);
        }

        @Override
        public PaypalAccount[] newArray(int size) {
            return new PaypalAccount[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaypalEmail() {
        return paypalEmail;
    }

    public void setPaypalEmail(String paypalEmail) {
        this.paypalEmail = paypalEmail;
    }

    public String getPaypalFirstName() {
        return paypalFirstName;
    }

    public void setPaypalFirstName(String paypalFirstName) {
        this.paypalFirstName = paypalFirstName;
    }

    public String getPaypalLastName() {
        return paypalLastName;
    }

    public void setPaypalLastName(String paypalLastName) {
        this.paypalLastName = paypalLastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getDrawableResource() {
        return drawableResource;
    }

    public void setDrawableResource(int drawableResource) {
        this.drawableResource = drawableResource;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(paypalEmail);
        dest.writeString(paypalFirstName);
        dest.writeString(paypalLastName);
        dest.writeString(status);
        dest.writeString(dateCreated);
        dest.writeInt(drawableResource);
    }
}
