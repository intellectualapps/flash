package com.affinislabs.zakatify.app.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.ReviewsResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.Review;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.CharityActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.CharitiesAdapter;
import com.affinislabs.zakatify.app.ui.adapters.UserReviewsAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;


public class UserReviewsFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, ZakatifyInterfaces.PaginationAdapterCallback {
    private CustomRecyclerView userReviewsRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<Review> reviews;
    private User user;
    private int pageNumber = 1;
    private int currentPage = pageNumber;
    private UserReviewsAdapter adapter;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new UserReviewsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public UserReviewsFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reviews = new ArrayList<Review>();
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER_REVIEWS_LIST)) {
                reviews = getArguments().getParcelableArrayList(Constants.USER_REVIEWS_LIST);
            }

            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }
        isLoading = false;
        hasListEnded = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_reviews, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.user_reviews_toolbar_label));

        adapter = new UserReviewsAdapter(getContext(), this, user, this);
        userReviewsRecyclerView = view.findViewById(R.id.user_reviews_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blue),
                ContextCompat.getColor(getContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        currentPage = 1;
                        fetchUserReviews(user.getUsername(), 1, pageSize, false);

                    } else {
                        endSwipeRefresh(swipeRefreshLayout);
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);
        mRetryButton = mEmptyView.findViewById(R.id.retry_button);
        mEmptyViewLabel.setText(getString(R.string.no_records_label));

        userReviewsRecyclerView.setEmptyView(mEmptyView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        userReviewsRecyclerView.setAdapter(adapter);
        userReviewsRecyclerView.setLayoutManager(linearLayoutManager);
        userReviewsRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        userReviewsRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        if (reviews != null && reviews.size() >= pageSize) {
                            fetchUserReviews(user.getUsername(), currentPage, pageSize, true);
                        }
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        initializeData();
    }

    private void initializeData() {
        if (NetworkUtils.isConnected(getContext())) {
            animateSwipeRefreshLayout(swipeRefreshLayout);
            showLoadingIndicator(getString(R.string.fetching_user_reviews_message));
            fetchUserReviews(user.getUsername(), 1, pageSize, false);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }

    private void updateReviewsList(List<Review> reviewListParam) {
        if (reviews == null) {
            reviews = new ArrayList<Review>();
        }
        if (reviewListParam != null && reviewListParam.size() > 0) {
            reviews.addAll(reviewListParam);
        }
    }

    private void loadAdapter(List<Review> userReviews) {
        if (isAdded() && isVisible()) {
            adapter.addAll(userReviews);
        }
    }

    private void fetchUserReviews(final String id, int pageNumber, final int pageSize, final boolean inLoadMoreMode) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_REVIEWS_REQUEST, id, String.valueOf(pageNumber), String.valueOf(pageSize), new ApiClientListener.FetchReviewsListener() {
            @Override
            public void onReviewsFetched(ReviewsResponse reviewsResponse) {
                hideLoadingIndicator();
                endSwipeRefresh(swipeRefreshLayout);
                isLoading = false;
                if (inLoadMoreMode) {
                    adapter.removeLoadingFooter(UserReviewsAdapter.class, adapter);
                }

                if (reviewsResponse != null) {
                    if (reviewsResponse.getStatus() == null && reviewsResponse.getMessage() == null) {
                        List<Review> userReviewsParam = reviewsResponse.getReviews();

                        if (userReviewsParam == null || userReviewsParam.size() == 0 || userReviewsParam.size() < pageSize) {
                            hasListEnded = true;
                        }

                        if (!inLoadMoreMode) {
                            if (reviews != null) {
                                reviews.clear();
                            }
                            adapter.clearItems();
                        }

                        updateReviewsList(userReviewsParam);

                        loadAdapter(userReviewsParam);

                        if (!hasListEnded) {
                            adapter.addLoadingFooter(UserReviewsAdapter.class, adapter);
                        }
                    } else {
                        showErrorPopup(reviewsResponse.getDeveloperMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1 && reviews != null && reviews.size() > 0) {
            Review review = reviews.get(position);
            switch (v.getId()) {
                default:
                    break;
                case R.id.charity_name:
                    Intent intent = new Intent(getContext(), CharityActivity.class);
                    Charity charity = new Charity(review.getCharityId(), review.getCharity());
                    intent.putExtra(Constants.CHARITY, charity);
                    intent.putExtra(Constants.USER, user);
                    intent.putExtra(Constants.VIEW_TYPE, Constants.CHARITY_DETAILS_VIEW_TAG);
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getContext())) {
            fetchUserReviews(user.getUsername(), currentPage, pageSize, true);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }
}
