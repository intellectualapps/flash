package com.affinislabs.zakatify.app.ui.customviews;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DonorViewItemDecorator extends RecyclerView.ItemDecoration {
    private final int space;

    public DonorViewItemDecorator(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (parent.getAdapter().getItemCount() > 1 && position < (parent.getAdapter().getItemCount() - 1)) {
            outRect.right = space;
        }
    }
}
