package com.affinislabs.zakatify.app.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ViewFlipper;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.FollowersResponse;
import com.affinislabs.zakatify.app.api.responses.PaymentResponse;
import com.affinislabs.zakatify.app.api.responses.PaypalAccountsResponse;
import com.affinislabs.zakatify.app.models.PaypalAccount;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.ProfileActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.PaymentOptionsAdapter;
import com.affinislabs.zakatify.app.ui.adapters.UsersAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BasePaymentsFragment extends BaseFragment {
    public final static int PAYPAL_ACCOUNT_SET_UP_REQUEST_CODE = 100;
    public final static int PAYPAL_SUCCESS_STATUS_CODE = 1;
    public final static int PAYPAL_FAILED_STATUS_CODE = -1;
    public final static int PAYPAL_CANCELLED_STATUS_CODE = -2;

    public void showInterestsView(User user) {
        PreferenceStorageManager.setPaymentsOnBoarded(ZakatifyApplication.getAppInstance().getApplicationContext(), true);

        updateUserProfile(user);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.USER, user);
        startFragment(OnboardingInterestsFragment.newInstance(bundle), true);
    }

    public void launchFragmentDialog(DialogFragment dialogFragment, Activity activity) {
        FragmentManager fragmentManager = ((BaseActivity) activity).getSupportFragmentManager();
        if (fragmentManager != null && dialogFragment != null) {
            dialogFragment.show(fragmentManager, dialogFragment.getClass().getSimpleName());
        }
    }

    public void fetchPaymentOptions(ApiClientListener.FetchPaypalAccountsListener fetchPaypalAccountsListener, SwipeRefreshLayout swipeRefreshLayout, boolean showLoadingIndicator) {
        if (NetworkUtils.isConnected(getContext())) {
            if(showLoadingIndicator) {
                showLoadingIndicator(getString(R.string.fetching_payment_options));
            }
            fetchPaypalAccounts(swipeRefreshLayout, fetchPaypalAccountsListener);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }

    public PaypalAccount getPrimaryPaymentOption(List<PaypalAccount> paypalAccounts) {
        if (ListUtils.isNotEmpty(paypalAccounts)) {
            for (PaypalAccount paypalAccount : paypalAccounts) {
                if (paypalAccount.getStatus().equalsIgnoreCase(Constants.PRIMARY_LABEL)) {
                    return paypalAccount;
                }
            }
        }

        return null;
    }


    public void confirmDeletePaymentOption(Activity activity, final String paymentOptionId, final SwipeRefreshLayout swipeRefreshLayout, final ApiClientListener.DeletePaymentOptionListener deletePaymentOptionListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(getString(R.string.delete_payment_option_title));
        builder.setMessage(getString(R.string.delete_payment_option_confirmation_conent));
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showLoadingIndicator(getString(R.string.deleting_payment_option));
                deletePaymentOption(paymentOptionId, swipeRefreshLayout, deletePaymentOptionListener);
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    public void confirmSetPrimaryPaymentOption(Activity activity, final String paymentOptionId, final SwipeRefreshLayout swipeRefreshLayout, final ApiClientListener.SetPrimaryPaymentOptionListener setPrimaryPaymentOptionListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(getString(R.string.make_primary_payment_option_title));
        builder.setMessage(getString(R.string.make_primary_payment_option_confirmation_conent));
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showLoadingIndicator(getString(R.string.setting_payment_option_progress));
                makePrimaryOption(paymentOptionId, swipeRefreshLayout, setPrimaryPaymentOptionListener);
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    public void fetchPaypalAccounts(final SwipeRefreshLayout swipeRefreshLayout, final ApiClientListener.FetchPaypalAccountsListener fetchPaypalAccountsListener) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_PAYPAL_ACCOUNTS_REQUEST, new ApiClientListener.FetchPaypalAccountsListener() {
            @Override
            public void onPaypalAccountsFetched(PaypalAccountsResponse paypalAccountsResponse) {
                hideLoadingIndicator();
                endSwipeRefresh(swipeRefreshLayout);
                isLoading = false;
                if (paypalAccountsResponse != null) {
                    if (paypalAccountsResponse.getStatus() == null && paypalAccountsResponse.getMessage() == null) {
                        if (fetchPaypalAccountsListener != null) {
                            fetchPaypalAccountsListener.onPaypalAccountsFetched(paypalAccountsResponse);
                        }
                    } else {
                        showErrorPopup(paypalAccountsResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    public void deletePaymentOption(String paymentOptionId, final SwipeRefreshLayout swipeRefreshLayout, final ApiClientListener.DeletePaymentOptionListener deletePaymentOptionListener) {
        new ApiClient.NetworkCallsRunner(Constants.DELETE_PAYMENT_OPTION_REQUEST, paymentOptionId, new ApiClientListener.DeletePaymentOptionListener() {
            @Override
            public void onPaymentOptionDeleted(PaymentResponse paymentResponse) {
                hideLoadingIndicator();
                endSwipeRefresh(swipeRefreshLayout);
                isLoading = false;
                if (paymentResponse != null) {
                    if (paymentResponse.getStatus() == null && paymentResponse.getMessage() == null) {
                        if (paymentResponse.getState()) {
                            if (deletePaymentOptionListener != null) {
                                deletePaymentOptionListener.onPaymentOptionDeleted(paymentResponse);
                            }
                        } else {
                            showErrorPopup(getString(R.string.delete_payment_option_error));
                        }
                    } else {
                        showErrorPopup(paymentResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    public void makePrimaryOption(String paymentOptionId, final SwipeRefreshLayout swipeRefreshLayout, final ApiClientListener.SetPrimaryPaymentOptionListener setPrimaryPaymentOptionListener) {
        new ApiClient.NetworkCallsRunner(Constants.SET_PRIMARY_PAYMENT_OPTION_REQUEST, paymentOptionId, new ApiClientListener.SetPrimaryPaymentOptionListener() {
            @Override
            public void onPrimaryPaymentOptionSet(PaymentResponse paymentResponse) {
                hideLoadingIndicator();
                endSwipeRefresh(swipeRefreshLayout);
                isLoading = false;
                if (paymentResponse != null) {
                    if (paymentResponse.getStatus() == null && paymentResponse.getMessage() == null) {
                        if (setPrimaryPaymentOptionListener != null) {
                            setPrimaryPaymentOptionListener.onPrimaryPaymentOptionSet(paymentResponse);
                        }
                    } else {
                        showErrorPopup(paymentResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }
}
