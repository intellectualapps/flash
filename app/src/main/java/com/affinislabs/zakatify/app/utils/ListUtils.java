package com.affinislabs.zakatify.app.utils;

import com.affinislabs.zakatify.app.models.Category;

import java.util.ArrayList;
import java.util.Collection;

public class ListUtils {
    public static boolean isEmpty(Collection items) {
        return items == null || items.size() < 1;
    }

    public static boolean isNotEmpty(Collection items) {
        return items != null && items.size() > 0;
    }

    public static String[] getCategoryNames(ArrayList<Category> categories) {
        String[] categoryArray = null;
        if (categories != null && categories.size() > 0) {
            categoryArray = new String[categories.size()];
            int i = 0;
            for (Category category : categories) {
                categoryArray[i] = category.getDescription();
                i++;
            }
        }
        return categoryArray;
    }
}
