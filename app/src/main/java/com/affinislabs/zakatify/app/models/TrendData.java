package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class TrendData implements Parcelable {
    private String label;
    private double value;

    public TrendData(String label, double value) {
        this.label = label;
        this.value = value;
    }

    public TrendData(Parcel in) {
        label = in.readString();
        value = in.readDouble();
    }

    public static final Creator<TrendData> CREATOR = new Creator<TrendData>() {
        @Override
        public TrendData createFromParcel(Parcel in) {
            return new TrendData(in);
        }

        @Override
        public TrendData[] newArray(int size) {
            return new TrendData[size];
        }
    };

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeDouble(value);
    }
}
