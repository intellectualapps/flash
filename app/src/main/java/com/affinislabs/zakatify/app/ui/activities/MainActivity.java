package com.affinislabs.zakatify.app.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.ApiModule;
import com.affinislabs.zakatify.app.api.responses.CharitiesResponse;
import com.affinislabs.zakatify.app.api.responses.UserMenuResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.DonationProgress;
import com.affinislabs.zakatify.app.models.Message;
import com.affinislabs.zakatify.app.models.Notification;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.models.UserBoard;
import com.affinislabs.zakatify.app.models.UserMenu;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.CharitiesAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.ui.customviews.CustomTextView;
import com.affinislabs.zakatify.app.utils.CommonUtils;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FCMTokenUtils;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PaginationScrollListener;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.affinislabs.zakatify.app.utils.ZakatifyUtils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, ZakatifyInterfaces.UserProfileUpdateListener, BaseRecyclerAdapter.ClickListener, ZakatifyInterfaces.PaginationAdapterCallback {
    private static final int DRAWER_CLOSER_INTERVAL = 250;
    private User user, notificationUserParam;
    private UserMenu userMenu;
    private ImageView mCloseDrawerButton, mUserProfilePhoto;
    private TextView mFullNameView, mUsernameView;
    private TextView mNotificationCountView, mMessageCountView;
    private TextView mCurrentZakatDonation, mTotalZakatGoal, mPercentageProgress;
    private TextView mRankView, mPointsView;
    private TextView mEmptyViewLabel, mRetryButton;
    private View mEmptyView;
    private View mHomeNav, mPortfolioNav, mPaymentsNav, mTopZakatifiersNav, mInviteFriendsNav, mNotificationsNav, mMessagesNav, mLogoutNav, mPreferencesNav, mProfileNav, mAboutAppNav, mPrivacyNav, mTermsNav, mFAQNav;
    private View userSidebarContainer;
    private DrawerLayout drawer;
    private SwipeRefreshLayout swipeRefreshLayout;

    private ProgressBar zakatProgressMeter;
    private CharitiesAdapter charitiesAdapter;
    private CustomRecyclerView charityRecyclerView;
    private View searchButton;
    private List<Charity> charityList = new ArrayList<Charity>();
    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private int pageNumber = 1, pageSize = 10;
    private int currentPage = pageNumber;
    public static ZakatifyInterfaces.UserProfileUpdateListener userProfileUpdateListener;

    private IntentFilter intentFilter;
    private boolean isPushNotificationMode = false;
    private String viewType, notificationViewType = "default", messageId, onlineUserEmail, offlineUserEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setTitle(getString(R.string.home_toolbar_label));
        setSupportActionBar(toolbar);

        charityList = PreferenceStorageManager.getSuggestedCharities(ZakatifyApplication.getAppInstance().getApplicationContext());

        if (getIntent().getExtras() != null) {
            user = getIntent().getExtras().getParcelable(Constants.USER);
            isPushNotificationMode = getIntent().getExtras().getBoolean(Constants.NOTIFICATION_FLAG);
            messageId = getIntent().getExtras().getString(Constants.PUSH_MESSAGE_ID);
            onlineUserEmail = getIntent().getExtras().getString(Constants.PUSH_ONLINE_EMAIL);
            offlineUserEmail = getIntent().getExtras().getString(Constants.PUSH_OFFLINE_EMAIL);
            notificationViewType = getIntent().getExtras().getString(Constants.NOTIFICATION_VIEW_TAG);
        }

        if (user == null) {
            user = PreferenceStorageManager.getUser(ZakatifyApplication.getAppInstance().getApplicationContext());
        }

        userMenu = PreferenceStorageManager.getUserMenu(ZakatifyApplication.getAppInstance().getApplicationContext());

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        searchButton = findViewById(R.id.search_view);
        searchButton.setOnClickListener(this);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        charityRecyclerView = findViewById(R.id.charity_list);
        mEmptyView = findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);
        mRetryButton = mEmptyView.findViewById(R.id.retry_button);
        mEmptyViewLabel.setText(getString(R.string.no_records_label));

        charityRecyclerView.setEmptyView(mEmptyView);
        charitiesAdapter = new CharitiesAdapter(this, this, this, user.getUsername());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        charityRecyclerView.setAdapter(charitiesAdapter);
        charityRecyclerView.setLayoutManager(linearLayoutManager);
        charityRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        charityRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getApplicationContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        if (charityList != null && charityList.size() >= pageSize) {
                            fetchSuggestedCharities(user.getUsername(), currentPage, pageSize, true);
                        }
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        final NavigationView navigationView = findViewById(R.id.nav_view);
        new Runnable() {
            @Override
            public void run() {
                View headerLayout = navigationView.inflateHeaderView(R.layout.nav_sidebar_header);
                userSidebarContainer = headerLayout.findViewById(R.id.user_sidebar_container);
                mCloseDrawerButton = headerLayout.findViewById(R.id.close_drawer);
                mHomeNav = headerLayout.findViewById(R.id.nav_home);
                mPortfolioNav = headerLayout.findViewById(R.id.nav_portfolio);
                mPaymentsNav = headerLayout.findViewById(R.id.nav_payments);
                mNotificationsNav = headerLayout.findViewById(R.id.nav_notifications);
                mMessagesNav = headerLayout.findViewById(R.id.nav_messages);
                mProfileNav = headerLayout.findViewById(R.id.nav_profile);
                mTopZakatifiersNav = headerLayout.findViewById(R.id.nav_top_zakatifiers);
                mInviteFriendsNav = headerLayout.findViewById(R.id.nav_invite_friends);
                mPreferencesNav = headerLayout.findViewById(R.id.nav_preferences);
                mLogoutNav = headerLayout.findViewById(R.id.nav_logout);
                mFullNameView = headerLayout.findViewById(R.id.fullname);
                mUsernameView = headerLayout.findViewById(R.id.username);
                mUserProfilePhoto = headerLayout.findViewById(R.id.profile_photo);
                zakatProgressMeter = headerLayout.findViewById(R.id.zakat_progress_wheel);
                mCurrentZakatDonation = headerLayout.findViewById(R.id.amount_donated);
                mTotalZakatGoal = headerLayout.findViewById(R.id.total_zakat_goal);
                mPercentageProgress = headerLayout.findViewById(R.id.zakat_percentage_completed);
                mNotificationCountView = headerLayout.findViewById(R.id.unread_notifications_count_view);
                mMessageCountView = headerLayout.findViewById(R.id.unread_messages_count_view);
                mRankView = headerLayout.findViewById(R.id.rank_view);
                mPointsView = headerLayout.findViewById(R.id.points_view);
                mAboutAppNav = headerLayout.findViewById(R.id.nav_about);
                mPrivacyNav = headerLayout.findViewById(R.id.nav_privacy);
                mTermsNav = headerLayout.findViewById(R.id.nav_terms);
                mFAQNav = headerLayout.findViewById(R.id.nav_faq);

                userSidebarContainer.setOnClickListener(MainActivity.this);
                mCloseDrawerButton.setOnClickListener(MainActivity.this);
                mHomeNav.setOnClickListener(MainActivity.this);
                mProfileNav.setOnClickListener(MainActivity.this);
                mNotificationsNav.setOnClickListener(MainActivity.this);
                mTopZakatifiersNav.setOnClickListener(MainActivity.this);
                mInviteFriendsNav.setOnClickListener(MainActivity.this);
                mMessagesNav.setOnClickListener(MainActivity.this);
                mPaymentsNav.setOnClickListener(MainActivity.this);
                mPreferencesNav.setOnClickListener(MainActivity.this);
                mPortfolioNav.setOnClickListener(MainActivity.this);
                mLogoutNav.setOnClickListener(MainActivity.this);
                mUserProfilePhoto.setOnClickListener(MainActivity.this);
                mUsernameView.setOnClickListener(MainActivity.this);
                mFullNameView.setOnClickListener(MainActivity.this);
                mAboutAppNav.setOnClickListener(MainActivity.this);
                mPrivacyNav.setOnClickListener(MainActivity.this);
                mTermsNav.setOnClickListener(MainActivity.this);
                mFAQNav.setOnClickListener(MainActivity.this);
                zakatProgressMeter.setProgress(0);

                if (NetworkUtils.isConnected(getApplicationContext())) {
                    fetchUserMenu(user.getUsername());
                }
            }
        }.run();

        loadAdapter(charityList);

        if (NetworkUtils.isConnected(getApplicationContext())) {
            initializeData();
            FCMTokenUtils.refreshToken();
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(this, R.color.green),
                ContextCompat.getColor(this, R.color.blue),
                ContextCompat.getColor(this, R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(MainActivity.this)) {
                        fetchSuggestedCharities(user.getUsername(), 1, pageSize, false);
                    } else {
                        endSwipeRefresh(swipeRefreshLayout);
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    endSwipeRefresh(swipeRefreshLayout);
                    e.printStackTrace();
                }
            }
        });
        userProfileUpdateListener = this;
        intentFilter = new IntentFilter(Constants.REFRESH_CHARITY_LIST_FILTER);
        registerReceiver(charityStateBroadcast, intentFilter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadUserData(user);
        loadSideMenu(userMenu);

        if (isPushNotificationMode) {
            resolveNotificationView();
        }
    }

    private void resolveNotificationView() {
        ChatSession chatSession = new ChatSession();
        chatSession.setMessageId(messageId);
        chatSession.setOtherParticipant(new User(onlineUserEmail));
        chatSession.setRequestingParticipant(new User(offlineUserEmail));
        switch (notificationViewType) {
            default:
            case Constants.MESSAGE_LIST_VIEW_TAG:
                Intent chatIntent = new Intent(MainActivity.this, ChatActivity.class);
                chatIntent.putExtra(Constants.REQUESTING_PARTICIPANT, chatSession.getRequestingParticipant());
                chatIntent.putExtra(Constants.OTHER_PARTICIPANT, chatSession.getOtherParticipant());
                chatIntent.putExtra(Constants.CHAT_SESSION, chatSession);
                chatIntent.putExtra(Constants.MESSAGE_ID, messageId);
                chatIntent.putExtra(Constants.VIEW_TYPE, Constants.MESSAGE_LIST_VIEW_TAG);
                startActivity(chatIntent);
                break;
        }

        isPushNotificationMode = false;
    }

    private void initializeData() {
        animateSwipeRefreshLayout(swipeRefreshLayout);
        fetchSuggestedCharities(user.getUsername(), 1, pageSize, false);
    }

    private void loadUserData(User user) {
        if (user != null) {
            mFullNameView.setText(user.getFullName());
            mUsernameView.setText(getString(R.string.username_placeholder, user.getUsername()));
            if (user.getPhotoUrl() != null) {
                ImageUtils.loadImageUrl(mUserProfilePhoto, this, user.getPhotoUrl());
            }
        }
    }

    private void loadSideMenu(UserMenu userMenu) {
        if (userMenu != null) {
            DonationProgress donationProgress = userMenu.getDonationProgress();
            Message message = userMenu.getMessageData();
            Notification notification = userMenu.getNotification();
            UserBoard userBoard = userMenu.getUserBoard();

            if (donationProgress != null) {
                zakatProgressMeter.setProgress((int) donationProgress.getPercentageCompleted());
                mCurrentZakatDonation.setText(ZakatifyUtils.formatCurrencyAmount(donationProgress.getTotalDonation()));
                String formattedAmount = getResources().getString(R.string.total_donation_format, ZakatifyUtils.formatCurrencyAmount(donationProgress.getZakatGoal()));
                mTotalZakatGoal.setText(formattedAmount);
                mPercentageProgress.setText(String.format("%s%%", ZakatifyUtils.formatDecimalNumber(donationProgress.getPercentageCompleted(), 2)));
            }

            if (message != null) {
                mMessageCountView.setText(String.valueOf(message.getCount()));
            }

            if (notification != null) {
                mNotificationCountView.setText(String.valueOf(notification.getCount()));
            }

            if (userBoard != null) {
                mRankView.setText(String.valueOf(userBoard.getRank()));
                mPointsView.setText(String.valueOf(userBoard.getPoints()));
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(MainActivity.this, charityStateBroadcast);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        item.setChecked(true);
        drawer.closeDrawer(GravityCompat.START);
        switch (id) {
            case R.id.nav_logout:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        signUserOut();
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_profile:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra(Constants.VIEW_TYPE, Constants.PROFILE_VIEW_TAG);
                        profileIntent.putExtra(Constants.USER, user);
                        startActivity(profileIntent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_payments:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra(Constants.VIEW_TYPE, Constants.PAYMENTS_OPTIONS_VIEW_TAG);
                        profileIntent.putExtra(Constants.USER, user);
                        startActivity(profileIntent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_preferences:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra(Constants.VIEW_TYPE, Constants.ZAKATIFY_PREFERENCES_VIEW_TAG);
                        profileIntent.putExtra(Constants.USER, user);
                        startActivity(profileIntent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_messages:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.CHAT_SESSIONS_LIST_VIEW_TAG);
                        intent.putExtra(Constants.USER, user);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
        }
        return true;
    }

    private void fetchUserMenu(final String username) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_MENU_REQUEST, username, new ApiClientListener.FetchUserMenuListener() {
            @Override
            public void onUserMenuFetched(UserMenuResponse userMenuResponse) {
                if (userMenuResponse != null) {
                    if (userMenuResponse.getStatus() == null && userMenuResponse.getDeveloperMessage() == null) {
                        UserMenu userMenu = extractUserMenu(userMenuResponse);
                        PreferenceStorageManager.saveUserMenu(ZakatifyApplication.getAppInstance().getApplicationContext(), userMenu);
                        loadSideMenu(userMenu);
                        loadUserData(userMenu.getUser());
                    }
                }
            }
        }).execute();
    }


    private void signUserOut() {
        PreferenceStorageManager.resetUser(ZakatifyApplication.getAppInstance().getApplicationContext());
        ApiClient.NetworkCallsRunner.resetApiService();
        ApiModule.resetApiClient();

        Intent intent = new Intent(MainActivity.this, TempActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_drawer:
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_preferences:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra(Constants.VIEW_TYPE, Constants.ZAKATIFY_PREFERENCES_VIEW_TAG);
                        profileIntent.putExtra(Constants.USER, user);
                        startActivity(profileIntent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_home:
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_portfolio:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra(Constants.VIEW_TYPE, Constants.USER_PORTFOLIO_VIEW_TAG);
                        profileIntent.putExtra(Constants.USER, user);
                        startActivity(profileIntent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_payments:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra(Constants.VIEW_TYPE, Constants.PAYMENTS_OPTIONS_VIEW_TAG);
                        profileIntent.putExtra(Constants.USER, user);
                        startActivity(profileIntent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.user_sidebar_container:
            case R.id.profile_photo:
            case R.id.fullname:
            case R.id.username:
            case R.id.nav_profile:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra(Constants.VIEW_TYPE, Constants.PROFILE_VIEW_TAG);
                        profileIntent.putExtra(Constants.USER, user);
                        startActivity(profileIntent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_logout:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        signUserOut();
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_faq:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        launchWebViewDialog(MainActivity.this, Constants.FAQ_LINK, getString(R.string.faq_label));
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_terms:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        launchWebViewDialog(MainActivity.this, Constants.TERMS_LINK, getString(R.string.terms_of_service_label));
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_privacy:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        launchWebViewDialog(MainActivity.this, Constants.PRIVACY_LINK, getString(R.string.privacy_label));
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_about:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        launchWebViewDialog(MainActivity.this, Constants.ABOUT_LINK, getString(R.string.about_zakatify));
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
                /*drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, MetaActivity.class);
                        intent.putExtra(Constants.USER, user);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.ABOUT_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
                */
            case R.id.nav_top_zakatifiers:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, MetaActivity.class);
                        intent.putExtra(Constants.USER, user);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.TOP_ZAKATIFIERS_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_invite_friends:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, MetaActivity.class);
                        intent.putExtra(Constants.USER, user);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.INVITE_FRIENDS_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.search_view:
                Intent searchIntent = new Intent(MainActivity.this, MetaActivity.class);
                searchIntent.putExtra(Constants.USER, user);
                searchIntent.putExtra(Constants.VIEW_TYPE, Constants.SEARCH_VIEW_TAG);
                startActivity(searchIntent);
                break;
            case R.id.nav_messages:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.CHAT_SESSIONS_LIST_VIEW_TAG);
                        intent.putExtra(Constants.USER, user);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            default:
                drawer.closeDrawer(GravityCompat.START);
                break;
        }
    }

    private void fetchSuggestedCharities(String username, int pageNumber, final int pageSize, final boolean inLoadMoreMode) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_SUGGESTED_CHARITIES_REQUEST, username, String.valueOf(pageNumber), String.valueOf(pageSize), new ApiClientListener.FetchSuggestedCharitiesListener() {
            @Override
            public void onSuggestedCharitiesFetched(CharitiesResponse charitiesResponse) {
                endSwipeRefresh(swipeRefreshLayout);
                isLoading = false;
                if (inLoadMoreMode) {
                    charitiesAdapter.removeLoadingFooter(CharitiesAdapter.class, charitiesAdapter);
                }

                if (charitiesResponse != null) {
                    if (charitiesResponse.getStatus() == null && charitiesResponse.getMessage() == null) {
                        List<Charity> charityListParam = charitiesResponse.getCharities();

                        if (charityListParam == null || charityListParam.size() == 0 || charityListParam.size() < pageSize) {
                            hasListEnded = true;
                        }

                        if (!inLoadMoreMode) {
                            if (charityList != null) {
                                charityList.clear();
                            }
                            charitiesAdapter.clearItems();
                            updateSavedCharities(charityListParam);
                        }

                        updateCharityList(charityListParam);

                        loadAdapter(charityListParam);

                        if (!hasListEnded) {
                            charitiesAdapter.addLoadingFooter(CharitiesAdapter.class, charitiesAdapter);
                        }
                    } else {
                        showErrorPopup(charitiesResponse.getDeveloperMessage());
                    }
                } else {
                    if (inLoadMoreMode) {
                        charitiesAdapter.showRetry(true, fetchErrorMessage(null), charityList);
                    }
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void updateSavedCharities(List<Charity> charities) {
        PreferenceStorageManager.saveSuggestedCharities(ZakatifyApplication.getAppInstance().getApplicationContext(), charities);
    }

    private void updateCharityList(List<Charity> charityListParam) {
        if (charityList == null) {
            charityList = new ArrayList<Charity>();
        }
        if (charityListParam != null && charityListParam.size() > 0) {
            charityList.addAll(charityListParam);
        }
    }

    private void loadAdapter(List<Charity> charityList) {
        charitiesAdapter.addAll(charityList);
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        switch (v.getId()) {
            default:
                Intent intent = new Intent(this, CharityActivity.class);
                intent.putExtra(Constants.CHARITY, charityList.get(position));
                intent.putExtra(Constants.USER, user);
                intent.putExtra(Constants.VIEW_TYPE, Constants.CHARITY_DETAILS_VIEW_TAG);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onProfileUpdated(User user) {
        this.user = user;
        loadUserData(user);
    }

    private BroadcastReceiver charityStateBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getIntExtra(Constants.POSITION, -1);
            boolean shouldReloadList = intent.getBooleanExtra(Constants.REFRESH_FLAG, false);
            if (shouldReloadList) {
                fetchSuggestedCharities(user.getUsername(), 1, pageSize, false);
            } else {
                if (position > -1) {
                    charitiesAdapter.remove(position);
                }
            }
        }
    };

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getApplicationContext())) {
            if (!hasListEnded) {
                fetchSuggestedCharities(user.getUsername(), currentPage, pageSize, true);
            }
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }
}
