package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

public class Invite implements Comparable<Invite>, Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("dateInvited")
    @Expose
    private String dateInvited;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("invitee")
    @Expose
    private String invitee;
    @SerializedName("invitor")
    @Expose
    private String invitor;
    @SerializedName("state")
    @Expose
    private String state;

    public Invite(String id, String dateInvited, String displayName, String invitee, String invitor, String state) {
        this.id = id;
        this.dateInvited = dateInvited;
        this.displayName = displayName;
        this.invitee = invitee;
        this.invitor = invitor;
        this.state = state;
    }

    public Invite() {

    }

    protected Invite(Parcel in) {
        id = in.readString();
        dateInvited = in.readString();
        displayName = in.readString();
        invitee = in.readString();
        invitor = in.readString();
        state = in.readString();
    }

    public static final Creator<Invite> CREATOR = new Creator<Invite>() {
        @Override
        public Invite createFromParcel(Parcel in) {
            return new Invite(in);
        }

        @Override
        public Invite[] newArray(int size) {
            return new Invite[size];
        }
    };

    @Override
    public int compareTo(Invite o) {
        if (getInvitee() == null || o.getInvitee() == null) {
            return 0;
        }
        return getInvitee().compareTo(o.getInvitee());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((invitee == null) ? 0 : invitee.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return !(obj == null || !(obj instanceof Invite)) && (this == obj || this.invitee.equals(((Invite) obj).getInvitee()));
    }

    public static Comparator<Invite> COMPARE_BY_NAME = new Comparator<Invite>() {
        public int compare(Invite one, Invite other) {
            String compareParam, otherParam;
            compareParam = !TextUtils.isEmpty(one.displayName) ? one.displayName : one.invitee;
            otherParam = !TextUtils.isEmpty(other.displayName) ? other.displayName : other.invitee;
            return compareParam.compareTo(otherParam);
        }
    };


    public static Comparator<Invite> COMPARE_BY_INVITEE_EMAIL = new Comparator<Invite>() {
        public int compare(Invite one, Invite other) {
            return one.invitee.compareTo(other.invitee);
        }
    };

    public static Comparator<Invite> COMPARE_BY_STATE = new Comparator<Invite>() {
        public int compare(Invite one, Invite other) {
            return one.state.compareTo(other.state);
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateInvited() {
        return dateInvited;
    }

    public void setDateInvited(String dateInvited) {
        this.dateInvited = dateInvited;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getInvitee() {
        return invitee;
    }

    public void setInvitee(String invitee) {
        this.invitee = invitee;
    }

    public String getInvitor() {
        return invitor;
    }

    public void setInvitor(String invitor) {
        this.invitor = invitor;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(dateInvited);
        dest.writeString(displayName);
        dest.writeString(invitee);
        dest.writeString(invitor);
        dest.writeString(state);
    }
}
