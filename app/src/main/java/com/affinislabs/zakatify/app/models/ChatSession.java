package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class ChatSession implements Parcelable {

    @SerializedName("messageId")
    @Expose
    private String messageId;
    @SerializedName("otherParticipant")
    @Expose
    private User otherParticipant;
    @SerializedName("requestingParticipant")
    @Expose
    private User requestingParticipant;
    @SerializedName("participants")
    @Expose
    private Map<String, Object> participantData;
    @SerializedName("currentStatus")
    @Expose
    private String currentStatus;
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    public ChatSession() {

    }

    protected ChatSession(Parcel in) {
        messageId = in.readString();
        otherParticipant = in.readParcelable(User.class.getClassLoader());
        requestingParticipant = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<ChatSession> CREATOR = new Creator<ChatSession>() {
        @Override
        public ChatSession createFromParcel(Parcel in) {
            return new ChatSession(in);
        }

        @Override
        public ChatSession[] newArray(int size) {
            return new ChatSession[size];
        }
    };

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public User getOtherParticipant() {
        return otherParticipant;
    }

    public void setOtherParticipant(User otherParticipant) {
        this.otherParticipant = otherParticipant;
    }

    public User getRequestingParticipant() {
        return requestingParticipant;
    }

    public void setRequestingParticipant(User requestingParticipant) {
        this.requestingParticipant = requestingParticipant;
    }

    public Map<String, Object> getParticipantData() {
        return participantData;
    }

    public void setParticipantData(Map<String, Object> participantData) {
        this.participantData = participantData;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(messageId);
        dest.writeParcelable(otherParticipant, flags);
        dest.writeParcelable(requestingParticipant, flags);
    }
}
