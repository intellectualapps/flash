package com.affinislabs.zakatify.app.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.dialogs.MediaPickerDialog;
import com.affinislabs.zakatify.app.ui.fragments.AuthFragment;
import com.affinislabs.zakatify.app.ui.fragments.FollowersFragment;
import com.affinislabs.zakatify.app.ui.fragments.MediaPickerBaseFragment;
import com.affinislabs.zakatify.app.ui.fragments.OnboardingInterestsFragment;
import com.affinislabs.zakatify.app.ui.fragments.OnboardingPaymentsFragment;
import com.affinislabs.zakatify.app.ui.fragments.OnboardingProfileFragment;
import com.affinislabs.zakatify.app.ui.fragments.OnboardingZakatPreferencesFragment;
import com.affinislabs.zakatify.app.ui.fragments.PaymentOptionsFragment;
import com.affinislabs.zakatify.app.ui.fragments.ProfileEditFragment;
import com.affinislabs.zakatify.app.ui.fragments.PublicUserProfileFragment;
import com.affinislabs.zakatify.app.ui.fragments.UserPortfolioFragment;
import com.affinislabs.zakatify.app.ui.fragments.ZakatPreferencesFragment;
import com.affinislabs.zakatify.app.utils.Constants;

public class ProfileActivity extends BaseActivity implements MediaPickerDialog.MediaPickedListener {
    private Bundle fragmentBundle;
    private String viewType;
    private User user;
    private Fragment frag;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setElevation(0);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            if (viewType != null) {
                switch (viewType) {
                    case Constants.PROFILE_VIEW_TAG:
                        frag = ProfileEditFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.ZAKATIFY_PREFERENCES_VIEW_TAG:
                        frag = ZakatPreferencesFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.PAYMENTS_OPTIONS_VIEW_TAG:
                        frag = PaymentOptionsFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.ONBOARDING_PROFILE_VIEW_TAG:
                        frag = OnboardingProfileFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.ONBOARDING_ZAKATIFY_PREFERENCES_VIEW_TAG:
                        frag = OnboardingZakatPreferencesFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.ONBOARDING_PAYMENTS_VIEW_TAG:
                        frag = OnboardingPaymentsFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.ONBOARDING_INTERESTS_VIEW_TAG:
                        frag = OnboardingInterestsFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.USER_PORTFOLIO_VIEW_TAG:
                        frag = UserPortfolioFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.PUBLIC_USER_PROFILE_VIEW_TAG:
                        frag = PublicUserProfileFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.FOLLOWERS_VIEW_TAG:
                        frag = FollowersFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = ProfileEditFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = ProfileEditFragment.newInstance(null);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(frag.getClass().getSimpleName());
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onMediaItemPicked(Uri fileUri, String mediaPath, int mediaType, String fragmentTag) {
        Fragment frag = findActiveFragmentByTag(fragmentTag, getSupportFragmentManager());
        if (frag != null) {
            if (frag instanceof MediaPickerBaseFragment) {
                ((MediaPickerBaseFragment) frag).onMediaPickerSuccess(fileUri, mediaPath, mediaType, fragmentTag);
            }
        }
    }

    @Override
    public void onCancel(String message, String fragmentTag) {
        Fragment frag = findActiveFragmentByTag(fragmentTag, getSupportFragmentManager());
        if (frag != null) {
            if (frag instanceof MediaPickerBaseFragment) {
                ((MediaPickerBaseFragment) frag).onMediaPickerError(message, fragmentTag);
            }
        }
    }
}
