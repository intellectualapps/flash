package com.affinislabs.zakatify.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.ForgotPasswordResponse;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.AuthActivity;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;

public class ForgotPasswordFragment extends BaseFragment implements TextWatcher, View.OnClickListener {
    private User user;
    private Button resetPasswordButton;
    private EditText usernameEmailInput;
    private TextView launchAboutView, launchPrivacyPolicyView;
    private Toolbar toolbar;
    private Bundle bundle = new Bundle();

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new ForgotPasswordFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public ForgotPasswordFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        toolbar = ((AuthActivity) getActivity()).getToolbar();
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFragment();
            }
        });
        resetPasswordButton = (Button) view.findViewById(R.id.reset_password_button);
        usernameEmailInput = (EditText) view.findViewById(R.id.username_or_email_text);
        usernameEmailInput.addTextChangedListener(this);
        launchAboutView = (TextView) view.findViewById(R.id.about_app_button);
        launchPrivacyPolicyView = (TextView) view.findViewById(R.id.privacy_policy_button);

        launchAboutView.setOnClickListener(this);
        launchPrivacyPolicyView.setOnClickListener(this);
        resetPasswordButton.setOnClickListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        if (charSequence.toString().trim().length() > 0) {
            resetPasswordButton.setEnabled(true);
        } else {
            resetPasswordButton.setEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reset_password_button:
                String message = getString(R.string.forgot_password_progress_label);
                if (NetworkUtils.isConnected(getContext())) {
                    if (validateFields(new EditText[]{usernameEmailInput})) {
                        hideKeyboard();
                        String email = usernameEmailInput.getText().toString().trim();
                        showLoadingIndicator(message);
                        makeAPICall(email);
                    }
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
                break;
            case R.id.about_app_button:
                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).launchWebViewDialog(getActivity(), Constants.ABOUT_LINK, getString(R.string.about_zakatify));
                }
                break;
            case R.id.privacy_policy_button:
                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).launchWebViewDialog(getActivity(), Constants.PRIVACY_LINK, getString(R.string.privacy_label));
                }
                break;
        }
    }

    private void makeAPICall(String emailAddress) {
        new ApiClient.NetworkCallsRunner(Constants.RESET_PASSWORD_REQUEST, emailAddress, new ApiClientListener.ForgotPasswordListener() {
            @Override
            public void onPasswordReset(ForgotPasswordResponse forgotPasswordResponse) {
                hideLoadingIndicator();
                if (forgotPasswordResponse != null) {
                    if (forgotPasswordResponse.getStatus() == null && forgotPasswordResponse.getMessage() == null) {
                        String message = getString(R.string.reset_password_success);
                        showSuccessPopup(message);
                        if (getActivity() != null) {
                            ((BaseActivity) getActivity()).navigateUp();
                        }
                    } else {
                        showErrorPopup(forgotPasswordResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }
}
