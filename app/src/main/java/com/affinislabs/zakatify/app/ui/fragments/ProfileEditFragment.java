package com.affinislabs.zakatify.app.ui.fragments;

import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.BuildConfig;
import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.CategoryResponse;
import com.affinislabs.zakatify.app.api.responses.LoginResponse;
import com.affinislabs.zakatify.app.api.responses.PublicUserResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Category;
import com.affinislabs.zakatify.app.models.Counts;
import com.affinislabs.zakatify.app.models.DonationProgress;
import com.affinislabs.zakatify.app.models.FollowState;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.models.UserBoard;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.MainActivity;
import com.affinislabs.zakatify.app.ui.dialogs.MediaPickerDialog;
import com.affinislabs.zakatify.app.utils.CommonUtils;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FileUtils;
import com.affinislabs.zakatify.app.utils.ImageOptimizerTask;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.affinislabs.zakatify.app.utils.Transformers;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.UploadRequest;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.preprocess.BitmapEncoder;
import com.cloudinary.android.preprocess.DimensionsValidator;
import com.cloudinary.android.preprocess.ImagePreprocessChain;
import com.cloudinary.android.preprocess.Limit;
import com.greenfrvr.hashtagview.HashtagView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileEditFragment extends MediaPickerBaseFragment implements View.OnClickListener, HashtagView.TagsSelectListener, ZakatifyInterfaces.SocialAuthenticationListener, ZakatifyInterfaces.ImageOptimizationListener {
    private View mSnackBarView;
    private View mFollowersContainer;
    private EditText firstnameInput, lastnameInput, usernameInput, mobileNumberInput, emailAddressInput, locationInput;
    private Button saveChangesButton;
    private TextView connectTwitterButton, connectFacebookButton, fullNameView, usernameView;
    private TextView portfolioCountView, pledgeCountView, badgeCountView, followerCountView;
    private ImageView disconnectFacebookButton, disconnectTwitterButton;
    private ImageView mProfilePhoto;
    Toolbar toolbar;
    private Uri mFileUri;
    private String profileImageUrl = "";
    private User user = null;
    private HashtagView mInterestsView;
    private ArrayList<Category> categories;
    private ArrayList<Category> userCategories;
    private static List<String> DATA = new ArrayList<String>();
    private static List<String> USER_DATA = new ArrayList<String>();
    private Counts counts;
    private boolean showImageUploadDialog = false;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new ProfileEditFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public ProfileEditFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            user = getArguments().getParcelable(Constants.USER);
        }
        categories = new ArrayList<Category>();
        userCategories = new ArrayList<Category>();
        categories = PreferenceStorageManager.getCategories(ZakatifyApplication.getAppInstance().getApplicationContext());
        userCategories = PreferenceStorageManager.getUserCategories(ZakatifyApplication.getAppInstance().getApplicationContext());
        counts = PreferenceStorageManager.getCounts(ZakatifyApplication.getAppInstance().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (showImageUploadDialog) {
            showPhotoUploadOptions();
            showImageUploadDialog = false;
        }
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.edit_profile_label));
        mFollowersContainer = view.findViewById(R.id.followers_container);
        saveChangesButton = (Button) view.findViewById(R.id.save_changes_button);
        mProfilePhoto = (ImageView) view.findViewById(R.id.profile_photo);
        usernameInput = (EditText) view.findViewById(R.id.username_input);
        firstnameInput = (EditText) view.findViewById(R.id.firstname_input);
        lastnameInput = (EditText) view.findViewById(R.id.lastname_input);
        emailAddressInput = (EditText) view.findViewById(R.id.email_address_input);
        mobileNumberInput = (EditText) view.findViewById(R.id.mobile_number_input);
        locationInput = (EditText) view.findViewById(R.id.location_input);

        connectFacebookButton = (TextView) view.findViewById(R.id.connect_facebook_button);
        connectTwitterButton = (TextView) view.findViewById(R.id.connect_twitter_button);
        disconnectFacebookButton = (ImageView) view.findViewById(R.id.disconnect_facebook);
        disconnectTwitterButton = (ImageView) view.findViewById(R.id.disconnect_twitter);

        portfolioCountView = (TextView) view.findViewById(R.id.portfolio_count);
        pledgeCountView = (TextView) view.findViewById(R.id.pledges_count);
        badgeCountView = (TextView) view.findViewById(R.id.badges_count);
        followerCountView = (TextView) view.findViewById(R.id.followers_count);

        fullNameView = (TextView) view.findViewById(R.id.fullname);
        usernameView = (TextView) view.findViewById(R.id.username);
        mInterestsView = (HashtagView) view.findViewById(R.id.interests_view);
        mInterestsView.addOnTagSelectListener(this);

        connectFacebookButton.setOnClickListener(this);
        connectTwitterButton.setOnClickListener(this);
        mFollowersContainer.setOnClickListener(this);
        disconnectFacebookButton.setOnClickListener(this);
        disconnectTwitterButton.setOnClickListener(this);

        mProfilePhoto.setOnClickListener(this);

        saveChangesButton.setOnClickListener(this);
        mFollowersContainer.setOnClickListener(this);

        loadUserData(user, counts);
        loadInterestsData(categories, userCategories);
        fetchUserDetails(user.getUsername());
        fetchCategories(user.getUsername());
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.profile_update_loading);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.save_changes_button:
                if (validateFields(new EditText[]{usernameInput})) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        if (mFileUri != null && !TextUtils.isEmpty(mFileUri.toString())) {
                            ImageOptimizerTask imageOptimizerTask = new ImageOptimizerTask(mFileUri, getContext(), this);
                            imageOptimizerTask.execute();
                        } else {
                            makeAPICall(sanitizeInput(getUserInput()));
                        }
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.profile_photo:
                if (checkMediaAccessPermission(getActivity())) {
                    showPhotoUploadOptions();
                }
                break;
            case R.id.connect_twitter_button:
            case R.id.disconnect_twitter:
                break;
            case R.id.connect_facebook_button:
                if (NetworkUtils.isConnected(getContext())) {
                    message = getString(R.string.link_social_account_progress);
                    showLoadingIndicator(message);
                    manageFacebookAuthentication(this);
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
                break;
            case R.id.disconnect_facebook:
                if (NetworkUtils.isConnected(getContext())) {
                    message = getString(R.string.unlink_social_account_progress);
                    showLoadingIndicator(message);
                    Map<String, String> queryMap = new HashMap<String, String>();
                    queryMap.put(Constants.PLATFORM, "facebook");
                    linkAccount(Constants.UNLINK_SOCIAL_NETWORK_REQUEST, user.getUsername(), queryMap);
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
                break;
            case R.id.followers_container:
                bundle = new Bundle();
                bundle.putString(Constants.USERNAME, user.getUsername());
                bundle.putString(Constants.MAIN_USERNAME, user.getUsername());
                startFragment(FollowersFragment.newInstance(bundle));
                break;
        }
    }

    private void showPhotoUploadOptions() {
        DialogFragment profileBannerFrag = MediaPickerDialog.newInstance("Set Profile Image", MediaPickerDialog.TYPE_IMAGE, ProfileEditFragment.class.getSimpleName());
        profileBannerFrag.show(((AppCompatActivity) getContext()).getSupportFragmentManager().beginTransaction(), MediaPickerDialog.class.getSimpleName());
    }

    private void loadUserData(User user, Counts counts) {
        if (user == null || isDetached() || !isVisible()) {
            return;
        }

        fullNameView.setText(user.getFullName());
        usernameView.setText(getString(R.string.username_placeholder, user.getUsername()));
        emailAddressInput.setText(user.getEmail());
        usernameInput.setText(user.getUsername());
        firstnameInput.setText(user.getFirstName());
        lastnameInput.setText(user.getLastName());
        mobileNumberInput.setText(user.getPhoneNumber());
        locationInput.setText(user.getLocation());

        if (user.getPhotoUrl() != null) {
            ImageUtils.loadImageUrl(mProfilePhoto, getContext(), user.getPhotoUrl());
            profileImageUrl = user.getPhotoUrl();
        }

        if (isNotEmpty(user.getFacebookEmail())) {
            connectFacebookButton.setText(user.getFacebookEmail());
        } else {
            connectFacebookButton.setText(getString(R.string.connect_label));
        }

        if (isNotEmpty(user.getTwitterEmail())) {
            connectTwitterButton.setText(user.getTwitterEmail());
        } else {
            connectTwitterButton.setText(getString(R.string.connect_label));
        }

        connectFacebookButton.setEnabled(!isNotEmpty(user.getFacebookEmail()));
        connectTwitterButton.setEnabled(!isNotEmpty(user.getTwitterEmail()));
        disconnectFacebookButton.setVisibility(isNotEmpty(user.getFacebookEmail()) ? View.VISIBLE : View.GONE);
        disconnectTwitterButton.setVisibility(isNotEmpty(user.getTwitterEmail()) ? View.VISIBLE : View.GONE);
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String firstName = firstnameInput.getText().toString().trim();
        String lastName = lastnameInput.getText().toString().trim();
        String username = usernameInput.getText().toString().trim();
        String mobileNumber = mobileNumberInput.getText().toString().trim();
        String emailAddress = emailAddressInput.getText().toString().trim();
        String location = locationInput.getText().toString().trim();

        map.put(Constants.FIRST_NAME, firstName);
        map.put(Constants.LAST_NAME, lastName);
        map.put(Constants.USERNAME, username);
        if (!TextUtils.isEmpty(profileImageUrl)) {
            map.put(Constants.PHOTO_URL, profileImageUrl);
        }
        map.put(Constants.PHONE_NUMBER, mobileNumber);
        map.put(Constants.EMAIL_ADDRESS, emailAddress);
        map.put(Constants.LOCATION, location);
        map.put(Constants.FACEBOOK_EMAIL, user.getFacebookEmail());
        map.put(Constants.TWITTER_EMAIL, user.getTwitterEmail());
        map.put(Constants.CATEGORY_IDS, getSelectedInterests());

        return map;
    }

    public Map<String, String> sanitizeInput(Map<String, String> input) {
        Map<String, String> sanitizedMap = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : input.entrySet()) {
            if (entry.getKey() != null) {
                if (entry.getValue() != null && entry.getValue().length() > 0) {
                    sanitizedMap.put(entry.getKey(), entry.getValue());
                } else {
                    sanitizedMap.remove(entry.getKey());
                }
            }
        }
        return sanitizedMap;
    }

    private void fetchUserDetails(String username) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_PUBLIC_USER_DETAILS_REQUEST, username, new ApiClientListener.FetchPublicUserDetailsListener() {
            @Override
            public void onUserDetailsFetched(PublicUserResponse publicUserResponse) {
                if (publicUserResponse != null) {
                    if (publicUserResponse.getStatus() == null && publicUserResponse.getDeveloperMessage() == null) {
                        user = publicUserResponse.getUser();
                        PreferenceStorageManager.saveCounts(ZakatifyApplication.getAppInstance().getApplicationContext(), publicUserResponse.getCounts());
                        loadUserData(user, publicUserResponse.getCounts());
                        loadMetaData(publicUserResponse.getUserBoard(), publicUserResponse.getDonationProgress(), publicUserResponse.getCounts(), publicUserResponse.getFollowState());
                    } else {
                        showErrorPopup(publicUserResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void loadMetaData(UserBoard userBoard, DonationProgress donationProgress, Counts counts, FollowState followState) {
        if (userBoard != null && donationProgress != null && counts != null && isAdded() && isVisible()) {
            portfolioCountView.setText(String.valueOf(counts.getPortfolioCount() > 0 ? counts.getPortfolioCount() : getString(R.string.none_label)));
            followerCountView.setText(String.valueOf(counts.getFollowersCount() > 0 ? counts.getFollowersCount() : getString(R.string.none_label)));
            pledgeCountView.setText(String.valueOf(counts.getDonationsCount() > 0 ? counts.getDonationsCount() : getString(R.string.none_label)));
            badgeCountView.setText(getString(R.string.none_label));
        }
    }

    private void connectSocialAccount(final Map<String, String> accountMap, final Map<String, String> profileData) {
        if (NetworkUtils.isConnected(getContext())) {
            hideKeyboard();
            String message = getString(R.string.link_social_account_progress);
            Map<String, String> queryMap = new HashMap<String, String>();
            queryMap.put(Constants.EMAIL_ADDRESS, accountMap.get(Constants.ID));
            queryMap.put(Constants.PLATFORM, accountMap.get(Constants.AUTH_TYPE));
            linkAccount(Constants.LINK_SOCIAL_NETWORK_REQUEST, user.getUsername(), queryMap);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }

    private void linkAccount(String REQUEST_TYPE, String username, Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(REQUEST_TYPE, username, accountMap, new ApiClientListener.SocialNetworkLinkingListener() {
            @Override
            public void onActionCommitted(LoginResponse socialNetworkLinkingResponse) {
                hideLoadingIndicator();
                if (socialNetworkLinkingResponse != null) {
                    if (socialNetworkLinkingResponse.getStatus() == null && socialNetworkLinkingResponse.getMessage() == null) {
                        user = extractUserDetails(socialNetworkLinkingResponse);
                        loadUserData(user, counts);
                    } else {
                        showErrorPopup(socialNetworkLinkingResponse.getMessage());
                    }
                }
            }
        }).execute();
    }

    private void fetchCategories(final String username) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_CATEGORIES_REQUEST, username, new ApiClientListener.FetchCategoriesListener() {
            @Override
            public void onCategoriesFetched(CategoryResponse categoryResponse) {
                if (categoryResponse != null) {
                    if (categoryResponse.getStatus() == null && categoryResponse.getMessage() == null) {
                        categories = categoryResponse.getCategories();
                        DATA = Arrays.asList(ListUtils.getCategoryNames(categories));

                        fetchUserCategories(username);
                        PreferenceStorageManager.saveCategories(ZakatifyApplication.getAppInstance().getApplicationContext(), categories);
                    } else {
                        showErrorPopup(categoryResponse.getMessage());
                    }

                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void fetchUserCategories(String username) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_CATEGORIES_REQUEST, username, new ApiClientListener.FetchCategoriesListener() {
            @Override
            public void onCategoriesFetched(CategoryResponse categoryResponse) {
                if (categoryResponse != null) {
                    if (categoryResponse.getStatus() == null && categoryResponse.getMessage() == null) {
                        userCategories = categoryResponse.getCategories();
                        loadInterestsData(categories, userCategories);
                        PreferenceStorageManager.saveUserCategories(ZakatifyApplication.getAppInstance().getApplicationContext(), userCategories);
                    } else {
                        showErrorPopup(categoryResponse.getMessage());
                    }

                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void loadInterestsData(ArrayList<Category> categories, ArrayList<Category> userCategories) {

        DATA = ListUtils.getCategoryNames(categories) != null ? Arrays.asList(ListUtils.getCategoryNames(categories)) : null;
        USER_DATA = ListUtils.getCategoryNames(userCategories) != null ? Arrays.asList(ListUtils.getCategoryNames(userCategories)) : null;

        mInterestsView.setData(DATA, Transformers.CAPITALIZE_TAG, new HashtagView.DataSelector<String>() {
            @Override
            public boolean preselect(String item) {
                return USER_DATA != null && USER_DATA.size() > 0 && USER_DATA.contains(item);
            }
        });
    }

    private void makeAPICall(Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(Constants.UPDATE_PROFILE_REQUEST, accountMap, new ApiClientListener.ProfileUpdateListener() {
            @Override
            public void onProfileUpdated(LoginResponse profileUpdateResponse) {
                hideLoadingIndicator();
                if (profileUpdateResponse != null) {
                    if (profileUpdateResponse.getStatus() == null && profileUpdateResponse.getMessage() == null) {
                        user = extractUserDetails(profileUpdateResponse);
                        loadUserData(user, counts);
                        updateUserProfile(user);
                        if (MainActivity.userProfileUpdateListener != null) {
                            MainActivity.userProfileUpdateListener.onProfileUpdated(user);
                        }

                        CommonUtils.displayShortToastMessage(getString(R.string.profile_update_message));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                closeFragment();
                            }
                        }, 1500);
                    } else {
                        showErrorPopup(profileUpdateResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MEDIA_PERMISSION_REQUEST_CODE:
                showImageUploadDialog = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
    }

    @Override
    public void onMediaPickerSuccess(Uri fileUri, String filePath, int mediaType, String fragmentTag) {
        try {
            if (TextUtils.isEmpty(filePath)) {
                if (!fileUri.toString().contains("content://")) {
                    setImagePreview(fileUri);
                } else {
                    Uri resolvedUri = Uri.parse("file://" + FileUtils.getRealPathFromURI(getContext(), fileUri));
                    setImagePreview(resolvedUri);
                }
            } else {
                Uri resolvedUri = Uri.parse(filePath);
                setImagePreview(resolvedUri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setImagePreview(Uri fileUri) {
        this.mFileUri = fileUri;
        ImageUtils.loadImageUri(mProfilePhoto, getContext(), fileUri);
    }

    @Override
    public void onMediaPickerError(String message, String fragmentTag) {
        if (getActivity() != null) {
            showErrorPopup(message);
        }
    }

    @Override
    public void onItemSelected(Object item, boolean selected) {

    }

    private String getSelectedInterests() {
        List<String> selectedInterests = new ArrayList<String>();
        for (Object categoryObject : mInterestsView.getSelectedItems()) {
            selectedInterests.add(getCategoryId(String.valueOf(categoryObject)));
        }

        return TextUtils.join(",", selectedInterests.toArray());
    }

    private String getCategoryId(String description) {
        for (Category category : categories) {
            if (category.getDescription().equalsIgnoreCase(description)) {
                return category.getId();
            }
        }

        return null;
    }

    @Override
    public void onAuthenticationCompleted(Map<String, String> accountData, Map<String, String> profileData) {
        connectSocialAccount(accountData, profileData);
    }

    @Override
    public void onImageOptimized(Uri mFileUri, byte[] bytes) {
        if (bytes == null || bytes.length < 1) {
            uploadImageToCloudinary(mFileUri, null);
        } else {
            uploadImageToCloudinary(mFileUri, bytes);
        }
    }

    private void uploadImageToCloudinary(Uri uri, byte[] imageByteArray) {
        profileImageUrl = "";
        MediaManager mediaManager = MediaManager.get();
        UploadRequest uploadRequest = null;
        if (imageByteArray != null && imageByteArray.length > 0) {
            uploadRequest = mediaManager.upload(imageByteArray);
        } else {
            uploadRequest = mediaManager.upload(uri);
        }

        String requestId = uploadRequest.callback(new UploadCallback() {
            @Override
            public void onStart(String requestId) {

            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {

            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                profileImageUrl = String.valueOf(resultData.get("secure_url"));
                makeAPICall(getUserInput());
            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                hideLoadingIndicator();
                profileImageUrl = "";
                showErrorPopup(error.getDescription());
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {

            }
        }).unsigned(BuildConfig.CLOUDINARY_PRESET)
                .option("angle", "ignore")
                .option("resource_type", "image")
                .preprocess(ImagePreprocessChain.limitDimensionsChain(1000, 1000)
                        .addStep(new DimensionsValidator(10, 10, 1000, 1000))
                        .addStep(new Limit(1000, 1000))
                        .saveWith(new BitmapEncoder(BitmapEncoder.Format.JPEG, 80)))
                .dispatch(getContext());
    }
}
