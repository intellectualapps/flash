package com.affinislabs.zakatify.app.utils;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.IntDef;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ToastUtil {

    public static final int SUCCESS = 0;
    public static final int ERROR = 1;
    public static final int INFORMATIONAL = 2;
    public static final int LENGTH_SHORT = 2000;
    public static final int LENGTH_LONG = 5000;
    public static final int LENGTH_EXTRA_LONG = 10000;

    @IntDef({LENGTH_SHORT, LENGTH_LONG, LENGTH_EXTRA_LONG})
    @Retention(RetentionPolicy.SOURCE)
    @interface Duration {
    }

    @IntDef({SUCCESS, ERROR, INFORMATIONAL})
    @Retention(RetentionPolicy.SOURCE)
    @interface Type {
    }

    public static void showErrorToast(Activity activity, String message) {
        if (activity != null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            View layout = inflater.inflate(R.layout.notification_popup_layout, null);
            TextView toastMessage = layout.findViewById(R.id.notification_message);
            toastMessage.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(activity, R.drawable.ic_alert_orange), null, null, null);
            toastMessage.setText(message);
            showToastView(layout, activity, LENGTH_LONG, message);
        } else {
            CommonUtils.displayLongToastMessage(message);
        }
    }

    public static void showSuccessToast(Activity activity, String message) {
        if (activity != null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            View layout = inflater.inflate(R.layout.notification_popup_layout, null);
            TextView toastMessage = layout.findViewById(R.id.notification_message);
            toastMessage.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(activity, R.drawable.ic_success_24dp), null, null, null);
            toastMessage.setText(message);
            showToastView(layout, activity, LENGTH_LONG, message);
        } else {
            CommonUtils.displayLongToastMessage(message);
        }
    }

    public static void showToast(Activity activity, String message) {
        if (activity != null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            View layout = inflater.inflate(R.layout.notification_popup_layout, null);
            TextView toastMessage = layout.findViewById(R.id.notification_message);
            toastMessage.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            toastMessage.setText(message);
            showToastView(layout, activity, LENGTH_LONG, message);
        } else {
            CommonUtils.displayLongToastMessage(message);
        }
    }

    public static void showToast(String message, @Type int type) {
        Context context = ZakatifyApplication.getAppInstance().getApplicationContext();
        if (context != null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View layout = inflater.inflate(R.layout.notification_popup_layout, null);
            TextView toastMessage = layout.findViewById(R.id.notification_message);
            toastMessage.setCompoundDrawablesWithIntrinsicBounds(type == INFORMATIONAL ? null : ContextCompat.getDrawable(context, type == ERROR ? R.drawable.ic_alert_orange : R.drawable.ic_success_24dp), null, null, null);
            toastMessage.setText(message);
            showToastView(layout, context, LENGTH_LONG, message);
        } else {
            CommonUtils.displayLongToastMessage(message);
        }
    }

    public static void showToast(Activity activity, String message, @Type int type) {
        if (activity != null) {
            LayoutInflater inflater = LayoutInflater.from(activity);
            View layout = inflater.inflate(R.layout.notification_popup_layout, null);
            TextView toastMessage = layout.findViewById(R.id.notification_message);
            toastMessage.setCompoundDrawablesWithIntrinsicBounds(type == INFORMATIONAL ? null : ContextCompat.getDrawable(activity, type == ERROR ? R.drawable.ic_alert_orange : R.drawable.ic_success_24dp), null, null, null);
            toastMessage.setText(message);
            showToastView(layout, activity, LENGTH_LONG, message);
        } else {
            CommonUtils.displayLongToastMessage(message);
        }
    }

    public static void showToast(String title, String message, @Type int type) {
        Context context = ZakatifyApplication.getAppInstance().getApplicationContext();
        if (context != null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View layout = inflater.inflate(R.layout.notification_popup_layout, null);
            TextView toastMessage = layout.findViewById(R.id.notification_message);
            TextView toastTitle = layout.findViewById(R.id.notification_title);
            toastMessage.setCompoundDrawablesWithIntrinsicBounds(type == INFORMATIONAL ? null : ContextCompat.getDrawable(context, type == ERROR ? R.drawable.ic_alert_orange : R.drawable.ic_success_24dp), null, null, null);
            toastMessage.setText(message);
            toastTitle.setText(title);
            showToastView(layout, context, LENGTH_LONG, message);
        } else {
            CommonUtils.displayLongToastMessage(message);
        }
    }

    public static void showToast(Activity activity, String message, @Type int type, @Duration int duration) {
        try {
            LayoutInflater inflater = activity.getLayoutInflater();
            View layout = inflater.inflate(R.layout.notification_popup_layout, null);
            TextView toastMessage = layout.findViewById(R.id.notification_message);
            toastMessage.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(activity, type == ERROR ? R.drawable.ic_alert_orange : R.drawable.ic_success_24dp), null, null, null);
            toastMessage.setText(message);
            showToastView(layout, activity, duration, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void showToastView(final View view, Context context, @Duration int duration, String message) {
        try {
        final Toast toast = new Toast(context);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.setDuration(duration);
        toast.setView(view);
        toast.show();
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.displayLongToastMessage(message);
        }
    }
}
