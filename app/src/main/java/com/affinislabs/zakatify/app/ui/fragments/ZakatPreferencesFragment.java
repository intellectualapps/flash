package com.affinislabs.zakatify.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.ZakatGoalResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.models.ZakatGoal;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.customviews.CustomNumericEditText;
import com.affinislabs.zakatify.app.ui.dialogs.CalculateGoalDialog;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.DateUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.affinislabs.zakatify.app.utils.ZakatifyUtils;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ZakatPreferencesFragment extends BaseFragment implements View.OnClickListener, ZakatifyInterfaces.ZakatCalculatorListener, TextView.OnEditorActionListener, View.OnFocusChangeListener {
    private User user = null;
    private Button saveChangesButton;
    private CustomNumericEditText donationAmountInput;
    private TextView automaticDonationTab, manualDonationTab, automaticDonationContent, manualDonationContent;
    private View automaticTabHighlight, manualTabHighlight;
    private ImageButton zakatCalculatorView;
    private Map<String, String> requestMap;
    private int donationFrequency = 3;
    private ZakatGoal zakatGoal, tempZakatGoal;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new ZakatPreferencesFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public ZakatPreferencesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = getArguments().getParcelable(Constants.USER);
        }
        requestMap = new HashMap<>();
        tempZakatGoal = new ZakatGoal();
        zakatGoal = PreferenceStorageManager.getZakatGoal(ZakatifyApplication.getAppInstance().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_zakat_preferences, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.edit_zakatify_preferences));
        donationAmountInput = (CustomNumericEditText) view.findViewById(R.id.amount_input);
        saveChangesButton = (Button) view.findViewById(R.id.save_changes_button);
        automaticDonationTab = (TextView) view.findViewById(R.id.automatic_donation_tab);
        manualDonationTab = (TextView) view.findViewById(R.id.manual_donation_tab);
        automaticDonationContent = (TextView) view.findViewById(R.id.automatic_donation_content);
        manualDonationContent = (TextView) view.findViewById(R.id.manual_donation_content);
        automaticTabHighlight = view.findViewById(R.id.automatic_donation_tab_highlight);
        manualTabHighlight = view.findViewById(R.id.manual_donation_tab_highlight);
        zakatCalculatorView = (ImageButton) view.findViewById(R.id.zakat_calculator_view);

        donationAmountInput.setOnEditorActionListener(this);
        donationAmountInput.setOnFocusChangeListener(this);
        donationAmountInput.addNumericValueChangedListener(new CustomNumericEditText.NumericValueWatcher() {
            @Override
            public void onChanged(double newValue) {
                saveChangesButton.setEnabled(true);
            }

            @Override
            public void onCleared() {
                saveChangesButton.setEnabled(false);
            }
        });

        automaticTabHighlight.setVisibility(View.VISIBLE);
        manualTabHighlight.setVisibility(View.GONE);
        automaticDonationTab.setOnClickListener(this);
        manualDonationTab.setOnClickListener(this);
        automaticDonationContent.setOnClickListener(this);
        manualDonationContent.setOnClickListener(this);
        saveChangesButton.setOnClickListener(this);
        zakatCalculatorView.setOnClickListener(this);
        makeAPICall(user.getUsername());
        loadZakatData(zakatGoal);
    }

    private void loadZakatData(ZakatGoal zakatGoal) {
        if (zakatGoal != null) {
            donationAmountInput.setText(ZakatifyUtils.formatAmount(zakatGoal.getAmount()));
            toggleDonationTabs(zakatGoal.getFrequencyId().equals(3));
        }
    }

    private String getCurrentDate() {
        Calendar calendar = GregorianCalendar.getInstance();
        return DateUtils.getSimpleDateFormat(calendar.getTimeInMillis(), "yyyy-MM-dd");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_changes_button:
                String message = getString(R.string.zakat_preferences_loading);
                if (NetworkUtils.isConnected(getContext())) {
                    if (validateAmountView(donationAmountInput)) {
                        hideKeyboard();
                        String goalAmount = String.format(Locale.getDefault(), "%.2f", donationAmountInput.getNumericValue());
                        String goalEndDate = getCurrentDate();
                        requestMap.put(Constants.AMOUNT, goalAmount);
                        requestMap.put(Constants.FREQUENCY_ID, String.valueOf(donationFrequency));
                        requestMap.put(Constants.PERIOD, String.valueOf(goalEndDate));
                        showLoadingIndicator(message);
                        makeAPICall(user.getUsername(), requestMap);
                    }
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
                break;
            case R.id.automatic_donation_content:
            case R.id.automatic_donation_tab:
                toggleDonationTabs(true);
                break;
            case R.id.manual_donation_content:
            case R.id.manual_donation_tab:
                toggleDonationTabs(false);
                break;
            case R.id.zakat_calculator_view:
                launchZakatCalculatorDialog();
                break;
            case R.id.amount_input:
                donationAmountInput.setCursorVisible(true);
                break;
        }
    }

    private void toggleDonationTabs(boolean toggleFlag) {
        automaticTabHighlight.setVisibility(toggleFlag ? View.VISIBLE : View.GONE);
        manualTabHighlight.setVisibility(toggleFlag ? View.GONE : View.VISIBLE);
        donationFrequency = (toggleFlag ? 3 : 0);
    }

    private void launchZakatCalculatorDialog() {
        Bundle args = new Bundle();
        args.putParcelable(Constants.ZAKAT_GOAL, (tempZakatGoal != null && tempZakatGoal.getAmount() != null ? tempZakatGoal : zakatGoal));

        DialogFragment zakatCalculatorDialog = CalculateGoalDialog.getInstance(args, this);
        if (getActivity() != null) {
            zakatCalculatorDialog.show(getActivity().getSupportFragmentManager(), zakatCalculatorDialog.getClass().getSimpleName());
        }
    }

    private void makeAPICall(final String username) {
        String message = getString(R.string.fetching_zakat_data_loading);
        showLoadingIndicator(message);
        new ApiClient.NetworkCallsRunner(Constants.FETCH_ZAKAT_GOAL_REQUEST, username, new ApiClientListener.FetchZakatGoalListener() {
            @Override
            public void onZakatGoalFetched(ZakatGoalResponse zakatGoalResponse) {
                hideLoadingIndicator();
                if (zakatGoalResponse != null) {
                    if (zakatGoalResponse.getStatus() == null && zakatGoalResponse.getMessage() == null) {
                        zakatGoal = extractZakatGoal(zakatGoalResponse);
                        loadZakatData(zakatGoal);
                        PreferenceStorageManager.saveZakatGoal(ZakatifyApplication.getAppInstance().getApplicationContext(), zakatGoal);
                    } else {
                        showErrorPopup(zakatGoalResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void makeAPICall(final String username, Map<String, String> requestMap) {
        new ApiClient.NetworkCallsRunner(Constants.UPDATE_ZAKAT_GOAL_REQUEST, username, requestMap, new ApiClientListener.UpdateZakatGoalListener() {
            @Override
            public void onZakatGoalUpdated(ZakatGoalResponse zakatGoalResponse) {
                hideLoadingIndicator();
                if (zakatGoalResponse != null) {
                    if (zakatGoalResponse.getStatus() == null && zakatGoalResponse.getMessage() == null) {
                        ZakatGoal zakatGoal = extractZakatGoal(zakatGoalResponse);
                        PreferenceStorageManager.saveZakatGoal(ZakatifyApplication.getAppInstance().getApplicationContext(), zakatGoal);
                        closeFragment();
                    } else {
                        showErrorPopup(zakatGoalResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onAmountCalculated(double amount, Map<String, String> zakatCalculationData) {
        donationAmountInput.setText(ZakatifyUtils.formatAmount(amount));
        requestMap.putAll(zakatCalculationData);
        tempZakatGoal = ZakatifyUtils.convertMapToObject(amount, zakatCalculationData);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            hideKeyboard();
        }
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.amount_input:
                donationAmountInput.setCursorVisible(hasFocus);
                break;
        }
    }
}
