package com.affinislabs.zakatify.app.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.fragments.AboutFragment;
import com.affinislabs.zakatify.app.ui.fragments.AuthFragment;
import com.affinislabs.zakatify.app.ui.fragments.AuthViewPagerFragment;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FCMTokenUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

public class AuthActivity extends BaseActivity{
    private Bundle fragmentBundle;
    private String viewType;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setElevation(0);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (PreferenceStorageManager.getLoggedInStatus(getApplicationContext())) {
            viewType = Constants.LOGIN_VIEW_TAG;
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.AUTH_VIEWPAGER_TAG:
                        AuthViewPagerFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.ABOUT_VIEW_TAG:
                        AboutFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = AuthFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = AuthFragment.newInstance(null);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
        FCMTokenUtils.refreshToken();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(AuthFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        final TwitterAuthClient twitterAuthClient = new TwitterAuthClient();

        if (twitterAuthClient.getRequestCode() == requestCode) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
    }
}
