package com.affinislabs.zakatify.app.ui.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.BooleanStateResponse;
import com.affinislabs.zakatify.app.api.responses.ChatSessionsResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.ChatSessionsAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.CommonUtils;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FirebaseUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PaginationScrollListener;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ChatSessionsFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, ZakatifyInterfaces.PaginationAdapterCallback {
    private CustomRecyclerView chatSessionsRecyclerView;
    private List<ChatSession> chatSessions = new ArrayList<ChatSession>();
    ;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private User user;
    private int pageNumber = 1;
    private int currentPage = pageNumber;
    private ChatSessionsAdapter adapter;
    private View mEmptyView;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new ChatSessionsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public ChatSessionsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }
        chatSessions = PreferenceStorageManager.getChatSessions(ZakatifyApplication.getAppInstance().getApplicationContext());

        if (user == null) {
            CommonUtils.displayShortToastMessage(getString(R.string.chat_load_error));
            closeFragment();
        }

        isLoading = false;
        hasListEnded = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_sessions, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.user_messages_toolbar_label));

        adapter = new ChatSessionsAdapter(getContext(), this, user, null);

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mEmptyView = view.findViewById(R.id.empty_view);
        chatSessionsRecyclerView = view.findViewById(R.id.chat_sessions_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        chatSessionsRecyclerView.hasFixedSize();
        chatSessionsRecyclerView.setLayoutManager(linearLayoutManager);
        chatSessionsRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        chatSessionsRecyclerView.setAdapter(adapter);
        chatSessionsRecyclerView.setEmptyView(mEmptyView);
        chatSessionsRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        if (chatSessions != null && chatSessions.size() >= pageSize) {
                            fetchChatSessions(user.getUsername(), currentPage, pageSize, false, true);
                        }
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        mSwipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.orange),
                ContextCompat.getColor(getActivity(), R.color.green),
                ContextCompat.getColor(getActivity(), R.color.blue)
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    fetchChatSessions(user.getUsername(), 1, pageSize, true, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (chatSessions != null && chatSessions.size() > 0) {
            loadAdapter(chatSessions);
            fetchChatSessions(user.getUsername(), 1, pageSize, false, false);
        } else {
            initializeData(true);
        }
    }

    void initializeData(boolean showVisualFeedback) {
        if (showVisualFeedback) {
            animateSwipeRefreshLayout(mSwipeRefreshLayout);
        }
        fetchChatSessions(user.getUsername(), 1, pageSize, showVisualFeedback, false);
    }

    private void loadAdapter(List<ChatSession> chatSessions) {
        adapter.addAll(chatSessions);
    }

    private void updateSavedChatSessions(List<ChatSession> chatSessions) {
        PreferenceStorageManager.saveChatSessions(ZakatifyApplication.getAppInstance().getApplicationContext(), chatSessions);
    }

    private void updateChatSessionList(List<ChatSession> chatSessionsParam) {
        if (chatSessions == null) {
            chatSessions = new ArrayList<ChatSession>();
        }
        if (chatSessionsParam != null && chatSessionsParam.size() > 0) {
            chatSessions.addAll(chatSessionsParam);
        }
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1 && chatSessions != null && chatSessions.size() > 0) {
            ChatSession chatSession = chatSessions.get(position);
            if (chatSession != null) {
                switch (v.getId()) {
                    case R.id.delete_thread:
                        confirmDeleteThread(getActivity(), chatSession.getMessageId(), position);
                        break;
                    case R.id.main_layout:
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.CHAT_SESSION, chatSession);
                        bundle.putString(Constants.MESSAGE_ID, chatSession.getMessageId());
                        bundle.putParcelable(Constants.OTHER_PARTICIPANT, chatSession.getOtherParticipant());
                        bundle.putParcelable(Constants.REQUESTING_PARTICIPANT, user);
                        switchFragment(MessagesFragment.newInstance(bundle));
                        break;
                }
            }
        }
    }

    public void confirmDeleteThread(Activity activity, final String messageId, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(getString(R.string.delete_chat_thread_dialog_title));
        builder.setMessage(getString(R.string.delete_chat_thread_dialog_content));
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showLoadingIndicator(getString(R.string.deleting_chat_thread_message));
                FirebaseUtils.deleteChatThread(messageId, new ZakatifyInterfaces.FirebaseChatDeletionCallback() {
                    @Override
                    public void onThreadDeleted(DatabaseError databaseError) {
                        if (databaseError != null) {
                            hideLoadingIndicator();
                            showErrorPopup(databaseError.getMessage());
                        } else {
                            adapter.onThreadDeletionCompleted(messageId, position);
                            ChatSession toBeDeleted = chatSessions.get(position);
                            if (toBeDeleted != null && toBeDeleted.getMessageId().equalsIgnoreCase(messageId)) {
                                chatSessions.remove(position);
                            }
                            deleteUserChatSessionOnServer(messageId, position);
                        }
                    }
                });
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.onThreadDeletionDismissed(messageId);
            }
        });
        builder.show();
    }

    private void deleteUserChatSessionOnServer(String messageId, int position) {
        showLoadingIndicator(getString(R.string.initiating_manual_donation_message));
        new ApiClient.NetworkCallsRunner(Constants.DELETE_USER_CHAT_SESSION_REQUEST, messageId, new ApiClientListener.DeleteUserChatSessionListener() {
            @Override
            public void onUserChatSessionDeleted(BooleanStateResponse booleanStateResponse) {
                hideLoadingIndicator();
                if (booleanStateResponse != null) {
                    if (booleanStateResponse.getStatus() == null && booleanStateResponse.getMessage() == null) {
                        if (booleanStateResponse.getState()) {
                            showSuccessPopup(getString(R.string.thread_deletion_success));
                            initializeData(false);
                        } else {
                            showErrorPopup(getString(R.string.chat_deletion_error));
                        }
                    } else {
                        showErrorPopup(booleanStateResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void fetchChatSessions(String username, int pageNumber, final int pageSize, boolean displayLoader, final boolean inLoadMoreMode) {
        final String message = ZakatifyApplication.getApplicationResources().getString(R.string.fetch_chat_sessions);
        if (NetworkUtils.isConnected(getContext())) {
            if (displayLoader) {
                showLoadingIndicator(message);
            }

            new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_CHAT_SESSIONS_REQUEST, username, new ApiClientListener.FetchUserChatSessionsListener() {
                @Override
                public void onUserChatSessionsFetched(ChatSessionsResponse chatSessionsResponse) {
                    hideLoadingIndicator();
                    endSwipeRefresh(mSwipeRefreshLayout);
                    isLoading = false;

                    if (inLoadMoreMode) {
                        adapter.removeLoadingFooter(ChatSessionsAdapter.class, adapter);
                    }

                    if (chatSessionsResponse != null) {
                        if (chatSessionsResponse.getStatus() == null && chatSessionsResponse.getMessage() == null) {
                            List<ChatSession> chatSessionsParam = chatSessionsResponse.getChatSessions();

                            if (chatSessionsParam == null || chatSessionsParam.size() == 0 || chatSessionsParam.size() < pageSize) {
                                hasListEnded = true;
                            }

                            if (!inLoadMoreMode) {
                                if (chatSessions != null) {
                                    chatSessions.clear();
                                }
                                adapter.clearItems();
                                updateSavedChatSessions(chatSessionsParam);
                            }

                            updateChatSessionList(chatSessionsParam);
                            if (isAdded() && isVisible()) {
                                loadAdapter(chatSessionsParam);
                            }

                            if (!hasListEnded) {
                                adapter.addLoadingFooter(ChatSessionsAdapter.class, adapter);
                            }
                        } else {
                            showErrorPopup(chatSessionsResponse.getMessage());
                        }

                    } else {
                        showErrorPopup(getString(R.string.null_response_label));
                    }
                }
            }).execute();
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getContext())) {
            fetchChatSessions(user.getUsername(), currentPage, pageSize, false, true);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }
}
