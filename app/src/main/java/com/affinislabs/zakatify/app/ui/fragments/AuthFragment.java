package com.affinislabs.zakatify.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.ApiModule;
import com.affinislabs.zakatify.app.api.responses.LoginResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.MainActivity;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.io.Serializable;
import java.util.Map;

public class AuthFragment extends BaseFragment implements View.OnClickListener, ZakatifyInterfaces.SocialAuthenticationListener {
    private User user;
    private Button emailAuthButton, facebookButton, twitterAuthButton;
    private TextView launchAboutView, launchPrivacyPolicyView;
    private String message, error;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new AuthFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public AuthFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }

        twitterAuthClient = new TwitterAuthClient();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        emailAuthButton = (Button) view.findViewById(R.id.email_auth_button);
        facebookButton = (Button) view.findViewById(R.id.facebook_auth_button);
        twitterAuthButton = (Button) view.findViewById(R.id.twitter_auth_button);
        launchPrivacyPolicyView = (TextView) view.findViewById(R.id.privacy_policy_button);
        launchAboutView = (TextView) view.findViewById(R.id.about_app_button);

        launchAboutView.setOnClickListener(this);
        launchPrivacyPolicyView.setOnClickListener(this);
        twitterAuthButton.setOnClickListener(this);
        emailAuthButton.setOnClickListener(this);
        facebookButton.setOnClickListener(this);

        PreferenceStorageManager.resetUser(ZakatifyApplication.getAppInstance().getApplicationContext());
    }

    @Override
    public void onAuthenticationCompleted(Map<String, String> accountData, Map<String, String> profileData) {
        makeAPICall(accountData, profileData);
    }

    private void makeAPICall(final Map<String, String> accountMap, final Map<String, String> profileData) {
        new ApiClient.NetworkCallsRunner(Constants.LOGIN_REQUEST, accountMap, new ApiClientListener.AccountAuthenticationListener() {
            @Override
            public void onAccountAuthenticated(LoginResponse loginResponse) {
                hideLoadingIndicator();
                if (loginResponse != null) {
                    if (loginResponse.getStatus() == null && loginResponse.getMessage() == null) {
                        User user = extractUserDetails(loginResponse);

                        if (user.getUsername() == null) {
                            Bundle bundle = new Bundle();
                            bundle.putParcelable(Constants.USER, user);
                            profileData.put(Constants.EMAIL_ADDRESS, accountMap.get(Constants.ID));
                            profileData.put(Constants.AUTH_TOKEN, user.getAuthToken());
                            bundle.putSerializable(Constants.PROFILE_DATA, (Serializable) profileData);
                            PreferenceStorageManager.saveAuthToken(ZakatifyApplication.getAppInstance().getApplicationContext(), user.getAuthToken());
                            startFragment(CreateUsernameFragment.newInstance(bundle), true);
                        } else {
                            ApiClient.NetworkCallsRunner.resetApiService();
                            ApiModule.resetApiClient();
                            saveUserData(ZakatifyApplication.getAppInstance().getApplicationContext(), user);
                            showMainActivity(user, getActivity());
                        }

                    } else {
                        showErrorPopup(loginResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.email_auth_button:
                startFragment(AuthViewPagerFragment.newInstance(bundle));
                break;
            case R.id.facebook_auth_button:
                if (NetworkUtils.isConnected(getContext())) {
                    manageFacebookAuthentication(this);
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
                break;
            case R.id.twitter_auth_button:
                if (NetworkUtils.isConnected(getContext())) {
                    twitterAuthClient.authorize(getActivity(), new TwitterHandler(this));
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
                break;
            case R.id.about_app_button:
                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).launchWebViewDialog(getActivity(), Constants.ABOUT_LINK, getString(R.string.about_zakatify));
                }
                break;
            case R.id.privacy_policy_button:
                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).launchWebViewDialog(getActivity(), Constants.PRIVACY_LINK, getString(R.string.privacy_label));
                }
                break;
        }
    }
}
