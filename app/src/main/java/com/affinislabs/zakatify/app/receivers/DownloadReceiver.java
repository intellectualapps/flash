package com.affinislabs.zakatify.app.receivers;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DownloadReceiver extends BroadcastReceiver {
    public DownloadReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
            long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            Intent localReceiver = new Intent();
            localReceiver.putExtra("ID", downloadId);
            localReceiver.setAction(Constants.LOCAL_RECEIVER_LISTENER);
            DownloadManager mDownloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(downloadId);

            Cursor cursor = mDownloadManager.query(query);
            if (cursor.moveToFirst()) {
                int downloadStatus = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                if (DownloadManager.STATUS_SUCCESSFUL == cursor.getInt(downloadStatus)) {
                    String downloadLocalUri = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                    localReceiver.putExtra(Constants.STATUS, Constants.DOWNLOAD_COMPLETE);
                    localReceiver.putExtra(Constants.DOWNLOAD_URI, downloadLocalUri);
                    localReceiver.putExtra(Constants.FILE_PATH, FileUtils.getRealPathFromURI(context, Uri.parse(downloadLocalUri)));
                } else if (DownloadManager.STATUS_FAILED == cursor.getInt(downloadStatus)) {
                    localReceiver.putExtra(Constants.STATUS, Constants.DOWNLOAD_FAILED);
                }

                context.sendBroadcast(localReceiver);
            }
        }
    }
}
