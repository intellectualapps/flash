package com.affinislabs.zakatify.app.models;

public class Counts {
    private int donationsCount;
    private int portfolioCount;
    private int followersCount;

    public int getDonationsCount() {
        return donationsCount;
    }

    public void setDonationsCount(int donationsCount) {
        this.donationsCount = donationsCount;
    }

    public int getPortfolioCount() {
        return portfolioCount;
    }

    public void setPortfolioCount(int portfolioCount) {
        this.portfolioCount = portfolioCount;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }
}
