package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.InvitationsResponse;
import com.affinislabs.zakatify.app.enums.InviteState;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Invite;
import com.affinislabs.zakatify.app.ui.customviews.CustomTextView;
import com.affinislabs.zakatify.app.utils.CommonUtils;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ListUtils;

import java.util.List;

public class InviteFriendsAdapter extends BaseRecyclerAdapter {

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomTextView userFullName, inviteFriendButton;
        ImageView userProfilePhoto;
        ProgressBar userDonationProgressMeter;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            userFullName = (CustomTextView) itemView.findViewById(R.id.full_name);
            userProfilePhoto = (ImageView) itemView.findViewById(R.id.profile_photo);
            inviteFriendButton = (CustomTextView) itemView.findViewById(R.id.invite_button);
            inviteFriendButton.setOnClickListener(this);
            itemView.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }

    private List<Invite> inviteList;
    private ClickListener clickListener;
    private String mainUsername;
    private ZakatifyInterfaces.ContactStateChangedListener contactStateChangedListener;
    private final int INVITE_ITEM = 2;

    public InviteFriendsAdapter(Context context, ClickListener clickListener, String mainUsername, ZakatifyInterfaces.ContactStateChangedListener contactStateChangedListener) {
        super(context);
        this.clickListener = clickListener;
        this.mainUsername = mainUsername;
        this.contactStateChangedListener = contactStateChangedListener;
    }

    public void setItems(List<Invite> items) {
        this.inviteList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (inviteList != null) {
            this.inviteList.clear();
            notifyDataSetChanged();
        }
    }

    public void addItem(Invite invite) {
        if (invite != null) {
            this.inviteList.add(invite);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.friend_item_layout, parent, false);

        return new InviteFriendsAdapter.ViewHolder(view, this.clickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final InviteFriendsAdapter.ViewHolder viewHolder = (InviteFriendsAdapter.ViewHolder) holder;
        final Invite invite = inviteList.get(position);
        if (invite != null) {
            if (!TextUtils.isEmpty(invite.getDisplayName())) {
                viewHolder.userFullName.setText(invite.getDisplayName());
            } else {
                viewHolder.userFullName.setText(invite.getInvitee());
            }
            if (!TextUtils.isEmpty(invite.getState())) {
                switch (invite.getState()) {
                    case Constants.INVITE_ACCEPTED:
                        viewHolder.inviteFriendButton.setEnabled(false);
                        viewHolder.inviteFriendButton.setText(R.string.invited_invite_state);
                        break;
                    case Constants.INVITE_REJECTED:
                        viewHolder.inviteFriendButton.setEnabled(false);
                        viewHolder.inviteFriendButton.setActivated(true);
                        viewHolder.inviteFriendButton.setText(R.string.rejected_invite_state);
                        break;
                    case Constants.INVITE_OPEN:
                        viewHolder.inviteFriendButton.setEnabled(false);
                        viewHolder.inviteFriendButton.setText(R.string.pending_invite_state);
                        break;
                    default:
                        viewHolder.inviteFriendButton.setEnabled(true);
                        viewHolder.inviteFriendButton.setText(R.string.invite_label);
                        break;
                }
            }

            viewHolder.inviteFriendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inviteUser(invite.getInvitor(), invite.getInvitee(), viewHolder, position);
                }
            });
        }
    }

    private void inviteUser(String invitorUsername, String inviteeEmail, final ViewHolder viewHolder, final int position) {
        new ApiClient.NetworkCallsRunner(Constants.SEND_USER_INVITATION_REQUEST, invitorUsername, inviteeEmail, new ApiClientListener.SendUserInvitationListener() {
            @Override
            public void onInvitationSent(InvitationsResponse invitationsResponse) {
                if (invitationsResponse != null) {
                    if (invitationsResponse.getStatus() == null && invitationsResponse.getDeveloperMessage() == null) {
                        inviteList.get(position).setState(Constants.INVITE_OPEN);
                        if (contactStateChangedListener != null) {
                            contactStateChangedListener.onInviteStateChanged(position, InviteState.fromString(Constants.INVITE_OPEN));
                        }
                        notifyDataSetChanged();
                    } else {
                        CommonUtils.displayShortToastMessage(String.valueOf(invitationsResponse.getMessage()));
                    }
                }
            }
        }).execute();
    }

    private void reloadInviteList() {
        Intent intent = new Intent();
        intent.setAction(Constants.REFRESH_INVITE_FRIEND_LIST_FILTER);
        intent.putExtra(Constants.REFRESH_FLAG, true);
        context.sendBroadcast(intent);
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(inviteList) ? 0 : inviteList.size();
    }
}