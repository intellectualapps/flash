package com.affinislabs.zakatify.app.ui.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ui.dialogs.CalculateGoalDialog;
import com.affinislabs.zakatify.app.ui.dialogs.WebviewDialog;
import com.affinislabs.zakatify.app.ui.fragments.AboutFragment;
import com.affinislabs.zakatify.app.ui.fragments.InviteFriendsFragment;
import com.affinislabs.zakatify.app.ui.fragments.SearchViewPagerFragment;
import com.affinislabs.zakatify.app.ui.fragments.TopZakatifiersViewPagerFragment;
import com.affinislabs.zakatify.app.utils.Constants;

public class MetaActivity extends BaseActivity {
    private Bundle fragmentBundle;
    private String viewType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meta);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setElevation(0);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.ABOUT_VIEW_TAG:
                        frag = AboutFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.TOP_ZAKATIFIERS_VIEW_TAG:
                        frag = TopZakatifiersViewPagerFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.INVITE_FRIENDS_VIEW_TAG:
                        frag = InviteFriendsFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.SEARCH_VIEW_TAG:
                        frag = SearchViewPagerFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        finish();
                        return;
                }
            } else {
                finish();
                return;
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }
}
