package com.affinislabs.zakatify.app.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.ReviewsResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.Review;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.ProfileActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.CharityReviewsAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;


public class CharityReviewsFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, ZakatifyInterfaces.PaginationAdapterCallback {
    private CustomRecyclerView charityReviewsRecyclerView;
    private List<Review> reviews;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CharityReviewsAdapter adapter;
    private int pageNumber = 1;
    private int currentPage = pageNumber;
    private Charity charity;
    private User user;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new CharityReviewsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public CharityReviewsFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reviews = new ArrayList<Review>();

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.CHARITY_REVIEWS_LIST)) {
                reviews = getArguments().getParcelableArrayList(Constants.CHARITY_REVIEWS_LIST);
            }

            if (getArguments().containsKey(Constants.CHARITY)) {
                charity = getArguments().getParcelable(Constants.CHARITY);
            }
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }

        isLoading = false;
        hasListEnded = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_charity_reviews, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.charity_reviews_toolbar_label));

        adapter = new CharityReviewsAdapter(getContext(), this, this);
        charityReviewsRecyclerView = view.findViewById(R.id.charity_reviews_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);
        mRetryButton = mEmptyView.findViewById(R.id.retry_button);
        mEmptyViewLabel.setText(getString(R.string.no_records_label));

        charityReviewsRecyclerView.setEmptyView(mEmptyView);

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blue),
                ContextCompat.getColor(getContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchCharityReviews(String.valueOf(charity.getCharityId()), 1, pageSize, false);
                    } else {
                        endSwipeRefresh(swipeRefreshLayout);
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        charityReviewsRecyclerView.setAdapter(adapter);
        charityReviewsRecyclerView.setLayoutManager(linearLayoutManager);
        charityReviewsRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        charityReviewsRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        if (reviews != null && reviews.size() >= pageSize) {
                            fetchCharityReviews(String.valueOf(charity.getCharityId()), currentPage, pageSize, true);
                        }
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        if (NetworkUtils.isConnected(getContext())) {
            showLoadingIndicator(getString(R.string.fetch_charity_reviews_loading_message));
            fetchCharityReviews(String.valueOf(charity.getCharityId()), 1, pageSize, false);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }

    private void loadAdapter(List<Review> charityReviews) {
        if (isAdded() && isVisible()) {
            adapter.addAll(charityReviews);
        }
    }

    private void updateReviewList(List<Review> reviewListParam) {
        if (reviews == null) {
            reviews = new ArrayList<Review>();
        }
        if (reviewListParam != null && reviewListParam.size() > 0) {
            reviews.addAll(reviewListParam);
        }
    }

    private void fetchCharityReviews(String charityId, int pageNumber, final int pageSize, final boolean inLoadMoreMode) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_CHARITY_REVIEWS_REQUEST, charityId, String.valueOf(pageNumber), String.valueOf(pageSize), new ApiClientListener.FetchReviewsListener() {
            @Override
            public void onReviewsFetched(ReviewsResponse reviewsResponse) {
                hideLoadingIndicator();
                endSwipeRefresh(swipeRefreshLayout);
                isLoading = false;

                if (inLoadMoreMode) {
                    adapter.removeLoadingFooter(CharityReviewsAdapter.class, adapter);
                }

                if (reviewsResponse != null) {
                    if (reviewsResponse.getStatus() == null && reviewsResponse.getMessage() == null) {
                        List<Review> reviewListParam = reviewsResponse.getReviews();

                        if (reviewListParam == null || reviewListParam.size() == 0 || reviewListParam.size() < pageSize) {
                            hasListEnded = true;
                        }

                        if (!inLoadMoreMode) {
                            if (reviews != null) {
                                reviews.clear();
                            }
                            adapter.clearItems();
                        }

                        updateReviewList(reviewListParam);

                        loadAdapter(reviewListParam);

                        if (!hasListEnded) {
                            adapter.addLoadingFooter(CharityReviewsAdapter.class, adapter);
                        }
                    } else {
                        showErrorPopup(reviewsResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1 && reviews != null && reviews.size() > 0) {
            Intent intent = new Intent(getContext(), ProfileActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constants.USERNAME, reviews.get(position).getUsername());
            intent.putExtra(Constants.MAIN_USERNAME, user.getUsername());
            intent.putExtra(Constants.VIEW_TYPE, Constants.PUBLIC_USER_PROFILE_VIEW_TAG);
            startActivity(intent);
        }
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getContext())) {
            fetchCharityReviews(String.valueOf(charity.getCharityId()), currentPage, pageSize, true);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }
}
