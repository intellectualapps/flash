package com.affinislabs.zakatify.app.api.responses;

import com.affinislabs.zakatify.app.models.TopZakatifier;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TopZakatifiersResponse extends DefaultResponse {
    @SerializedName("topZakatifiers")
    @Expose
    private List<TopZakatifier> topZakatifiers = null;

    public List<TopZakatifier> getTopZakatifiers() {
        return topZakatifiers;
    }

    public void setTopZakatifiers(List<TopZakatifier> topZakatifiers) {
        this.topZakatifiers = topZakatifiers;
    }
}
