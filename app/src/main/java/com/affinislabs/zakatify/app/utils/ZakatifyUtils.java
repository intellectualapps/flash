package com.affinislabs.zakatify.app.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.Category;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.FeedActivity;
import com.affinislabs.zakatify.app.models.ZakatGoal;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ZakatifyUtils {

    public static String formatAmount(Double amount) {
        return String.format(Locale.getDefault(), "%.0f", amount);
    }

    public static String formatNumberAmount(float amount) {
        return String.format(Locale.getDefault(), "%.0f", amount);
    }

    public static String formatCurrencyAmount(Object amount) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        numberFormat.setMinimumFractionDigits(0);
        return numberFormat.format(amount);
    }

    public static String formatCurrencyAmountWithDecimalPlaces(Object amount) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        numberFormat.setMinimumFractionDigits(2);
        return numberFormat.format(amount);
    }

    public static String formatCurrencyAmountWithDecimalPlaces(Object amount, int decimalPlaces) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        numberFormat.setMinimumFractionDigits(decimalPlaces);
        return numberFormat.format(amount);
    }

    public static String timeSinceReview(String reviewDateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date currentDate = new Date();
        try {
            Date reviewDate = dateFormat.parse(reviewDateString);
            long diff = currentDate.getTime() - reviewDate.getTime();
            return getInterval(diff);
        } catch (ParseException e) {
        }
        return null;
    }

    public static String getInterval(long difference) {
        long daysSince = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
        daysSince = daysSince < 1 ? 1 : daysSince;
        if (daysSince < 7) {
            return daysSince + "d";
        } else if (daysSince >= 7 && daysSince < 30) {
            return daysSince / 7 + "w";
        } else if (daysSince >= 30) {
            return daysSince / 30 + "m";
        }
        return null;
    }

    public static String formatDecimal(Object amount) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        numberFormat.setMinimumFractionDigits(0);
        return numberFormat.format(amount);
    }

    public static Spannable formatDonorAndAmountSummary(Context context, Integer numberOfDonors, Double totalAmountDonated) {
        String summary = context.getString(R.string.no_donation_summary);
        Spannable styledSummary = new SpannableString(summary);
        if (numberOfDonors != null && numberOfDonors > 0 && totalAmountDonated > 0) {
            String formattedAmount = ZakatifyUtils.formatCurrencyAmount(totalAmountDonated);
            summary = (numberOfDonors > 5 ? " +" : " ").concat(context.getString(R.string.donor_amount_summary_placeholder_label, numberOfDonors, formattedAmount));
            styledSummary = new SpannableString(summary);
            int startIndex = summary.indexOf(formattedAmount);
            int endIndex = startIndex + formattedAmount.length();
            styledSummary.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorPrimary)), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return styledSummary;
    }

    public static Spannable getStyledNewsFeedActivity(List<FeedActivity> feedActivityList, Context context) {
        if (ListUtils.isNotEmpty(feedActivityList)) {
            FeedActivity feedActivity = feedActivityList.get(0);
            String summary = String.format("%s %s", feedActivity.getActivityBy(), feedActivity.getActivityType());
            Spannable styledSummary = new SpannableString(summary);
            int startIndex = 0, endIndex = startIndex + feedActivity.getActivityBy().length();
            styledSummary.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.colorBlack)), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return styledSummary;
        }
        return null;
    }

    public static String formatDonorSummary(Context context, Integer numberOfDonors) {
        String summary = "";
        if (numberOfDonors != null && numberOfDonors > 0) {
            summary = (numberOfDonors > 5 ? " +" : " ").concat(context.getString(numberOfDonors > 1 ? R.string.plural_donor_summary_placeholder_label : R.string.single_donor_summary_placeholder_label, numberOfDonors));
        }

        return summary;
    }


    public static String formatDecimalNumber(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return String.valueOf(bd.doubleValue());
    }

    public static ZakatGoal convertMapToObject(Double amount, Map<String, String> zakatCalculationData) {
        ZakatGoal tempZakatGoal = new ZakatGoal();
        tempZakatGoal.setAmount(amount);
        tempZakatGoal.setCash(Double.valueOf(zakatCalculationData.get(Constants.CASH)));
        tempZakatGoal.setGold(Double.valueOf(zakatCalculationData.get(Constants.GOLD)));
        tempZakatGoal.setRealEstate(Double.valueOf(zakatCalculationData.get(Constants.REAL_ESTATE)));
        tempZakatGoal.setInvestment(Double.valueOf(zakatCalculationData.get(Constants.INVESTMENT)));
        tempZakatGoal.setPersonalUse(Double.valueOf(zakatCalculationData.get(Constants.PERSONAL_USE)));
        tempZakatGoal.setLiability(Double.valueOf(zakatCalculationData.get(Constants.LIABILITY)));
        return tempZakatGoal;
    }

    public static String getCategoryTags(List<Category> categories, String tabSeparator) {
        List<String> categoryTags = new ArrayList<String>();

        for (Category category : categories) {
            categoryTags.add("#" + category.getDescription());
        }

        return TextUtils.join(tabSeparator, categoryTags.toArray());
    }
}
