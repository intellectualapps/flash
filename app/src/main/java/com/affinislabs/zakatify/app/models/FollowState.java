package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowState implements Parcelable {

    @SerializedName("state")
    @Expose
    private boolean state;

    public FollowState() {

    }

    public FollowState(Parcel in) {
        state = in.readByte() != 0;
    }

    public static final Creator<FollowState> CREATOR = new Creator<FollowState>() {
        @Override
        public FollowState createFromParcel(Parcel in) {
            return new FollowState(in);
        }

        @Override
        public FollowState[] newArray(int size) {
            return new FollowState[size];
        }
    };

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (state ? 1 : 0));
    }
}
