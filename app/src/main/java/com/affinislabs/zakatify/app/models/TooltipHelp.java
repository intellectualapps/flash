package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class TooltipHelp implements Parcelable{

    private String tooltipId;
    private String tooltipTitle;
    private String tooltipContent;

    public TooltipHelp(String tooltipId, String tooltipTitle, String tooltipContent) {
        this.tooltipId = tooltipId;
        this.tooltipTitle = tooltipTitle;
        this.tooltipContent = tooltipContent;
    }

    protected TooltipHelp(Parcel in) {
        tooltipId = in.readString();
        tooltipTitle = in.readString();
        tooltipContent = in.readString();
    }

    public static final Creator<TooltipHelp> CREATOR = new Creator<TooltipHelp>() {
        @Override
        public TooltipHelp createFromParcel(Parcel in) {
            return new TooltipHelp(in);
        }

        @Override
        public TooltipHelp[] newArray(int size) {
            return new TooltipHelp[size];
        }
    };

    public String getTooltipId() {
        return tooltipId;
    }

    public void setTooltipId(String tooltipId) {
        this.tooltipId = tooltipId;
    }

    public String getTooltipTitle() {
        return tooltipTitle;
    }

    public void setTooltipTitle(String tooltipTitle) {
        this.tooltipTitle = tooltipTitle;
    }

    public String getTooltipContent() {
        return tooltipContent;
    }

    public void setTooltipContent(String tooltipContent) {
        this.tooltipContent = tooltipContent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tooltipId);
        dest.writeString(tooltipTitle);
        dest.writeString(tooltipContent);
    }
}
