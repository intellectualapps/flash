package com.affinislabs.zakatify.app.api.responses;

import com.affinislabs.zakatify.app.models.PaypalAccount;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentResponse extends DefaultResponse{

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("paymentOptions")
    @Expose
    private List<PaypalAccount> paymentOptions = null;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("state")
    @Expose
    private boolean state;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<PaypalAccount> getPaymentOptions() {
        return paymentOptions;
    }

    public void setPaymentOptions(List<PaypalAccount> paymentOptions) {
        this.paymentOptions = paymentOptions;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
