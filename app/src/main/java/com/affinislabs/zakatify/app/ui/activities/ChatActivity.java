package com.affinislabs.zakatify.app.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ui.fragments.ChatSessionsFragment;
import com.affinislabs.zakatify.app.ui.fragments.MessagesFragment;
import com.affinislabs.zakatify.app.utils.Constants;

public class ChatActivity extends BaseActivity{
    private Bundle fragmentBundle;
    private String viewType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        toolbar = findViewById(R.id.toolbar);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.CHAT_SESSIONS_LIST_VIEW_TAG:
                        frag = ChatSessionsFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.MESSAGE_LIST_VIEW_TAG:
                        toolbar.setTitle(getString(R.string.conversation_view_tag));
                        frag = MessagesFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = ChatSessionsFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = ChatSessionsFragment.newInstance(null);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }
}
