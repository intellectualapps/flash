package com.affinislabs.zakatify.app.ui.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.OnBoardingContent;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.fragments.OnBoardingChildFragment;
import com.affinislabs.zakatify.app.utils.Constants;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.FragmentSlide;

import java.util.ArrayList;

public class OnboardingActivity extends IntroActivity {
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setFullscreen(false);

        for (Object o : getOnBoardingContentList()) {
            OnBoardingContent onBoardingContent = (OnBoardingContent) o;
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.ONBOARDING_CONTENT, onBoardingContent);
            Fragment onBoardingChildFragment = OnBoardingChildFragment.newInstance(bundle);

            addSlide(new FragmentSlide.Builder()
                    .background(R.color.colorWhite)
                    .backgroundDark(R.color.colorPrimary)
                    .fragment(onBoardingChildFragment)
                    .build());
        }

        setButtonBackVisible(true);
        setButtonBackFunction(BUTTON_BACK_FUNCTION_BACK);

        setButtonNextVisible(true);
        setButtonNextFunction(BUTTON_NEXT_FUNCTION_NEXT_FINISH);
        setButtonCtaVisible(false);

        setPageScrollDuration(500);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            setImmersive(true);
        }
    }

    private ArrayList getOnBoardingContentList() {
        ArrayList<OnBoardingContent> onBoardingContentArrayList = new ArrayList<OnBoardingContent>();

        onBoardingContentArrayList.add(new OnBoardingContent(getString(R.string.onboarding_title_one), getString(R.string.onboarding_content_one), R.drawable.ic_onboarding2));
        onBoardingContentArrayList.add(new OnBoardingContent(getString(R.string.onboarding_title_two), getString(R.string.onboarding_content_two), R.drawable.ic_onboarding1));
        onBoardingContentArrayList.add(new OnBoardingContent(getString(R.string.onboarding_title_three), getString(R.string.onboarding_content_three), R.drawable.ic_onboarding3));
        onBoardingContentArrayList.add(new OnBoardingContent(getString(R.string.onboarding_title_four), getString(R.string.onboarding_content_four), R.drawable.ic_onboarding4));
        return onBoardingContentArrayList;
    }
}
