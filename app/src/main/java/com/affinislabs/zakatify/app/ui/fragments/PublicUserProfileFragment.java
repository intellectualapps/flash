package com.affinislabs.zakatify.app.ui.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.BooleanStateResponse;
import com.affinislabs.zakatify.app.api.responses.ChatSessionsResponse;
import com.affinislabs.zakatify.app.api.responses.PublicUserResponse;
import com.affinislabs.zakatify.app.api.responses.ReviewsResponse;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.Counts;
import com.affinislabs.zakatify.app.models.Donation;
import com.affinislabs.zakatify.app.models.DonationProgress;
import com.affinislabs.zakatify.app.models.FollowState;
import com.affinislabs.zakatify.app.models.Review;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.models.UserBoard;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.CharityActivity;
import com.affinislabs.zakatify.app.ui.activities.ChatActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.UserDonationsAdapter;
import com.affinislabs.zakatify.app.ui.adapters.UserReviewsAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.ZakatifyUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PublicUserProfileFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener {
    private View mSnackBarView;
    private View mFollowersContainer;
    private View mActionButtonsContainer;
    private Button messageButton, followButton;
    private ImageView mUserProfilePhoto;

    private TextView mUserFullnameView, mUsernameView;
    private TextView portfolioCountView, pledgeCountView, badgeCountView, followerCountView;
    private TextView mDonationActivityLabel, mUserReviewsLabel, mViewDonationsButton, mReadReviewsButton;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mCurrentZakatDonation, mTotalZakatGoal, mPercentageProgress;
    private TextView mRankView, mPointsView;
    private ProgressBar zakatProgressMeter;
    private CustomRecyclerView reviewsRecyclerView;
    private CustomRecyclerView donationsRecyclerView;

    private User user = null;
    private FollowState followState;
    private String username, mainUsername;
    private ProgressDialog progressDialog;
    private String pageNumber = "1", pageSize = "10";
    private UserReviewsAdapter userReviewsAdapter;
    private UserDonationsAdapter userDonationsAdapter;

    private List<Review> reviews = new ArrayList<Review>();

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new PublicUserProfileFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public PublicUserProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USERNAME)) {
                username = getArguments().getString(Constants.USERNAME);
            }

            if (getArguments().containsKey(Constants.MAIN_USERNAME)) {
                mainUsername = getArguments().getString(Constants.MAIN_USERNAME);
            }
        }

        progressDialog = new ProgressDialog(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_public_profile, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.profile_details_label));
        mFollowersContainer = view.findViewById(R.id.followers_container);
        mActionButtonsContainer = view.findViewById(R.id.action_buttons_container);
        messageButton = (Button) view.findViewById(R.id.message_button);
        followButton = (Button) view.findViewById(R.id.follow_button);
        mUserProfilePhoto = (ImageView) view.findViewById(R.id.profile_photo);
        mUserFullnameView = (TextView) view.findViewById(R.id.fullname);
        mUsernameView = (TextView) view.findViewById(R.id.username);
        mDonationActivityLabel = (TextView) view.findViewById(R.id.donation_activity_label);
        mUserReviewsLabel = (TextView) view.findViewById(R.id.user_reviews_label);
        mReadReviewsButton = (TextView) view.findViewById(R.id.read_reviews_button);
        mViewDonationsButton = (TextView) view.findViewById(R.id.view_donations_button);
        reviewsRecyclerView = (CustomRecyclerView) view.findViewById(R.id.reviews_list);
        donationsRecyclerView = (CustomRecyclerView) view.findViewById(R.id.donations_list);
        zakatProgressMeter = (ProgressBar) view.findViewById(R.id.zakat_progress_wheel);
        mCurrentZakatDonation = (TextView) view.findViewById(R.id.amount_donated);
        mTotalZakatGoal = (TextView) view.findViewById(R.id.total_zakat_goal);
        mPercentageProgress = (TextView) view.findViewById(R.id.zakat_percentage_completed);
        mRankView = (TextView) view.findViewById(R.id.rank_view);
        mPointsView = (TextView) view.findViewById(R.id.points_view);
        portfolioCountView = (TextView) view.findViewById(R.id.portfolio_count);
        pledgeCountView = (TextView) view.findViewById(R.id.pledges_count);
        badgeCountView = (TextView) view.findViewById(R.id.badges_count);
        followerCountView = (TextView) view.findViewById(R.id.followers_count);

        userReviewsAdapter = new UserReviewsAdapter(getContext(), this, user, null);
        userDonationsAdapter = new UserDonationsAdapter(getContext(), this);
        reviewsRecyclerView.setAdapter(userReviewsAdapter);
        reviewsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        reviewsRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        donationsRecyclerView.setAdapter(userReviewsAdapter);
        donationsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        donationsRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));


        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blue),
                ContextCompat.getColor(getContext(), R.color.colorRed),
                ContextCompat.getColor(getContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchUserDetails(username, true);
                        fetchUserReviews(username, pageNumber, pageSize);
                    } else {
                        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (mainUsername != null) {
            if (mainUsername.equalsIgnoreCase(username)) {
                mActionButtonsContainer.setVisibility(View.GONE);
            } else {
                mActionButtonsContainer.setVisibility(View.VISIBLE);
            }
        }

        messageButton.setOnClickListener(this);
        mFollowersContainer.setOnClickListener(this);
        followButton.setOnClickListener(this);
        mViewDonationsButton.setOnClickListener(this);
        mReadReviewsButton.setOnClickListener(this);

        loadReviewsResponse(null);
        loadDonationsHistory(null);

        if (NetworkUtils.isConnected(getContext())) {
            fetchUserDetails(username, true);
            fetchUserReviews(username, pageNumber, pageSize);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void loadUserData(User user) {
        if (user != null && isAdded() && isVisible()) {
            mUserFullnameView.setText(user.getFullName());
            mUsernameView.setText(getString(R.string.username_placeholder, user.getUsername()));
            ImageUtils.loadImageUrl(mUserProfilePhoto, getContext(), user.getPhotoUrl());
        }
    }

    private void loadMetaData(UserBoard userBoard, DonationProgress donationProgress, Counts counts, FollowState followState) {
        if (userBoard != null && donationProgress != null && counts != null && isAdded() && isVisible()) {
            this.followState = followState;
            zakatProgressMeter.setProgress((int) donationProgress.getPercentageCompleted());
            mCurrentZakatDonation.setText(ZakatifyUtils.formatCurrencyAmount(donationProgress.getTotalDonation()));
            String formattedAmount = getResources().getString(R.string.total_donation_format, ZakatifyUtils.formatCurrencyAmount(donationProgress.getZakatGoal()));
            mTotalZakatGoal.setText(formattedAmount);
            mPercentageProgress.setText(String.format("%s%%", ZakatifyUtils.formatDecimalNumber(donationProgress.getPercentageCompleted(), 2)));

            mRankView.setText(String.valueOf(userBoard.getRank()));
            mPointsView.setText(String.valueOf(userBoard.getPoints()));

            portfolioCountView.setText(String.valueOf(counts.getPortfolioCount() > 0 ? counts.getPortfolioCount() : getString(R.string.none_label)));
            followerCountView.setText(String.valueOf(counts.getFollowersCount() > 0 ? counts.getFollowersCount() : getString(R.string.none_label)));
            pledgeCountView.setText(String.valueOf(counts.getDonationsCount() > 0 ? counts.getDonationsCount() : getString(R.string.none_label)));
            badgeCountView.setText(R.string.none_label);

            if (followState != null) {
                boolean state = followState.getState();
                adjustFollowState(state);
            }
        }
    }

    private void adjustFollowState(boolean state) {
        followButton.setText(state ? getString(R.string.unfollow_label) : getString(R.string.follow_label));
        followButton.setBackground(ContextCompat.getDrawable(getContext(), state ? R.drawable.curved_bordered_deselected_button : R.drawable.curved_bordered_selected_button));
    }

    private void loadReviewsResponse(ReviewsResponse reviewsResponse) {
        if (reviewsResponse != null && reviewsResponse.getReviews() != null && reviewsResponse.getReviews().size() > 0) {
            List<Review> reviews = reviewsResponse.getReviews();
            int reviewsCount = reviewsResponse.getTotalCount();
            List<Review> displayedReviews = reviews.subList(0, reviewsCount > 5 ? 5 : reviewsCount);
            mUserReviewsLabel.setText(getString(R.string.latest_reviews_label));
            mReadReviewsButton.setVisibility(reviewsCount > 5 ? View.VISIBLE : View.GONE);
            mReadReviewsButton.setText(getString(R.string.read_reviews_label, reviewsCount));
            reviewsRecyclerView.setVisibility(View.VISIBLE);
            userReviewsAdapter.setItems(displayedReviews);
        } else {
            mReadReviewsButton.setVisibility(View.GONE);
            reviewsRecyclerView.setVisibility(View.GONE);
            mUserReviewsLabel.setText(getString(R.string.no_reviews_label));
        }
    }

    private void loadDonationsHistory(List<Donation> donations) {
        if (donations != null && donations.size() > 0 && isAdded() && isVisible()) {
            int donationsCount = donations.size();
            List<Donation> donationList = donations.subList(0, donationsCount > 5 ? 5 : donationsCount);
            mViewDonationsButton.setText(getString(R.string.view_donations_label));
            mViewDonationsButton.setVisibility(donationsCount > 5 ? View.VISIBLE : View.GONE);
            mViewDonationsButton.setText(getString(R.string.view_donations_label, donationsCount));
            donationsRecyclerView.setVisibility(View.VISIBLE);
            userDonationsAdapter.setItems(donationList);
        } else {
            mViewDonationsButton.setVisibility(View.GONE);
            donationsRecyclerView.setVisibility(View.GONE);
            mDonationActivityLabel.setText(getString(R.string.no_donation_activity_label));
        }
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.profile_update_loading);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.message_button:
                initializeChatSession(mainUsername, username, true);
                break;
            case R.id.follow_button:
                if (followState.getState()) {
                    followUser(mainUsername, user.getUsername(), false);
                } else {
                    followUser(mainUsername, user.getUsername(), true);
                }
                break;
            case R.id.read_reviews_button:
                bundle.putParcelable(Constants.USER, user);
                bundle.putSerializable(Constants.USER_REVIEWS_LIST, (Serializable) reviews);
                startFragment(UserReviewsFragment.newInstance(bundle));
                break;
            case R.id.view_donations_button:
                break;
            case R.id.followers_container:
                bundle = new Bundle();
                bundle.putString(Constants.USERNAME, username);
                bundle.putString(Constants.MAIN_USERNAME, mainUsername);
                startFragment(FollowersFragment.newInstance(bundle));
                break;
        }
    }

    private void initializeChatSession(String requestingParticipant, String otherParticipant, boolean showLoader) {
        showLoadingIndicator(showLoader, getString(R.string.initialize_chat_session_progress_message));
        new ApiClient.NetworkCallsRunner(Constants.INITIALIZE_CHAT_SESSION_REQUEST, requestingParticipant, otherParticipant, new ApiClientListener.InitializeChatSessionsListener() {
            @Override
            public void onChatSessionInitialized(ChatSessionsResponse chatSessionsResponse) {
                endSwipeRefresh(swipeRefreshLayout);
                hideLoadingIndicator();

                if (chatSessionsResponse != null) {
                    if (chatSessionsResponse.getStatus() == null && chatSessionsResponse.getDeveloperMessage() == null) {
                        ChatSession chatSession = new ChatSession();
                        chatSession.setMessageId(chatSessionsResponse.getMessageId());
                        chatSession.setRequestingParticipant(chatSessionsResponse.getRequestingParticipant());
                        chatSession.setOtherParticipant(chatSessionsResponse.getOtherParticipant());

                        Intent intent = new Intent(getActivity(), ChatActivity.class);
                        intent.putExtra(Constants.CHAT_SESSION, chatSession);
                        intent.putExtra(Constants.MESSAGE_ID, chatSession.getMessageId());
                        intent.putExtra(Constants.OTHER_PARTICIPANT, chatSession.getOtherParticipant());
                        intent.putExtra(Constants.REQUESTING_PARTICIPANT, chatSession.getRequestingParticipant());
                        intent.putExtra(Constants.VIEW_TYPE, Constants.MESSAGE_LIST_VIEW_TAG);
                        startActivity(intent);
                    } else {
                        showErrorPopup(chatSessionsResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void fetchUserDetails(String username, boolean showLoader) {
        showLoadingIndicator(showLoader, getString(R.string.fetching_user_details_progress_message));
        new ApiClient.NetworkCallsRunner(Constants.FETCH_PUBLIC_USER_DETAILS_REQUEST, username, new ApiClientListener.FetchPublicUserDetailsListener() {
            @Override
            public void onUserDetailsFetched(PublicUserResponse publicUserResponse) {
                hideLoadingIndicator();
                endSwipeRefresh(swipeRefreshLayout);

                if (publicUserResponse != null) {
                    if (publicUserResponse.getStatus() == null && publicUserResponse.getDeveloperMessage() == null) {
                        user = publicUserResponse.getUser();
                        loadUserData(user);
                        loadMetaData(publicUserResponse.getUserBoard(), publicUserResponse.getDonationProgress(), publicUserResponse.getCounts(), publicUserResponse.getFollowState());
                    } else {
                        showErrorPopup(publicUserResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void followUser(String follower, String followed, final boolean state) {
        showLoadingIndicator(state ? getString(R.string.processing_follow_request_message) : getString(R.string.processing_unfollow_request_message));
        new ApiClient.NetworkCallsRunner(Constants.FOLLOW_USER_REQUEST, follower, followed, state, new ApiClientListener.FollowUserListener() {
            @Override
            public void onFollowStateChanged(BooleanStateResponse booleanStateResponse) {
                hideLoadingIndicator();
                if (booleanStateResponse != null) {
                    if (booleanStateResponse.getStatus() == null && booleanStateResponse.getDeveloperMessage() == null) {
                        adjustFollowState(state);
                        followState.setState(state);
                        fetchUserDetails(username, false);
                    } else {
                        showErrorPopup(booleanStateResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void fetchUserReviews(final String id, String pageNumber, String pageSize) {
        new ApiClient.NetworkCallsRunner(Constants.FETCH_USER_REVIEWS_REQUEST, id, pageNumber, pageSize, new ApiClientListener.FetchReviewsListener() {
            @Override
            public void onReviewsFetched(ReviewsResponse reviewsResponse) {
                if (reviewsResponse != null) {
                    if (reviewsResponse.getStatus() == null && reviewsResponse.getMessage() == null) {
                        reviews = reviewsResponse.getReviews();
                        if (isAdded() && isVisible()) {
                            loadReviewsResponse(reviewsResponse);
                        }
                    }
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1) {
            Review review = reviews.get(position);
            switch (v.getId()) {
                default:
                    break;
                case R.id.charity_name:
                    Intent intent = new Intent(getContext(), CharityActivity.class);
                    Charity charity = new Charity(review.getCharityId(), review.getCharity());
                    intent.putExtra(Constants.CHARITY, charity);
                    intent.putExtra(Constants.USER, user);
                    intent.putExtra(Constants.VIEW_TYPE, Constants.CHARITY_DETAILS_VIEW_TAG);
                    startActivity(intent);
                    break;
            }
        }
    }
}
