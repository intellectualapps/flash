package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.PaypalAccount;
import com.affinislabs.zakatify.app.ui.customviews.SwipeRevealLayout;
import com.affinislabs.zakatify.app.ui.customviews.ViewBinderHelper;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class PaymentOptionsAdapter extends BaseRecyclerAdapter {
    private List<PaypalAccount> paypalAccounts;
    private ClickListener clickListener;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();

    public PaymentOptionsAdapter(Context context, ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
        binderHelper.setOpenOnlyOne(true);
    }

    public void setItems(List<PaypalAccount> items) {
        this.paypalAccounts = items;
        resetSwipeLockState();
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (paypalAccounts != null) {
            this.paypalAccounts.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(PaypalAccount paypalAccount) {
        if (paypalAccount != null) {
            this.paypalAccounts.add(paypalAccount);
            notifyDataSetChanged();
        }
    }

    private void resetSwipeLockState() {
        if (ListUtils.isNotEmpty(paypalAccounts)) {
            for (PaypalAccount paypalAccount : paypalAccounts) {
                binderHelper.unlockSwipe(paypalAccount.getId());
                binderHelper.closeLayout(paypalAccount.getId());
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.payment_option_layout, parent, false);

        return new PaymentOptionsAdapter.ViewHolder(view, this.clickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final PaymentOptionsAdapter.ViewHolder viewHolder = (PaymentOptionsAdapter.ViewHolder) holder;
        PaypalAccount paypalAccount = paypalAccounts.get(position);
        if (paypalAccount != null) {
            viewHolder.mPaymentOptionEmailAddress.setText(getObfuscatedEmailAddress(paypalAccount.getPaypalEmail()));
            binderHelper.bind(viewHolder.swipeLayout, paypalAccount.getId());
            if (!paypalAccount.getStatus().equalsIgnoreCase(Constants.PRIMARY_LABEL)) {
                viewHolder.accountStatusLabel.setText(R.string.make_primary_button_label_one_line);
            } else {
                binderHelper.lockSwipe(paypalAccount.getId());
                viewHolder.accountStatusLabel.setText(R.string.primary_payment_option_label);
            }
        }
    }

    private String getObfuscatedEmailAddress(String emailAddress) {
        if (!TextUtils.isEmpty(emailAddress)) {
            String values[] = emailAddress.split("@");
            String email = values[0];
            String domain = values[1];
            return email.substring(0, 3).concat("***").concat("@").concat(domain.substring(0, 3)).concat("***");
        }

        return emailAddress;
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(paypalAccounts) ? 0 : paypalAccounts.size();
    }

    public void saveStates(Bundle outState) {
        binderHelper.saveStates(outState);
    }

    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mPaymentOptionEmailAddress, accountStatusLabel;
        private SwipeRevealLayout swipeLayout;
        private View mainLayout, subLayout;
        private Button mMakePrimaryButton, mDeletePaymentOptionButton;
        ClickListener clickListener;

        public ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            swipeLayout = itemView.findViewById(R.id.swipe_view_layout);
            mainLayout = itemView.findViewById(R.id.main_layout);
            subLayout = itemView.findViewById(R.id.sub_layout);
            mPaymentOptionEmailAddress = itemView.findViewById(R.id.payment_option_email_address);
            mDeletePaymentOptionButton = itemView.findViewById(R.id.delete_account_button);
            mMakePrimaryButton = itemView.findViewById(R.id.make_primary_button);
            accountStatusLabel = itemView.findViewById(R.id.payment_status_view);

            itemView.setOnClickListener(this);
            mainLayout.setOnClickListener(this);
            subLayout.setOnClickListener(this);
            mDeletePaymentOptionButton.setOnClickListener(this);
            mMakePrimaryButton.setOnClickListener(this);
            mDeletePaymentOptionButton.setOnClickListener(this);
            accountStatusLabel.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }
}