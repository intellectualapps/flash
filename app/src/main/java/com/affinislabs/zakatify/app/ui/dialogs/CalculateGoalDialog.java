package com.affinislabs.zakatify.app.ui.dialogs;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.GoldValueResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.TooltipHelp;
import com.affinislabs.zakatify.app.models.ZakatGoal;
import com.affinislabs.zakatify.app.ui.customviews.CustomNumericEditText;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.affinislabs.zakatify.app.utils.ZakatifyUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CalculateGoalDialog extends BaseDialogFragment implements View.OnClickListener, ApiClientListener.FetchGoldValueListener, TextView.OnEditorActionListener {

    private static ZakatifyInterfaces.ZakatCalculatorListener zakatCalculatorListener;
    private ImageButton clearInputButton;
    private TextView calculateButton, backButton;
    private TextView wealthTabView, deductionsTabView;
    private View wealthTabHighlight, deductionsTabHighlight;
    private ViewFlipper customFormFlipper;
    private CustomNumericEditText amountInputView;
    private CustomNumericEditText cashInput, goldSilverInput, realEstateInput, investmentsInput, personalInput, liabilitiesInput;
    private Map<Integer, TooltipHelp> tooltipContentMap;
    private static Double GOLD_VALUE = 0.00;
    private double wealthAmount = 0.00, cashAmount = 0.00, goldSilverAmount = 0.00, realEstateAmount = 0.0, investmentsAmount = 0.0;
    private double deductionsAmount = 0.0, personalUseAmount = 0.0, liabilitiesAmount = 0.0;
    private double newWealthAmount = 0.0, zakatAmount = 0.0;
    private Map<String, String> zakatCalculationData;
    private ZakatGoal zakatGoal;

    public static DialogFragment getInstance(Bundle args, ZakatifyInterfaces.ZakatCalculatorListener zakatCalculatorListenerParam) {
        CalculateGoalDialog frag = new CalculateGoalDialog();

        frag.setArguments(args);
        zakatCalculatorListener = zakatCalculatorListenerParam;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GOLD_VALUE = PreferenceStorageManager.getGoldValue(ZakatifyApplication.getAppInstance().getApplicationContext());
        if (getArguments() != null) {
            zakatGoal = getArguments().getParcelable(Constants.ZAKAT_GOAL);
        }
        zakatCalculationData = new HashMap<String, String>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.dialog_calculate_zakat_goal, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        clearInputButton = (ImageButton) view.findViewById(R.id.clear_input);
        calculateButton = (TextView) view.findViewById(R.id.calculate_button);
        backButton = (TextView) view.findViewById(R.id.back_button);
        wealthTabView = (TextView) view.findViewById(R.id.wealth_tab_title);
        deductionsTabView = (TextView) view.findViewById(R.id.deductions_tab_title);
        customFormFlipper = (ViewFlipper) view.findViewById(R.id.custom_form_viewflipper);
        wealthTabHighlight = view.findViewById(R.id.wealth_tab_highlight);
        deductionsTabHighlight = view.findViewById(R.id.deductions_tab_highlight);
        amountInputView = (CustomNumericEditText) view.findViewById(R.id.amount_input);
        cashInput = (CustomNumericEditText) view.findViewById(R.id.cash_input);
        goldSilverInput = (CustomNumericEditText) view.findViewById(R.id.gold_silver_input);
        realEstateInput = (CustomNumericEditText) view.findViewById(R.id.real_estate_input);
        investmentsInput = (CustomNumericEditText) view.findViewById(R.id.investments_input);
        personalInput = (CustomNumericEditText) view.findViewById(R.id.personal_use_input);
        liabilitiesInput = (CustomNumericEditText) view.findViewById(R.id.liabilities_input);

        investmentsInput.setOnEditorActionListener(this);

        liabilitiesInput.setOnEditorActionListener(this);

        cashInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onTooltipTouch(v, event);
            }
        });

        goldSilverInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onTooltipTouch(v, event);
            }
        });

        realEstateInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onTooltipTouch(v, event);
            }
        });

        investmentsInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onTooltipTouch(v, event);
            }
        });

        personalInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onTooltipTouch(v, event);
            }
        });

        liabilitiesInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onTooltipTouch(v, event);
            }
        });

        calculateButton.setOnClickListener(this);
        clearInputButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        wealthTabView.setOnClickListener(this);
        deductionsTabView.setOnClickListener(this);
        ApiClient.NetworkCallsRunner.updateGoldValue(CalculateGoalDialog.this);
        loadZakatData(zakatGoal);
    }

    private void loadZakatData(ZakatGoal zakatGoal) {
        if (zakatGoal != null && zakatGoal.getAmount() != null) {
            amountInputView.setText(ZakatifyUtils.formatAmount(zakatGoal.getAmount()));
            cashInput.setText(ZakatifyUtils.formatAmount(zakatGoal.getCash()));
            goldSilverInput.setText(ZakatifyUtils.formatAmount(zakatGoal.getGold()));
            realEstateInput.setText(ZakatifyUtils.formatAmount(zakatGoal.getRealEstate()));
            investmentsInput.setText(ZakatifyUtils.formatAmount(zakatGoal.getInvestment()));
            personalInput.setText(ZakatifyUtils.formatAmount(zakatGoal.getPersonalUse()));
            liabilitiesInput.setText(ZakatifyUtils.formatAmount(zakatGoal.getLiability()));
        }
    }

    private boolean onTooltipTouch(final View view, MotionEvent event) {
        //Reference https://stackoverflow.com/a/26269435 || https://stackoverflow.com/a/24360046
        final int DRAWABLE_RIGHT = 2;
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (event.getRawX() >= (((EditText) view).getRight() - ((EditText) view).getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.TOOLTIP_HELP, buildToolTipContentMap().get(view.getId()));
                DialogFragment tooltipDialog = TooltipDialog.getInstance(bundle);
                if (getActivity() != null) {
                    tooltipDialog.show(getActivity().getSupportFragmentManager(), tooltipDialog.getClass().getSimpleName());
                }
                return true;
            }
        }
        return false;
    }

    private Map<Integer, TooltipHelp> buildToolTipContentMap() {
        if (tooltipContentMap == null) {
            tooltipContentMap = new HashMap<>();
            tooltipContentMap.put(cashInput.getId(), new TooltipHelp("cash_equivalents_help", getString(R.string.cash_equivalents_title), getString(R.string.cash_equivalents_content)));
            tooltipContentMap.put(goldSilverInput.getId(), new TooltipHelp("gold_silver_help", getString(R.string.gold_silver_title), getString(R.string.gold_silver_content)));
            tooltipContentMap.put(realEstateInput.getId(), new TooltipHelp("real_estate_help", getString(R.string.real_estate_title), getString(R.string.real_estate_content)));
            tooltipContentMap.put(investmentsInput.getId(), new TooltipHelp("investments_help", getString(R.string.investment_title), getString(R.string.investment_content)));
            tooltipContentMap.put(personalInput.getId(), new TooltipHelp("personal_use_help", getString(R.string.personal_use_title), getString(R.string.personal_use_content)));
            tooltipContentMap.put(liabilitiesInput.getId(), new TooltipHelp("liabilities_help", getString(R.string.liabilities_title), getString(R.string.liabilities_content)));
        }

        return tooltipContentMap;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wealth_tab_title:
                customFormFlipper.setDisplayedChild(0);
                wealthTabHighlight.setVisibility(View.VISIBLE);
                deductionsTabHighlight.setVisibility(View.GONE);
                break;
            case R.id.deductions_tab_title:
                customFormFlipper.setDisplayedChild(1);
                wealthTabHighlight.setVisibility(View.GONE);
                deductionsTabHighlight.setVisibility(View.VISIBLE);
                break;
            case R.id.calculate_button:
                calculateZakat();
                break;
            case R.id.back_button:
                dismiss();
                break;
            case R.id.clear_input:
                amountInputView.setText(null);
                amountInputView.setHint("0.00");
                break;
        }
    }

    private double getInputAmount(CustomNumericEditText inputView, String fieldKey) {
        Double value = inputView.getNumericValue();
        if (value.isNaN()) {
            value = 0.00;
        }

        zakatCalculationData.put(fieldKey, String.format(Locale.getDefault(), "%.2f", value));
        return value;
    }

    private void calculateZakat() {
        cashAmount = getInputAmount(cashInput, Constants.CASH);
        goldSilverAmount = getInputAmount(goldSilverInput, Constants.GOLD);
        realEstateAmount = getInputAmount(realEstateInput, Constants.REAL_ESTATE);
        investmentsAmount = getInputAmount(investmentsInput, Constants.INVESTMENT);
        personalUseAmount = getInputAmount(personalInput, Constants.PERSONAL_USE);
        liabilitiesAmount = getInputAmount(liabilitiesInput, Constants.LIABILITY);

        wealthAmount = cashAmount + goldSilverAmount + realEstateAmount + investmentsAmount;
        deductionsAmount = personalUseAmount + liabilitiesAmount;
        newWealthAmount = wealthAmount - deductionsAmount;
        if (newWealthAmount < GOLD_VALUE) {
            zakatAmount = 0.00;
        } else {
            zakatAmount = newWealthAmount * 0.02578;
        }

        if(zakatCalculatorListener != null) {
            zakatCalculatorListener.onAmountCalculated(zakatAmount, zakatCalculationData);
        }
        dismiss();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public void onGoldValueFetched(GoldValueResponse goldValueResponse) {
        if (goldValueResponse != null && goldValueResponse.getMessage() == null) {
            PreferenceStorageManager.setGoldValue(ZakatifyApplication.getAppInstance().getApplicationContext(), goldValueResponse.getValue());
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        switch (v.getId()) {
            case R.id.liabilities_input:
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard();
                }
                break;
            case R.id.investments_input:
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    customFormFlipper.setDisplayedChild(1);
                    wealthTabHighlight.setVisibility(View.GONE);
                    deductionsTabHighlight.setVisibility(View.VISIBLE);
                }
                break;
        }
        return false;
    }
}
