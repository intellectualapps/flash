package com.affinislabs.zakatify.app.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;

public class ImageOptimizerTask extends AsyncTask<Void, Void, byte[]> {
    private Uri mFileUri;
    private WeakReference<Context> context;
    private ZakatifyInterfaces.ImageOptimizationListener imageOptimizationListener;

    public ImageOptimizerTask(final Uri mFileUri, Context context, ZakatifyInterfaces.ImageOptimizationListener imageOptimizationListener) {
        this.mFileUri = mFileUri;
        this.context = new WeakReference<Context>(context);
        this.imageOptimizationListener = imageOptimizationListener;
    }

    @Override
    protected byte[] doInBackground(Void... voids) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.get().getContentResolver(), mFileUri);
            Bitmap optimizedBitmap = FileUtils.rotateBitmap(mFileUri.getPath(), bitmap);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            optimizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
            return stream.toByteArray();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        super.onPostExecute(bytes);
        if (imageOptimizationListener != null) {
            imageOptimizationListener.onImageOptimized(mFileUri, bytes);
        }
    }
}
