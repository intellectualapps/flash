package com.affinislabs.zakatify.app.ui.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.BuildConfig;
import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.LoginResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.OnboardingActivity;
import com.affinislabs.zakatify.app.ui.activities.ProfileActivity;
import com.affinislabs.zakatify.app.ui.dialogs.MediaPickerDialog;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FileUtils;
import com.affinislabs.zakatify.app.utils.ImageOptimizerTask;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.UploadRequest;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.preprocess.BitmapEncoder;
import com.cloudinary.android.preprocess.DimensionsValidator;
import com.cloudinary.android.preprocess.ImagePreprocessChain;
import com.cloudinary.android.preprocess.Limit;

import java.util.HashMap;
import java.util.Map;

public class OnboardingProfileFragment extends MediaPickerBaseFragment implements View.OnClickListener, ZakatifyInterfaces.SocialAuthenticationListener, ZakatifyInterfaces.ImageOptimizationListener {
    private User user;
    private EditText firstnameInput, lastnameInput, usernameInput, mobileNumberInput, emailAddressInput, locationInput;
    private Button nextButton;
    private TextView connectTwitterButton, connectFacebookButton;
    private ImageView disconnectFacebookButton, disconnectTwitterButton;
    private ImageView mProfilePhoto;
    private Toolbar toolbar;
    private Uri mFileUri;
    private String profileImageUrl = "";
    private static int REQUEST_CODE_INTRO = 10101;
    private boolean showImageUploadDialog = false;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new OnboardingProfileFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public OnboardingProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = getArguments().getParcelable(Constants.USER);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!PreferenceStorageManager.isOnBoardingActivityShown(ZakatifyApplication.getAppInstance().getApplicationContext())) {
            Intent intent = new Intent(getContext(), OnboardingActivity.class);
            startActivityForResult(intent, REQUEST_CODE_INTRO);
        }

        if (showImageUploadDialog) {
            showPhotoUploadOptions();
            showImageUploadDialog = false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_INTRO) {
            PreferenceStorageManager.setOnBoardingActivityShown(ZakatifyApplication.getAppInstance().getApplicationContext(), true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboarding_profile, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((ProfileActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        nextButton = (Button) view.findViewById(R.id.next_button);
        mProfilePhoto = (ImageView) view.findViewById(R.id.profile_photo);
        usernameInput = (EditText) view.findViewById(R.id.username_input);
        firstnameInput = (EditText) view.findViewById(R.id.firstname_input);
        lastnameInput = (EditText) view.findViewById(R.id.lastname_input);
        emailAddressInput = (EditText) view.findViewById(R.id.email_address_input);
        mobileNumberInput = (EditText) view.findViewById(R.id.mobile_number_input);
        locationInput = (EditText) view.findViewById(R.id.location_input);

        connectFacebookButton = (TextView) view.findViewById(R.id.connect_facebook_button);
        connectTwitterButton = (TextView) view.findViewById(R.id.connect_twitter_button);

        disconnectFacebookButton = (ImageView) view.findViewById(R.id.disconnect_facebook);
        disconnectTwitterButton = (ImageView) view.findViewById(R.id.disconnect_twitter);

        connectFacebookButton.setOnClickListener(this);
        connectTwitterButton.setOnClickListener(this);
        disconnectFacebookButton.setOnClickListener(this);
        disconnectTwitterButton.setOnClickListener(this);
        mProfilePhoto.setOnClickListener(this);

        nextButton.setOnClickListener(this);
        loadUserData(user);
    }

    private void loadUserData(User user) {
        if (user == null) {
            return;
        } else {

            emailAddressInput.setText(user.getEmail() != null ? user.getEmail() : user.getFacebookEmail() != null ? user.getFacebookEmail() : user.getTwitterEmail());
            usernameInput.setText(user.getUsername());
            firstnameInput.setText(user.getFirstName());
            lastnameInput.setText(user.getLastName());
            mobileNumberInput.setText(user.getPhoneNumber());
            locationInput.setText(user.getLocation());

            if (isNotEmpty(user.getPhotoUrl())) {
                ImageUtils.loadImageUrl(mProfilePhoto, getContext(), user.getPhotoUrl());
                profileImageUrl = user.getPhotoUrl();
            }

            if (isNotEmpty(user.getFacebookEmail())) {
                connectFacebookButton.setText(user.getFacebookEmail());
            } else {
                connectFacebookButton.setText(getString(R.string.connect_label));
            }

            if (isNotEmpty(user.getTwitterEmail())) {
                connectTwitterButton.setText(user.getTwitterEmail());
            } else {
                connectTwitterButton.setText(getString(R.string.connect_label));
            }

            connectFacebookButton.setEnabled(!isNotEmpty(user.getFacebookEmail()));
            connectTwitterButton.setEnabled(!isNotEmpty(user.getTwitterEmail()));
            disconnectFacebookButton.setVisibility(isNotEmpty(user.getFacebookEmail()) ? View.VISIBLE : View.GONE);
            disconnectTwitterButton.setVisibility(isNotEmpty(user.getTwitterEmail()) ? View.VISIBLE : View.GONE);
        }

        Map<String, String> profileData = PreferenceStorageManager.getProfileData(ZakatifyApplication.getAppInstance().getApplicationContext());
        if (profileData != null && profileData.size() > 0) {

            if (profileData.containsKey(Constants.FULL_NAME) && profileData.get(Constants.FULL_NAME) != null) {
                String[] split_names = profileData.get(Constants.FULL_NAME).split(" ");
                String firstName = split_names[0];
                String lastName = split_names[1];
                firstnameInput.setText(firstName);
                lastnameInput.setText(lastName);
            }

            if (profileData.containsKey(Constants.EMAIL_ADDRESS) && profileData.get(Constants.EMAIL_ADDRESS) != null) {
                emailAddressInput.setText(profileData.get(Constants.EMAIL_ADDRESS));
            }

            if (profileData.get(Constants.PROFILE_PHOTO) != null) {
                ImageUtils.loadImageUrl(mProfilePhoto, getContext(), profileData.get(Constants.PROFILE_PHOTO));
                profileImageUrl = user.getPhotoUrl();
            }
        }
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.profile_update_loading);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.next_button:
                if (validateFields(new EditText[]{usernameInput})) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        if (mFileUri != null && !TextUtils.isEmpty(mFileUri.toString())) {
                            ImageOptimizerTask imageOptimizerTask = new ImageOptimizerTask(mFileUri, getContext(), this);
                            imageOptimizerTask.execute();
                        } else {
                            makeAPICall(sanitizeInput(getUserInput()));
                        }
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.profile_photo:
                if (checkMediaAccessPermission(getActivity())) {
                    showPhotoUploadOptions();
                }
                break;
            case R.id.connect_twitter_button:
            case R.id.disconnect_twitter:
                break;
            case R.id.connect_facebook_button:
                if (NetworkUtils.isConnected(getContext())) {
                    message = getString(R.string.link_social_account_progress);
                    showLoadingIndicator(message);
                    manageFacebookAuthentication(this);
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
                break;
            case R.id.disconnect_facebook:
                if (NetworkUtils.isConnected(getContext())) {
                    message = getString(R.string.unlink_social_account_progress);
                    showLoadingIndicator(message);
                    Map<String, String> queryMap = new HashMap<String, String>();
                    queryMap.put(Constants.PLATFORM, "facebook");
                    linkAccount(Constants.UNLINK_SOCIAL_NETWORK_REQUEST, user.getUsername(), queryMap);
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
                break;
        }
    }

    private void showPhotoUploadOptions() {
        DialogFragment profileBannerFrag = MediaPickerDialog.newInstance("Set Profile Image", MediaPickerDialog.TYPE_IMAGE, OnboardingProfileFragment.class.getSimpleName());
        profileBannerFrag.show(((AppCompatActivity) getContext()).getSupportFragmentManager().beginTransaction(), MediaPickerDialog.class.getSimpleName());
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String firstName = firstnameInput.getText().toString().trim();
        String lastName = lastnameInput.getText().toString().trim();
        String username = usernameInput.getText().toString().trim();
        String mobileNumber = mobileNumberInput.getText().toString().trim();
        String emailAddress = emailAddressInput.getText().toString().trim();
        String location = locationInput.getText().toString().trim();

        map.put(Constants.FIRST_NAME, firstName);
        map.put(Constants.LAST_NAME, lastName);
        map.put(Constants.USERNAME, username);
        if (!TextUtils.isEmpty(profileImageUrl)) {
            map.put(Constants.PHOTO_URL, profileImageUrl);
        }
        map.put(Constants.PHONE_NUMBER, mobileNumber);
        map.put(Constants.EMAIL_ADDRESS, emailAddress);
        map.put(Constants.LOCATION, location);
        map.put(Constants.FACEBOOK_EMAIL, user.getFacebookEmail());
        map.put(Constants.TWITTER_EMAIL, user.getTwitterEmail());

        return map;
    }

    public Map<String, String> sanitizeInput(Map<String, String> input) {
        Map<String, String> sanitizedMap = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : input.entrySet()) {
            if (entry.getKey() != null) {
                if (entry.getValue() != null && entry.getValue().length() > 0) {
                    sanitizedMap.put(entry.getKey(), entry.getValue());
                } else {
                    sanitizedMap.remove(entry.getKey());
                }
            }
        }
        return sanitizedMap;
    }

    private void linkAccount(String REQUEST_TYPE, String username, Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(REQUEST_TYPE, username, accountMap, new ApiClientListener.SocialNetworkLinkingListener() {
            @Override
            public void onActionCommitted(LoginResponse socialNetworkLinkingResponse) {
                hideLoadingIndicator();
                if (socialNetworkLinkingResponse != null) {
                    if (socialNetworkLinkingResponse.getStatus() == null && socialNetworkLinkingResponse.getMessage() == null) {
                        user = extractUserDetails(socialNetworkLinkingResponse);
                        loadUserData(user);
                    } else {
                        showErrorPopup(socialNetworkLinkingResponse.getMessage());
                    }
                }
            }
        }).execute();
    }

    private void makeAPICall(Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(Constants.UPDATE_PROFILE_REQUEST, accountMap, new ApiClientListener.ProfileUpdateListener() {
            @Override
            public void onProfileUpdated(LoginResponse profileUpdateResponse) {
                hideLoadingIndicator();
                if (profileUpdateResponse != null) {
                    if (profileUpdateResponse.getStatus() == null && profileUpdateResponse.getMessage() == null) {
                        user = extractUserDetails(profileUpdateResponse);
                        showZakatPreferencesView(user);
                    } else {
                        showErrorPopup(profileUpdateResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    private void connectSocialAccount(final Map<String, String> accountMap, final Map<String, String> profileData) {
        if (NetworkUtils.isConnected(getContext())) {
            hideKeyboard();
            String message = getString(R.string.link_social_account_progress);
            Map<String, String> queryMap = new HashMap<String, String>();
            queryMap.put(Constants.EMAIL_ADDRESS, accountMap.get(Constants.ID));
            queryMap.put(Constants.PLATFORM, accountMap.get(Constants.AUTH_TYPE));
            linkAccount(Constants.LINK_SOCIAL_NETWORK_REQUEST, user.getUsername(), queryMap);
        } else {
            showErrorPopup(getString(R.string.network_connection_label));
        }
    }

    private void showZakatPreferencesView(User user) {
        PreferenceStorageManager.setProfileOnBoarded(ZakatifyApplication.getAppInstance().getApplicationContext(), true);

        updateUserProfile(user);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.USER, user);
        if(isAdded() && !isDetached()) {
            startFragment(OnboardingZakatPreferencesFragment.newInstance(bundle), true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MEDIA_PERMISSION_REQUEST_CODE:
                showImageUploadDialog = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
    }

    @Override
    public void onMediaPickerSuccess(Uri fileUri, String filePath, int mediaType, String fragmentTag) {
        try {
            if (TextUtils.isEmpty(filePath)) {
                if (!fileUri.toString().contains("content://")) {
                    setImagePreview(fileUri);
                } else {
                    Uri resolvedUri = Uri.parse("file://" + FileUtils.getRealPathFromURI(getContext(), fileUri));
                    setImagePreview(resolvedUri);
                }
            } else {
                Uri resolvedUri = Uri.parse(filePath);
                setImagePreview(resolvedUri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMediaPickerError(String message, String fragmentTag) {
        if (getActivity() != null) {
            showErrorPopup(message);
        }
    }

    private void setImagePreview(Uri fileUri) {
        this.mFileUri = fileUri;
        ImageUtils.loadImageUri(mProfilePhoto, getContext(), fileUri);
    }


    @Override
    public void onAuthenticationCompleted(Map<String, String> accountData, Map<String, String> profileData) {
        connectSocialAccount(accountData, profileData);
    }

    @Override
    public void onImageOptimized(Uri mFileUri, byte[] bytes) {
        if (bytes == null || bytes.length < 1) {
            uploadImageToCloudinary(mFileUri, null);
        } else {
            uploadImageToCloudinary(mFileUri, bytes);
        }
    }

    private void uploadImageToCloudinary(Uri uri, byte[] imageByteArray) {
        profileImageUrl = "";
        MediaManager mediaManager = MediaManager.get();
        UploadRequest uploadRequest = null;
        if (imageByteArray != null && imageByteArray.length > 0) {
            uploadRequest = mediaManager.upload(imageByteArray);
        } else {
            uploadRequest = mediaManager.upload(uri);
        }

        String requestId = uploadRequest.callback(new UploadCallback() {
            @Override
            public void onStart(String requestId) {

            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {

            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                profileImageUrl = String.valueOf(resultData.get("secure_url"));
                makeAPICall(getUserInput());
            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                hideLoadingIndicator();
                profileImageUrl = "";
                showErrorPopup(error.getDescription());
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {

            }
        }).unsigned(BuildConfig.CLOUDINARY_PRESET)
                .option("angle", "ignore")
                .option("resource_type", "image")
                .preprocess(ImagePreprocessChain.limitDimensionsChain(1000, 1000)
                        .addStep(new DimensionsValidator(10, 10, 1000, 1000))
                        .addStep(new Limit(1000, 1000))
                        .saveWith(new BitmapEncoder(BitmapEncoder.Format.JPEG, 80)))
                .dispatch(getContext());
    }
}
