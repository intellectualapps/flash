package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.BooleanStateResponse;
import com.affinislabs.zakatify.app.models.DonationProgress;
import com.affinislabs.zakatify.app.models.FollowState;
import com.affinislabs.zakatify.app.models.TopZakatifier;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.utils.CommonUtils;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;

import java.util.List;

public class TopZakatifiersAdapter extends BaseRecyclerAdapter {
    private List<TopZakatifier> topZakatifiers;
    private ClickListener clickListener;
    private String mainUsername;
    private final int TOP_ZAKATIFIER_ITEM = 2;

    public TopZakatifiersAdapter(Context context, ClickListener clickListener, String mainUsername) {
        super(context);
        this.clickListener = clickListener;
        this.mainUsername = mainUsername;
    }

    public void setItems(List<TopZakatifier> items) {
        this.topZakatifiers = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (topZakatifiers != null)
            this.topZakatifiers.clear();
        notifyDataSetChanged();
    }

    public void addItem(TopZakatifier topZakatifier) {
        if (topZakatifier != null) {
            this.topZakatifiers.add(topZakatifier);
            notifyDataSetChanged();
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView userFullName, userPoints;
        ImageView userProfilePhoto, addUserView;
        ProgressBar userDonationProgressMeter;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            userFullName = (TextView) itemView.findViewById(R.id.user_full_name);
            userPoints = (TextView) itemView.findViewById(R.id.user_points);
            userProfilePhoto = (ImageView) itemView.findViewById(R.id.user_profile_photo);
            addUserView = (ImageView) itemView.findViewById(R.id.add_user);
            userDonationProgressMeter = (ProgressBar) itemView.findViewById(R.id.donate_progress_wheel);
            addUserView.setOnClickListener(this);
            itemView.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.top_zakatifier_item_layout, parent, false);

        return new TopZakatifiersAdapter.ViewHolder(view, this.clickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final TopZakatifiersAdapter.ViewHolder viewHolder = (TopZakatifiersAdapter.ViewHolder) holder;
        TopZakatifier topZakatifier = topZakatifiers.get(position);
        if (topZakatifier != null && topZakatifier.getUser() != null && topZakatifier.getDonationProgress() != null) {
            DonationProgress donationProgress = topZakatifier.getDonationProgress();
            final User user = topZakatifier.getUser();
            boolean isCurrentUser = false;
            isCurrentUser = user.getUsername().equalsIgnoreCase(mainUsername);

            viewHolder.addUserView.setVisibility(isCurrentUser ? View.GONE : View.VISIBLE);

            final FollowState followStateParam = topZakatifier.getFollowState();
            final boolean followState = followStateParam != null && followStateParam.getState();

            viewHolder.addUserView.setImageDrawable(ContextCompat.getDrawable(context, followState ? R.drawable.ic_following_blue : R.drawable.ic_follow_blue));

            viewHolder.userFullName.setText(user.getFullName());
            viewHolder.userPoints.setText(context.getString(R.string.user_points_placeholder, donationProgress.getPoints()));
            viewHolder.addUserView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    followUser(mainUsername, user.getUsername(), !followState, viewHolder, position);
                }
            });

            if (user.getPhotoUrl() != null) {
                ImageUtils.loadImageUrl(viewHolder.userProfilePhoto, context, user.getPhotoUrl());
            } else {
                ImageUtils.loadImageResource(viewHolder.userProfilePhoto, context, R.drawable.ic_user);
            }
            viewHolder.userDonationProgressMeter.setProgress((int) donationProgress.getPercentageCompleted());
        } else {
            viewHolder.itemView.setVisibility(View.GONE);
        }
    }

    private void followUser(String follower, String followed, final boolean state, final ViewHolder viewHolder, final int position) {
        new ApiClient.NetworkCallsRunner(Constants.FOLLOW_USER_REQUEST, follower, followed, state, new ApiClientListener.FollowUserListener() {
            @Override
            public void onFollowStateChanged(BooleanStateResponse booleanStateResponse) {
                if (booleanStateResponse != null) {
                    if (booleanStateResponse.getStatus() == null && booleanStateResponse.getDeveloperMessage() == null) {
                        viewHolder.addUserView.setImageDrawable(ContextCompat.getDrawable(context, state ? R.drawable.ic_following_blue : R.drawable.ic_follow_blue));
                        reloadTopZakatifiersList();
                    } else {
                        CommonUtils.displayShortToastMessage(String.valueOf(booleanStateResponse.getMessage()));
                    }
                }
            }
        }).execute();
    }

    private void reloadTopZakatifiersList() {
        Intent intent = new Intent();
        intent.setAction(Constants.REFRESH_TOP_ZAKATIFIERS_LIST_FILTER);
        intent.putExtra(Constants.REFRESH_FLAG, true);
        context.sendBroadcast(intent);
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(topZakatifiers) ? 0 : topZakatifiers.size();
    }
}