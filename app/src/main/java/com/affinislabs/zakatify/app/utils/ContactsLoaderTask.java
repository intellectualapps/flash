package com.affinislabs.zakatify.app.utils;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.text.TextUtils;

import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.Invite;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class ContactsLoaderTask extends AsyncTask<Void, Void, List<Invite>> {
    private static final String TAG = ContactsLoaderTask.class.getSimpleName();
    WeakReference<Context> context;
    private String invitor;
    private ZakatifyInterfaces.ContactLoaderListener contactLoaderListener;

    private final String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY;

    private final String FILTER = DISPLAY_NAME + " NOT LIKE '%@%'";

    private final String ORDER = String.format("%1$s COLLATE NOCASE", DISPLAY_NAME);

    @SuppressLint("InlinedApi")
    private final String[] PROJECTION = {
            ContactsContract.Contacts._ID,
            DISPLAY_NAME,
            ContactsContract.Contacts.HAS_PHONE_NUMBER
    };

    public ContactsLoaderTask(Context context, String invitor, ZakatifyInterfaces.ContactLoaderListener contactLoaderListener) {
        this.context = new WeakReference<Context>(context);
        this.contactLoaderListener = contactLoaderListener;
        this.invitor = invitor;
    }

    @Override
    protected ArrayList<Invite> doInBackground(Void... params) {
        return fetchContactDetails(context.get());
    }

    @Override
    protected void onPostExecute(List<Invite> invites) {
        super.onPostExecute(invites);
        if (contactLoaderListener != null) {
            contactLoaderListener.onContactsLoaded(invites);
        }
    }

    private ArrayList<Invite> fetchContactDetails(Context context) {
        long startTime = GregorianCalendar.getInstance().getTimeInMillis();
        ArrayList<Invite> deviceContacts = new ArrayList<Invite>();

        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String email = null;
                    String displayName = null;

                    try {
                        displayName = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Cursor emailCursor = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (emailCursor != null && emailCursor.moveToFirst()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    }

                    if ((!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
                        Invite invite = new Invite();
                        email = email.toLowerCase();
                        invite.setInvitee(email);
                        invite.setState(Constants.PRE_INVITE);
                        invite.setInvitor(invitor);
                        if (!TextUtils.isEmpty(displayName)) {
                            displayName = Transformers.capitalizeTag(displayName);
                            invite.setDisplayName(displayName);
                        }

                        if (!deviceContacts.contains(invite)) {
                            deviceContacts.add(invite);
                        }
                    }

                    if (emailCursor != null) {
                        emailCursor.close();
                    }

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            cursor.close();
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        long stopTime = GregorianCalendar.getInstance().getTimeInMillis();

        System.out.println((stopTime - startTime) / 1000 + " seconds");
        return deviceContacts;
    }
}
