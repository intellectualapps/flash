package com.affinislabs.zakatify.app.ui.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.adapters.TabPagerAdapter;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FontUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;

import java.util.Timer;
import java.util.TimerTask;

public class SearchViewPagerFragment extends BaseFragment implements View.OnClickListener, TextWatcher, ZakatifyInterfaces.SearchProgressListener, TextView.OnEditorActionListener {
    private User user;
    private ViewPager viewPager;
    private EditText searchInputView;
    private TabLayout tabLayout;
    private int SELECTED_TAB = 0;
    private ProgressBar progressBar;
    public static ZakatifyInterfaces.SearchProgressListener searchProgressListener;

    private int tabHeaderIds[] = new int[]{
            R.string.charities_tab_label,
            R.string.zakatifiers_tab_label
    };

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new SearchViewPagerFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public SearchViewPagerFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchProgressListener = this;

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }

        if (user == null) {
            user = new User();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_viewpager, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.search_zakatify_label));
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        searchInputView = (EditText) view.findViewById(R.id.search_input);

        searchInputView.setOnClickListener(this);
        searchInputView.addTextChangedListener(this);
        //searchInputView.setOnEditorActionListener(this);

        if (viewPager != null) {
            setupViewPager(viewPager);
            setupTabLayout(tabLayout);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    SELECTED_TAB = position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        if (viewPager != null) {
            TabPagerAdapter mAdapter = new TabPagerAdapter(getChildFragmentManager());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.USER, user);
            mAdapter.addFragment(SearchCharitiesFragment.newInstance(bundle), getString(tabHeaderIds[0]));
            mAdapter.addFragment(SearchZakatifiersFragment.newInstance(bundle), getString(tabHeaderIds[1]));
            viewPager.setAdapter(mAdapter);
        }
    }

    private void setupTabLayout(TabLayout tabLayout) {
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);

            Typeface typeface = FontUtils.selectTypeface(getContext(), FontUtils.STYLE_BOLD);
            tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.removeAllTabs();
            ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
            PagerAdapter adapter = viewPager.getAdapter();
            for (int i = 0, count = adapter.getCount(); i < count; i++) {
                TabLayout.Tab tab = tabLayout.newTab();
                tabLayout.addTab(tab.setText(adapter.getPageTitle(i)));
                AppCompatTextView view = (AppCompatTextView) ((ViewGroup) viewGroup.getChildAt(i)).getChildAt(1);
                view.setTypeface(typeface);
            }
        }
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    private void performSearch(final String query) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                switch (SELECTED_TAB) {
                    case 0:
                    default:
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adjustSearchState(true);
                                }
                            });
                            if (SearchCharitiesFragment.searchQueryListener != null) {
                                SearchCharitiesFragment.searchQueryListener.onQueryEntered(query, user.getUsername());
                            }
                        }
                        break;
                    case 1:
                        break;
                }
            }
        }, 1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                searchInputView.setEnabled(true);
            }
        }, 20000);
    }

    @Override
    public void afterTextChanged(final Editable s) {
        if (s.length() > 0 && !s.toString().trim().equals("")) {
            String query = s.toString().trim();
            if (!TextUtils.isEmpty(query) && query.length() > 1) {
                if (NetworkUtils.isConnected(getContext())) {
                    performSearch(query);
                } else {
                    showErrorPopup(getString(R.string.network_connection_label));
                }
            }
        }
    }

    @Override
    public void adjustSearchState(final boolean state) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(state ? View.VISIBLE : View.GONE);
                    searchInputView.setEnabled(true);
                }
            });
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String query = searchInputView.getText().toString().trim();
            if (!TextUtils.isEmpty(query) && query.length() > 1) {
                switch (SELECTED_TAB) {
                    case 0:
                    default:
                        if (SearchCharitiesFragment.searchQueryListener != null) {
                            SearchCharitiesFragment.searchQueryListener.onQueryEntered(query, user.getUsername());
                        }
                        break;
                    case 1:
                        break;
                }
            }
        }
        return false;
    }
}
