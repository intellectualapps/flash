package com.affinislabs.zakatify.app.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ui.fragments.ChatSessionsFragment;
import com.affinislabs.zakatify.app.ui.fragments.ManualDonationFragment;
import com.affinislabs.zakatify.app.ui.fragments.MessagesFragment;
import com.affinislabs.zakatify.app.utils.Constants;

public class PaymentActivity extends BaseActivity {
    private Bundle fragmentBundle;
    private String viewType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        toolbar = findViewById(R.id.toolbar);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.MANUAL_DONATION_VIEW_TAG:
                        frag = ManualDonationFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = ManualDonationFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = ManualDonationFragment.newInstance(null);
            }

            ft.replace(R.id.container, frag, frag.getClass().getSimpleName());
            ft.commit();
        }
    }
}
