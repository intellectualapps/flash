package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.affinislabs.zakatify.app.api.responses.DefaultResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Charity extends DefaultResponse implements Parcelable {
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("charityId")
    @Expose
    private Integer charityId;
    @SerializedName("charityLogo")
    @Expose
    private String charityLogo;
    @SerializedName("charityName")
    @Expose
    private String charityName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("ein")
    @Expose
    private String ein;
    @SerializedName("numberOfDonators")
    @Expose
    private int numberOfDonators;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("rating")
    @Expose
    private int rating = 0;
    @SerializedName("reportUrl")
    @Expose
    private String reportUrl;
    @SerializedName("selected")
    @Expose
    private Boolean selected;
    @SerializedName("state")
    @Expose
    private Boolean state;
    @SerializedName("totalDonation")
    @Expose
    private Double totalDonation;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("trendData")
    @Expose
    private List<TrendData> trendData = null;
    @SerializedName("slogan")
    @Expose
    private String slogan;
    @SerializedName("donors")
    @Expose
    private List<User> donors = null;
    @SerializedName("email")
    @Expose
    private String emailAddress;
    @SerializedName("websiteUrl")
    @Expose
    private String website;
    @SerializedName("newsFeed")
    @Expose
    private List<FeedActivity> newsFeed = null;

    public Charity() {

    }

    public Charity(int charityId, String charityName) {
        this.charityId = charityId;
        this.charityName = charityName;
    }

    public Charity(String charityName) {
        this.charityName = charityName;
    }

    protected Charity(Parcel in) {
        charityId = in.readInt();
        address = in.readString();
        charityLogo = in.readString();
        charityName = in.readString();
        description = in.readString();
        ein = in.readString();
        phoneNumber = in.readString();
        reportUrl = in.readString();
        selected = in.readByte() != 0;
        state = in.readByte() != 0;
        totalDonation = in.readDouble();
        numberOfDonators = in.readInt();
        rating = in.readInt();
        categories = in.createTypedArrayList(Category.CREATOR);
        trendData = in.createTypedArrayList(TrendData.CREATOR);
        slogan = in.readString();
        donors = in.createTypedArrayList(User.CREATOR);
        emailAddress = in.readString();
        website = in.readString();
        newsFeed = in.createTypedArrayList(FeedActivity.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(charityId);
        dest.writeString(address);
        dest.writeString(charityLogo);
        dest.writeString(charityName);
        dest.writeString(description);
        dest.writeString(ein);
        dest.writeString(phoneNumber);
        dest.writeString(reportUrl);
        dest.writeByte(selected != null ? (byte) (selected ? 1 : 0) : (byte) 0);
        dest.writeByte(state != null ? (byte) (state ? 1 : 0) : (byte) 0);
        dest.writeDouble(totalDonation != null ? totalDonation : 0);
        dest.writeInt(numberOfDonators);
        dest.writeInt(rating);
        dest.writeTypedList(categories);
        dest.writeTypedList(trendData);
        dest.writeString(slogan);
        dest.writeTypedList(donors);
        dest.writeString(emailAddress);
        dest.writeString(website);
        dest.writeTypedList(newsFeed);
    }

    public static final Creator<Charity> CREATOR = new Creator<Charity>() {
        @Override
        public Charity createFromParcel(Parcel in) {
            return new Charity(in);
        }

        @Override
        public Charity[] newArray(int size) {
            return new Charity[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Integer getCharityId() {
        return charityId;
    }

    public void setCharityId(Integer charityId) {
        this.charityId = charityId;
    }

    public String getCharityLogo() {
        return charityLogo;
    }

    public void setCharityLogo(String charityLogo) {
        this.charityLogo = charityLogo;
    }

    public String getCharityName() {
        return charityName;
    }

    public void setCharityName(String charityName) {
        this.charityName = charityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEin() {
        return ein;
    }

    public void setEin(String ein) {
        this.ein = ein;
    }

    public Integer getNumberOfDonators() {
        return numberOfDonators;
    }

    public void setNumberOfDonators(Integer numberOfDonators) {
        this.numberOfDonators = numberOfDonators;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Double getTotalDonation() {
        return totalDonation;
    }

    public void setTotalDonation(Double totalDonation) {
        this.totalDonation = totalDonation;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public List<TrendData> getTrendData() {
        return trendData;
    }

    public void setTrendData(List<TrendData> trendDataList) {
        this.trendData = trendDataList;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public void setNumberOfDonators(int numberOfDonators) {
        this.numberOfDonators = numberOfDonators;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<User> getDonors() {
        return donors;
    }

    public void setDonors(List<User> donors) {
        this.donors = donors;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<FeedActivity> getNewsFeed() {
        return newsFeed;
    }

    public void setNewsFeed(List<FeedActivity> newsFeed) {
        this.newsFeed = newsFeed;
    }
}
