package com.affinislabs.zakatify.app.api.responses;

import com.affinislabs.zakatify.app.models.Counts;
import com.affinislabs.zakatify.app.models.DonationProgress;
import com.affinislabs.zakatify.app.models.Notification;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.models.UserBoard;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserMenuResponse extends DefaultResponse {
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("donationProgess")
    @Expose
    private DonationProgress donationProgress;
    @SerializedName("userBoard")
    @Expose
    private UserBoard userBoard;
    @SerializedName("notification")
    @Expose
    private Notification notification;

    @SerializedName("counts")
    @Expose
    private Counts counts;

    /*@SerializedName("message")
    @Expose
    private Object message;*/

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DonationProgress getDonationProgress() {
        return donationProgress;
    }

    public void setDonationProgress(DonationProgress donationProgress) {
        this.donationProgress = donationProgress;
    }

    public Counts getCounts() {
        return counts;
    }

    public void setCounts(Counts counts) {
        this.counts = counts;
    }

    public UserBoard getUserBoard() {
        return userBoard;
    }

    public void setUserBoard(UserBoard userBoard) {
        this.userBoard = userBoard;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    /*public Object getMessageData() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }*/
}
