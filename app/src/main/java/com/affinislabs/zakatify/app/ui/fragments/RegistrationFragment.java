package com.affinislabs.zakatify.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.ApiModule;
import com.affinislabs.zakatify.app.api.responses.RegistrationResponse;
import com.affinislabs.zakatify.app.api.responses.UsernameCheckResponse;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;

import java.util.HashMap;
import java.util.Map;

public class RegistrationFragment extends BaseFragment implements View.OnClickListener, ApiClientListener.UsernameAvailabilityListener, View.OnFocusChangeListener {
    private User user;
    private EditText emailInput, usernameInput, passwordInput, confirmPasswordInput;
    private Button registerButton;
    private boolean isUsernameUnique = false;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new RegistrationFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public RegistrationFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        emailInput = (EditText) view.findViewById(R.id.email_address);
        usernameInput = (EditText) view.findViewById(R.id.username);
        passwordInput = (EditText) view.findViewById(R.id.password);
        confirmPasswordInput = (EditText) view.findViewById(R.id.confirm_password);
        registerButton = (Button) view.findViewById(R.id.register_button);

        usernameInput.setOnFocusChangeListener(this);
        registerButton.setOnClickListener(this);
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String emailAddress = emailInput.getText().toString().trim();
        String password = passwordInput.getText().toString().trim();
        String username = usernameInput.getText().toString().trim();
        map.put(Constants.EMAIL_ADDRESS, emailAddress);
        map.put(Constants.PASSWORD, password);
        map.put(Constants.USERNAME, username);
        return map;
    }

    private User extractUserDetails(RegistrationResponse registrationResponse) {
        User user = new User();
        user.setAuthToken(registrationResponse.getAuthToken());
        user.setEmail(registrationResponse.getEmail());
        user.setUsername(registrationResponse.getUsername());
        user.setCreationDate(registrationResponse.getCreationDate());
        return user;
    }


    @Override
    public void onClick(View v) {
        String message = getString(R.string.registration_progress_label);
        switch (v.getId()) {
            case R.id.register_button:
                if (isUsernameUnique) {
                    if (validateFields(new EditText[]{usernameInput, emailInput, passwordInput, confirmPasswordInput})) {
                        if (validatePasswordViews(passwordInput, confirmPasswordInput)) {
                            if (NetworkUtils.isConnected(getContext())) {
                                hideKeyboard();
                                showLoadingIndicator(message);
                                makeAPICall(getUserInput());
                            } else {
                                showErrorPopup(getString(R.string.network_connection_label));
                            }
                        }
                    }
                } else {
                    usernameInput.requestFocus();
                    usernameInput.setError(getString(R.string.unique_username_error));
                }
                break;
        }
    }

    private void makeAPICall(Map<String, String> accountMap) {
        new ApiClient.NetworkCallsRunner(Constants.REGISTER_REQUEST, accountMap, new ApiClientListener.AccountRegistrationListener() {
            @Override
            public void onAccountRegistered(RegistrationResponse registrationResponse) {
                hideLoadingIndicator();
                if (registrationResponse != null) {
                    if (registrationResponse.getStatus() == null && registrationResponse.getMessage() == null) {
                        User user = extractUserDetails(registrationResponse);

                        PreferenceStorageManager.clearOnBoardingPreferences(ZakatifyApplication.getAppInstance().getApplicationContext());
                        ApiClient.NetworkCallsRunner.resetApiService();
                        ApiModule.resetApiClient();
                        saveUserData(ZakatifyApplication.getAppInstance().getApplicationContext(), user);
                        showMainActivity(user, getActivity());
                    } else {
                        showErrorPopup(registrationResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onUsernameChecked(UsernameCheckResponse usernameCheckResponse) {
        if (usernameCheckResponse != null) {
            if (usernameCheckResponse.getStatus()) {
                usernameInput.setError(null);
                isUsernameUnique = true;
            } else {
                usernameInput.requestFocus();
                showErrorPopup(getString(R.string.username_verification_error));
                isUsernameUnique = false;
            }
        } else {
            isUsernameUnique = true;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.username:
                if (!hasFocus) {
                    final String usernameToCheck = usernameInput.getText().toString();
                    if (usernameToCheck.length() > 0) {
                        ApiClient.NetworkCallsRunner.checkUsernameAvailability(usernameToCheck, RegistrationFragment.this);
                    }
                }
                break;
        }
    }
}
