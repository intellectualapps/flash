package com.affinislabs.zakatify.app.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class User implements Parcelable {
    private String email;
    private String username;
    private String authToken;
    private String facebookEmail;
    private String twitterEmail;
    private String creationDate;
    private String firstName;
    private String lastName;
    private String location;
    private String phoneNumber;
    private String photoUrl;
    private ArrayList<Category> userInterests;
    private String tempKey;
    private boolean invited;
    @SerializedName("isFollowing")
    @Expose
    private FollowState followState;

    public User() {

    }

    public User(String email) {
        this.email = email;
    }

    public User(String firstName, String lastName, boolean invited) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.invited = invited;
    }

    protected User(Parcel in) {
        email = in.readString();
        username = in.readString();
        authToken = in.readString();
        facebookEmail = in.readString();
        twitterEmail = in.readString();
        creationDate = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        location = in.readString();
        phoneNumber = in.readString();
        photoUrl = in.readString();
        userInterests = in.readArrayList(Category.class.getClassLoader());
        tempKey = in.readString();
        followState = in.readParcelable(FollowState.class.getClassLoader());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getFullName() {
        return (firstName + " " + lastName).trim();
    }

    public boolean isInvited() {
        return invited;
    }

    public void setInvited(boolean invited) {
        this.invited = invited;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getFacebookEmail() {
        return facebookEmail;
    }

    public void setFacebookEmail(String facebookEmail) {
        this.facebookEmail = facebookEmail;
    }

    public String getTwitterEmail() {
        return twitterEmail;
    }

    public void setTwitterEmail(String twitterEmail) {
        this.twitterEmail = twitterEmail;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public ArrayList<Category> getUserInterests() {
        return userInterests;
    }

    public void setUserInterests(ArrayList<Category> userInterests) {
        this.userInterests = userInterests;
    }

    public String getTempKey() {
        return tempKey;
    }

    public void setTempKey(String tempKey) {
        this.tempKey = tempKey;
    }

    public FollowState getFollowState() {
        return followState;
    }

    public void setFollowState(FollowState followState) {
        this.followState = followState;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(username);
        dest.writeString(authToken);
        dest.writeString(facebookEmail);
        dest.writeString(twitterEmail);
        dest.writeString(creationDate);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(location);
        dest.writeString(phoneNumber);
        dest.writeString(photoUrl);
        dest.writeList(userInterests);
        dest.writeString(tempKey);
        dest.writeParcelable(followState, flags);
    }
}
