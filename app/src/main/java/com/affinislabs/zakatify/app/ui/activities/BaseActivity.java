package com.affinislabs.zakatify.app.ui.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.responses.UserMenuResponse;
import com.affinislabs.zakatify.app.api.responses.ZakatGoalResponse;
import com.affinislabs.zakatify.app.models.Message;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.models.UserMenu;
import com.affinislabs.zakatify.app.models.ZakatGoal;
import com.affinislabs.zakatify.app.ui.customviews.CustomTextView;
import com.affinislabs.zakatify.app.ui.dialogs.WebviewDialog;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.affinislabs.zakatify.app.utils.ToastUtil;
import com.google.gson.Gson;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;


public class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();
    public Toolbar toolbar;
    public TextView toolbarTitle;
    private List<WeakReference<Fragment>> mFragList = new ArrayList<WeakReference<Fragment>>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public Toolbar initializeToolbar(String title) {
        if (title == null || title.length() < 2) {
            getToolbar().setVisibility(View.GONE);
            return getToolbar();
        }

        getToolbar().setVisibility(View.VISIBLE);
        setTitle(title);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateUp();
            }
        });
        getToolbar().getMenu().clear();
        return getToolbar();
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
        CustomTextView toolbarTitle = getToolbar().findViewById(R.id.toolbar_title);
        toolbarTitle.setText(title);
    }

    public void navigateUp() {
        int backstack = getSupportFragmentManager().getBackStackEntryCount();
        if (backstack > 0) {
            //just pop
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    public void animateSwipeRefreshLayout(final SwipeRefreshLayout mSwipeRefreshLayout) {
        mSwipeRefreshLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mSwipeRefreshLayout
                        .getViewTreeObserver()
                        .removeOnGlobalLayoutListener(this);
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    public String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getZakatifyApplicationContext().getString(R.string.error_msg_unknown);
        if (!NetworkUtils.isNetworkConnected(getZakatifyApplicationContext())) {
            errorMsg = getResources().getString(R.string.network_connection_label);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private Context getZakatifyApplicationContext() {
        return ZakatifyApplication.getAppInstance().getApplicationContext();
    }

    public void endSwipeRefresh(SwipeRefreshLayout mSwipeRefreshLayout) {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void showSuccessPopup(Object message) {
        ToastUtil.showToast((String) message, ToastUtil.SUCCESS);
    }

    public void showErrorPopup(Object message) {
        ToastUtil.showToast((String) message, ToastUtil.ERROR);
    }

    public void showMainActivity(User user) {
        if (user == null) {
            return;
        }

        Intent intent = null;
        Context context = ZakatifyApplication.getAppInstance().getApplicationContext();

        if (!PreferenceStorageManager.isFirstUseOnDevice(context)) {
            intent = new Intent(context, MainActivity.class);
        } else {
            intent = new Intent(context, ProfileActivity.class);
            if (!PreferenceStorageManager.isProfileOnBoarded(context)) {
                intent.putExtra(Constants.VIEW_TYPE, Constants.ONBOARDING_PROFILE_VIEW_TAG);
            } else {
                if (!PreferenceStorageManager.isZakatifyOnBoarded(context)) {
                    intent.putExtra(Constants.VIEW_TYPE, Constants.ONBOARDING_ZAKATIFY_PREFERENCES_VIEW_TAG);
                } else {
                    if (!PreferenceStorageManager.isPaymentsOnBoarded(context)) {
                        intent.putExtra(Constants.VIEW_TYPE, Constants.ONBOARDING_PAYMENTS_VIEW_TAG);
                    } else {
                        if (!PreferenceStorageManager.isInterestsOnBoarded(context)) {
                            intent.putExtra(Constants.VIEW_TYPE, Constants.ONBOARDING_INTERESTS_VIEW_TAG);
                        } else {
                            intent = new Intent(this, MainActivity.class);
                            PreferenceStorageManager.setFirstUseOnDevice(context, false);
                        }
                    }
                }
            }
        }

        intent.putExtra(Constants.USER, user);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public UserMenu extractUserMenu(UserMenuResponse userMenuResponse) {
        UserMenu userMenu = new UserMenu();
        userMenu.setUser(userMenuResponse.getUser());
        userMenu.setDonationProgress(userMenuResponse.getDonationProgress());
        userMenu.setMessageData(new Gson().fromJson(String.valueOf(userMenuResponse.getMessage()), Message.class));
        userMenu.setNotification(userMenuResponse.getNotification());
        userMenu.setUserBoard(userMenuResponse.getUserBoard());
        return userMenu;
    }

    public ZakatGoal extractZakatGoal(ZakatGoalResponse zakatGoalResponse) {
        ZakatGoal zakatGoal = new ZakatGoal();
        if (zakatGoalResponse != null) {
            zakatGoal.setAmount(zakatGoalResponse.getAmount());
            zakatGoal.setCash(zakatGoalResponse.getCash());
            zakatGoal.setFrequencyId(zakatGoalResponse.getFrequencyId());
            zakatGoal.setGold(zakatGoalResponse.getGold());
            zakatGoal.setInvestment(zakatGoalResponse.getInvestment());
            zakatGoal.setLiability(zakatGoalResponse.getLiability());
            zakatGoal.setPeriod(zakatGoalResponse.getPeriod());
            zakatGoal.setPersonalUse(zakatGoalResponse.getPersonalUse());
            zakatGoal.setProgress(zakatGoalResponse.getProgress());
            zakatGoal.setRealEstate(zakatGoalResponse.getRealEstate());
            zakatGoal.setUsername(zakatGoalResponse.getUsername());
            zakatGoal.setState(zakatGoalResponse.getState());
        }
        return zakatGoal;
    }

    public void unregisterReceiver(Context context, BroadcastReceiver broadcastReceiver) {
        try {
            context.unregisterReceiver(broadcastReceiver);
        } catch (Exception ignored) {
        }
    }

    public Fragment findActiveFragmentByTag(String fragmentTag, FragmentManager fragmentManager) {
        List<Fragment> activeFragments = getActiveFragments();
        if (fragmentManager != null && activeFragments != null && activeFragments.size() > 0) {
            for (Fragment fragment : activeFragments) {
                if (fragment != null && fragment.getTag().contains(fragmentTag)) {
                    return fragment;
                }
            }
        }
        return fragmentManager != null ? fragmentManager.findFragmentByTag(fragmentTag) : null;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        mFragList.add(new WeakReference(fragment));
    }

    public List<Fragment> getActiveFragments() {
        ArrayList<Fragment> ret = new ArrayList<Fragment>();
        for (WeakReference<Fragment> ref : mFragList) {
            Fragment f = ref.get();
            if (f != null) {
                if (f.isVisible()) {
                    ret.add(f);
                }
            }
        }
        return ret;
    }

    public void launchWebViewDialog(Activity activity, String url, String title) {
        Bundle args = new Bundle();
        args.putString(Constants.URL, url);
        args.putString(Constants.TITLE, title);

        DialogFragment webViewDialog = WebviewDialog.getInstance(args);
        if (activity != null) {
            webViewDialog.show(((BaseActivity) activity).getSupportFragmentManager(), webViewDialog.getClass().getSimpleName());
        }
    }
}
