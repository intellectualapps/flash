package com.affinislabs.zakatify.app.api.responses;

import com.affinislabs.zakatify.app.models.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryResponse extends DefaultResponse {
    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories;

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
