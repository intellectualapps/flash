package com.affinislabs.zakatify.app.fcm;

import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.utils.FCMTokenUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class InstanceIdListenerService extends FirebaseInstanceIdService {
    private static final String TAG = "FCM LISTENER";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        PreferenceStorageManager.saveFCMToken(ZakatifyApplication.getAppInstance().getApplicationContext(), refreshedToken);
        FCMTokenUtils.refreshToken();
    }
}
