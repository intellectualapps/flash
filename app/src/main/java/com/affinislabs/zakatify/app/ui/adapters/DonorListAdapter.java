package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DonorListAdapter extends BaseRecyclerAdapter {
    private List<User> donorList;

    public DonorListAdapter(Context context) {
        super(context);
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView donorPhoto;

        ClickListener clickListener;

        ViewHolder(View itemView) {
            super(itemView);
            donorPhoto = itemView.findViewById(R.id.donor_avatar);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        view = mInflater.inflate(R.layout.donor_avatar_item_layout, parent, false);
        viewHolder = new DonorListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final DonorListAdapter.ViewHolder viewHolder = (DonorListAdapter.ViewHolder) holder;
        final User donor = donorList.get(position);
        if (donor != null) {
            final String donorAvatarUrl = donor.getPhotoUrl();
            if (!TextUtils.isEmpty(donorAvatarUrl)) {
                Picasso.with(context).load(donorAvatarUrl).into(viewHolder.donorPhoto);
            }
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(donorList) ? 0 : donorList.size();
    }

    public void setItems(List<User> items) {
        if(ListUtils.isNotEmpty(items) && items.size() > 5){
           items = items.subList(0, 6);
        }
        this.donorList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (donorList != null) {
            this.donorList.clear();
        }
        notifyDataSetChanged();
    }

}