package com.affinislabs.zakatify.app.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.MainActivity;
import com.affinislabs.zakatify.app.utils.AppLifecycleHandler;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.affinislabs.zakatify.app.utils.ToastUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class FirebaseCloudMessagingListenerService extends FirebaseMessagingService {
    private static final String TAG = "FCM";
    public static NotificationManager mNotificationManager;
    NotificationCompat.Builder notificationBuilder;
    Handler handler;
    Map<String, String> bundle = new HashMap<>();
    String notificationCategory = null;
    User user = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> data = remoteMessage.getData();
            this.bundle = data;
            Log.w(TAG, data.toString());
            Context context = ZakatifyApplication.getAppInstance().getApplicationContext();
            user = PreferenceStorageManager.getUser(context);
            if (user != null && PreferenceStorageManager.getSignInStatus(context)) {
                buildNotification(remoteMessage.getData());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void buildNotification(Map<String, String> notificationData) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String channelId = getString(R.string.default_notification_channel_id);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence channelName = getString(R.string.app_name);
            String channelDescription = "";
            int channelImportance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName,
                    channelImportance);
            mChannel.setDescription(channelDescription);
            mChannel.setLightColor(Color.CYAN);
            mChannel.canShowBadge();
            mChannel.enableLights(true);
            mChannel.enableVibration(true);
            mChannel.setShowBadge(true);
            getNotificationManager().createNotificationChannel(mChannel);
        }

        notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_stat_ic_launcher)
                .setAutoCancel(true)
                .setLights(Color.parseColor("blue"), 5000, 1000)
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{500, 500, 500, 500, 500});

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        String photoUrl = notificationData.get(Constants.PUSH_PHOTO_URL);
        if (!TextUtils.isEmpty(photoUrl)) {
            bitmap = getBitmapFromURL(photoUrl);
        }

        if (!AppLifecycleHandler.isApplicationInForeground() && !AppLifecycleHandler.isApplicationVisible()) {
            displayNotification(bitmap, notificationData, notificationBuilder);
        } else {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            Vibrator vibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
            if (vibrator != null) {
                vibrator.vibrate(500);
            }
            showChatMessagePopup("Chat Notification", notificationData.get(Constants.NOTIFICATION_MESSAGE));
        }
    }

    private NotificationManager getNotificationManager() {
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mNotificationManager;
    }

    private void showChatMessagePopup(String title, String message) {
        ToastUtil.showToast(title, message, ToastUtil.INFORMATIONAL);
    }

    private void displayNotification(Bitmap bitmap, Map<String, String> notificationData, NotificationCompat.Builder notificationBuilder) {
        Random rnd = new Random();
        int notificationId = 100000 + rnd.nextInt(900000);

        Intent intent = new Intent(this, MainActivity.class);
        notificationBuilder.setLargeIcon(bitmap);

        String notificationType = notificationData.get(Constants.NOTIFICATION_TYPE);
        String messageId = notificationData.get(Constants.CONNECTION_ID);
        String senderFullName = notificationData.get(Constants.PUSH_SENDER_FULL_NAME);
        String offlineUserEmail = notificationData.get(Constants.PUSH_OFFLINE_EMAIL);
        String onlineUserEmail = notificationData.get(Constants.PUSH_ONLINE_EMAIL);
        String photoUrl = notificationData.get(Constants.PUSH_PHOTO_URL);
        String notificationPlatform = notificationData.get(Constants.PUSH_NOTIFICATION_PLATFORM);

        if (messageId == null) {
            return;
        }

        notificationBuilder.setLargeIcon(bitmap);


        String notificationMessage = notificationData.get(Constants.NOTIFICATION_MESSAGE);

        notificationBuilder.setContentTitle("Chat Notification");
        notificationBuilder.setContentText(notificationMessage);
        notificationBuilder.setWhen(0);
        notificationBuilder.setTicker("Zakatify");

        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        intent.putExtra(Constants.NOTIFICATION_FLAG, true);
        intent.putExtra(Constants.PUSH_OFFLINE_EMAIL, offlineUserEmail);
        intent.putExtra(Constants.PUSH_ONLINE_EMAIL, onlineUserEmail);
        intent.putExtra(Constants.NOTIFICATION_VIEW_TAG, Constants.MESSAGE_LIST_VIEW_TAG);
        intent.putExtra(Constants.PUSH_MESSAGE_ID, messageId);
        intent.putExtra(Constants.NOTIFICATION_MESSAGE, notificationMessage);

        User user = new User();
        user.setPhotoUrl(photoUrl);
        user.setEmail(offlineUserEmail);
        intent.putExtra(Constants.NOTIFICATION_USER_PARAM, user);
        PendingIntent notificationPendingIntent =
                PendingIntent.getActivity(
                        this,
                        (int) System.currentTimeMillis(),
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        notificationBuilder.setContentIntent(notificationPendingIntent);
        mNotificationManager.notify(notificationId, notificationBuilder.build());
    }
}
