package com.affinislabs.zakatify.app.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ViewFlipper;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.PaymentResponse;
import com.affinislabs.zakatify.app.api.responses.PaypalAccountsResponse;
import com.affinislabs.zakatify.app.models.PaypalAccount;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.PaypalActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.PaymentOptionsAdapter;
import com.affinislabs.zakatify.app.ui.customviews.CustomRecyclerView;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ListUtils;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.ToastUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PaymentOptionsFragment extends BasePaymentsFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, ApiClientListener.FetchPaypalAccountsListener, ApiClientListener.DeletePaymentOptionListener, ApiClientListener.SetPrimaryPaymentOptionListener {
    private User user;
    private Button paypalButton, creditCardButton;
    private CustomRecyclerView paymentOptionsRecyclerView;
    private PaymentOptionsAdapter adapter;
    private List<PaypalAccount> paypalAccounts;
    private String paypalAccountUrl;
    private ViewFlipper paymentOptionsFlipper;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new PaymentOptionsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public PaymentOptionsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = getArguments().getParcelable(Constants.USER);
        }
        paypalAccounts = new ArrayList<PaypalAccount>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_payment_options, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.edit_payment_options));

        paypalButton = view.findViewById(R.id.add_paypal_button);
        creditCardButton = view.findViewById(R.id.add_credit_card_button);
        paymentOptionsRecyclerView = view.findViewById(R.id.payment_options_list);
        paymentOptionsFlipper = view.findViewById(R.id.payment_options_flipper);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

        adapter = new PaymentOptionsAdapter(getContext(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        paymentOptionsRecyclerView.setAdapter(adapter);
        paymentOptionsRecyclerView.setLayoutManager(linearLayoutManager);

        mSwipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.orange),
                ContextCompat.getColor(getActivity(), R.color.green),
                ContextCompat.getColor(getActivity(), R.color.blue)
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    fetchPaymentOptions(PaymentOptionsFragment.this, mSwipeRefreshLayout, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        paypalButton.setOnClickListener(this);
        creditCardButton.setOnClickListener(this);
        fetchPaymentOptions(PaymentOptionsFragment.this, mSwipeRefreshLayout, true);
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.profile_update_loading);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.add_paypal_button:
                if (!TextUtils.isEmpty(paypalAccountUrl)) {
                    bundle = new Bundle();
                    bundle.putString(Constants.PAYPAL_ACCOUNT_URL, paypalAccountUrl);
                    Intent intent = new Intent(getContext(), PaypalActivity.class);
                    intent.putExtra(Constants.PAYPAL_ACCOUNT_URL, paypalAccountUrl);
                    startActivityForResult(intent, PAYPAL_ACCOUNT_SET_UP_REQUEST_CODE, bundle);
                } else {
                    fetchPaymentOptions(this, mSwipeRefreshLayout, true);
                }
                break;
            case R.id.add_credit_card_button:
                break;
        }
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1 && ListUtils.isNotEmpty(paypalAccounts)) {
            PaypalAccount paypalAccount = paypalAccounts.get(position);
            switch (v.getId()) {
                case R.id.make_primary_button:
                    confirmSetPrimaryPaymentOption(getActivity(), paypalAccount.getId(), mSwipeRefreshLayout, PaymentOptionsFragment.this);
                    break;
                case R.id.payment_status_view:
                    break;
                case R.id.delete_account_button:
                    confirmDeletePaymentOption(getActivity(), paypalAccount.getId(), mSwipeRefreshLayout, PaymentOptionsFragment.this);
                    break;
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (adapter != null) {
            adapter.saveStates(outState);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (adapter != null) {
            adapter.restoreStates(savedInstanceState);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYPAL_ACCOUNT_SET_UP_REQUEST_CODE && data != null && data.getExtras() != null && data.getExtras().containsKey(Constants.PAYPAL_CALLBACK_CODE)) {
            int callbackStatus = data.getIntExtra(Constants.PAYPAL_CALLBACK_CODE, -1);
            switch (callbackStatus) {
                case BasePaymentsFragment.PAYPAL_SUCCESS_STATUS_CODE:
                    showSuccessPopup(getString(R.string.paypal_successful_setup));
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchPaypalAccounts(null, this);
                    } else {
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                    break;
                case BasePaymentsFragment.PAYPAL_FAILED_STATUS_CODE:
                    showErrorPopup(getString(R.string.payment_option_failed_message));
                    break;
                case BasePaymentsFragment.PAYPAL_CANCELLED_STATUS_CODE:
                    //showErrorPopup(getString(R.string.payment_option_cancelled_message));
                    break;
            }
        }
    }

    @Override
    public void onPaypalAccountsFetched(PaypalAccountsResponse paypalAccountsResponse) {
        if (paypalAccountsResponse != null) {
            paypalAccountUrl = paypalAccountsResponse.getAddNewPayPalAccountUrl();
            paypalAccounts = paypalAccountsResponse.getExistingPayPalAccounts();
            if (ListUtils.isNotEmpty(paypalAccounts)) {
                adapter.setItems(paypalAccounts);
                paymentOptionsFlipper.setDisplayedChild(1);
            } else {
                paymentOptionsFlipper.setDisplayedChild(0);
                ToastUtil.showToast(getActivity(), getString(R.string.paypal_setup_prompt), ToastUtil.INFORMATIONAL);
            }
        }
    }

    @Override
    public void onPaymentOptionDeleted(PaymentResponse paymentResponse) {
        if (paymentResponse != null) {
            fetchPaymentOptions(this, mSwipeRefreshLayout, false);
        }
    }

    @Override
    public void onPrimaryPaymentOptionSet(PaymentResponse paymentResponse) {
        if (paymentResponse != null) {
            fetchPaymentOptions(this, mSwipeRefreshLayout, false);
        }
    }
}
