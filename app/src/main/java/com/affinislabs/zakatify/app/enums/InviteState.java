package com.affinislabs.zakatify.app.enums;

public enum InviteState {
    INVITE_ACCEPTED("ACCEPTED"),
    INVITE_REJECTED("REJECTED"),
    INVITE_OPEN("OPEN"),
    PRE_INVITE("NULL"),
    INVITE_PENDING("PENDING");

    private String value;

    InviteState(String value) {
        this.value = value;
    }

    public static InviteState fromString(String text) {
        for (InviteState inviteState : InviteState.values()) {
            if (inviteState.value.equalsIgnoreCase(text)) {
                return inviteState;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
