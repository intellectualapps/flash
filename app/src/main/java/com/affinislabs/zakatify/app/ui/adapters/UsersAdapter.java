package com.affinislabs.zakatify.app.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.BooleanStateResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.FollowState;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.utils.CommonUtils;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.ImageUtils;
import com.affinislabs.zakatify.app.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class UsersAdapter extends BaseRecyclerAdapter {
    private List<User> users;
    private ClickListener clickListener;
    private String mainUsername;
    private final int USER_ITEM = 2;

    public UsersAdapter(Context context, ClickListener clickListener, String mainUsername, ZakatifyInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        callback = paginationAdapterCallback;
        this.mainUsername = mainUsername;
        users = new ArrayList<User>();
    }

    public void setItems(List<User> items) {
        this.users = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (users != null) {
            this.users.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(User user) {
        if (user != null) {
            this.users.add(user);
            notifyDataSetChanged();
        }
    }

    public void add(User user) {
        users.add(user);
        notifyItemInserted(users.size() - 1);
    }

    public void addAll(List<User> users) {
        if (users != null && users.size() > 0) {
            for (User user : users) {
                add(user);
            }
        }
        notifyDataSetChanged();
    }

    public void removeItem(User user) {
        int position = users.indexOf(user);
        if (position > -1) {
            users.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(int position) {
        if (position > -1) {
            users.remove(position);
            notifyItemRemoved(position);
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView userFullName;
        ImageView userProfilePhoto, addUserView;
        View container;
        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            userFullName = (TextView) itemView.findViewById(R.id.user_full_name);
            container = itemView.findViewById(R.id.container);
            userProfilePhoto = (ImageView) itemView.findViewById(R.id.user_profile_photo);
            addUserView = (ImageView) itemView.findViewById(R.id.add_user);
            itemView.setOnClickListener(this);
            container.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        switch (viewType) {
            default:
            case USER_ITEM:
                view = mInflater.inflate(R.layout.follower_item_layout, parent, false);
                viewHolder = new UsersAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            case USER_ITEM:
                final UsersAdapter.ViewHolder viewHolder = (UsersAdapter.ViewHolder) holder;
                final User user = users.get(position);
                if (user != null) {
                    boolean isCurrentUser = false;
                    isCurrentUser = user.getUsername().equalsIgnoreCase(mainUsername);

                    viewHolder.addUserView.setVisibility(isCurrentUser ? View.GONE : View.VISIBLE);
                    viewHolder.userFullName.setText(user.getFullName());

                    final FollowState followStateParam = user.getFollowState();
                    final boolean followState = followStateParam != null && followStateParam.getState();

                    viewHolder.addUserView.setImageDrawable(ContextCompat.getDrawable(context, followState ? R.drawable.ic_following_blue : R.drawable.ic_follow_blue));

                    viewHolder.addUserView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            followUser(mainUsername, user.getUsername(), !followState, viewHolder, position);
                        }
                    });

                    if (user.getPhotoUrl() != null) {
                        ImageUtils.loadImageUrl(viewHolder.userProfilePhoto, context, user.getPhotoUrl());
                    }
                }
        }
    }

    private void followUser(String follower, String followed, final boolean state, final ViewHolder viewHolder, final int position) {
        new ApiClient.NetworkCallsRunner(Constants.FOLLOW_USER_REQUEST, follower, followed, state, new ApiClientListener.FollowUserListener() {
            @Override
            public void onFollowStateChanged(BooleanStateResponse booleanStateResponse) {
                if (booleanStateResponse != null) {
                    if (booleanStateResponse.getStatus() == null && booleanStateResponse.getDeveloperMessage() == null) {
                        viewHolder.addUserView.setImageDrawable(ContextCompat.getDrawable(context, state ? R.drawable.ic_following_blue : R.drawable.ic_follow_blue));
                        updateFollowerList(position);
                    } else {
                        CommonUtils.displayShortToastMessage(String.valueOf(booleanStateResponse.getMessage()));
                    }
                }
            }
        }).execute();
    }

    private void updateFollowerList(int position) {
        Intent intent = new Intent();
        intent.setAction(Constants.REFRESH_FOLLOWERS_LIST_FILTER);
        intent.putExtra(Constants.REFRESH_FLAG, false);
        intent.putExtra(Constants.POSITION, position);
        context.sendBroadcast(intent);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == users.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            return USER_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(users) ? 0 : users.size();
    }

    public List<User> getUsers() {
        return users;
    }
}