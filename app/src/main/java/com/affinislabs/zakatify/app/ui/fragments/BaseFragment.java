package com.affinislabs.zakatify.app.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.responses.LoginResponse;
import com.affinislabs.zakatify.app.api.responses.ZakatGoalResponse;
import com.affinislabs.zakatify.app.interfaces.ZakatifyInterfaces;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.models.ZakatGoal;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.MainActivity;
import com.affinislabs.zakatify.app.ui.activities.ProfileActivity;
import com.affinislabs.zakatify.app.ui.customviews.CustomNumericEditText;
import com.affinislabs.zakatify.app.ui.dialogs.WebviewDialog;
import com.affinislabs.zakatify.app.utils.AppUtils;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.FileUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;
import com.affinislabs.zakatify.app.utils.ToastUtil;
import com.affinislabs.zakatify.app.utils.Validator;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BaseFragment extends Fragment {
    private ProgressDialog progressDialog;
    public CallbackManager callbackManager;
    public TwitterAuthClient twitterAuthClient;
    public FacebookCallback<LoginResult> callback;
    public static final int MEDIA_PERMISSION_REQUEST_CODE = 103;
    public static final int CONTACT_PERMISSION_REQUEST_CODE = 104;
    public boolean isLoading = false;
    public boolean hasListEnded = false;
    public TextView mEmptyViewLabel, mRetryButton;
    public View mEmptyView;
    public final int pageSize = 10;

    private String twitterEmailAddress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    public ZakatifyApplication getApplication() {
        if (getActivity() != null) {
            return (ZakatifyApplication) getActivity().getApplication();
        } else {
            return null;
        }
    }


    public void closeFragment() {
        if (getActivity() != null) {
            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                hideKeyboard();
                getActivity().getSupportFragmentManager().popBackStack();
            } else {
                hideKeyboard();
                getActivity().finish();
            }
        }
    }

    public void startFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.addToBackStack(frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    public void switchFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = ((BaseActivity) getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.addToBackStack(frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    private void clearBackStack(FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                while (fragmentManager.getBackStackEntryCount() > 0) {
                    fragmentManager.popBackStackImmediate();
                }
            }
        }
        fragmentManager.getBackStackEntryCount();
    }

    public void startFragment(Fragment frag, boolean shouldClearBackstack) {
        if (getActivity() != null && frag != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            if (shouldClearBackstack) {
                clearBackStack(fragmentManager);
            }
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.addToBackStack(frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    public void startMetaFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
            FragmentTransaction fragmentTransaction = manager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, backStateName);
            fragmentTransaction.addToBackStack(backStateName);
            fragmentTransaction.commit();
        }
    }

    public boolean checkFileAccessPermission(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(getString(R.string.permission_request_dialog_title));
                    builder.setMessage(getString(R.string.storage_permission_rationale));
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MEDIA_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MEDIA_PERMISSION_REQUEST_CODE);
                }

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public boolean checkMediaAccessPermission(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.CAMERA)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(getString(R.string.permission_request_dialog_title));
                    builder.setMessage(getString(R.string.media_permission_rationale));
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MEDIA_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.show();

                    return false;

                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            MEDIA_PERMISSION_REQUEST_CODE);
                }

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public boolean checkContactAccessPermission(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_CONTACTS)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(getString(R.string.permission_request_dialog_title));
                    builder.setMessage(getString(R.string.contact_permission_rationale));
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACT_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                            CONTACT_PERMISSION_REQUEST_CODE);
                }

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public boolean validateFields(EditText[] views) {
        return Validator.validateInputViewsNotEmpty(views);
    }

    public boolean validatePasswordViews(EditText passwordInput, EditText confirmPasswordInput) {
        return Validator.validatePasswordsEqual(passwordInput, confirmPasswordInput);
    }

    public boolean validateInputLength(EditText editText, int length) {
        return Validator.validateInputLength(editText, length);
    }

    public boolean validatePhoneNumber(EditText view) {
        return Validator.validatePhoneNumber(view);
    }

    public boolean validateAmountView(EditText view) {
        return false;
    }

    public boolean validateAmountView(CustomNumericEditText view) {
        return Validator.validateAmountView(view);
    }


    public void loadImageData(String imageKey, ImageView imageView) {
        String imageData = FileUtils.getImageDataFromCache(imageKey);
        try {
            byte[] decodedImageResource = Base64.decode(imageData, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImageResource, 0, decodedImageResource.length);
            imageView.setImageBitmap(decodedByte);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void hideKeyboard() {
        if (getActivity() != null) {
            AppUtils.hideKeyboard(getActivity());
        }
    }


    public void showMainActivity(User user, Activity activity) {
        if (user == null) {
            return;
        }
        if (activity == null || activity.isDestroyed()) {
            return;
        }
        hideKeyboard();
        Intent intent = null;
        Context context = ZakatifyApplication.getAppInstance().getApplicationContext();

        if (!PreferenceStorageManager.isFirstUseOnDevice(context)) {
            intent = new Intent(activity, MainActivity.class);
        } else {
            intent = new Intent(activity, ProfileActivity.class);
            if (!PreferenceStorageManager.isProfileOnBoarded(context)) {
                intent.putExtra(Constants.VIEW_TYPE, Constants.ONBOARDING_PROFILE_VIEW_TAG);
            } else {
                if (!PreferenceStorageManager.isZakatifyOnBoarded(context)) {
                    intent.putExtra(Constants.VIEW_TYPE, Constants.ONBOARDING_ZAKATIFY_PREFERENCES_VIEW_TAG);
                } else {
                    if (!PreferenceStorageManager.isPaymentsOnBoarded(context)) {
                        intent.putExtra(Constants.VIEW_TYPE, Constants.ONBOARDING_PAYMENTS_VIEW_TAG);
                    } else {
                        if (!PreferenceStorageManager.isInterestsOnBoarded(context)) {
                            intent.putExtra(Constants.VIEW_TYPE, Constants.ONBOARDING_INTERESTS_VIEW_TAG);
                        } else {
                            intent = new Intent(activity, MainActivity.class);
                            PreferenceStorageManager.setFirstUseOnDevice(context, false);
                        }
                    }
                }
            }
        }

        intent.putExtra(Constants.USER, user);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        activity.finish();
    }

    public void saveUserData(Context context, User user) {
        PreferenceStorageManager.setLoggedInStatus(context, true);
        PreferenceStorageManager.saveUser(context, user);
        PreferenceStorageManager.setSignInStatus(context, true);
        PreferenceStorageManager.saveAuthToken(context, user.getAuthToken());
    }

    public void updateUserProfile(User user) {
        if (user.getAuthToken() == null) {
            user.setAuthToken(PreferenceStorageManager.getAuthToken(ZakatifyApplication.getAppInstance().getApplicationContext()));
        }
        PreferenceStorageManager.saveUser(ZakatifyApplication.getAppInstance().getApplicationContext(), user);
        PreferenceStorageManager.saveAuthToken(ZakatifyApplication.getAppInstance().getApplicationContext(), user.getAuthToken());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        if (twitterAuthClient != null) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
    }

    public ZakatGoal extractZakatGoal(ZakatGoalResponse zakatGoalResponse) {
        ZakatGoal zakatGoal = new ZakatGoal();
        if (zakatGoalResponse != null) {
            zakatGoal.setAmount(zakatGoalResponse.getAmount());
            zakatGoal.setCash(zakatGoalResponse.getCash());
            zakatGoal.setFrequencyId(zakatGoalResponse.getFrequencyId());
            zakatGoal.setGold(zakatGoalResponse.getGold());
            zakatGoal.setInvestment(zakatGoalResponse.getInvestment());
            zakatGoal.setLiability(zakatGoalResponse.getLiability());
            zakatGoal.setPeriod(zakatGoalResponse.getPeriod());
            zakatGoal.setPersonalUse(zakatGoalResponse.getPersonalUse());
            zakatGoal.setProgress(zakatGoalResponse.getProgress());
            zakatGoal.setRealEstate(zakatGoalResponse.getRealEstate());
            zakatGoal.setUsername(zakatGoalResponse.getUsername());
            zakatGoal.setState(zakatGoalResponse.getState());
        }
        return zakatGoal;
    }

    public User extractUserDetails(LoginResponse loginResponse) {
        User user = new User();
        user.setAuthToken(loginResponse.getAuthToken());
        user.setEmail(loginResponse.getEmail());
        user.setUsername(loginResponse.getUsername());
        user.setPhotoUrl(loginResponse.getPhotoUrl());
        user.setFirstName(loginResponse.getFirstName());
        user.setLastName(loginResponse.getLastName());
        user.setLocation(loginResponse.getLocation());
        user.setPhoneNumber(loginResponse.getPhoneNumber());
        user.setCreationDate(loginResponse.getCreationDate());
        user.setFacebookEmail(loginResponse.getFacebookEmail());
        user.setTwitterEmail(loginResponse.getTwitterEmail());
        user.setTempKey(loginResponse.getTempKey());
        return user;
    }

    public void manageFacebookAuthentication(final ZakatifyInterfaces.SocialAuthenticationListener socialAuthenticationListener) {
        callbackManager = CallbackManager.Factory.create();
        final String message = getString(R.string.registration_facebook_progress_label);
        final String error = getString(R.string.facebook_signup_error);

        final String PERMISSION_EMAIL = "email";
        final String PERMISSION_PUBLIC_PROFILE = "public_profile";
        final String PERMISSION_USER_FRIENDS = "user_friends";

        ArrayList<String> permissions = new ArrayList<String>();
        permissions.add(PERMISSION_EMAIL);
        permissions.add(PERMISSION_PUBLIC_PROFILE);
        permissions.add(PERMISSION_USER_FRIENDS);

        callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult != null) {
                    final AccessToken accessToken = loginResult.getAccessToken();
                    if (accessToken != null) {
                        Set<String> declinedPermissions = accessToken.getDeclinedPermissions();
                        if (declinedPermissions.isEmpty()) {
                            GraphRequest req = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                                    if (jsonObject != null) {
                                        try {
                                            Map<String, String> accountMap = new HashMap<String, String>();
                                            Map<String, String> profileData = new HashMap<String, String>();
                                            Map<String, String> facebookAuthParamsMap = new HashMap<String, String>();

                                            String fbId = "";
                                            String email = "";
                                            String gender = "";
                                            String name = "";
                                            String profilePhoto = "";
                                            String firstName = "", lastName = "";
                                            String accessTokenValue = "";


                                            if (jsonObject.has("id"))
                                                fbId = jsonObject.getString("id");
                                            if (jsonObject.has("email"))
                                                email = jsonObject.getString("email");
                                            if (jsonObject.has("gender"))
                                                gender = jsonObject.getString("gender");
                                            if (jsonObject.has("name"))
                                                name = jsonObject.getString("name");
                                            if (jsonObject.has("picture"))
                                                profilePhoto = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");

                                            if (name.contains(" ")) {
                                                String[] split_names = name.split(" ");
                                                firstName = split_names[0];
                                                lastName = split_names[1];
                                            } else {
                                                firstName = name;
                                                lastName = "";
                                            }

                                            accessTokenValue = accessToken.getToken();

                                            facebookAuthParamsMap.put(Constants.FACEBOOK_ID, fbId);
                                            facebookAuthParamsMap.put(Constants.FACEBOOK_ACCESS_TOKEN, accessTokenValue);

                                            String gender_short = null;
                                            if (gender != null) {
                                                if (gender.equalsIgnoreCase("male")) {
                                                    gender_short = "m";
                                                } else if (gender.equalsIgnoreCase("female")) {
                                                    gender_short = "f";
                                                }
                                            }

                                            profileData.put(Constants.FIRST_NAME, firstName);
                                            profileData.put(Constants.LAST_NAME, lastName);
                                            profileData.put(Constants.FULL_NAME, name);
                                            profileData.put(Constants.PROFILE_PHOTO, profilePhoto);

                                            profileData.putAll(facebookAuthParamsMap);

                                            PreferenceStorageManager.saveFacebookAuthParams(getZakatifyApplicationContext(), facebookAuthParamsMap);

                                            accountMap.put(Constants.ID, email);
                                            accountMap.put(Constants.PASSWORD, "na");
                                            accountMap.put(Constants.AUTH_TYPE, Constants.FACEBOOK_AUTH_TYPE);
                                            Log.w("Facebook User data: ", email + " : " + gender + " : " + name + " : " + fbId + " : " + accessToken.getToken());
                                            showLoadingIndicator(message);
                                            if (socialAuthenticationListener != null) {
                                                socialAuthenticationListener.onAuthenticationCompleted(accountMap, profileData);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });

                            String fields = "id,email,gender,name,picture.type(large)";
                            Bundle params = new Bundle();
                            params.putString("fields", fields);
                            req.setParameters(params);
                            req.executeAsync();
                        }
                    }
                } else {
                    Log.e("Facebook Login", "Null Login Result");
                    hideLoadingIndicator();
                    showErrorPopup(error);
                }
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                exception.printStackTrace();
                hideLoadingIndicator();
                showErrorPopup(error);
            }
        };
        LoginManager.getInstance().logInWithReadPermissions(this, permissions);
        LoginManager.getInstance().registerCallback(callbackManager, callback);
    }

    public class TwitterHandler extends Callback<TwitterSession> {
        final String message = getString(R.string.registration_facebook_progress_label);
        String error = getString(R.string.facebook_signup_error);
        private ZakatifyInterfaces.SocialAuthenticationListener socialAuthenticationListener;

        public TwitterHandler(final ZakatifyInterfaces.SocialAuthenticationListener socialAuthenticationListener) {
            this.socialAuthenticationListener = socialAuthenticationListener;
        }

        @Override
        public void success(final Result<TwitterSession> result) {
            final TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
            TwitterAuthToken authToken = session.getAuthToken();
            String token = authToken.token;
            String secret = authToken.secret;
            TwitterAuthClient authClient = new TwitterAuthClient();
            authClient.requestEmail(session, new Callback<String>() {
                @Override
                public void success(Result<String> result) {
                    twitterEmailAddress = result.data;
                    if (twitterEmailAddress != null && twitterEmailAddress.trim().length() > 0) {
                        PreferenceStorageManager.saveTwitterEmailAddress(ZakatifyApplication.getAppInstance().getApplicationContext(), twitterEmailAddress);
                    } else {
                        twitterEmailAddress = PreferenceStorageManager.getTwitterEmailAddress(ZakatifyApplication.getAppInstance().getApplicationContext());
                    }

                    Log.d("Twitter", twitterEmailAddress + "|");

                    TwitterCore.getInstance().getApiClient().getAccountService().verifyCredentials(false, true, true)
                            .enqueue(new Callback<com.twitter.sdk.android.core.models.User>() {
                                @Override
                                public void success(Result<com.twitter.sdk.android.core.models.User> userResult) {
                                    Map<String, String> accountMap = new HashMap<String, String>();
                                    Map<String, String> profileData = new HashMap<String, String>();

                                    String firstName = null, lastName = null, username = null;

                                    if (userResult.data.name.contains(" ")) {
                                        String names[] = userResult.data.name.split(" ");
                                        firstName = names[1];
                                        lastName = names[0];
                                    } else {
                                        firstName = userResult.data.name;
                                        lastName = "";
                                    }

                                    username = userResult.data.screenName;

                                    profileData.put(Constants.FIRST_NAME, firstName);
                                    profileData.put(Constants.LAST_NAME, lastName);
                                    profileData.put(Constants.USERNAME, username);
                                    profileData.put(Constants.FULL_NAME, userResult.data.name);

                                    String profilePhoto = userResult.data.profileImageUrl.replace("_normal", "");
                                    Log.d("Twitter", userResult.data.name + "|" + profilePhoto);
                                    profileData.put(Constants.PROFILE_PHOTO, profilePhoto);

                                    accountMap.put(Constants.ID, twitterEmailAddress);
                                    accountMap.put(Constants.PASSWORD, "na");
                                    accountMap.put(Constants.AUTH_TYPE, Constants.TWITTER_AUTH_TYPE);

                                    showLoadingIndicator(message);

                                    socialAuthenticationListener.onAuthenticationCompleted(accountMap, profileData);
                                }

                                @Override
                                public void failure(TwitterException exc) {

                                    error = exc.getMessage();
                                    hideLoadingIndicator();
                                    showErrorPopup(error);
                                    exc.printStackTrace();
                                    Log.d("TwitterKit", "Verify Credentials Failure", exc);
                                }
                            });
                }

                @Override
                public void failure(TwitterException exception) {
                    exception.printStackTrace();
                }
            });
        }

        @Override
        public void failure(TwitterException exception) {
            exception.printStackTrace();
        }
    }

    public TwitterAuthClient getTwitterAuthClient() {
        if (twitterAuthClient == null) {
            twitterAuthClient = new TwitterAuthClient();
        }

        return twitterAuthClient;
    }

    public boolean isNotEmpty(String value) {
        return value != null && value.trim().length() > 0;
    }

    public void showLoadingIndicator(String message) {

        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
            }

            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        } catch (Exception ignored) {
        }
    }

    public void hideLoadingIndicator() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
        } catch (Exception ignored) {
        }
    }

    public void showLoadingIndicator(boolean showProgressLoader, String message) {
        if (showProgressLoader) {
            showLoadingIndicator(message);
        } else {
            hideLoadingIndicator();
        }
    }

    public void endSwipeRefresh(SwipeRefreshLayout mSwipeRefreshLayout) {
        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void animateSwipeRefreshLayout(final SwipeRefreshLayout mSwipeRefreshLayout) {
        mSwipeRefreshLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mSwipeRefreshLayout
                        .getViewTreeObserver()
                        .removeOnGlobalLayoutListener(this);
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    public void showSuccessPopup(Object message) {
        if (isAdded() && getActivity() != null) {
            ToastUtil.showToast(requireActivity(), (String) message, ToastUtil.SUCCESS);
        }
    }

    public void showErrorPopup(Object message) {
        if (isAdded() && getActivity() != null) {
            ToastUtil.showToast(requireActivity(), (String) message, ToastUtil.ERROR);
        }
    }

    public void registerReceiver(Context context, BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        try {
            context.registerReceiver(broadcastReceiver, intentFilter);
        } catch (Exception ignored) {
        }
    }

    public void unregisterReceiver(Context context, BroadcastReceiver broadcastReceiver) {
        try {
            context.unregisterReceiver(broadcastReceiver);
        } catch (Exception ignored) {
        }
    }

    public Context getZakatifyApplicationContext() {
        return ZakatifyApplication.getAppInstance().getApplicationContext();
    }
}
