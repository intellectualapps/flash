package com.affinislabs.zakatify.app.utils;

import android.text.TextUtils;

import com.affinislabs.zakatify.app.models.Category;
import com.affinislabs.zakatify.app.models.Charity;
import com.affinislabs.zakatify.app.models.ChatSession;
import com.affinislabs.zakatify.app.models.Counts;
import com.affinislabs.zakatify.app.models.Invite;
import com.affinislabs.zakatify.app.models.TopZakatifier;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.models.UserMenu;
import com.affinislabs.zakatify.app.models.ZakatGoal;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class StringUtils {

    /**
     * Gets whether or not a string is null, empty or "null"
     *
     * @param str string to check
     * @return true if the string is null, empty or "null"
     */
    public static boolean isEmpty(String str) {
        return TextUtils.isEmpty(str) || str.equalsIgnoreCase("null");
    }

    /**
     * This method nullifies a string.
     *
     * @param value string to nullify
     * @return the input string if the string is not empty, null otherwise
     */
    public static String nullify(String value) {
        if (isEmpty(value)) {
            return null;
        }
        return value;
    }

    public static String userToString(User user) {
        Gson gson = new Gson();
        return gson.toJson(user, User.class);
    }

    public static String countsToString(Counts counts) {
        Gson gson = new Gson();
        return gson.toJson(counts, Counts.class);
    }

    public static String zakatGoalToString(ZakatGoal zakatGoal) {
        Gson gson = new Gson();
        return gson.toJson(zakatGoal, ZakatGoal.class);
    }

    public static String userMenuToString(UserMenu userMenu) {
        Gson gson = new Gson();
        return gson.toJson(userMenu, UserMenu.class);
    }

    public static String categoriesToString(ArrayList<Category> categories) {
        Gson gson = new Gson();
        try {
            return gson.toJson(categories);
        } catch (Exception e) {
            return null;
        }
    }

    public static ArrayList<Category> categoriesFromString(String sCategories) {
        Gson gson = new Gson();
        ArrayList<Category> categories = null;
        try {
            categories = gson.fromJson(sCategories, new TypeToken<ArrayList<Category>>() {
            }.getType());
        } catch (Exception e) {
            return null;
        }
        return categories;
    }

    public static String invitesToString(List<Invite> invites) {
        Gson gson = new Gson();
        try {
            return gson.toJson(invites);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Invite> invitesFromString(String sInvites) {
        Gson gson = new Gson();
        List<Invite> invites = null;
        try {
            invites = gson.fromJson(sInvites, new TypeToken<List<Invite>>() {
            }.getType());
        } catch (Exception e) {
            return null;
        }
        return invites;
    }

    public static String charitiesToString(List<Charity> charities) {
        Gson gson = new Gson();
        try {
            return gson.toJson(charities);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Charity> charitiesFromString(String sCharities) {
        Gson gson = new Gson();
        ArrayList<Charity> charities = null;
        try {
            charities = gson.fromJson(sCharities, new TypeToken<ArrayList<Charity>>() {
            }.getType());
        } catch (Exception e) {
            return null;
        }
        return charities;
    }

    public static String topZakatifiersToString(List<TopZakatifier> topZakatifiers) {
        Gson gson = new Gson();
        try {
            return gson.toJson(topZakatifiers);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<TopZakatifier> topZakatifiersFromString(String sTopZakatifiers) {
        Gson gson = new Gson();
        List<TopZakatifier> topZakatifiers = null;
        try {
            topZakatifiers = gson.fromJson(sTopZakatifiers, new TypeToken<List<TopZakatifier>>() {
            }.getType());
        } catch (Exception e) {
            return null;
        }
        return topZakatifiers;
    }

    public static String chatSessionsToString(List<ChatSession> chatSessions) {
        Gson gson = new Gson();
        try {
            return gson.toJson(chatSessions);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<ChatSession> chatSessionsFromString(String sChatSessions) {
        Gson gson = new Gson();
        List<ChatSession> chatSessions = null;
        try {
            chatSessions = gson.fromJson(sChatSessions, new TypeToken<List<ChatSession>>() {
            }.getType());
        } catch (Exception e) {
            return null;
        }
        return chatSessions;
    }

    public static User userFromString(String sUser) {
        Gson gson = new Gson();
        User user = null;
        try {
            user = gson.fromJson(sUser, User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    public static Counts countsFromString(String sCounts) {
        Gson gson = new Gson();
        Counts counts = null;
        try {
            counts = gson.fromJson(sCounts, Counts.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return counts;
    }

    public static ZakatGoal zakatGoalFromString(String sZakatGoal) {
        Gson gson = new Gson();
        ZakatGoal zakatGoal = null;
        try {
            zakatGoal = gson.fromJson(sZakatGoal, ZakatGoal.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return zakatGoal;
    }

    public static UserMenu userMenuFromString(String sUserMenu) {
        Gson gson = new Gson();
        UserMenu userMenu = null;
        try {
            userMenu = gson.fromJson(sUserMenu, UserMenu.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userMenu;
    }

    private static int indexOf(String string, char character, int start, boolean reverseOrder) {
        if (isEmpty(string)) return -1;
        char[] chars = string.toCharArray();

        if (start < 0 || start > string.length()) return -1;

        if (reverseOrder) {
            for (int i = start; i > -1; i--) {
                if (character == chars[i]) return i;
            }
        } else {
            for (int i = 0; i < start; i--) {
                if (character == chars[i]) return i;
            }
        }
        return -1;
    }

    public static int indexOf(String string, char character, int start) {
        return indexOf(string, character, start, false);
    }
}
