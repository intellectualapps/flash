package com.affinislabs.zakatify.app.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.utils.FontUtils;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

public class CustomTwitterLoginButton extends TwitterLoginButton {

    public CustomTwitterLoginButton(Context context) {
        super(context);
        init();
    }

    public CustomTwitterLoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTwitterLoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        if (isInEditMode()) {
            return;
        }
        setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable
                .ic_twitter_icon), null, null, null);
        setBackgroundResource(R.drawable.twitter_auth_button_drawable);
        setTextSize(20);
        //setPadding(0, 0, 0, 0);
        //setPadding(30, 0, 10, 0);
        //setTextColor(getResources().getColor(R.color.tw__blue_default));
        setText(getContext().getString(R.string.twitter_signup_label));
        //setTextSize(Dimension.SP, getResources().getDimension(R.dimen.button_text_size_medium));
        //setTextSize(getResources().getDimension(R.dimen.button_text_size_medium));
        //setTextSize(R.dimen.label_text_size_medium);
        setTypeface(FontUtils.selectTypeface(getContext(), FontUtils.STYLE_BOLD));
    }
}
