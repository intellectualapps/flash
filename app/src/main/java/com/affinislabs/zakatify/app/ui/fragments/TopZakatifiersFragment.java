package com.affinislabs.zakatify.app.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.zakatify.app.R;
import com.affinislabs.zakatify.app.ZakatifyApplication;
import com.affinislabs.zakatify.app.api.ApiClient;
import com.affinislabs.zakatify.app.api.ApiClientListener;
import com.affinislabs.zakatify.app.api.responses.TopZakatifiersResponse;
import com.affinislabs.zakatify.app.models.TopZakatifier;
import com.affinislabs.zakatify.app.models.User;
import com.affinislabs.zakatify.app.ui.activities.BaseActivity;
import com.affinislabs.zakatify.app.ui.activities.ProfileActivity;
import com.affinislabs.zakatify.app.ui.adapters.BaseRecyclerAdapter;
import com.affinislabs.zakatify.app.ui.adapters.TopZakatifiersAdapter;
import com.affinislabs.zakatify.app.utils.Constants;
import com.affinislabs.zakatify.app.utils.NetworkUtils;
import com.affinislabs.zakatify.app.utils.PreferenceStorageManager;

import java.util.ArrayList;
import java.util.List;


public class TopZakatifiersFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener {
    private RecyclerView topZakatifiersRecyclerview;
    private List<TopZakatifier> topZakatifiersList = new ArrayList<TopZakatifier>();
    private TopZakatifiersAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private User user;
    private String mainUsername;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new TopZakatifiersFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public TopZakatifiersFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
        }
        topZakatifiersList = PreferenceStorageManager.getCurrentMonthTopZakatifiers(ZakatifyApplication.getAppInstance().getApplicationContext());

        getContext().registerReceiver(refreshListReceiver, new IntentFilter(Constants.REFRESH_TOP_ZAKATIFIERS_LIST_FILTER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_zakatifiers, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(getContext(), refreshListReceiver);
    }

    private void init(View view, Bundle savedInstanceState) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blue),
                ContextCompat.getColor(getContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchTopZakatifiers(true);
                    } else {
                        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        showErrorPopup(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        adapter = new TopZakatifiersAdapter(getContext(), this, user.getUsername());
        topZakatifiersRecyclerview = view.findViewById(R.id.top_zakatifiers_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        topZakatifiersRecyclerview.setAdapter(adapter);
        topZakatifiersRecyclerview.setLayoutManager(linearLayoutManager);
        topZakatifiersRecyclerview.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        initializeData();
    }

    private void initializeData() {
        animateSwipeRefreshLayout(swipeRefreshLayout);
        fetchTopZakatifiers(true);
    }

    private void loadAdapter(List<TopZakatifier> topZakatifiers) {
        adapter.setItems(topZakatifiers);
    }

    private void fetchTopZakatifiers(boolean showLoader) {
        showLoadingIndicator(showLoader, getString(R.string.fetching_top_zakatifiers_message));
        new ApiClient.NetworkCallsRunner(Constants.FETCH_CURRENT_MONTH_TOP_ZAKATIFIERS_REQUEST, new ApiClientListener.FetchTopZakatifiersListener() {
            @Override
            public void onTopZakatifiersFetched(TopZakatifiersResponse topZakatifiersResponse) {
                endSwipeRefresh(swipeRefreshLayout);
                hideLoadingIndicator();

                if (topZakatifiersResponse != null) {
                    if (topZakatifiersResponse.getStatus() == null && topZakatifiersResponse.getMessage() == null) {
                        topZakatifiersList = topZakatifiersResponse.getTopZakatifiers();
                        PreferenceStorageManager.saveCurrentMonthTopZakatifiers(ZakatifyApplication.getAppInstance().getApplicationContext(), topZakatifiersList);
                        loadAdapter(topZakatifiersList);
                    } else {
                        showErrorPopup(topZakatifiersResponse.getMessage());
                    }
                } else {
                    showErrorPopup(getString(R.string.null_response_label));
                }
            }
        }).execute();
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1 && topZakatifiersList != null && topZakatifiersList.size() > 0) {
            Intent intent = new Intent(getContext(), ProfileActivity.class);
            intent.putExtra(Constants.VIEW_TYPE, Constants.PUBLIC_USER_PROFILE_VIEW_TAG);
            intent.putExtra(Constants.USERNAME, topZakatifiersList.get(position).getUser().getUsername());
            intent.putExtra(Constants.MAIN_USERNAME, user.getUsername());
            startActivity(intent);
        }
    }

    private BroadcastReceiver refreshListReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            fetchTopZakatifiers(false);
        }
    };
}
