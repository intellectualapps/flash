package com.affinislabs.zakatify.app.models;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedActivity implements Parcelable {
    private String activityBy;
    private String activityType;

    public FeedActivity() {
    }

    protected FeedActivity(Parcel in) {
        activityBy = in.readString();
        activityType = in.readString();
    }

    public static final Creator<FeedActivity> CREATOR = new Creator<FeedActivity>() {
        @Override
        public FeedActivity createFromParcel(Parcel in) {
            return new FeedActivity(in);
        }

        @Override
        public FeedActivity[] newArray(int size) {
            return new FeedActivity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(activityBy);
        dest.writeString(activityType);
    }

    public String getActivityBy() {
        return activityBy;
    }

    public void setActivityBy(String activityBy) {
        this.activityBy = activityBy;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }
}
